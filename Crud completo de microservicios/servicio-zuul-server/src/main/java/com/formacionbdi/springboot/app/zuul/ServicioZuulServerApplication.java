package com.formacionbdi.springboot.app.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
@EnableEurekaClient
@EnableAspectJAutoProxy
@SpringBootApplication
public class ServicioZuulServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicioZuulServerApplication.class, args);
	}

}
