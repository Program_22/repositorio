package com.example.demo.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="monedas")
public class MonedaRegistro implements Serializable {
	

	/**
	 * 
	 */

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
private Long Id;
private String Moneda_Origen;
private String Moneda_Destino;
private double Valor_Origen;
private double Valor_Destino;

private double devolver_cambio=Valor_Origen*Valor_Destino;





public MonedaRegistro() {

}

public MonedaRegistro(Long id, String moneda_Origen, String moneda_Destino, double valor_Origen, double valor_Destino,
		double devolver_cambio) {

	Id = id;
	Moneda_Origen = moneda_Origen;
	Moneda_Destino = moneda_Destino;
	Valor_Origen = valor_Origen;
	Valor_Destino = valor_Destino;
	this.devolver_cambio = devolver_cambio;
}

public Long getId() {
	return Id;
}

public void setId(Long id) {
	Id = id;
}

public String getMoneda_Origen() {
	return Moneda_Origen;
}

public void setMoneda_Origen(String moneda_Origen) {
	Moneda_Origen = moneda_Origen;
}

public String getMoneda_Destino() {
	return Moneda_Destino;
}

public void setMoneda_Destino(String moneda_Destino) {
	Moneda_Destino = moneda_Destino;
}

public double getValor_Origen() {
	return Valor_Origen;
}

public void setValor_Origen(double valor_Origen) {
	Valor_Origen = valor_Origen;
}

public double getValor_Destino() {
	return Valor_Destino;
}

public void setValor_Destino(double valor_Destino) {
	Valor_Destino = valor_Destino;
}

public double getDevolver_cambio() {
	return devolver_cambio;
}

public void setDevolver_cambio(double devolver_cambio) {
	this.devolver_cambio = devolver_cambio;
}


private static final long serialVersionUID = 1L;


}