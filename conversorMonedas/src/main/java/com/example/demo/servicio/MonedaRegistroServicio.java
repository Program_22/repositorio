package com.example.demo.servicio;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.example.demo.configuracion.NotFoundException;
import com.example.demo.model.MonedaRegistro;
import com.example.demo.repositorio.MonedaRegistroRepo;

@Service
@Transactional
public class MonedaRegistroServicio {
	
	@Autowired
	MonedaRegistroRepo monedaRegistroRepo;

	public MonedaRegistro create(MonedaRegistro  monedaRegistro) {
		MonedaRegistro mc = monedaRegistroRepo.save(monedaRegistro);
		return  mc;
	}

	public List<MonedaRegistro> findAll() {
		List<MonedaRegistro> mlist = monedaRegistroRepo.findAll();
		return mlist;
	}

	public MonedaRegistro findbyId(Long id) {
		if (!monedaRegistroRepo.existsById(id)) {
			throw new NotFoundException("Moneda no encontrada " + id);
		} else {
			MonedaRegistro mid = monedaRegistroRepo.getOne(id);
			return mid ;
		}
	}

	public MonedaRegistro update(Long id, MonedaRegistro monedaRegistro) {
		if (!monedaRegistroRepo.existsById(id)) {
			throw new NotFoundException("Moneda no encontrada  " + id);
		} else {
			monedaRegistro.setId(id);;
			MonedaRegistro mupdate = monedaRegistroRepo.save(monedaRegistro);
			return mupdate ;
		}
	}

	public void delete(Long id) {
		if (!monedaRegistroRepo.existsById(id)) {
			throw new NotFoundException("Moneda no encontrada : " + id);
		} else {
			monedaRegistroRepo.deleteById(id);
		}
	}

}
