package com.example.demo.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.MonedaRegistro;

@Repository
public interface MonedaRegistroRepo extends JpaRepository<MonedaRegistro,Long> {

}
