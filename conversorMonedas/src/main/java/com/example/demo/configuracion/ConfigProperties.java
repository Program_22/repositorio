package com.example.demo.configuracion;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "auth")
public class ConfigProperties {
	
	
	  private String user;
	  private String password;

	  public String getUserName() {
	    return user;
	  }

	  public void setUser(String user) {
	    this.user = user;
	  }

	  public String getPassword() {
	    return password;
	  }

	  public void setPassword(String password) {
	    this.password = password;
	  }

}
