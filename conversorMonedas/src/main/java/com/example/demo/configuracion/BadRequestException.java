package com.example.demo.configuracion;

public class BadRequestException extends RuntimeException{
	public BadRequestException(String exception) {
		super(exception);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
