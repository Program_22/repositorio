package com.example.demo.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.MonedaRegistro;
import com.example.demo.servicio.MonedaRegistroServicio;

@RestController
public class MonedaRegistroControlador {
	@Autowired
	MonedaRegistroServicio monedaRegistroServicio;
	
	@PostMapping("/api/monedas")
	public ResponseEntity<MonedaRegistro> postMonedas(@RequestBody MonedaRegistro monedaregistro) {
		return ResponseEntity.status(HttpStatus.CREATED).body(monedaRegistroServicio.create(monedaregistro));
	}

	@GetMapping("/api/monedas")
	public ResponseEntity<List<MonedaRegistro>> getMonedas() {
		return ResponseEntity.status(HttpStatus.OK).body(monedaRegistroServicio.findAll());
	}

	@GetMapping("/api/monedas/{Id}")
	public ResponseEntity<MonedaRegistro> getMonedas(@PathVariable Long id) {
		return ResponseEntity.status(HttpStatus.OK).body(monedaRegistroServicio.findbyId(id));
	}

	@PatchMapping("/api/monedas/{Id}")

	public ResponseEntity<MonedaRegistro> patchMonedaRegistro(@PathVariable Long Id, @RequestBody MonedaRegistro monedaregistro) {
		return ResponseEntity.status(HttpStatus.OK).body(monedaRegistroServicio.update(Id, monedaregistro));
	}

	@DeleteMapping("/api/monedas/{Id}")
	public ResponseEntity<Void> deleteMonedaRegistro(@PathVariable Long Id) {
		monedaRegistroServicio.delete(Id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
	}

}
	

