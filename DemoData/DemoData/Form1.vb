﻿Public Class Form1

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Height = 387
        listadoFilas("", 1)
    End Sub
    Private Sub BtnSalir_Click(sender As Object, e As EventArgs) Handles BtnSalir.Click
        Me.Close()
    End Sub


    Private Sub listadoFilas(xnom As String, wop As Integer)
        Me.lvDocumentos.Items.Clear()
        Dim obj As New Capa.ClsDocumento 'Obj es un objeto que pertenece a la clase Clstipomov
        Dim lv As New ListViewItem
        Dim dt As New DataTable
        If wop = 1 Then
            dt = obj.p_listaDocumentos
        Else
            dt = obj.p_listaDocumentosxnom(xnom)
        End If
        Dim xnum As Int16 = dt.Rows.Count
        If xnum = 0 Then Exit Sub
        For k = 0 To xnum - 1
            lv = New ListViewItem(dt.Rows(k)(0).ToString) 'Código
            lv.SubItems.Add(dt.Rows(k)(1).ToString) 'Descripción
            lv.SubItems.Add(dt.Rows(k)(2).ToString) 'Descripción
            lv.SubItems.Add(dt.Rows(k)(3).ToString) 'Descripción
            lv.SubItems.Add(dt.Rows(k)(4).ToString) 'Descripción
            lvDocumentos.Items.Add(lv)
        Next
    End Sub

    Private Sub BtnNuevo_Click(sender As Object, e As EventArgs) Handles BtnNuevo.Click
        Me.Height = 561
        txtdes.Clear() : lblcod.Text = ""
        txtdes.Focus()
        BtnGrabar.Text = "Grabar"
        cboEstado.Text = "Activado"
    End Sub

    Private Sub BtnEditar_Click(sender As Object, e As EventArgs) Handles BtnEditar.Click
        If lvDocumentos.SelectedItems.Count > 0 Then
            Me.Height = 561
            Me.txtdes.Text = lvDocumentos.SelectedItems(0).SubItems(1).Text
            Me.lblcod.Text = lvDocumentos.SelectedItems(0).Text
            Me.txtcodsunat.Text = lvDocumentos.SelectedItems(0).SubItems(2).Text
            Me.txttipo.Text = lvDocumentos.SelectedItems(0).SubItems(3).Text
            BtnGrabar.Text = "Actualiza"
            If lvDocumentos.SelectedItems(0).SubItems(4).Text = "A" Then
                cboEstado.Text = "Activado"
            Else
                cboEstado.Text = "Desactivado"
            End If
            txtdes.Focus()
        End If
    End Sub

    Private Sub BtnGrabar_Click(sender As Object, e As EventArgs) Handles BtnGrabar.Click
        Try
            If Trim(txtdes.Text) = "" Then Exit Sub
            If MessageBox.Show("Desea grabar los datos", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim obj As New Capa.ClsDocumento
                If BtnGrabar.Text = "Grabar" Then
                    obj.p_ingDoc(Me.txtdes.Text, txtcodsunat.Text, txttipo.Text, IIf(cboEstado.Text = "Activado", "A", "X"))
                Else
                    obj.p_modDoc(Me.lblcod.Text, Me.txtdes.Text, txtcodsunat.Text, txttipo.Text, IIf(cboEstado.Text = "Activado", "A", "X"))
                End If
                listadoFilas("", 1)
                txtbusDoc.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Aviso")
        End Try
        Me.Height = 390
    End Sub

    Private Sub txtbusDoc_TextChanged(sender As Object, e As EventArgs) Handles txtbusDoc.TextChanged
        listadoFilas(txtbusDoc.Text, 2)
    End Sub

    Private Sub btncancelar_Click(sender As Object, e As EventArgs) Handles btncancelar.Click
        Me.Height = 390
    End Sub

    Private Sub BtnImprimir_Click(sender As Object, e As EventArgs) Handles BtnImprimir.Click
        Dim Frep As New FrmDoc_rep
        Frep.ShowDialog()
    End Sub
End Class
