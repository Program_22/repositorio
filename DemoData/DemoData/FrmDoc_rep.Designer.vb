﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDoc_rep
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.ventasDataSet = New DemoData.ventasDataSet()
        Me.usp_listaDocumentosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.usp_listaDocumentosTableAdapter = New DemoData.ventasDataSetTableAdapters.usp_listaDocumentosTableAdapter()
        CType(Me.ventasDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.usp_listaDocumentosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        ReportDataSource1.Name = "DataSet1"
        ReportDataSource1.Value = Me.usp_listaDocumentosBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "DemoData.Report1.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(12, 25)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(436, 352)
        Me.ReportViewer1.TabIndex = 0
        '
        'ventasDataSet
        '
        Me.ventasDataSet.DataSetName = "ventasDataSet"
        Me.ventasDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'usp_listaDocumentosBindingSource
        '
        Me.usp_listaDocumentosBindingSource.DataMember = "usp_listaDocumentos"
        Me.usp_listaDocumentosBindingSource.DataSource = Me.ventasDataSet
        '
        'usp_listaDocumentosTableAdapter
        '
        Me.usp_listaDocumentosTableAdapter.ClearBeforeFill = True
        '
        'FrmDoc_rep
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(460, 389)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "FrmDoc_rep"
        Me.Text = "FrmDoc_rep"
        CType(Me.ventasDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.usp_listaDocumentosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents usp_listaDocumentosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ventasDataSet As DemoData.ventasDataSet
    Friend WithEvents usp_listaDocumentosTableAdapter As DemoData.ventasDataSetTableAdapters.usp_listaDocumentosTableAdapter
End Class
