create database ventas
go
use ventas
go
create table documento
(coddoc char(2) not null primary key,
nomdoc varchar (30) not null,
codsunat char(2) not null,
tipo char(1) not null,
estdoc char(1) not null)
go

insert documento values ('01','Pedido de Venta','01',0,'A')
insert documento values ('02','Factura de Venta','02',1,'A')
insert documento values ('03','Boleta','02',1,'A')
go

----- Documento
-- USP User Stored Procedure
create proc usp_ingDoc @nom varchar(30),@codsunat char(2),@tipo char(1), @est char(1)
as
Declare @x int, @cod char (2) -- Creamos variable
select @x=max(coddoc) from documento 
set @cod = right ('00'+ltrim (str(@x+1)),2)
insert into documento values (@cod,@nom,@codsunat,@tipo,@est)
go

usp_ingDoc 'Nota de Abono','06','1', 'A'
go

create proc usp_modDoc @cod int, @nom varchar(35),@codsunat char (2), @tipo int, @est char(1)
as
update documento set nomdoc=@nom, codSunat=@codsunat, tipo=@tipo, estdoc=@est 
where coddoc=@cod
go

usp_modDoc '05','N/Abono','03','1','A'
go

create proc usp_listaDocumentosxnom @nom varchar(20)
as
select * from Documento
Where nomdoc like @nom + '%'
go


create proc usp_listaDocumentos
as
select * from Documento
go
