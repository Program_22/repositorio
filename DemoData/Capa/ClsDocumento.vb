﻿Imports System.Data.SqlClient
Public Class ClsDocumento

    Private conex As New SqlConnection("Data Source=(Local);Initial Catalog=Ventas;Integrated security=True")
    'Propiedad de solo lectura
    Public ReadOnly Property cn() As SqlConnection
        Get
            Return conex
        End Get
    End Property

    Public Function p_ingDoc(wnom As String, wcodsunat As String, wtipo As Integer, west As String) As String
        cn.Open() 'Abre la conexión
        Dim cmd As New SqlCommand(), par As New SqlParameter()
        cmd.Connection = cn
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "usp_ingDoc" 'Procedimiento almacenado 
        par = cmd.Parameters.Add("@nom", SqlDbType.VarChar, 35)
        par.Value = wnom
        par = cmd.Parameters.Add("@codsunat", SqlDbType.Char, 2)
        par.Value = wcodsunat
        par = cmd.Parameters.Add("@tipo", SqlDbType.Int)
        par.Value = wtipo
        par = cmd.Parameters.Add("@est", SqlDbType.Char, 1)
        par.Value = west
        cmd.ExecuteNonQuery()
        cn.Close() 'cierra la conexión
        Return "Si"
    End Function
    Public Function p_modDoc(wcod As String, wnom As String, wcodsunat As String, wtipo As Integer, west As String) As String
        cn.Open() 'Abre la conexión
        Dim cmd As New SqlCommand(), par As New SqlParameter()
        cmd.Connection = cn
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "usp_modDoc" 'Procedimiento almacenado 
        par = cmd.Parameters.Add("@cod", SqlDbType.VarChar, 35)
        par.Value = wcod
        par = cmd.Parameters.Add("@nom", SqlDbType.VarChar, 35)
        par.Value = wnom
        par = cmd.Parameters.Add("@codsunat", SqlDbType.Char, 2)
        par.Value = wcodsunat
        par = cmd.Parameters.Add("@tipo", SqlDbType.Int)
        par.Value = wtipo
        par = cmd.Parameters.Add("@est", SqlDbType.Char, 1)
        par.Value = west
        cmd.ExecuteNonQuery()
        cn.Close() 'cierra la conexión
        Return "Si"
    End Function
    Public Function p_listaDocumentosxnom(wnom As String) As DataTable
        Dim ds As New DataTable
        Dim da As New SqlDataAdapter("Usp_listaDocumentosxnom", cn)
        da.SelectCommand.CommandType = CommandType.StoredProcedure
        Dim par As New SqlParameter("@nom", SqlDbType.VarChar, 20)
        par.Direction = ParameterDirection.Input
        da.SelectCommand.Parameters.Add(par)
        par.Value = wnom
        da.Fill(ds)
        Return ds
    End Function
    Public Function SQLtable(wsql As String) As DataTable
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(wsql, cn)
        da.SelectCommand.CommandType = CommandType.Text
        da.Fill(dt)
        Return dt
    End Function
    Public Function p_listaDocumentos() As DataTable
        Return SQLtable("select * from Documento order by 1")
    End Function
End Class
