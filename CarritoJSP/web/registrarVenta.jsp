
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Modelo.*"%>
<%@page import="java.util.*"%>
<%@page session="true"%>
<%@page session="true"%>
<%
String usu="";
String nom="";
HttpSession sesionOK=request.getSession();

if(sesionOK.getAttribute("perfil")!=null)
nom=(String)sesionOK.getAttribute("nom")+" "+(String)sesionOK.getAttribute("ape");
%>
<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Carrito Online</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilos.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/flaticon.css"> 
    </head>
    <body>
         <h1 align="center"></h1>
    </body>
     <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Ricsatec</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
                <li><a href="index.jsp">Catálogo</a></li>
            <%
            if(sesionOK.getAttribute("perfil")!=null && sesionOK.getAttribute("perfil").equals("Admin")){
            %>
            <li><a href="registrarProducto.jsp">Registrar Producto</a></li>
            <%
                }
           
                if(sesionOK.getAttribute("perfil")!=null){
                %>
            <li><a href="registrarVenta.jsp">Registrar ventas</a></li> 
            <%
                }
      if(sesionOK.getAttribute("perfil")!=null && sesionOK.getAttribute("perfil").equals("Admin")){
                %>
          <li><a href="ServletControlador?accion=MostrarVentas">Consultar Ventas</a></li>
          <%
              }
            %>
               <%if(sesionOK.getAttribute("perfil")!=null){      
            %>
            <li><a href="ServletLogueo?accion=cerrar">Cerrar Sesión</a></li>            
            <th width="200"><%out.println("Bienvenido: "+nom);%>
            </th>
            <%
                }
                %>
                 </ul>      
      <ul class="nav navbar-nav navbar-right">
                <%
                if(sesionOK.getAttribute("perfil")==null){
                %>                
            <li width="200"><a href="login.jsp">Iniciar Sesion</a></li>
            <% } %>
           
        </ul>   
               </div>
  </div>
</nav>
   
       <div class="container-fluid">
           <div class="row">
               <div class="col-md-12">
                   <div class="jumbotron">
                       <h1 align="center"></h1>
                   
                    </div>
               </div>
           </div>
       </div>
      
      <section id="producto">
           <div class="container">
               <div class="row">
                   <div class="col-md-12">
                       <div class="titulo">
                           <h2>Catalogo de Productos</h2>
                           <div class="hr"></div>
                       </div>
                   </div>
        <h2 align="center">Carrito de Compras</h2>
        <form method="post" action="ServletControlador">  
            
            <input type="hidden" name="accion" value="RegistrarVenta"/>
            <table border="1" align="center" width="450">
                <input type="hidden" name="txtCliente" value="<%
                        if(sesionOK.getAttribute("perfil")!=null)
                    out.println(nom);%>" readonly="readonly"></th>

                <tr style="background-color: skyblue;color:black;font-weight:bold">
                    <td width="180">Nombre</td>  
                    <td>Precio</td>  
                    <td>Cantidad</td>  
                    <td>Descuento</td>  
                    <td>Sub. Total</td>                                 
                </tr>
                <%--Los productos que tenemos en la sesion que se llama carrito --%>
                <%
                double total=0;
           
                ArrayList<DetalleVenta> lista=
                        (ArrayList<DetalleVenta>)session.getAttribute("carrito");
                if(lista!=null){
                    
                    for (DetalleVenta d:lista){
          %>
                     <tr>
            <td><%=d.getProducto().getNombre()%></td>
            <td><%=d.getProducto().getPrecio()%></td>    
            <td><%=d.getCantidad()%></td>
            <td><%=String.format("%.2f",d.getDescuento())%></td>
            <td><%=String.format("%.2f",(d.getProducto().getPrecio()*d.getCantidad())-d.getDescuento())%></td>;  
           
            </tr>
            <%
                total=total+(d.getProducto().getPrecio()*d.getCantidad())-d.getDescuento();
                }
}
%>
<tr>
    <th colspan="4" align="right">Total</th>
    <th><%=String.format("%.2f",total)%></th>
</tr>
<tr>
    <th colspan="5">
        <input type="submit" value="Registrar Venta" name="btnVenta"
</tr>
            </table>
<input type="hidden" name="total" value="<%=total%>" />
        </form>
    <div>
    <h3 align="center"><a href="index.jsp">Seguir Comprando</a> ||
        <a href="ServletLogueo?accion=Cancelar">Cancelar Compra</a>
                 
    </h3>
    </div>
         <script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script src="js/code.js"></script>   
    </body>
</html>
