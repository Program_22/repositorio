<%-- 
    Document   : login
    Created on : 15-oct-2017, 21:10:03
    Author     : maylon
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page session="true"%>
<%
String usu="";
String nom="";
HttpSession sesionOK=request.getSession();

if(sesionOK.getAttribute("perfil")!=null)
nom=(String)sesionOK.getAttribute("nom")+" "+(String)sesionOK.getAttribute("ape");
%>
<!DOCTYPE html>
<html>
    <head>
           <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilos.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/flaticon.css">  
    </head>
    <body>
        <h1 align="center"></h1>
          </body>
         <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Ricsatec</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      
            <li><a href="index.jsp">Catálogo</a></li>
          
           <%
            if(sesionOK.getAttribute("perfil")!=null && sesionOK.getAttribute("perfil").equals("Admin")){
            %>
            <li><a href="registrarProducto.jsp">Registrar Producto</a></li>
            <%
                }
           
                if(sesionOK.getAttribute("perfil")!=null){
                %>
            <li><a href="registrarVenta.jsp">Registrar ventas</a></li> 
            <%
                }
      if(sesionOK.getAttribute("perfil")!=null && sesionOK.getAttribute("perfil").equals("Admin")){
                %>
          <li><a href="ServletControlador?accion=MostrarVentas">Consultar Ventas</a></li>
          <%
              }
            %>
            <%if(sesionOK.getAttribute("perfil")!=null){      
            %>
            <li><a href="ServletLogueo?accion=cerrar">Cerrar Sesión</a></li>            
            <li width="200"><%out.println("Bienvenido: "+nom);%>
            </li>
            <%
                }
                %>
                
                <%
                if(sesionOK.getAttribute("perfil")==null){
                %>                
            <li width="200"><a href="login.jsp">Iniciar Sesion</a></li>
            <% } %>
      </ul>
</div>
  </div>
</nav>
   
       <div class="container-fluid">
           <div class="row">
               <div class="col-md-12">
                   <div class="jumbotron">
                       <h1 align="center"></h1>
                   
                    </div>
               </div>
           </div>
       </div>
      
      <section id="producto">
           <div class="container">
               <div class="row">
                   <div class="col-md-12">
                       <div class="titulo">
                       
                           <div class="hr"></div>
                       </div>
                   </div>
      
    <h2 align="center">Bienvenido</h2>
    <table border="0" width="300" align="center">
        <form action="ServletLogueo" method="post">    
            <input type="hidden" name="accion" value="loguin"/>
            <tr>
                <td>Usuario:</td>
                <td><input type="text" placeholder="Usuario" name="txtUsu"></td>
            </tr>
            <tr>
                 <td>Password:</td> 
                 <td><input type="password" placeholder="Password" name="txtPas"></td>
            </tr>
            <tr>
                <th colspan="2">
                    <input type="submit" name="btn" value="Iniciar Sesion">
                </th>
                
            </tr>
        </form>
    </table>
    <h4 align="center">
        <%
        if(request.getAttribute("msg")!=null)
        out.println(request.getAttribute("msg"));
        %>
        
    </h4>
  <script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script src="js/code.js"></script>   
</html>
