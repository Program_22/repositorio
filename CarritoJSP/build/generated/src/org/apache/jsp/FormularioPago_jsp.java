package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class FormularioPago_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

String usu="";
String nom="";
HttpSession sesionOK=request.getSession();

if(sesionOK.getAttribute("perfil")!=null)
nom=(String)sesionOK.getAttribute("nom")+" "+(String)sesionOK.getAttribute("ape");

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"css/estilos.css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"css/owl.carousel.css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css\">\n");
      out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/flaticon.css\"> \n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("         <h1 align=\"center\"></h1>\n");
      out.write("    </body>\n");
      out.write("           <nav class=\"navbar navbar-default navbar-fixed-top\">\n");
      out.write("  <div class=\"container\">\n");
      out.write("    <div class=\"navbar-header\">\n");
      out.write("      <a class=\"navbar-brand\" href=\"#\">Ricsatec</a>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n");
      out.write("      <ul class=\"nav navbar-nav\">\n");
      out.write("                <li><a href=\"index.jsp\">Catálogo</a></li>\n");
      out.write("            ");

            if(sesionOK.getAttribute("perfil")!=null && sesionOK.getAttribute("perfil").equals("Admin")){
            
      out.write("\n");
      out.write("            <li><a href=\"registrarProducto.jsp\">Registrar Producto</a></li>\n");
      out.write("            ");

                }
           
                if(sesionOK.getAttribute("perfil")!=null){
                
      out.write("\n");
      out.write("            <li><a href=\"registrarVenta.jsp\">Registrar ventas</a></li> \n");
      out.write("            ");

                }
      if(sesionOK.getAttribute("perfil")!=null && sesionOK.getAttribute("perfil").equals("Admin")){
                
      out.write("\n");
      out.write("          <li><a href=\"ServletControlador?accion=MostrarVentas\">Consultar Ventas</a></li>\n");
      out.write("          ");

              } 
            
      out.write("\n");
      out.write("               ");
if(sesionOK.getAttribute("perfil")!=null){      
            
      out.write("\n");
      out.write("            <li><a href=\"ServletLogueo?accion=cerrar\">Cerrar Sesión</a></li>            \n");
      out.write("            <th width=\"200\">");
out.println("Bienvenido: "+nom);
      out.write("\n");
      out.write("            </th>\n");
      out.write("            ");

                }
                
      out.write("\n");
      out.write("      </ul>\n");
      out.write("       <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("                ");

                if(sesionOK.getAttribute("perfil")==null){
                
      out.write("                \n");
      out.write("            <li width=\"200\"><a href=\"login.jsp\">Iniciar Sesion</a></li>\n");
      out.write("            ");
 } 
      out.write("\n");
      out.write("         \n");
      out.write("       </ul>\n");
      out.write("            </div>\n");
      out.write("  </div>\n");
      out.write("</nav>\n");
      out.write("   \n");
      out.write("       <div class=\"container-fluid\">\n");
      out.write("           <div class=\"row\">\n");
      out.write("               <div class=\"col-md-12\">\n");
      out.write("                   <div class=\"jumbotron\">\n");
      out.write("                       <h1 align=\"center\"></h1>\n");
      out.write("                   \n");
      out.write("                    </div>\n");
      out.write("               </div>\n");
      out.write("           </div>\n");
      out.write("       </div>\n");
      out.write("      \n");
      out.write("      <section id=\"producto\">\n");
      out.write("           <div class=\"container\">\n");
      out.write("               <div class=\"row\">\n");
      out.write("                   <div class=\"col-md-12\">\n");
      out.write("                       <div class=\"titulo\">\n");
      out.write("                           <h2>Catalogo de Productos</h2>\n");
      out.write("                           <div class=\"hr\"></div>\n");
      out.write("                       </div>\n");
      out.write("                   </div>\n");
      out.write("        <h1 align=\"center\">\n");
      out.write("            ");

            double total=Double.parseDouble(request.getParameter("total"));
            out.println("Total a Pagar:" +total+"<p>");
            
      out.write("\n");
      out.write("            \n");
      out.write("\n");
      out.write("<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\">\n");
      out.write("<input type=\"hidden\" name=\"cmd\" value=\"_ext-enter\" />\n");
      out.write("<input type=\"hidden\" name=\"redirect_cmd\" value=\"_xclick\" />\n");
      out.write("<input type=\"hidden\" name=\"business\" value=\"xxxxxx@hotmail.com\" />\n");
      out.write("<input type=\"hidden\" name=\"item_name\" value=\"Productos varios\" />\n");
      out.write("<input type=\"hidden\" name=\"quantity\" value=\"1\" />\n");
      out.write("<input type=\"hidden\" name=\"amount\" value=\"");
      out.print(total);
      out.write("\" />\n");
      out.write("<input type=\"hidden\" name=\"currency_code\" value=\"USD\" />\n");
      out.write("<input type=\"hidden\" name=\"return\" value=\"http://localhost:8084/CarritoJSP/\" />\n");
      out.write("<input type=\"hidden\" name=\"bn\" value=\"PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest\" />\n");
      out.write("<input type=\"image\" src=\"http://www.paypal.com/es_XC/i/btn/x-click-but01.gif\" border=\"0\" name=\"submit\" alt=\"Pagar para completar la compra.\" />\n");
      out.write("\n");
      out.write("</form>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("            \n");
      out.write("            \n");
      out.write("        </h1>\n");
      out.write(" <script src=\"js/jquery.js\"></script>\n");
      out.write("<script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("<script src=\"js/owl.carousel.js\"></script>\n");
      out.write("<script src=\"js/code.js\"></script>  \n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
