package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class registrarUsuario_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("   <html>\n");
      out.write("    <head>\n");
      out.write("       <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Registro</title>\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"css/estilos.css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"css/owl.carousel.css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css\">\n");
      out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/flaticon.css\"> \n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("         <h1 align=\"center\"></h1>\n");
      out.write("         </body>\n");
      out.write("           <nav class=\"navbar navbar-default navbar-fixed-top\">\n");
      out.write("  <div class=\"container\">\n");
      out.write("    <div class=\"navbar-header\">\n");
      out.write("      <a class=\"navbar-brand\" href=\"#\">Ricsatec</a>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n");
      out.write("      <ul class=\"nav navbar-nav\">\n");
      out.write("            <li><a href=\"index.jsp\">Catálogo</a></li>\n");
      out.write("          \n");
      out.write("            </li>\n");
      out.write("               \n");
      out.write("            <li width=\"200\"><a href=\"login.jsp\">Iniciar Sesion</a></li>\n");
      out.write("        </ul>\n");
      out.write("     </div>\n");
      out.write("  </div>\n");
      out.write("</nav>\n");
      out.write("   \n");
      out.write("       <div class=\"container-fluid\">\n");
      out.write("           <div class=\"row\">\n");
      out.write("               <div class=\"col-md-12\">\n");
      out.write("                   <div class=\"jumbotron\">\n");
      out.write("                       <h1 align=\"center\"></h1>\n");
      out.write("                   \n");
      out.write("                    </div>\n");
      out.write("               </div>\n");
      out.write("           </div>\n");
      out.write("       </div>\n");
      out.write("      \n");
      out.write("      <section id=\"producto\">\n");
      out.write("           <div class=\"container\">\n");
      out.write("               <div class=\"row\">\n");
      out.write("                   <div class=\"col-md-12\">\n");
      out.write("                       <div class=\"titulo\">\n");
      out.write("                           <h2>Bienvenido al  Registro</h2>\n");
      out.write("                           <div class=\"hr\"></div>\n");
      out.write("                       </div>\n");
      out.write("                   </div>\n");
      out.write("    </table>\n");
      out.write("    <h2 align=\"center\">Registro</h2>\n");
      out.write("    <table border=\"0\" width=\"300\" align=\"center\">\n");
      out.write("        <form action=\"ServletLogueo\" method=\"post\">    \n");
      out.write("            <input type=\"hidden\" name=\"accion\" value=\"loguin\"/>\n");
      out.write("            <tr>\n");
      out.write("                <td>Apellidos:</td>\n");
      out.write("                <td><input type=\"text\" placeholder=\"Apellido\" name=\"txtUsu\"></td>\n");
      out.write("            </tr>\n");
      out.write("               <tr>\n");
      out.write("                <td>Nombre:</td>\n");
      out.write("                <td><input type=\"text\" placeholder=\"Nombres\" name=\"txtUsu\"></td>\n");
      out.write("            </tr>\n");
      out.write("               <tr>\n");
      out.write("                <td>Correo:</td>\n");
      out.write("                <td><input type=\"text\" placeholder=\"Correo\" name=\"txtUsu\"></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("                 <td>Password:</td> \n");
      out.write("                 <td><input type=\"password\" placeholder=\"Contraseña\" name=\"txtPas\"></td>\n");
      out.write("            </tr>\n");
      out.write("              <tr>\n");
      out.write("                 <td>Password:</td> \n");
      out.write("                 <td><input type=\"password\" placeholder=\"Confirmar Contraseña\" name=\"txtPas\"></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("                <th colspan=\"2\">\n");
      out.write("                    <input type=\"submit\" name=\"btn\" value=\"Registrar\">\n");
      out.write("                </th>\n");
      out.write("                \n");
      out.write("            </tr>\n");
      out.write("        </form>\n");
      out.write("    </table>\n");
      out.write("    <h4 align=\"center\">\n");
      out.write("       \n");
      out.write("        \n");
      out.write("    </h4>\n");
      out.write("   \n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
