package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Modelo.*;
import java.util.*;

public final class registrarVenta_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");

String usu="";
String nom="";
HttpSession sesionOK=request.getSession();

if(sesionOK.getAttribute("perfil")!=null)
nom=(String)sesionOK.getAttribute("nom")+" "+(String)sesionOK.getAttribute("ape");

      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("      <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Carrito Online</title>\n");
      out.write("        <link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"css/estilos.css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"css/owl.carousel.css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css\">\n");
      out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/flaticon.css\"> \n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("         <h1 align=\"center\"></h1>\n");
      out.write("    </body>\n");
      out.write("     <nav class=\"navbar navbar-default navbar-fixed-top\">\n");
      out.write("  <div class=\"container\">\n");
      out.write("    <div class=\"navbar-header\">\n");
      out.write("      <a class=\"navbar-brand\" href=\"#\">Ricsatec</a>\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n");
      out.write("      <ul class=\"nav navbar-nav\">\n");
      out.write("                <li><a href=\"index.jsp\">Catálogo</a></li>\n");
      out.write("            ");

            if(sesionOK.getAttribute("perfil")!=null && sesionOK.getAttribute("perfil").equals("Admin")){
            
      out.write("\n");
      out.write("            <li><a href=\"registrarProducto.jsp\">Registrar Producto</a></li>\n");
      out.write("            ");

                }
           
                if(sesionOK.getAttribute("perfil")!=null){
                
      out.write("\n");
      out.write("            <li><a href=\"registrarVenta.jsp\">Registrar ventas</a></li> \n");
      out.write("            ");

                }
      if(sesionOK.getAttribute("perfil")!=null && sesionOK.getAttribute("perfil").equals("Admin")){
                
      out.write("\n");
      out.write("          <li><a href=\"ServletControlador?accion=MostrarVentas\">Consultar Ventas</a></li>\n");
      out.write("          ");

              }
            
      out.write("\n");
      out.write("               ");
if(sesionOK.getAttribute("perfil")!=null){      
            
      out.write("\n");
      out.write("            <li><a href=\"ServletLogueo?accion=cerrar\">Cerrar Sesión</a></li>            \n");
      out.write("            <th width=\"200\">");
out.println("Bienvenido: "+nom);
      out.write("\n");
      out.write("            </th>\n");
      out.write("            ");

                }
                
      out.write("\n");
      out.write("                 </ul>      \n");
      out.write("      <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("                ");

                if(sesionOK.getAttribute("perfil")==null){
                
      out.write("                \n");
      out.write("            <li width=\"200\"><a href=\"login.jsp\">Iniciar Sesion</a></li>\n");
      out.write("            ");
 } 
      out.write("\n");
      out.write("           \n");
      out.write("        </ul>   \n");
      out.write("               </div>\n");
      out.write("  </div>\n");
      out.write("</nav>\n");
      out.write("   \n");
      out.write("       <div class=\"container-fluid\">\n");
      out.write("           <div class=\"row\">\n");
      out.write("               <div class=\"col-md-12\">\n");
      out.write("                   <div class=\"jumbotron\">\n");
      out.write("                       <h1 align=\"center\"></h1>\n");
      out.write("                   \n");
      out.write("                    </div>\n");
      out.write("               </div>\n");
      out.write("           </div>\n");
      out.write("       </div>\n");
      out.write("      \n");
      out.write("      <section id=\"producto\">\n");
      out.write("           <div class=\"container\">\n");
      out.write("               <div class=\"row\">\n");
      out.write("                   <div class=\"col-md-12\">\n");
      out.write("                       <div class=\"titulo\">\n");
      out.write("                           <h2>Catalogo de Productos</h2>\n");
      out.write("                           <div class=\"hr\"></div>\n");
      out.write("                       </div>\n");
      out.write("                   </div>\n");
      out.write("        <h2 align=\"center\">Carrito de Compras</h2>\n");
      out.write("        <form method=\"post\" action=\"ServletControlador\">  \n");
      out.write("            \n");
      out.write("            <input type=\"hidden\" name=\"accion\" value=\"RegistrarVenta\"/>\n");
      out.write("            <table border=\"1\" align=\"center\" width=\"450\">\n");
      out.write("                <input type=\"hidden\" name=\"txtCliente\" value=\"");

                        if(sesionOK.getAttribute("perfil")!=null)
                    out.println(nom);
      out.write("\" readonly=\"readonly\"></th>\n");
      out.write("\n");
      out.write("                <tr style=\"background-color: skyblue;color:black;font-weight:bold\">\n");
      out.write("                    <td width=\"180\">Nombre</td>  \n");
      out.write("                    <td>Precio</td>  \n");
      out.write("                    <td>Cantidad</td>  \n");
      out.write("                    <td>Descuento</td>  \n");
      out.write("                    <td>Sub. Total</td>                                 \n");
      out.write("                </tr>\n");
      out.write("                ");
      out.write("\n");
      out.write("                ");

                double total=0;
           
                ArrayList<DetalleVenta> lista=
                        (ArrayList<DetalleVenta>)session.getAttribute("carrito");
                if(lista!=null){
                    
                    for (DetalleVenta d:lista){
          
      out.write("\n");
      out.write("                     <tr>\n");
      out.write("            <td>");
      out.print(d.getProducto().getNombre());
      out.write("</td>\n");
      out.write("            <td>");
      out.print(d.getProducto().getPrecio());
      out.write("</td>    \n");
      out.write("            <td>");
      out.print(d.getCantidad());
      out.write("</td>\n");
      out.write("            <td>");
      out.print(String.format("%.2f",d.getDescuento()));
      out.write("</td>\n");
      out.write("            <td>");
      out.print(String.format("%.2f",(d.getProducto().getPrecio()*d.getCantidad())-d.getDescuento()));
      out.write("</td>;  \n");
      out.write("           \n");
      out.write("            </tr>\n");
      out.write("            ");

                total=total+(d.getProducto().getPrecio()*d.getCantidad())-d.getDescuento();
                }
}

      out.write("\n");
      out.write("<tr>\n");
      out.write("    <th colspan=\"4\" align=\"right\">Total</th>\n");
      out.write("    <th>");
      out.print(String.format("%.2f",total));
      out.write("</th>\n");
      out.write("</tr>\n");
      out.write("<tr>\n");
      out.write("    <th colspan=\"5\">\n");
      out.write("        <input type=\"submit\" value=\"Registrar Venta\" name=\"btnVenta\"\n");
      out.write("</tr>\n");
      out.write("            </table>\n");
      out.write("<input type=\"hidden\" name=\"total\" value=\"");
      out.print(total);
      out.write("\" />\n");
      out.write("        </form>\n");
      out.write("    <div>\n");
      out.write("    <h3 align=\"center\"><a href=\"index.jsp\">Seguir Comprando</a> ||\n");
      out.write("        <a href=\"ServletLogueo?accion=Cancelar\">Cancelar Compra</a>\n");
      out.write("                 \n");
      out.write("    </h3>\n");
      out.write("    </div>\n");
      out.write("         <script src=\"js/jquery.js\"></script>\n");
      out.write("<script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("<script src=\"js/owl.carousel.js\"></script>\n");
      out.write("<script src=\"js/code.js\"></script>   \n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
