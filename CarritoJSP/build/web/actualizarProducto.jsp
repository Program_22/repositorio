
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Modelo.*"%>
<%@page import="java.util.*"%>
<%@page session="true"%>
<%@page session="true"%>
<%
String usu="";
String nom="";
HttpSession sesionOK=request.getSession();

if(sesionOK.getAttribute("perfil")!=null)
nom=(String)sesionOK.getAttribute("nom")+" "+(String)sesionOK.getAttribute("ape");
%>
<%
    Producto p=ProductoDB.obtenerProducto(Integer.parseInt(request.getParameter("id")));
    %>
<!DOCTYPE html>
<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Carrito Online</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/estilos.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/flaticon.css">
    </head>
    <body>
          <h1 align="center"></h1>
    </body>
         <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Ricsatec</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
            <li><a href="index.jsp">Catálogo</a></li>
             <%
            if(sesionOK.getAttribute("perfil")!=null && sesionOK.getAttribute("perfil").equals("Admin")){
            %>
            <li><a href="registrarProducto.jsp">Registrar Producto</a></li>
            <%
                }
           
                if(sesionOK.getAttribute("perfil")!=null){
                %>
            <li><a href="registrarVenta.jsp">Registrar ventas</a></li> 
            <%
                }
      if(sesionOK.getAttribute("perfil")!=null && sesionOK.getAttribute("perfil").equals("Admin")){
                %>
          <li><a href="ServletControlador?accion=MostrarVentas">Consultar Ventas</a></li>
          <%
              }
            %>
          <%if(sesionOK.getAttribute("perfil")!=null){      
            %>
            <li><a href="ServletLogueo?accion=cerrar">Cerrar Sesión</a></li>            
            <th width="200"><%out.println("Bienvenido: "+nom);%>
            </th>
            <%
                }
                %> 
                 </ul>   
                  <ul class="nav navbar-nav navbar-right">
                <%
                if(sesionOK.getAttribute("perfil")==null){
                %>                
            <li width="200"><a href="login.jsp">Iniciar Sesion</a></li>
            <% } %>
        </ul>
    </div>
  </div>
</nav>
   
       <div class="container-fluid">
           <div class="row">
               <div class="col-md-12">
                   <div class="jumbotron">
                       <h1 align="center"></h1>
                   
                    </div>
               </div>
           </div>
       </div>
      
      <section id="producto">
           <div class="container">
               <div class="row">
                   <div class="col-md-12">
                       <div class="titulo">
                           <h2>Catalogo de Productos</h2>
                           <div class="hr"></div>
                       </div>
                   </div>
        <h2 align="center">Actualizar Productos</h2>
        <form action="ServletControlador" method="post">
            <table border="0" width="400" align="center">
                <tr>
                    <td>Código: </td>   
                    <td><input type="text" name="txtCodigo" value="<%=p.getCodigoProducto()%>" readonly></td>
                </tr><tr>
                    <td>Nombre: </td>
                    <td><input type="text" name="txtNombre" value="<%=p.getNombre()%>"></td>
                </tr><tr>
                    <td>Precio: </td>
                    <td><input type="text" name="txtPrecio" value="<%=p.getPrecio()%>">
                    </td>
                </tr><tr>
                    <th colspan="2"><input type="submit" value="Actualizar" name="btnActualizar" /></th>
                 </tr>  
                 <input type="hidden" name="accion" value="ModificarProducto" /> 
        </form>
                     <script src="js/jquery.js"></script>
          <script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script src="js/code.js"></script>           
    </body>
</html>
