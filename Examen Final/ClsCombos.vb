﻿Public Class ClsCombos
    Public Sub llenar_C1(cbopro As ComboBox, pros As String())

        For Each xpro As String In pros
            cbopro.Items.Add(xpro)
        Next

    End Sub

    Public Sub llenar_C2(cbouni As ComboBox, uni As String())

        For Each xuni As String In uni
            cbouni.Items.Add(xuni)
        Next

    End Sub

End Class
