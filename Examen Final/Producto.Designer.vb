﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Producto
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblpre = New System.Windows.Forms.Label()
        Me.lblpro = New System.Windows.Forms.Label()
        Me.cbopro = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'lblpre
        '
        Me.lblpre.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.lblpre.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblpre.Location = New System.Drawing.Point(171, 25)
        Me.lblpre.Name = "lblpre"
        Me.lblpre.Size = New System.Drawing.Size(61, 23)
        Me.lblpre.TabIndex = 7
        Me.lblpre.Text = "lblpre"
        '
        'lblpro
        '
        Me.lblpro.AutoSize = True
        Me.lblpro.Location = New System.Drawing.Point(16, 9)
        Me.lblpro.Name = "lblpro"
        Me.lblpro.Size = New System.Drawing.Size(50, 13)
        Me.lblpro.TabIndex = 6
        Me.lblpro.Tag = ""
        Me.lblpro.Text = "Producto"
        '
        'cbopro
        '
        Me.cbopro.FormattingEnabled = True
        Me.cbopro.Location = New System.Drawing.Point(19, 25)
        Me.cbopro.Name = "cbopro"
        Me.cbopro.Size = New System.Drawing.Size(128, 21)
        Me.cbopro.TabIndex = 5
        '
        'Producto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblpre)
        Me.Controls.Add(Me.lblpro)
        Me.Controls.Add(Me.cbopro)
        Me.Name = "Producto"
        Me.Size = New System.Drawing.Size(258, 71)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblpre As System.Windows.Forms.Label
    Friend WithEvents lblpro As System.Windows.Forms.Label
    Friend WithEvents cbopro As System.Windows.Forms.ComboBox

End Class
