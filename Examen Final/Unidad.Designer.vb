﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Unidad
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbluni = New System.Windows.Forms.Label()
        Me.cbouni = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'lbluni
        '
        Me.lbluni.AutoSize = True
        Me.lbluni.Location = New System.Drawing.Point(12, 3)
        Me.lbluni.Name = "lbluni"
        Me.lbluni.Size = New System.Drawing.Size(41, 13)
        Me.lbluni.TabIndex = 3
        Me.lbluni.Text = "Unidad"
        '
        'cbouni
        '
        Me.cbouni.FormattingEnabled = True
        Me.cbouni.Location = New System.Drawing.Point(15, 19)
        Me.cbouni.Name = "cbouni"
        Me.cbouni.Size = New System.Drawing.Size(128, 21)
        Me.cbouni.TabIndex = 2
        '
        'Unidad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lbluni)
        Me.Controls.Add(Me.cbouni)
        Me.Name = "Unidad"
        Me.Size = New System.Drawing.Size(157, 55)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbluni As System.Windows.Forms.Label
    Friend WithEvents cbouni As System.Windows.Forms.ComboBox

End Class
