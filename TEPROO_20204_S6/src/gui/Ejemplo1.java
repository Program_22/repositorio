package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import clase.Curso;
import clase.UnidadTematica;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejemplo1 extends JFrame implements ActionListener {
	private JButton btnAceptar;
	private JScrollPane scpSalida;
	private JTextArea txtSalida;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejemplo1 frame = new Ejemplo1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejemplo1() {
		setBounds(100, 100, 450, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(this);
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAceptar.setBounds(150, 10, 120, 25);
		getContentPane().add(btnAceptar);
		
		scpSalida = new JScrollPane();
		scpSalida.setBounds(13, 53, 389, 147);
		getContentPane().add(scpSalida);
		
		txtSalida = new JTextArea();
		scpSalida.setViewportView(txtSalida);

	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btnAceptar) {
			btnAceptaractionPerformed(arg0);
		}
	}
	
	protected void btnAceptaractionPerformed(ActionEvent arg0) {
		//Crear la UnidadTematica
		UnidadTematica objUnidad1 = new UnidadTematica("UT001","Herencia de clases",3.0f);
		
		//Crear un Curso
		Curso objCurso = new Curso("C001", "Programación OO", 4.5f, objUnidad1);
		listado(objCurso);
	}
	
	public void imprimir(String s) {
		txtSalida.append(s + "\n");
	}

	public void listado(Curso objC) {
		imprimir("Id: " + objC.getIdCurso());
		imprimir("Nombre: " + objC.getNombre());
		imprimir("Creditos: " + objC.getCreditos());
		imprimir("Unidad Tematica 1: " + ((objC.getUnidad1() != null)?objC.getUnidad1().getTitulo():"Indefinido"));
		imprimir("Unidad Tematica 2: " + ((objC.getUnidad2() != null)?objC.getUnidad2().getTitulo():"Indefinido"));
		imprimir("Unidad Tematica 3: " + ((objC.getUnidad3() != null)?objC.getUnidad3().getTitulo():"Indefinido"));
		imprimir("Total horas: " + objC.totalHoras());

	}
}
