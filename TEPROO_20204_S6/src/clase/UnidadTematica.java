package clase;

public class UnidadTematica {
	private String idUnidadTematica, titulo;
	private float horas;
	
	public UnidadTematica (String idUnidadTematica, String titulo, float horas) {
		this.idUnidadTematica = idUnidadTematica;
		this.titulo = titulo;
		this.horas = horas;
	}

	public String getIdUnidadTematica() {
		return idUnidadTematica;
	}

	public void setIdUnidadTematica(String idUnidadTematica) {
		this.idUnidadTematica = idUnidadTematica;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public float getHoras() {
		return horas;
	}

	public void setHoras(float horas) {
		this.horas = horas;
	}
}
