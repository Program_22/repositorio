package clase;

public class Curso {
	private String idCurso, nombre;
	private float creditos;
	//Atributos segun la relaci�n con la clase UnidadTematica
	private UnidadTematica unidad1, unidad2, unidad3;
	
	public Curso(String idCurso, String nombre, float creditos, UnidadTematica unidad1) {
		this.idCurso = idCurso;
		this.nombre = nombre;
		this.creditos = creditos;
		this.unidad1 = unidad1;
		this.unidad2 = null;
		this.unidad3 = null;
	}

	public String getIdCurso() {
		return idCurso;
	}

	public void setIdCurso(String idCurso) {
		this.idCurso = idCurso;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getCreditos() {
		return creditos;
	}

	public void setCreditos(float creditos) {
		this.creditos = creditos;
	}

	public UnidadTematica getUnidad1() {
		return unidad1;
	}

	public void setUnidad1(UnidadTematica unidad1) {
		this.unidad1 = unidad1;
	}

	public UnidadTematica getUnidad2() {
		return unidad2;
	}

	public void setUnidad2(UnidadTematica unidad2) {
		this.unidad2 = unidad2;
	}

	public UnidadTematica getUnidad3() {
		return unidad3;
	}

	public void setUnidad3(UnidadTematica unidad3) {
		this.unidad3 = unidad3;
	}

	public float totalHoras() {
		float totalH = 0;
		totalH += unidad1.getHoras();
		totalH += (unidad2 != null)?unidad2.getHoras():0;
		totalH += (unidad3 != null)?unidad3.getHoras():0;
		return totalH;
	}
}
