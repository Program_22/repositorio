package com.example.divisa.app.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.divisa.app.model.DivisaConversion;

@RestController
public class DivisaConversionController {
	 @GetMapping("/currency-converter/from/{from}/to/{to}/{quantity}")
 public DivisaConversion convertCurrency(@PathVariable("from") String from, @PathVariable("to") String to, @PathVariable BigDecimal quantity) {
Map<String,String> urivars=new HashMap<>();

urivars.put("from", from);
urivars.put("to", to);
ResponseEntity<DivisaConversion> responseEntity=new RestTemplate().
getForEntity("http://localhost:8000/currency-exchange/from/{from}/to/{to}",DivisaConversion.class,urivars);
DivisaConversion result =responseEntity.getBody();

return new DivisaConversion(result.getId(),result.getFrom(),result.getTo(),result.getConversionMultiple(),
		quantity,quantity.multiply(result.getConversionMultiple()),8100);




	 
}
}