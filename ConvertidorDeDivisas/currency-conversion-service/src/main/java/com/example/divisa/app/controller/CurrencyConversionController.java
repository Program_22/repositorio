package com.example.divisa.app.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.divisa.app.model.CurrencyConversion;

@RestController
public class CurrencyConversionController {
	 @GetMapping("/currency-converter/from/{from}/to/{to}/{quantity}")
 public CurrencyConversion convertCurrency(@PathVariable("from") String from, @PathVariable("to") String to, @PathVariable BigDecimal quantity) {
Map<String,String> urivars=new HashMap<>();

urivars.put("from", from);
urivars.put("to", to);
ResponseEntity<CurrencyConversion> responseEntity=new RestTemplate().
getForEntity("http://localhost:8000/currency-exchange/from/{from}/to/{to}",CurrencyConversion.class,urivars);
CurrencyConversion result =responseEntity.getBody();

return new CurrencyConversion(result.getId(),result.getFrom(),result.getTo(),result.getConversionMultiple(),
		quantity,quantity.multiply(result.getConversionMultiple()),8100);




	 
}
}