package com.example.divisa.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurrencyConvercionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurrencyConvercionServiceApplication.class, args);
	}

}
