package com.example.divisa.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DivisaConvercionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DivisaConvercionServiceApplication.class, args);
	}

}
