INSERT INTO divisa_valor(id,currency_from,currency_to,conversion_multiple,port) values(1,'USD','SOL',3.89,0);
INSERT INTO divisa_valor(id,currency_from,currency_to,conversion_multiple,port) values(2,'SOL','PESO',4.88,0);
INSERT INTO divisa_valor(id,currency_from,currency_to,conversion_multiple,port) values(3,'YUAN','SOL',0.56,0);
INSERT INTO divisa_valor(id,currency_from,currency_to,conversion_multiple,port) values(4,'BOLIVAR VENEZOLANO','SOL',0.16,0);
INSERT INTO divisa_valor(id,currency_from,currency_to,conversion_multiple,port) values(5,'REAL BRASILENA','SOL',0.73,0);
INSERT INTO divisa_valor(id,currency_from,currency_to,conversion_multiple,port) values(6,'PESO COLOMBIANO','RUPI ESRILANDESA',0.77,0);
INSERT INTO divisa_valor(id,currency_from,currency_to,conversion_multiple,port) values(7,'CHELIN','FRANCO',8.67,0);
