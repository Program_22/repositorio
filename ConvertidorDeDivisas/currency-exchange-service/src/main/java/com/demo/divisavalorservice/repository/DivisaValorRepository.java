package com.demo.divisavalorservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.divisavalorservice.controller.DivisaValor;

@Repository
public interface DivisaValorRepository extends JpaRepository<DivisaValor, Long> {

    DivisaValor findByFromAndTo(String from, String to);
}
