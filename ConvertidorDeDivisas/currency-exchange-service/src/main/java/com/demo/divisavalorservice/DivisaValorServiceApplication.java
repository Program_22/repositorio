package com.demo.divisavalorservice;

import brave.sampler.Sampler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;

@SpringBootApplication

public class DivisaValorServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DivisaValorServiceApplication.class, args);
	}


	}
