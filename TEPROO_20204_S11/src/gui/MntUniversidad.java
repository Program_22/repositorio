package gui;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;

public class MntUniversidad extends JInternalFrame implements ActionListener {
	private JLabel lblNewLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MntUniversidad frame = new MntUniversidad();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MntUniversidad() {
		setClosable(true);
		setBounds(100, 100, 363, 281);
		getContentPane().setLayout(null);
		
		lblNewLabel = new JLabel("Esta es una ventana tipo JInternalFrame");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel.setBounds(0, 19, 347, 23);
		getContentPane().add(lblNewLabel);

	}
	public void actionPerformed(ActionEvent arg0) {

	}
}
