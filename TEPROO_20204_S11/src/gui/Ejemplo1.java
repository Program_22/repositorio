package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JDesktopPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import java.awt.BorderLayout;
import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ScrollPaneConstants;

public class Ejemplo1 extends JFrame implements ActionListener {
	private JMenuBar mbrBarraMenu;
	private JMenu mnuArchivo;
	private JMenu mnuReportes;
	private JSeparator separator;
	private JMenuItem mitSalir;
	private JMenu mnuMantenimientos;
	private JMenuItem mitUniversidad;
	
	private static int posInicialInternalFrame;
	private JScrollPane scpEscritorio;
	private JDesktopPane dspEscritorio;
	private JMenuItem mitEstudiante;
	private JMenuItem mitRegistrarEstudiante;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejemplo1 frame = new Ejemplo1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		posInicialInternalFrame = 10;
	}

	/**
	 * Create the frame.
	 */
	public Ejemplo1() {
		setBounds(100, 100, 800, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		mbrBarraMenu = new JMenuBar();
		setJMenuBar(mbrBarraMenu);
		
		mnuArchivo = new JMenu("Archivo");
		mbrBarraMenu.add(mnuArchivo);
		
		mnuMantenimientos = new JMenu("Mantenimientos");
		mnuArchivo.add(mnuMantenimientos);
		
		mitUniversidad = new JMenuItem("Universidad");
		mitUniversidad.addActionListener(this);
		mnuMantenimientos.add(mitUniversidad);
		
		mitEstudiante = new JMenuItem("Estudiante");
		mitEstudiante.addActionListener(this);
		mnuMantenimientos.add(mitEstudiante);
		
		separator = new JSeparator();
		mnuArchivo.add(separator);
		
		mitSalir = new JMenuItem("Salir");
		mitSalir.addActionListener(this);
		mnuArchivo.add(mitSalir);
		
		mnuReportes = new JMenu("Reportes");
		mbrBarraMenu.add(mnuReportes);
		
		mitRegistrarEstudiante = new JMenuItem("Reporte de Estudiante");
		mnuReportes.add(mitRegistrarEstudiante);
		
		scpEscritorio = new JScrollPane();
		getContentPane().add(scpEscritorio, BorderLayout.CENTER);
		
		dspEscritorio = new JDesktopPane();
		scpEscritorio.setViewportView(dspEscritorio);
	}
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == mitEstudiante) {
			mitEstudianteActionPerformed(e);
		}
		if (e.getSource() == mitSalir) {
			mitSalirActionPerformed(e);
		}
		if (e.getSource() == mitUniversidad) {
			mitUniversidadActionPerformed(e);
		}
	}
	protected void mitUniversidadActionPerformed(ActionEvent e) {
		MntUniversidad objMntUniversidad = new MntUniversidad();
		objMntUniversidad.setLocation(posInicialInternalFrame, posInicialInternalFrame);
		objMntUniversidad.setVisible(true);
		dspEscritorio.add(objMntUniversidad);
		posInicialInternalFrame += 10;
		
	}
	
	protected void mitSalirActionPerformed(ActionEvent e) {
		System.exit(0);
	}
	
	protected void mitEstudianteActionPerformed(ActionEvent e) {
		MntEstudiante objMntEstudiante = new MntEstudiante();
		objMntEstudiante.setModal(true);
		objMntEstudiante.setLocation(this.getX()+((this.getWidth()-objMntEstudiante.getWidth())/2), this.getY()+((this.getHeight()-objMntEstudiante.getHeight())/2));
		objMntEstudiante.setVisible(true);
	}
}
