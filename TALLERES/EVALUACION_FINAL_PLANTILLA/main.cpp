#include <iostream>
#include "CaminoMinimo.h"
#include "ArbolExpansionMinimo.h"
#include "Arbolbinario.h"


//APELLIDOS Y NOMBRES : 

void EjercicioDijkstra(){
	GrafoMatriz g(3);
	
	g.nuevoVertice("V1");
	g.nuevoVertice("V2");
	g.nuevoVertice("V3");
	
	g.nuevoArco(g.numVertice("V1"),g.numVertice("V2"),3);	
	g.nuevoArco(g.numVertice("V1"),g.numVertice("V3"),4);
	
	g.VerMatriz();
	
	CaminoMinimo caminoMinimo(g,g.numVertice("V1"));

	caminoMinimo.Dijkstra(g,g.numVertice("V1")); 	
	
	cout << endl << endl;
	cout << "Camino mas corto:" << endl;
	caminoMinimo.recuperaCamino(g.numVertice("V3"));
	cout << "Costo: " << caminoMinimo.OdistanciaMinima()[g.numVertice("V3")] << endl;
}

void EjercicioPrim(){
	GrafoMatriz g(3);
	
	g.nuevoVertice("V1");
	g.nuevoVertice("V2");
	g.nuevoVertice("V3");
	
	g.nuevoArco(g.numVertice("V1"),g.numVertice("V2"),3);	
	g.nuevoArco(g.numVertice("V1"),g.numVertice("V3"),4);

	ArbolExpansionMinimo arbolExpansion(g);
	arbolExpansion.arbolExpansionPrim(g);
	
	
}

void EjercicioArbol(){
		Nodo<char> * n1,*n2,*n3, *n4, *n5, *n6,*n7,*n8;
	
	n1= new Nodo<char>('1');
	n2= new Nodo<char>('2');
	n3= new Nodo<char>('3');
	n4= new Nodo<char>('4');
	n5= new Nodo<char>('5');
	n6= new Nodo<char>('6');
	n7= new Nodo<char>('7');
	n8= new Nodo<char>('8');
	
	n1->ramaIzdo(n2);
	n1->ramaDcho(n3);
	
	n2->ramaIzdo(n4);
	n2->ramaDcho(n5);

	n4->ramaIzdo(n7);
	
	n3->ramaIzdo(n6);
	
	n6->ramaIzdo(n8);
	
	Arbolbinario<char> arbol;
	
	arbol.Praiz(n1);
	
	cout << "PreOrden:" << endl;
	arbol.postorden();


}

int main(int argc, char** argv) {
	
	cout << "Ejericio Dijkstra :" << endl;
	EjercicioDijkstra();

	cout << "Ejericio Prim :" << endl;
	EjercicioPrim();
	
	cout << "Ejericio Arbol :" << endl;
	EjercicioArbol();
	
	return 0;
}


