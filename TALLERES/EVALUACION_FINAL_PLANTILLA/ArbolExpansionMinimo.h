#include <iostream>
using namespace std;

class ArbolExpansionMinimo{
	protected :
		int ** T;
		int longMin;
		private:
		int n;
		int INFINITO;
	public:
		int ** OT(){ return T;} // Arbol de expansi�n
		int OlongMin(){ return longMin;} // peso del �rbol expansi�n
		ArbolExpansionMinimo(GrafoMatriz g); // constructor
		int arbolExpansionPrim(GrafoMatriz g); // algoritmo de Prim
};

ArbolExpansionMinimo::ArbolExpansionMinimo(GrafoMatriz g){
	n = g.OnumeroDeVertices();
	INFINITO = 0xFFFF;
	longMin = 0;
	typedef int * pint;
	T = new pint [n];

	for ( int i = 0 ; i < n; i++){
		T[i] = new int [n];
		for(int j = 0; j < n; j++)
		T[i][j] = INFINITO;
	}
}

int ArbolExpansionMinimo::arbolExpansionPrim(GrafoMatriz g){
	int menor;
	int i, j, z;
	int *coste = new int [n];
	int * masCerca = new int [n];
	bool *W = new bool [n];
	for (i = 0; i < n; i++)
	W[i] = false; // conjunto vac�o
	W[0] = true; //se parte del v�rtice 0
	// inicialmente, coste[i] es la arista (0,i)
	for (i = 1; i < n; i++){
		coste[i] = g.Ovalor(0, i);
		masCerca[i] = 0;
	}
	coste[0] = INFINITO;
	for (i = 1; i < n; i++){ // busca v�rtice z de V-W mas cercano,
		// de menor longitud de arista, a alg�n v�rtice de W
		menor = coste[1];
		z = 1;
	for (j = 2; j < n; j++)
	if (coste[j] < menor){
		menor = coste[j];
		z = j;
	}
	longMin += menor;
	// se escribe el arco incorporado al �rbol de expansi�n
	
	
	cout << g.Overtice(masCerca[z]).OnomVertice() << "(" << g.Overtice(masCerca[z]).OnumVertice() << ")"  << " ->" << g.Overtice(z).OnomVertice() << "(" << g.Overtice(z).OnumVertice() << ")";
	//cout << "V" << masCerca[z] << " -> V" << z;
	W[z] = true; // v�rtice z se a�ade al conjunto W
	T[masCerca[z]][z] = T[z][masCerca[z]] = coste[z];
	coste[z] = INFINITO;
	// debido a la incorporaci�n de z,
	// se ajusta coste[] para el resto de v�rtices
	for (j = 1; j < n; j++)
		if ((g.Ovalor(z, j) < coste[j]) && !W[j]){
			coste[j] = g.Ovalor(z, j);
			masCerca[j] = z;
		}
	}
	
	return longMin;
}
		
	
