#include <iostream>
#include "Vertice.h"

using namespace std;
typedef int * pint;

class GrafoMatriz{
	protected:
		int maxVerts; // m�ximo numero de v�rtices
		int numVerts; // n�mero de v�rtices actual
		Vertice* verts; // array de v�rtices
		int **matAd; // matriz de adyacencia
	public:
	// m�todos p�blicos de la clase GrafoMatriz
		GrafoMatriz(int mx);
		GrafoMatriz();
		void nuevoVertice (string nom);
		int OnumeroDeVertices();
		void PnumeroDeVertices(int n);
		int numVertice(string v);
		void nuevoArco(string a, string b);
		void nuevoArco(int va, int vb);
		void nuevoArco(int va, int vb, int valor);
		bool adyacente(string a, string b);
		bool adyacente(int va, int vb);
		int Ovalor(string a, string b);
		int Ovalor( int va, int vb);
		void Pvalor(int va, int vb, int v);
		void Pvalor( string a, string b, int v);
		void VerMatriz();
		
		Vertice Overtice(int va);
		void Pvertice( int va, Vertice vert);
		
	private:
	// m�todos privados de la clase GrafoMariz
};


GrafoMatriz::GrafoMatriz(int mx){
	maxVerts = mx;
	verts = new Vertice[mx] ; // vector de v�rtices
	matAd = new pint[mx]; // vector de punteros
	numVerts = 0;
	
	for (int i = 0; i < mx; i++)
		matAd[i] = new int [mx]; // matriz de adyacencia
		
	for (int x=0; x<maxVerts ; x++){
		for (int y=0; y<maxVerts ; y++){
			matAd[x][y] = 0xFFFF;
		}
	}

}

GrafoMatriz::GrafoMatriz(){
	maxVerts = 1 ;
	GrafoMatriz(maxVerts);
}

int GrafoMatriz::OnumeroDeVertices(){
	return numVerts;
}

void GrafoMatriz::PnumeroDeVertices(int n){
	numVerts = n;
}

void GrafoMatriz::nuevoVertice (string nom){
	bool esta = numVertice(nom) >= 0;
	if (!esta){
		Vertice v = Vertice(nom, numVerts);
		verts[numVerts++] = v; // se asigna a la lista.
		// No se comprueba que sobrepase el m�ximo
	}
}

int GrafoMatriz::numVertice(string v){
	int i;
	bool encontrado = false;
	for ( i = 0; (i < numVerts) && !encontrado;){
		encontrado = verts[i].igual(v);
		if (!encontrado) 
			i++ ;
	}
	
	return (i < numVerts) ? i : -1 ;
}

void GrafoMatriz::nuevoArco(string a, string b){
	int va, vb;
	va = numVertice(a);
	vb = numVertice(b);
	if (va < 0 || vb < 0) throw "V�rtice no existe";
		matAd[va][vb] = 1;
}

void GrafoMatriz::nuevoArco(int va, int vb){
	if (va < 0 || vb < 0 || va > numVerts || vb > numVerts)
		throw "V�rtice no existe";
	matAd[va][vb] = 1;
}

void GrafoMatriz::nuevoArco(int va, int vb, int valor){
	if (va < 0 || vb < 0 || va > numVerts || vb > numVerts)
		throw "V�rtice no existe";
	
	matAd[va][vb] = valor;
}

bool GrafoMatriz::adyacente(string a, string b){
	int va, vb;
	va = numVertice(a);
	vb = numVertice(b);
	
	if (va < 0 || vb < 0) throw "V�rtice no existe";
		return matAd[va][vb] == 1;
}

bool GrafoMatriz::adyacente(int va, int vb){
	if (va < 0 || vb < 0 || va >= numVerts || vb >= numVerts)
		throw "V�rtice no existe";
	
	return matAd[va][vb] == 1;
}

int GrafoMatriz::Ovalor(string a, string b){
	int va, vb;
	va = numVertice(a);
	vb = numVertice(b);
	
	if (va < 0 || vb < 0) 
		throw "V�rtice no existe";

	return matAd[va][vb];
}
int GrafoMatriz::Ovalor( int va, int vb){
	if (va < 0 || vb < 0 || va >= numVerts || vb >= numVerts)
		throw "V�rtice no existe";
	
	return matAd[va][vb];
}


void GrafoMatriz::Pvalor(int va, int vb, int v){
	if (va < 0 || vb < 0 || va >= numVerts || vb >= numVerts)
		throw "V�rtice no existe";
	matAd[va][vb] = v;
}

void GrafoMatriz::Pvalor( string a, string b, int v){
	int va, vb;
	va = numVertice(a);
	vb = numVertice(b);
	
	if (va < 0 || vb < 0) 
		throw "V�rtice no existe";
	
	matAd[va][vb] = v;
}

Vertice GrafoMatriz::Overtice(int va){
	if (va < 0 || va >= numVerts)
		throw "V�rtice no existe";
	else 
		return verts[va];
}

void GrafoMatriz::Pvertice( int va, Vertice vert){
	if (va < 0 || va >= numVerts)
		throw "V�rtice no existe";
	else 
		verts[va] = vert;
}

void GrafoMatriz::VerMatriz(){
		cout << "maxVerts: " << maxVerts << endl;
		cout << "numVerts: " << numVerts << endl; 
		
		for (int x=0 ; x<numVerts ; x++){
			cout << "\t" << Overtice(x).OnomVertice() << "(" << Overtice(x).OnumVertice() << ")";
		}
		cout << endl;
		for (int x=0 ; x<numVerts ; x++){
			cout << Overtice(x).OnomVertice() << "(" << Overtice(x).OnumVertice() << ")";
			for (int y=0 ; y<numVerts ; y++){
				cout << "\t" << Ovalor(x,y);
			}
			cout << endl;
		}
		
		
}




