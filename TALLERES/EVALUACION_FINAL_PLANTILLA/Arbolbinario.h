#include "Nodo.h"


template <class T>
class Arbolbinario{
	protected:
		Nodo<T> *raiz;
		int auxX;
	public:
		ArbolBinario(){
			raiz = NULL;
			auxX=0;
		}
		
		ArbolBinario(Nodo<T> *r){
			raiz = r;
		}
		
		void Praiz( Nodo<T> *r){
			raiz = r;
		}
		
		Nodo<T> * Oraiz(){
			return raiz;
		}
		
		Nodo<T> raizArbol(){
			if(raiz)
				return *raiz;
			else
				throw " arbol vacio";
		}

		bool esVacio(){
			return raiz == NULL;
		}
	
		Nodo<T> * hijoIzdo(){
			if(raiz)
				return raiz->subArbolIzdo();
			else
				throw " arbol vacio";
		}
		
		Nodo<T> * hijoDcho(){
			if(raiz)
				return raiz->subArbolDcho();
			else
				throw " arbol vacio";
		}
		
		Nodo<T>* nuevoArbol(Nodo<T>* ramaIzqda, T dato, Nodo<T>* ramaDrcha){
			return new Nodo<T>(dato,ramaIzqda, ramaDrcha);
		}

void verArbol(){
	verArbol(raiz,0);
}

		
	void preorden(){
		preorden(raiz);
	}
		
	void inorden(){
		inorden(raiz);
	}	
	
	void postorden(){
		postorden(raiz);
	}
	
	void preorden(Nodo<T> *r){
		if (r != NULL){
			r->visitar();
			preorden (r->subArbolIzdo());
			preorden (r->subArbolDcho());
		}
	}	
	
	void inorden(Nodo<T> *r){
		if (r != NULL){
			inorden (r->subArbolIzdo());
			r->visitar();
			inorden (r->subArbolDcho());
		}
	}

void postorden(Nodo<T> *r)
{
	if (r != NULL){
		postorden (r->subArbolIzdo());
		postorden (r->subArbolDcho());
		r->visitar();
	}
}

int altura (){
	return altura(raiz);
}

int altura(Nodo<T> *raiz){
	if (raiz == NULL)
		return 0 ;
	else{
		int alturaIz = altura (raiz->subArbolIzdo());
		int alturaDr = altura (raiz->subArbolDcho());
		if (alturaIz > alturaDr)
			return alturaIz + 1;
		else
			return alturaDr + 1;
	}
}

int numNodos(){
	return numNodos(raiz);
}

int numNodos(Nodo<T> *raiz){
	if (raiz == NULL)
		return 0;
	else
		return 1 + numNodos(raiz->subArbolIzdo()) + numNodos(raiz->subArbolDcho());
}


};

