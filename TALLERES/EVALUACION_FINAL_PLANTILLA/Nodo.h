#include <iostream>

template <class T>
class Nodo{
	protected:
		T dato;
		Nodo *izdo;
		Nodo *dcho;
	public:
		Nodo(T valor){
			dato = valor;
			izdo = dcho = NULL;
		}

		Nodo(T valor, Nodo* ramaIzdo, Nodo* ramaDcho){
			dato = valor;
			izdo = ramaIzdo;
			dcho = ramaDcho;
		}
		// operaciones de acceso
		T valorNodo(){ return dato; }
		Nodo* subArbolIzdo(){ return izdo; }
		Nodo* subArbolDcho(){ return dcho; }
		// operaciones de modificación
		void nuevoValor(T d){ dato = d; }
		void ramaIzdo(Nodo* n){ izdo = n; }
		void ramaDcho(Nodo* n){ dcho = n; }
		
		void visitar()
		{
		cout << dato ;
		}
	};

