#include <iostream>
using namespace std;
#include "Nodo.h"


typedef char Dato;

class Lista{
protected:
     Nodo*primero;
		 
public:
		 	Lista(){ };
		 	void crearLista();
		 	void insertarCabezaLista(Dato entrada);
		 	void visualizar();
		 	Nodo* ultimo();
		 	Nodo*buscarLista(Dato destino);
		 	void insertarLista(Nodo*anterior, Dato entrada);
		 	void insertarUltimo(Dato entrada);
		 	void eliminar(Dato entrada);
	
};

void Lista::crearLista(){
	primero=0;
	
}
void Lista::insertarCabezaLista(Dato entrada){
	Nodo*nuevo;
	nuevo=new Nodo(entrada);
   nuevo->ponerEnlace(primero);
	primero=nuevo;
	

}
Nodo* Lista::ultimo(){
	
	Nodo*p=primero;
	if(p == NULL) throw "Error,lista vacia";
	while (p->enlaceNodo() !=NULL)
	p=p->enlaceNodo();
	
	return p;
	
}
Nodo* Lista::buscarLista(Dato destino){
	Nodo* indice;
	for(indice= primero;indice !=NULL;indice=indice->enlaceNodo())
	if(destino==indice->datoNodo())
	return indice;
	return NULL;	
}

void Lista::insertarUltimo(Dato entrada){
	Nodo*ultimo=this->ultimo();
	
	Nodo* nodo=new Nodo(entrada);
	ultimo->ponerEnlace(nodo);
	
}
void Lista::visualizar(){
	Nodo*nodo;
	nodo=primero;
	
	while(nodo !=NULL){
		cout<<nodo->datoNodo()<<endl;
		nodo=nodo->enlaceNodo();
	}
}
void Lista::eliminar(Dato entrada){
	Nodo* actual,* anterior;
	bool encontrado;
	actual = primero;
	anterior = NULL;
	encontrado = false;
	while((actual != NULL) && !encontrado){
		encontrado = (actual->datoNodo() == entrada);
		if(!encontrado){
			anterior = actual;
			actual = actual->enlaceNodo();
	
			}
		}
		if (actual != NULL){
			if(actual==primero){
				primero = actual->enlaceNodo();
			}
			
		else{
		anterior->ponerEnlace(actual->enlaceNodo());	
		}
		delete actual;
	}
	
}
