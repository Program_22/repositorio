#include "NodoDoble.h"
using namespace std;

typedef char Dato;

class ListaDoble{
	private:
		NodoDoble* cabeza=NULL;
		
		public:
		void insertarCabeza(Dato entrada);
		void insertarDespues(NodoDoble* anterior,Dato entrada);
		void eliminar(Dato entrada);
		void visualizar();
		NodoDoble* buscarLista(Dato destino);
		void visualizarAdelante(NodoDoble* nodo);
		void visualizarAtras(NodoDoble* nodo);
		NodoDoble* ultimo();
		void insertarUltimo(Dato entrada);

};

void ListaDoble::insertarCabeza(Dato entrada){
	
	NodoDoble* nuevo;
	nuevo=new NodoDoble(entrada);
	nuevo->ponerAdelante(cabeza);
	
	if(cabeza !=NULL)
	cabeza->ponerAtras(nuevo);
	
	cabeza=nuevo;
}

void ListaDoble::visualizar(){
	NodoDoble* nodo;
	nodo= cabeza;
	while(nodo !=NULL){
		cout << nodo->datoNodo() << endl;
		nodo=nodo->adelanteNodo();
	}
	
}
void ListaDoble::insertarDespues(NodoDoble* anterior,Dato entrada){
NodoDoble* nuevo;
nuevo= new NodoDoble(entrada);
nuevo-> ponerAdelante(anterior -> adelanteNodo()); 
	
if(anterior ->adelanteNodo() !=NULL)
anterior ->adelanteNodo() -> ponerAtras(nuevo);

 anterior->ponerAdelante(nuevo);
 nuevo-> ponerAtras(anterior);	
}

NodoDoble* ListaDoble::buscarLista(Dato destino){
	NodoDoble* indice;
	for(indice = cabeza; indice !=NULL; indice=indice->adelanteNodo())
	if(destino==indice->datoNodo())
	return indice;
	return NULL;
	
}

