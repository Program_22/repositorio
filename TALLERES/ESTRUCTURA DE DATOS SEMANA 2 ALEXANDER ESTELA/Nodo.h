#include <iostream>
typedef char Dato;

class Nodo
{
	Dato dato;
	Nodo*enlace;
	
public:
	Nodo(Dato t){
	dato=t;
	enlace=0;//0 es el puntero NULL c++
		
	}
	Nodo(Dato p, Nodo*n)//crea el nodo y lo enlaza a n
	{
		dato=p;
		enlace=n;
		
	}
	Dato datoNodo() const
	{
	return dato;
		}
	Nodo*enlaceNodo() const
	{
		return enlace;
		
	}
	void ponerEnlace(Nodo*sgte){

	enlace=sgte;
		}
};
