#include <iostream>
using namespace std;
#include "Lista.h"

typedef string Dato;


int main(int argc, char** argv) {
	Lista lista;
	
	lista.crearLista();
	lista.insertarInicio("Juan");
	lista.insertarUltimo("jose");
    lista.insertarUltimo("Pedro");
    lista.insertarInicio("Alberto");
    lista.insertarFinal("Juan");
	lista.insertarUltimo("Francisco");
	lista.insertarUltimo("Pedro");


	lista.eliminar("Juan"); 

	
	lista.visualizar();
	
	return 0;
}



