#include <iostream>
#include "Lista.h"
typedef int Dato;

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {
	
	Lista lista;
	
	lista.crearLista();
	lista.insertarCabezaLista(1);
	lista.insertarUltimo(2);

	lista.insertarUltimo(3);
	lista.visualizar();
	
	return 0;
}
