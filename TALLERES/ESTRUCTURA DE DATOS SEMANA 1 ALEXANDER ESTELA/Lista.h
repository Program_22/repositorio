#include <iostream>
#include "Nodo.h"
using namespace std;

typedef int Dato;

class Lista{
	protected:
		Nodo*primero;
		 
		 public:
		 	Lista(){};
		 	void crearLista();
		 	void insertarCabezaLista(Dato entrada);
		 	void visualizar();
		 	Nodo* ultimo();
		 	Nodo*buscarLista(Dato destino);
		 	void insertarLista(Nodo*anterior, Dato entrada);
		 	void insertarUltimo(Dato entrada);
		 	void eliminar(Dato entrada);
	
};

void Lista::crearLista(){
	primero=0;
	
}
void Lista::insertarCabezaLista(Dato entrada){
	Nodo*nuevo;
	nuevo=new Nodo(entrada);
	
	primero=nuevo;
	

}
Nodo* Lista::ultimo(){
	
	Nodo*p=primero;
	if(p == NULL) throw "Error,lista vacia";
	while (p->enlaceNodo() !=NULL)
	p=p->enlaceNodo();
	
	return p;
	
}

void Lista::insertarUltimo(Dato entrada){
	Nodo*ultimo=this->ultimo();
	
	Nodo* nodo=new Nodo(entrada);
	ultimo->ponerEnlace(nodo);
	
}
void Lista::visualizar(){
	Nodo*nodo;
	nodo=primero;
	
	while(nodo !=NULL){
		cout<<nodo->datoNodo()<<endl;
		nodo=nodo->enlaceNodo();
	}
}
