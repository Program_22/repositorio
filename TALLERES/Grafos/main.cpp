#include <iostream>
#include "Vertice.h"



using namespace std;

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

void MatrizAdyacencia(int NumeroVertices){
	Vertice v[NumeroVertices];
	
	int matrizAd[NumeroVertices][NumeroVertices];
	
	cout << "Ingreso de Nodos:" << endl;
	cout << "======= == ======" << endl;
	for(int x= 0; x<NumeroVertices;x++){
		string nombreNodo;
		cout << "Ingrese el nombre del Nodo " << x << ":";
		cin >> nombreNodo;
		v[x]=nombreNodo;	
	}

	cout << "Ingreso de Relaciones:" << endl;
	cout << "======= == ===========" << endl;

	
	for(int x= 0; x<NumeroVertices;x++){
		for(int y= 0; y<NumeroVertices;y++){
			if (v[x].igual(v[y])){
				matrizAd[x][y]=0;
			}
			else{
				char ingresoTeclado;
				matrizAd[x][y]=0;
				cout << "El nodo: " << v[x].OnomVertice() << " esta relacionado con el nodo:" <<v[y].OnomVertice() << " (s/n)";
				cin >> ingresoTeclado;
				if (ingresoTeclado=='s')
					matrizAd[x][y]=1;
			}
			
		}
	}
	cout << endl << endl;
	cout << "Mostrar Matriz de Adyacencia:" << endl;
	cout << "======= ====== == ===========" << endl << endl;	
	
		
		for(int x= 0; x<NumeroVertices;x++){
			cout << "\t" << v[x].OnomVertice();
		}
		cout << endl;
		
		
		for(int x= 0; x<NumeroVertices;x++){
			cout << v[x].OnomVertice() << "\t";
			for(int y= 0; y<NumeroVertices;y++){
				cout << matrizAd[x][y] << "\t" ;
			}
			cout << endl;
			
		}

}

int main(int argc, char** argv) {
	int numeroVertices;
	cout << "Ingresar el numero de vertices:" << endl;
	cin >> numeroVertices;
	
	
	MatrizAdyacencia(numeroVertices);
	
	
	return 0;
}
