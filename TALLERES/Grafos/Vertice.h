#include <iostream>
using namespace std;

class Vertice{
	protected:
		string nombre;
		int numVertice;
	public:
		Vertice () {}
		Vertice(string x){ // inicializa el nombre y pone el n�mero de v�rtice e -1
			nombre = x;
			numVertice = -1;
		}
		Vertice(string x, int n){ // inicializa el nombre y el n�mero de v�rtice
			nombre = x;
			numVertice = n;
		}
		string OnomVertice(){ // retorna el nombre del v�rtice
			return nombre;
		}
		void PnomVertice(string nom){ // cambia el nombre del v�rtice
			nombre = nom;
		}
		bool igual(Vertice n){ // decide entre la igualdad de nombres
			return nombre == n.nombre;
		}
		void PnumVertice(int n){ // cambia el n�mero del v�rtice
			numVertice = n;
		}
		int OnumVertice(){ // retorna el n�mero del v�rtice
			return numVertice;
		}
		

		
		
	};

