#include <iostream>
typedef char Dato;

class Nodo{
	protected:
		Dato dato;
		Nodo* enlace;
	public:
		Nodo(Dato t){
			dato = t;
			enlace = 0; 
		}
		Nodo(Dato p, Nodo* n){
			dato = p;
			enlace = n;
		}
		Dato datoNodo() const{
			return dato;
		}
			Nodo* enlaceNodo() const{
			return enlace;
		}
		void ponerEnlace(Nodo* sgte){
			enlace = sgte;
		}
};

