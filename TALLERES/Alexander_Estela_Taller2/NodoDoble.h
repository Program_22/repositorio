typedef char Dato;

class NodoDoble{
	protected:
	Dato dato;
	
	NodoDoble* atras;
	NodoDoble* adelante;
	
	public:
		
		NodoDoble(Dato d){
		dato= d;
		
		atras=adelante=NULL;			
		}
		
		Dato datoNodo() const{
		return dato;
		}
	NodoDoble* adelanteNodo() const{
	return adelante;
	}	
		NodoDoble* atrasNodo() const{
			return atras;
			}
			void ponerAdelante(NodoDoble* a) {
			adelante= a;
				
			}
			void ponerAtras(NodoDoble* b){
				atras=b;
			}
};
