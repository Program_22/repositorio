#include <iostream>
#include "ListaDoble.h"

using namespace std;

typedef char Dato;

int main(int argc, char** argv) {
	
	ListaDoble lista;
	
lista.insertarCabeza('e');
lista.insertarCabeza('d');
lista.insertarCabeza('b');
lista.insertarCabeza('a');

NodoDoble* nodo=lista.buscarLista('b');

if(nodo !=NULL)
lista.insertarDespues(nodo,'c');

	lista.visualizar();
	
	return 0;
	
}
