#include "NodoDoble.h"
typedef char Dato;
class ListaDoble{
	private:
		NodoDoble* cabeza=NULL;
		protected:
			public:
				void insertarCabezaLista(Dato entrada);
				void insertaDespues(NodoDoble* anterior,Dato entrada);
				void eliminar(Dato entrada);
				void visualizar();
				NodoDoble* buscarLista(Dato destino);
				void visualizarAdelante(NodoDoble* nodo);
					void visualizarAtras(NodoDoble* nodo);
	NodoDoble* ultimo();
	void insertarUltimo(Dato entrada);
};
void ListaDoble::insertarCabezaLista(Dato entrada){
	NodoDoble* nuevo;
	nuevo=new NodoDoble(entrada);
	nuevo->ponerAdelante(cabeza);
	if(cabeza !=NULL)
	cabeza->ponerAtras(nuevo);
	cabeza=nuevo;
}
void ListaDoble::insertaDespues(NodoDoble* anterior,Dato entrada){
NodoDoble* nuevo;
nuevo=new NodoDoble(entrada);
nuevo->ponerAdelante(anterior -> adelanteNodo());

if(anterior->adelanteNodo() !=NULL)
anterior -> adelanteNodo() -> ponerAtras(nuevo);

anterior -> ponerAdelante(nuevo);
nuevo -> ponerAtras(anterior);	
}
void ListaDoble::eliminar(Dato entrada){
	NodoDoble* actual;
	bool encontrado=false;
	actual= cabeza;
	


while((actual !=NULL) && (!encontrado)){
	encontrado=(actual -> datoNodo()==entrada);
	if(!encontrado)
	actual=actual -> adelanteNodo();
}
if(actual !=NULL){
	if(actual !=NULL){
		cabeza=actual->adelanteNodo();
		if(actual -> adelanteNodo() !=NULL)
		actual -> adelanteNodo() -> ponerAtras(NULL);
		
	}
	else if(actual ->adelanteNodo() !=NULL){
		actual->atrasNodo()->ponerAdelante(actual->adelanteNodo());
		actual->adelanteNodo()->ponerAtras(actual->atrasNodo());
	}
	else
	actual->atrasNodo()->ponerAdelante(NULL);
}
}
void ListaDoble::visualizar(){
	NodoDoble* n;
	n=cabeza;
	while (n !=NULL){
		cout<<n->datoNodo()<<endl;
		n=n->adelanteNodo();
	}
}
void ListaDoble::visualizarAdelante(NodoDoble* nodo){
	NodoDoble* n;
	n=nodo;
	while(n !=NULL){
		cout<<n->datoNodo()<<endl;
		n=n->adelanteNodo();
	}
	
}
void ListaDoble::visualizarAtras(NodoDoble* nodo){
	NodoDoble* n;
	n=nodo;
	while(n !=NULL){
		cout<<n->datoNodo()<<endl;
		n=n->atrasNodo();
		
	}
}
NodoDoble* ListaDoble::buscarLista(Dato destino){
	NodoDoble* indice;
	for(indice=cabeza;indice !=NULL;indice=indice->adelanteNodo())
	if(destino==indice->datoNodo())
	return indice;
	return NULL;
	
}
NodoDoble* ListaDoble::ultimo(){
	NodoDoble* p=cabeza;
	if(p==NULL) throw"Error,lista vacia";
	while(p->adelanteNodo() !=NULL) p=p->adelanteNodo();
	return p;
	
}
void ListaDoble::insertarUltimo(Dato entrada){
	NodoDoble* ultimo=this ->ultimo();
	NodoDoble* nuevoNodo=new NodoDoble(entrada);
	ultimo->ponerAdelante(nuevoNodo);
	nuevoNodo->ponerAtras(ultimo);
	
}


