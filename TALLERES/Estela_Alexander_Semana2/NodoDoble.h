typedef char Dato;
 class NodoDoble
 {
 	protected:
 		Dato dato;
 	    NodoDoble* adelante;
 	    NodoDoble* atras;
 	public:
 		NodoDoble(Dato t){
 			dato=t;
 			adelante=atras=NULL;
 			
		 }
		 Dato datoNodo() const{
		 return dato;
		 }
		 NodoDoble* adelanteNodo() const{
		 return adelante;
		 }
		  NodoDoble* atrasNodo() const{
		 return atras;
		 }
		 	  NodoDoble* ponerAdelante(NodoDoble* a){
		 adelante=a;
		 }
		 	  NodoDoble* ponerAtras(NodoDoble* a){
	atras=a;
		 }
 };
 
