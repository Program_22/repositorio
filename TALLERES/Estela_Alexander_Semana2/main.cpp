#include <iostream>
using namespace std;
#include "ListaDoble.h"
typedef char Dato;

int main(int argc, char** argv) {
	
	ListaDoble listaDb;
	
	listaDb.insertarCabezaLista('a');
	listaDb.insertarUltimo('b');
	listaDb.insertarUltimo('c');
    listaDb.insertarUltimo('d');
    listaDb.insertarUltimo('e');
	listaDb.insertarUltimo('f');
	listaDb.insertarUltimo('g');
	listaDb.eliminar('a');
	NodoDoble* nodo=listaDb.buscarLista('d');
						
	if(nodo=NULL)
	cout<<"Nodo no encontrado."<<endl;
	else 
	cout<<"Nodo encontrado-valor del Nodo:"<<endl;
	listaDb.insertaDespues(nodo, 'z');
	cout<<"Nodos:"<<endl;
	listaDb.visualizar();
	cout<<"Nodos Adelante:"<<endl;
	listaDb.visualizarAdelante(nodo);
	cout<<"Nodos Atras:"<< endl;
	listaDb.visualizarAtras(nodo);
	return 0;
}
