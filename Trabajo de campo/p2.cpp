#include <string.h>
#include <iostream>
#include <cstdlib>
#include <conio.h>
using namespace std;
#define ruta "articulo.dat"
struct articulo {
char descripcion[20];
int code;
double precio;
};
typedef struct articulo arti;
struct nodo{
	articulo dato;
	struct nodo *izq, *der;
};
typedef struct nodo *ABB;
arti busdato;
ABB crearNodo(arti at)                
{
	ABB nuevoNodo = new(struct nodo);
	//strcpy(nuevoNodo->nom, x);
	nuevoNodo->dato=at;
	nuevoNodo->izq = NULL;
	nuevoNodo->der = NULL;
	
	return nuevoNodo;
}
void insertar(ABB &arbol, arti at)
{
	if(arbol==NULL)
	{
		arbol = crearNodo(at);
	}
	else if(strcmp(at.descripcion ,arbol->dato.descripcion)<0)
	   insertar(arbol->izq, at);
	else if(strcmp(at.descripcion , arbol->dato.descripcion)>0)
		insertar(arbol->der, at);
	else cout<<"dato existente";
	
}

void preOrden(ABB arbol) //RID
{
	if(arbol!=NULL)
	{
		cout << arbol->dato.descripcion <<"->"<<arbol->dato.code<<"->"<<arbol->dato.precio<<endl;
		preOrden(arbol->izq);
		preOrden(arbol->der);
	}
}

void enOrden(ABB arbol)  //IRD
{
	if(arbol!=NULL)
	{
		enOrden(arbol->izq);
	cout << arbol->dato.descripcion <<"->"<<arbol->dato.code<<"->"<<arbol->dato.precio<<endl;
		enOrden(arbol->der);
	}
}

void postOrden(ABB arbol) //IDR
{
	if(arbol!=NULL)
	{
		postOrden(arbol->izq);
		postOrden(arbol->der);
	cout << arbol->dato.descripcion <<"->"<<arbol->dato.code<<"->"<<arbol->dato.precio<<endl;
	}
}

void verArbol(ABB arbol, int n)
{
	if(arbol==NULL)
		return;
	verArbol(arbol->der, n+1);
	
	for(int i=0; i<n; i++)
		cout<<"   ";
	
	cout << arbol->dato.descripcion <<"->"<<arbol->dato.code<<"->"<<arbol->dato.precio<<endl;
	
	verArbol(arbol->izq, n+1);
}
void suma(ABB arbol,double *sm){
	if(arbol!=NULL){
		
			*sm=*sm+ arbol->dato.precio;
	
		
		suma(arbol->izq, sm);
		suma(arbol->der, sm);
		
	}

}

void  busca(ABB arbol,char const  *b){
	if(arbol!=NULL){
		if(strcmp(b, arbol->dato.descripcion)==0) 
		{ 
			busdato=arbol->dato;
			
		}
		
		busca(arbol->izq, b);
		busca(arbol->der, b);
		
	}
	
}	
// anulacion de nodos
	ABB anula(ABB hoja , char const *dat){
		// num es el numero a buscar en el arbol
		if(strcmp(hoja->dato.descripcion,dat)==0){
			ABB p,p2;
			if(hoja==NULL){//en caso de que la hoja sea el ultimo nodo
				free(hoja);
				return NULL;
			}
			else if(hoja->izq==NULL){//si la parte izquierda es nula
				p=hoja->der; //nos quedamos con la parte derecha
				free(hoja);
				return p;
			}
			else if(hoja->der==NULL){
				p=hoja->izq;
				free(hoja);
				return p;
			}  
			else {//si en ambas direcciones no es nula recorrer los nodos
				p=hoja->der;
				
				free(hoja);
				return p;
			}
		} //si no existe el valor
		else{
			if(strcmp(dat,hoja->dato.descripcion)<0)
				hoja->izq=anula(hoja->izq,dat);   
			else 
				hoja->der=anula(hoja->der,dat);
			return hoja;     
		}
		
	}

int main(void){
		/*
Pregunta 02:
Crear un �rbol binario que permita almacenar los datos de un Articulo :
C�digo (entero)
Descripci�n cadena)
Precio (real).
Se pide realizar:
a) El ingreso de art�culos , el c�digo debe ser �nico
b) Listado de Articulos
c) .Muestre el articulo o art�culos de mayor precio
d) Anulaci�n, Ingrese un c�digo de un art�culo si existe proceda a la anulaci�n de lo contrario muestre c�digo no existe.
e) Grabar la informaci�n del �rbol en un archivo y cuando se activa el programa cargar los datos del archivo
*/
	ABB arbol=NULL;
	articulo x;	
	int n;
	int a=0;
    char cad[20];
   	double total=0;
	int opcion=0;
	do{
cout<<"\n:::.Bienvenidos al menu de Arbol.:::\n";
cout<<"\n1.Ingresar datos: ";
cout<<"\n2.Mostrar Recorrido en PostOrden: ";
cout<<"\n3.Mostrar Recorrido en PreOrden: ";
cout<<"\n4.Mostrar Recorrido en InOrden: ";
cout<<"\n5.Buscar";
cout<<"\n6.Eliminacion de un archivo: ";
cout<<"\n7.Suma";
cout<<"\n8.Salir\n";
cout<<"Escoja una opcion-----"<<"->";
cin>>opcion;
switch(opcion){
	case 1:
		cout<<"Ingresa la cantidad de nodos"<<endl;
		cin>>n;
	for(int i=1;i<=n; i++){
		cout<<"Articulo:"<<i;
		cout<<"\nDescripcion:";
		fflush (stdin);
		gets(x.descripcion);
		cout<<"Codigo:";
		cin>>x.code;
		cout<<"Precio :";
		cin>>x.precio;
		insertar(arbol,x); ;
	}
 

	break;
		case 2:
		cout<<"Recorrido PostOrden";
		cout <<"\n\n PostOrden : ";postOrden(arbol);
		break;
		case 3:
		cout<<"Recorrido PreOrden";
		cout <<"\n\n PreOrden :  ";preOrden(arbol);
		break;
			case 4:
		cout<<"Recorrido InOrden";
		cout <<"\n\nInOrden : ";preOrden(arbol);
		break;
		case 5:
		cout<<"\n dato a buscar "; fflush(stdin); gets(cad);
	strcpy(busdato.descripcion,"");
	busca(arbol,cad);
	if(strlen(busdato.descripcion)>0)
	cout << "\n existe a  "<<busdato.precio<<endl;
	else
		cout << "\n no existe a  "<<endl;	
		break;
	case 6:
	cout<<"\n dato a anular "; fflush(stdin); gets(cad);
	anula(arbol, cad);
	break;
	case 7:
		suma(arbol,&total);
	cout << "\n\n total :  "<<total;
	cout << endl << endl;
	
	system("pause");
			case 9:
		cout<<"Programa finalizado";
		break;
		default:
			cout<<"Opcion no valida";
}

}while(opcion!=9);
return 0;
	}
