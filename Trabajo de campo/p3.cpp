#include <string.h>
#include <iostream>
#include <cstdlib>
#include <conio.h>
using namespace std;
#define ruta "ciudad.dat"
struct distrito {
  int code;
  char nom[30];
  int infectado;
}rg;
struct nodo{
  distrito dato;
  struct nodo *next, *ant;
}*inicio=NULL, *ult, *p;
FILE*fp; 
void adicion(distrito ep){
  p=new nodo;
  p->dato=ep;
  if(inicio==NULL){
    inicio=ult=p;
    p->next=NULL;
    p->ant=NULL;
  }else{
    p->next=NULL;
    p->ant=ult;
    ult->next=p;
    ult=p;
  }
}
int genera(){
  int nro;
  if(inicio==NULL)
    nro=001;
  else 
    nro=ult->dato.code+1;
  return nro;
}
void ingreso(){
  char res;
  do{
     rg.code=genera();
     cout<<"\nCodigo: "<<rg.code;
     cout<<"\nDistrito: ";fflush (stdin); gets(rg.nom);
     cout<<"cantidad de contagiados: "; cin>>rg.infectado;
     adicion(rg);
     cout<<"\nOtro dato s/n: "; cin>>res;
  }while(res=='s');
}
void listado(){  
  double suma=0;
  int conta=0;
  double prom=0;
  cout<<"\nCodigo\tNombre\tContagio\n";
  p=inicio; //De Izquierda a derechar.
  while(p!=NULL){
    rg=p->dato; //Asignando el dato.
    cout<<rg.code<<"\t"<<rg.nom<<"\t"<<rg.infectado<<endl;
    suma=suma+rg.infectado;
    conta+=1;
    prom=suma/conta;
    p=p->next;
  }
  cout<<"\nContagiados Promedio: "<<prom<<endl;
}
void mayor(){
  double max=0;
  p=inicio;
  while(p!=NULL){
    if(p->dato.infectado>max)
      max=p->dato.infectado;
      p=p->next;
    }
  cout<<"\nEl mayor distrito: "<<max<<endl;
}
void carga(){
  //Leer el archivo de datos y pasarlo a la lista.
  fp=fopen(ruta,"rb");
  if(fp==NULL)
    return;
    fread(&rg,sizeof(rg),1,fp);
  while(!feof(fp)){
    adicion(rg);
    fread(&rg,sizeof(rg),1,fp);
  }
  fclose(fp);
}  
void grabar(){
  //Todo lo que esta en la lista pasarlo al archivo.
  fp=fopen(ruta,"wb");
  p=inicio;
  while(p!=NULL){
    rg=p->dato; //Extraer el dato del nodo.
    fwrite(&rg,sizeof(rg),1,fp);
    p=p->next;          
  }
  fclose(fp);
}
void consulta(){
  int nd;
  cout<<"\n\nCodigo a buscar: "; cin>>nd;
  p=inicio;
  while(p!=NULL){
	  if(p->dato.code==nd){
		  
		  cout<<"\nNombre: "<<p->dato.nom;
		  cout<<"\ninfectados: "<<p->dato.infectado;  
		 break; 
	  }
	  p=p->next;
  }
  if(p==NULL){
	  cout<<"\n\nNo existe Codigo  "  ;
  }
 
}
void anula(int cod){
  p=inicio;
  if(inicio->dato.code==cod){
    inicio=inicio->next;
    free(p); return;
  }   
  while (p!=NULL && p->dato.code!=cod){
    p=p->next;
  }
  if(p!=NULL){
    if(p==ult){
      ult=ult->ant;
      ult->next=NULL;
    }else{
      struct nodo *w, *z;
      w=p->ant;
      z=p->next;
      w->next=z;
      z->ant=w;
    }
    free(p);
  } 
}
void cambia(){
  
}//fin de cambia
main(){
	
	/*
Crear un �rbol que almacene el nombre del distrito y la cantidad de pacientes infectados
Por corona virus 
a)	Realizar el ingreso de datos los distritos deben ser �nicos
b)	Muestre la lista de datos ordenados por nombre, con sus cantidades de infectados
c)	Muestre el total de contagiados
d)	Muestre el porcentaje de infectados por cada distrito
e)	Muestre todos los distritos cuya cantidad sea mayor al promedio de infectados


	*/
       int op;
       carga();
       do{
          cout<<"\n";
          cout<<"\n 1.- Ingresar distrito";
          cout<<"\n 2.- Listado de distrito";
          cout<<"\n 3.- Consultar distrito";
          cout<<"\n 4.- Eliminar distrito";
          cout<<"\n 5.- Cambiar estadisticas";
          cout<<"\n 6.- Grabar al archivo y Salir";
          cout<<"\nINGRESE OPCION: "; cin>>op;
          cout<<"\n================================================================================";               
          switch(op){
          case 1: ingreso(); break;
          case 2: listado(); mayor(); break; 
          case 3: consulta(); break;
          case 4: double xcod;
                  cout<<"\n\nCodigo a eliminar: "; cin>>xcod;
                  anula(xcod);
                  listado();
                  break;    
          case 5: cambia(); break;                  
          }
       }while(op!=6);
   grabar();
}
 

