﻿Public Class frmABCempleado
    Dim xpasadas As Integer = 0
    Dim val As Boolean = False

    Private Sub frmABCempleado_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim cargo(4) As String
        cargo(0) = "Administrador"
        cargo(1) = "Vendedor"
        cargo(2) = "Comprobador"
        cargo(3) = "Asistente "
        cargo(4) = "cajero"

        For Each i In cargo
            cbocargo.Items.Add(i)
        Next
    End Sub

    
    Private Sub btngrabar_Click(sender As Object, e As EventArgs) Handles btngrabar.Click
        val = True
        validar(Trim(txtcod.Text), Trim(txtnom.Text))
        If val = False Then Exit Sub

        grabar1()
        limpiar()

    End Sub

    Public Sub validar(codigo As String, nombre As String)
        ErrorProvider1.Clear()
        If codigo = "" Then ErrorProvider1.SetError(txtcod, "Ingrese Codigo") : val = False : Exit Sub
        If Not IsNumeric(codigo) Then ErrorProvider1.SetError(txtcod, "Ingrese solo numeros") : val = False : Exit Sub
        If codigo.Length <> 6 Then ErrorProvider1.SetError(txtcod, "Codigo de 6 digitos") : val = False : Exit Sub
        If nombre = "" Then ErrorProvider1.SetError(txtnom, "Ingrese Nombre") : val = False : Exit Sub
        If cbocargo.SelectedIndex < 0 Then ErrorProvider1.SetError(cbocargo, "Eliga cargo") : val = False : Exit Sub
        If ListView1.Items.Count > 0 Then
            For i = 0 To ListView1.Items.Count - 1
                If codigo = ListView1.Items(i).SubItems(0).Text Then MessageBox.Show("Codigo ya ingresado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop) : val = False : Exit Sub
            Next
        End If

    End Sub

    Public Sub grabar1()
        Dim lv As ListViewItem
        lv = New ListViewItem(Trim(txtcod.Text))
        lv.SubItems.Add(Trim(txtnom.Text))
        lv.SubItems.Add(Me.cbocargo.SelectedItem)
        Me.ListView1.Items.Add(lv)
    End Sub

    Public Sub grabar2()
        If ListView1.Items.Count > 0 Then
            For i = 0 To ListView1.Items.Count - 1
                ReDim Preserve emnombre(i)
                emnombre(i) = ListView1.Items(i).SubItems(1).Text
            Next
        End If
        
    End Sub

    Public Sub limpiar()
        txtcod.Clear()
        txtnom.Clear()
        cbocargo.ResetText()
        txtcod.Focus()

    End Sub

   
    Private Sub btnsalir_Click(sender As Object, e As EventArgs) Handles btnsalir.Click
        grabar2()
        Me.Close()
    End Sub

    Private Sub txtcod_TextChanged(sender As Object, e As EventArgs) Handles txtcod.TextChanged

    End Sub
End Class