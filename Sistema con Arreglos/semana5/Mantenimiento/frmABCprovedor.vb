﻿Public Class frmABCprovedor
    Public giro(4) As String
    Dim xpasadas As Integer
    Dim val As Boolean = True
    Private Sub frmABCprovedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        giro(0) = "Distribuidora Mayorista"
        giro(1) = "Restaurante"
        giro(2) = "Farmacia"
        giro(3) = "Hipermercado"
        giro(4) = "Hotel"
        For Each i In giro
            cbogiro.Items.Add(i)
        Next
    End Sub
    Private Sub btnlogo_Click(sender As Object, e As EventArgs) Handles btnlogo.Click
        OpenFileDialog1.ShowDialog()
        PictureBox1.Image = Image.FromFile(OpenFileDialog1.FileName)
        PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
    End Sub

    Private Sub btngrabar_Click(sender As Object, e As EventArgs) Handles btngrabar.Click
        validar(Trim(txtruc.Text), Trim(txtnom.Text), Trim(txttele.Text))
        If val = False Then Exit Sub
        grabar1()
        grabar2()
        limpiar()

    End Sub

    Public Sub validar(ruc As String, nombre As String, telefono As String)
        val = True
        ErrorProvider1.Clear()
        If ruc = "" Then ErrorProvider1.SetError(txtruc, "Ingrese RUC") : val = False : Exit Sub
        If Not IsNumeric(ruc) Then ErrorProvider1.SetError(txtruc, "Ingrese solo numeros") : val = False : Exit Sub
        If ruc.Length <> 11 Then ErrorProvider1.SetError(txtruc, "Codigo de 11 digitos") : val = False : Exit Sub
        If nombre = "" Then ErrorProvider1.SetError(txtnom, "Ingrese Nombre") : val = False : Exit Sub
        If telefono = "" Then ErrorProvider1.SetError(txttele, "Ingrese Numero") : val = False : Exit Sub
        If Not IsNumeric(telefono) Then ErrorProvider1.SetError(txttele, "Ingrese solo numeros") : val = False : Exit Sub
        If cbogiro.SelectedIndex < 0 Then ErrorProvider1.SetError(cbogiro, "Eliga Marca") : val = False : Exit Sub
        If PictureBox1.Image Is Nothing Then ErrorProvider1.SetError(btnlogo, "Eliga Imagen") : val = False : Exit Sub

        If ListView1.Items.Count > 0 Then
            For i = 0 To ListView1.Items.Count - 1
                If ruc = ListView1.Items(i).SubItems(0).Text Then MessageBox.Show("Codigo ya ingresado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop) : val = False : Exit Sub
            Next
        End If

    End Sub

    Public Sub grabar1()
        Dim lv As ListViewItem
        lv = New ListViewItem(Trim(txtruc.Text))
        lv.SubItems.Add(Trim(txtnom.Text))
        lv.SubItems.Add(Trim(txttele.Text))
        lv.SubItems.Add(Me.cbogiro.SelectedItem)
        Me.ListView1.Items.Add(lv)

        frmmenu.listaproveedor.Images.Add(PictureBox1.Image)

    End Sub

    Public Sub grabar2()
        If ListView1.Items.Count <= 0 Then Exit Sub
        For i = 0 To ListView1.Items.Count - 1
            ReDim Preserve cliruc(i)
            cliruc(i) = ListView1.Items(i).SubItems(0).Text
            ReDim Preserve clinombre(i)
            clinombre(i) = ListView1.Items(i).SubItems(1).Text
            ReDim Preserve clitellefono(i)
            clitellefono(i) = ListView1.Items(i).SubItems(2).Text
            ReDim Preserve cligiro(i)
            cligiro(i) = ListView1.Items(i).SubItems(3).Text
        Next

    End Sub

    Public Sub limpiar()
        txtnom.Clear()
        txtruc.Clear()
        txttele.Clear()
        PictureBox1.Image = Nothing
        cbogiro.ResetText()
        txtnom.Focus()

    End Sub


    Private Sub btnsalir_Click(sender As Object, e As EventArgs) Handles btnsalir.Click
        Me.Close()
    End Sub
End Class