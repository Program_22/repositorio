﻿Public Class frmABCproducto
    Dim xpasadas As Integer
    Dim val As Boolean = True

    Private Sub frmABCproducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If promarca Is Nothing Then
            MessageBox.Show("No Hay marcas registradas" & Chr(13) & "No podra grabar ningun producto", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            For i = 0 To promarca.GetUpperBound(0)
                cbomarca.Items.Add(promarca(i))
            Next
        End If

    End Sub

    Private Sub btngrabar_Click(sender As Object, e As EventArgs) Handles btngrabar.Click

        validar(Trim(txtcod.Text), Trim(txtnom.Text), Trim(txtpre.Text))
        If val = False Then Exit Sub
        grabar1()
        grabar2()
        limpiar()

    End Sub

    Public Sub validar(codigo As String, nombre As String, precio As String)
        val = True
        ErrorProvider1.Clear()
        If codigo = "" Then ErrorProvider1.SetError(txtcod, "Ingrese Codigo") : val = False : Exit Sub
        If Not IsNumeric(codigo) Then ErrorProvider1.SetError(txtcod, "Ingrese solo numeros") : val = False : Exit Sub
        If codigo.Length <> 6 Then ErrorProvider1.SetError(txtcod, "Codigo de 6 digitos") : val = False : Exit Sub
        If nombre = "" Then ErrorProvider1.SetError(txtnom, "Ingrese Nombre") : val = False : Exit Sub
        If cbomarca.SelectedIndex < 0 Then ErrorProvider1.SetError(cbomarca, "Eliga Marca") : val = False : Exit Sub
        If precio = "" Then ErrorProvider1.SetError(txtpre, "Ingrese Precio") : val = False : Exit Sub
        If Not IsNumeric(precio) Then ErrorProvider1.SetError(txtpre, "Ingrese solo numeros") : val = False : Exit Sub
        If ListView1.Items.Count > 0 Then
            For i = 0 To ListView1.Items.Count - 1
                If codigo = ListView1.Items(i).SubItems(0).Text Then MessageBox.Show("Codigo ya ingresado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Stop) : val = False : Exit Sub
            Next
        End If

    End Sub


    Public Sub grabar1()
        Dim lv As ListViewItem
        lv = New ListViewItem(Trim(txtcod.Text))
        lv.SubItems.Add(Trim(txtnom.Text))
        lv.SubItems.Add(cbomarca.SelectedItem)
        lv.SubItems.Add(Trim(txtpre.Text))

        Me.ListView1.Items.Add(lv)
    End Sub

    Public Sub grabar2()
        If ListView1.Items.Count <= 0 Then Exit Sub

        For i = 0 To ListView1.Items.Count - 1
            ReDim Preserve pronombre(i)
            pronombre(i) = ListView1.Items(i).SubItems(1).Text
            ReDim Preserve proprecio(i)
            proprecio(i) = ListView1.Items(i).SubItems(3).Text
            ReDim Preserve procodigo(i)
            procodigo(i) = ListView1.Items(i).SubItems(0).Text
            ReDim Preserve marca(i)
            marca(i) = ListView1.Items(i).SubItems(2).Text
        Next
    End Sub

    Public Sub limpiar()
        txtcod.Clear()
        txtnom.Clear()
        txtpre.Clear()
        cbomarca.ResetText()
        txtcod.Focus()

    End Sub

    Private Sub btnsalir_Click(sender As Object, e As EventArgs) Handles btnsalir.Click
        grabar2()
        Me.Close()

    End Sub

   
End Class