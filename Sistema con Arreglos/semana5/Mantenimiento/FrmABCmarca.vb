﻿Public Class FrmABCmarca
    Dim x As Integer
    Dim val As Boolean = True
    Private Sub btnaceptar_Click(sender As Object, e As EventArgs) Handles btnaceptar.Click

        validar(Trim(txtmarca.Text))
        If val = False Then Exit Sub
        ingresar()
        contar()
        limpiar()

    End Sub

    Public Sub ingresar()
        ListBox1.Items.Add(txtmarca.Text)
    End Sub

    Public Sub validar(marca As String)

        val = True
        If marca = "" Then lblmensaje.Text = "Mensaje: Ingrese nombre" : val = False : Timer1.Start() : Exit Sub
        If IsNumeric(marca) Then lblmensaje.Text = "Mensaje: No se acepta solo numeros" : val = False : Timer1.Start() : Exit Sub
        If ListBox1.Items.Count > 0 Then
            For i = 0 To ListBox1.Items.Count - 1
                If marca = ListBox1.Items.Item(i) Then lblmensaje.Text = "Mensaje: Marca ya ingresada" : val = False : Timer1.Start() : Exit Sub
            Next
        End If

    End Sub

    Public Sub contar()
        lblcantidad.Text = ListBox1.Items.Count
    End Sub

    Public Sub limpiar()
        txtmarca.Clear() : txtmarca.Focus()
    End Sub

    Public Sub deshabilitar()
        btnaceptar.Enabled = False : btneliminar.Enabled = False
        btngrabar.Enabled = False : ListBox1.Enabled = False
    End Sub

    Public Sub habilitar()
        btnaceptar.Enabled = True : btneliminar.Enabled = True
        btngrabar.Enabled = True : ListBox1.Enabled = True
    End Sub

    Private Sub btngrabar_Click(sender As Object, e As EventArgs) Handles btngrabar.Click

        If ListBox1.Items.Count <= 0 Then lblmensaje.Text = "Mensaje: No hay elementos" : Timer1.Start() : Exit Sub
        grabar1()


    End Sub

    Public Sub grabar1()

        For i As Integer = 0 To ListBox1.Items.Count - 1
            ReDim Preserve promarca(i)
            promarca(i) = ListBox1.Items.Item(i)
        Next

        lblmensaje.Text = "Mensaje : Se grabo satisfactoriamente" : Timer1.Start()
        deshabilitar()

    End Sub

    Private Sub btnelimi_Click(sender As Object, e As EventArgs) Handles btneliminar.Click

        If ListBox1.SelectedIndex < 0 Then lblmensaje.Text = "Mensaje: Seleccione elemento" : Timer1.Start() : Exit Sub
        ListBox1.Items.RemoveAt(ListBox1.SelectedIndex)
        contar()
    End Sub

    Private Sub btnsalir_Click(sender As Object, e As EventArgs) Handles btnsalir.Click
        Me.Close()
    End Sub

    Private Sub FrmABCmarca_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        habilitar()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        x += 1

        If x Mod 2 = 0 Then
            lblmensaje.ForeColor = Color.Azure
        Else
            lblmensaje.ForeColor = Color.Black
        End If
        If x = 15 Then lblmensaje.Text = "Mensaje:" : Timer1.Stop() : x = 0

    End Sub
End Class