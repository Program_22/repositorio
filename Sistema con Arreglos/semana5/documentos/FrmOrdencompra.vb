﻿Public Class FrmOrdencompra
    Dim xrofac As Integer = 1
    Dim xpasadas As Integer
    Dim vali As Boolean = False

    Private Sub FrmOrdencompra_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblnrofac.Text = Format(1, "0000")
        llenado() : habilitar1() : habilitar2()
    End Sub

    Private Sub cboprovedor1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboprovedor1.SelectedIndexChanged

        If cboprovedor1.SelectedIndex >= 0 Then
            lblruc.Text = cliruc(cboprovedor1.SelectedIndex)
            lbltele.Text = clitellefono(cboprovedor1.SelectedIndex)
            picproveedor.Image = frmmenu.listaproveedor.Images.Item(cboprovedor1.SelectedIndex)
        End If

    End Sub

    Private Sub cbopro_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbopro.SelectedIndexChanged
        If cbopro.SelectedIndex >= 0 Then
            lblpre.Text = proprecio(cbopro.SelectedIndex)
        End If
    End Sub

    Private Sub btnaceptar_Click(sender As Object, e As EventArgs) Handles btnaceptar.Click
        validar(Trim(txtcan.Text))
        If vali = False Then Exit Sub
        aceptar() : cantidad() : neto() : limpiar1() : deshabilitar1()
    End Sub

    Private Sub btngrabar_Click(sender As Object, e As EventArgs) Handles btngrabar.Click
        xpasadas += 1
        If ListView1.Items.Count > 0 Then
            ReDim Preserve comnumero(xpasadas)
            ReDim Preserve comfecha(xpasadas)
            ReDim Preserve comproveedor(xpasadas)
            ReDim Preserve comimporte(xpasadas)
            comnumero(xpasadas) = lblnrofac.Text
            comfecha(xpasadas) = fecha2.Value
            comproveedor(xpasadas) = cboprovedor1.SelectedItem
            comimporte(xpasadas) = lblneto.Text


            deshablitar2() : limpiar2()
        Else
            MsgBox("No hay elementos que grabar")
        End If
    End Sub


    Private Sub btnelimi_Click(sender As Object, e As EventArgs) Handles btnelimi.Click

        ListView1.Items.RemoveAt(ListView1.SelectedIndices(0))
        neto() : cantidad()

    End Sub

    Private Sub btnnuevo_Click(sender As Object, e As EventArgs) Handles btnnuevo.Click
        habilitar1() : habilitar2() : limpiar2()
        ListView1.Items.Clear()
        xrofac += 1
        lblnrofac.Text = Format(xrofac, "0000")
    End Sub



    Public Sub llenado()
        If emnombre Is Nothing Then
            MsgBox("No hay datos de empleados ingresados", MsgBoxStyle.Information, "Aviso")
        Else
            For i = 0 To emnombre.GetUpperBound(0)
                cboempleado.Items.Add(emnombre(i))
            Next
        End If

        If pronombre Is Nothing Then
            MsgBox("No hay datos de productos ingresados", MsgBoxStyle.Information, "Aviso")
        Else
            For i = 0 To pronombre.GetUpperBound(0)
                cbopro.Items.Add(pronombre(i))
            Next
        End If

        If clinombre Is Nothing Then
            MsgBox("No hay datos de Clientes ingresados", MsgBoxStyle.Information, "Aviso")
        Else
            For Each x In clinombre
                cboprovedor1.Items.Add(x)
            Next
           
        End If
    End Sub

    Public Sub deshabilitar1()
        cboprovedor1.Enabled = False : cboempleado.Enabled = False : fecha1.Enabled = False : fecha2.Enabled = False
    End Sub
    Public Sub habilitar1()
        cboprovedor1.Enabled = True : cboempleado.Enabled = True : fecha1.Enabled = True : fecha2.Enabled = True
    End Sub
    Public Sub deshablitar2()
        btnaceptar.Enabled = False : btnelimi.Enabled = False : btngrabar.Enabled = False : btnnuevo.Enabled = True
    End Sub
    Public Sub habilitar2()
        btnaceptar.Enabled = True : btnelimi.Enabled = True : btngrabar.Enabled = True : btnnuevo.Enabled = False
    End Sub

    Public Sub aceptar()
        validar(Trim(txtcan.Text))
        If vali = False Then Exit Sub

        Dim lv As ListViewItem
        lv = New ListViewItem(cbopro.SelectedItem.ToString)
        lv.SubItems.Add(lblpre.Text)
        lv.SubItems.Add(Trim(txtcan.Text))
        lv.SubItems.Add(lbldesc.Text)
        lv.SubItems.Add(lblim.Text)

        Me.ListView1.Items.Add(lv)
    End Sub

    Public Sub validar(cantidad As String)
        vali = True
        ErrorProvider1.Clear()
        If cboprovedor1.SelectedIndex < 0 Then ErrorProvider1.SetError(cboprovedor1, "Eliga Proveedor") : vali = False : Exit Sub
        If cboempleado.SelectedIndex < 0 Then ErrorProvider1.SetError(cboempleado, "Eliga Empleado") : vali = False : Exit Sub
        If cbopro.SelectedIndex < 0 Then ErrorProvider1.SetError(cbopro, "Eliga Producto") : vali = False : Exit Sub
        If cantidad = "" Then ErrorProvider1.SetError(txtcan, "Ingrese Cantidad") : vali = False : Exit Sub
        If Not IsNumeric(cantidad) Then ErrorProvider1.SetError(txtcan, "Ingrese solo numeros") : vali = False : Exit Sub
        If fecha2.Value < fecha1.Value Then ErrorProvider1.SetError(fecha2, "La fecha de entrega tiene que ser posterior a la fecha de pedido") : vali = False : Exit Sub
    End Sub

    Public Sub cantidad()
        lblson.Text = "Son " & ListView1.Items.Count & " Productos"
    End Sub
    Public Sub importe()
       If lblpre.Text = "" Then
        ElseIf txtcan.Text = "" Or Not IsNumeric(txtcan.Text) Then
            lblim.Text = ""
        Else
            Dim ximporte As Decimal
            ximporte = (Val(lblpre.Text) * Val(txtcan.Text)) - lbldesc.Text

            lblim.Text = Format(ximporte, "0.00")
        End If
    End Sub
    Public Sub neto()
        If ListView1.Items.Count > 0 Then
            Dim xsuma As Integer
            xsuma = 0
            For i = 0 To ListView1.Items.Count - 1
                xsuma += Val(ListView1.Items(i).SubItems(4).Text)
            Next
            lblneto.Text = xsuma
        End If
    End Sub

    Public Sub limpiar1()
        txtcan.Clear()
        cbopro.ResetText()
        lblpre.Text = ""
        lbldesc.Text = ""
        lblim.Text = "0.00"
    End Sub
    Public Sub limpiar2()
        limpiar1()
        cboprovedor1.ResetText() : cboempleado.ResetText()
        lblruc.Text = "" : lbltele.Text = "" : picproveedor.Image = Nothing
    End Sub

    Private Sub txtcan_LostFocus(sender As Object, e As EventArgs) Handles txtcan.LostFocus
        importe()
    End Sub

    Private Sub txtcan_TextChanged(sender As Object, e As EventArgs) Handles txtcan.TextChanged
        If txtcan.Text = String.Empty Then Exit Sub

        If Trim(txtcan.Text) > 10 Then

            Dim ximporte, xdescuento As Decimal
            ximporte = Val(lblpre.Text) * Val(txtcan.Text)
            xdescuento = ximporte * 0.05

            lbldesc.Text = Format(xdescuento, "0.00")

        Else
            lbldesc.Text = "0.00"
        End If
        importe()
    End Sub

    Private Sub btnsalir_Click(sender As Object, e As EventArgs) Handles btnsalir.Click
        Me.Close()
    End Sub
End Class