﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmOrdencompra
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.btnsalir = New System.Windows.Forms.Button()
        Me.btngrabar = New System.Windows.Forms.Button()
        Me.btnnuevo = New System.Windows.Forms.Button()
        Me.lblson = New System.Windows.Forms.Label()
        Me.lblneto = New System.Windows.Forms.Label()
        Me.btnelimi = New System.Windows.Forms.Button()
        Me.btnaceptar = New System.Windows.Forms.Button()
        Me.lblim = New System.Windows.Forms.Label()
        Me.txtcan = New System.Windows.Forms.TextBox()
        Me.lblpre = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cbopro = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.cboempleado = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.fecha1 = New System.Windows.Forms.DateTimePicker()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.picproveedor = New System.Windows.Forms.PictureBox()
        Me.lbltele = New System.Windows.Forms.Label()
        Me.lblruc = New System.Windows.Forms.Label()
        Me.cboprovedor1 = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblnrofac = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lbldesc = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.fecha2 = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.picproveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(161, 50)
        Me.Label24.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(90, 15)
        Me.Label24.TabIndex = 58
        Me.Label24.Text = "telf : 976-3653"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(161, 35)
        Me.Label25.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(210, 15)
        Me.Label25.TabIndex = 57
        Me.Label25.Text = "Av.28 de julio 715 - Cercado de Lima"
        '
        'btnsalir
        '
        Me.btnsalir.Location = New System.Drawing.Point(567, 659)
        Me.btnsalir.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btnsalir.Name = "btnsalir"
        Me.btnsalir.Size = New System.Drawing.Size(100, 27)
        Me.btnsalir.TabIndex = 56
        Me.btnsalir.Text = "Salir"
        Me.btnsalir.UseVisualStyleBackColor = True
        '
        'btngrabar
        '
        Me.btngrabar.Location = New System.Drawing.Point(163, 659)
        Me.btngrabar.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btngrabar.Name = "btngrabar"
        Me.btngrabar.Size = New System.Drawing.Size(100, 27)
        Me.btngrabar.TabIndex = 55
        Me.btngrabar.Text = "Grabar"
        Me.btngrabar.UseVisualStyleBackColor = True
        '
        'btnnuevo
        '
        Me.btnnuevo.Location = New System.Drawing.Point(25, 659)
        Me.btnnuevo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btnnuevo.Name = "btnnuevo"
        Me.btnnuevo.Size = New System.Drawing.Size(100, 27)
        Me.btnnuevo.TabIndex = 54
        Me.btnnuevo.Text = "Nuevo"
        Me.btnnuevo.UseVisualStyleBackColor = True
        '
        'lblson
        '
        Me.lblson.AutoSize = True
        Me.lblson.Location = New System.Drawing.Point(22, 623)
        Me.lblson.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblson.Name = "lblson"
        Me.lblson.Size = New System.Drawing.Size(99, 15)
        Me.lblson.TabIndex = 52
        Me.lblson.Text = "Son 0 productos"
        '
        'lblneto
        '
        Me.lblneto.BackColor = System.Drawing.Color.LightGray
        Me.lblneto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblneto.Location = New System.Drawing.Point(480, 623)
        Me.lblneto.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblneto.Name = "lblneto"
        Me.lblneto.Size = New System.Drawing.Size(69, 27)
        Me.lblneto.TabIndex = 51
        '
        'btnelimi
        '
        Me.btnelimi.Location = New System.Drawing.Point(570, 593)
        Me.btnelimi.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btnelimi.Name = "btnelimi"
        Me.btnelimi.Size = New System.Drawing.Size(100, 27)
        Me.btnelimi.TabIndex = 50
        Me.btnelimi.Text = "Eliminar"
        Me.btnelimi.UseVisualStyleBackColor = True
        '
        'btnaceptar
        '
        Me.btnaceptar.Location = New System.Drawing.Point(571, 311)
        Me.btnaceptar.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.btnaceptar.Name = "btnaceptar"
        Me.btnaceptar.Size = New System.Drawing.Size(100, 31)
        Me.btnaceptar.TabIndex = 48
        Me.btnaceptar.Text = "Aceptar"
        Me.btnaceptar.UseVisualStyleBackColor = True
        '
        'lblim
        '
        Me.lblim.BackColor = System.Drawing.Color.Wheat
        Me.lblim.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblim.Location = New System.Drawing.Point(480, 314)
        Me.lblim.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblim.Name = "lblim"
        Me.lblim.Size = New System.Drawing.Size(69, 24)
        Me.lblim.TabIndex = 47
        '
        'txtcan
        '
        Me.txtcan.Location = New System.Drawing.Point(299, 314)
        Me.txtcan.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtcan.Name = "txtcan"
        Me.txtcan.Size = New System.Drawing.Size(55, 22)
        Me.txtcan.TabIndex = 46
        '
        'lblpre
        '
        Me.lblpre.BackColor = System.Drawing.Color.Wheat
        Me.lblpre.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblpre.Location = New System.Drawing.Point(224, 312)
        Me.lblpre.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblpre.Name = "lblpre"
        Me.lblpre.Size = New System.Drawing.Size(67, 24)
        Me.lblpre.TabIndex = 45
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(478, 287)
        Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(54, 15)
        Me.Label18.TabIndex = 44
        Me.Label18.Text = "Importe"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(296, 287)
        Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(58, 15)
        Me.Label17.TabIndex = 43
        Me.Label17.Text = "Cantidad"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(221, 287)
        Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(44, 15)
        Me.Label16.TabIndex = 42
        Me.Label16.Text = "Precio"
        '
        'cbopro
        '
        Me.cbopro.FormattingEnabled = True
        Me.cbopro.Location = New System.Drawing.Point(21, 312)
        Me.cbopro.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cbopro.Name = "cbopro"
        Me.cbopro.Size = New System.Drawing.Size(184, 23)
        Me.cbopro.TabIndex = 41
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(22, 287)
        Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(60, 15)
        Me.Label15.TabIndex = 40
        Me.Label15.Text = "Producto"
        '
        'cboempleado
        '
        Me.cboempleado.FormattingEnabled = True
        Me.cboempleado.Location = New System.Drawing.Point(476, 228)
        Me.cboempleado.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cboempleado.Name = "cboempleado"
        Me.cboempleado.Size = New System.Drawing.Size(160, 23)
        Me.cboempleado.TabIndex = 39
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(454, 210)
        Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(63, 15)
        Me.Label14.TabIndex = 38
        Me.Label14.Text = "Empleado"
        '
        'fecha1
        '
        Me.fecha1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.fecha1.Location = New System.Drawing.Point(501, 141)
        Me.fecha1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.fecha1.MaxDate = New Date(2100, 12, 31, 0, 0, 0, 0)
        Me.fecha1.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.fecha1.Name = "fecha1"
        Me.fecha1.Size = New System.Drawing.Size(135, 22)
        Me.fecha1.TabIndex = 37
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(454, 123)
        Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(97, 15)
        Me.Label13.TabIndex = 36
        Me.Label13.Text = "Fecha de pedido"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.picproveedor)
        Me.GroupBox1.Controls.Add(Me.lbltele)
        Me.GroupBox1.Controls.Add(Me.lblruc)
        Me.GroupBox1.Controls.Add(Me.cboprovedor1)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 123)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox1.Size = New System.Drawing.Size(430, 161)
        Me.GroupBox1.TabIndex = 35
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del cliente"
        '
        'picproveedor
        '
        Me.picproveedor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.picproveedor.Location = New System.Drawing.Point(280, 53)
        Me.picproveedor.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.picproveedor.Name = "picproveedor"
        Me.picproveedor.Size = New System.Drawing.Size(139, 77)
        Me.picproveedor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picproveedor.TabIndex = 7
        Me.picproveedor.TabStop = False
        '
        'lbltele
        '
        Me.lbltele.BackColor = System.Drawing.SystemColors.HighlightText
        Me.lbltele.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbltele.Location = New System.Drawing.Point(85, 80)
        Me.lbltele.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbltele.Name = "lbltele"
        Me.lbltele.Size = New System.Drawing.Size(103, 22)
        Me.lbltele.TabIndex = 6
        '
        'lblruc
        '
        Me.lblruc.BackColor = System.Drawing.Color.White
        Me.lblruc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblruc.Location = New System.Drawing.Point(85, 53)
        Me.lblruc.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblruc.Name = "lblruc"
        Me.lblruc.Size = New System.Drawing.Size(103, 22)
        Me.lblruc.TabIndex = 4
        '
        'cboprovedor1
        '
        Me.cboprovedor1.FormattingEnabled = True
        Me.cboprovedor1.Location = New System.Drawing.Point(85, 21)
        Me.cboprovedor1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.cboprovedor1.Name = "cboprovedor1"
        Me.cboprovedor1.Size = New System.Drawing.Size(235, 23)
        Me.cboprovedor1.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(16, 87)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 15)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Telefono "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(42, 60)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 15)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "RUC"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 29)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 15)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Provedor"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblnrofac)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Location = New System.Drawing.Point(461, 14)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.GroupBox2.Size = New System.Drawing.Size(209, 93)
        Me.GroupBox2.TabIndex = 34
        Me.GroupBox2.TabStop = False
        '
        'lblnrofac
        '
        Me.lblnrofac.BackColor = System.Drawing.Color.Black
        Me.lblnrofac.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.lblnrofac.Location = New System.Drawing.Point(72, 39)
        Me.lblnrofac.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblnrofac.Name = "lblnrofac"
        Me.lblnrofac.Size = New System.Drawing.Size(87, 15)
        Me.lblnrofac.TabIndex = 3
        Me.lblnrofac.Text = "00001"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(35, 39)
        Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(24, 15)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "N° "
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(35, 65)
        Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(103, 15)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "RUC 4578965210"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Californian FB", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(17, 18)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(141, 15)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "FACTURA DE VENTA "
        '
        'ListView1
        '
        Me.ListView1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5})
        Me.ListView1.GridLines = True
        Me.ListView1.Location = New System.Drawing.Point(21, 358)
        Me.ListView1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(541, 262)
        Me.ListView1.TabIndex = 59
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Preducto"
        Me.ColumnHeader1.Width = 198
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Precio"
        Me.ColumnHeader2.Width = 73
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Cantidad"
        Me.ColumnHeader3.Width = 72
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Descuento"
        Me.ColumnHeader4.Width = 98
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Importe"
        Me.ColumnHeader5.Width = 93
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(161, 14)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(226, 15)
        Me.Label1.TabIndex = 60
        Me.Label1.Text = "Distribuidora ADVANCE TOUSHIRO"
        '
        'lbldesc
        '
        Me.lbldesc.BackColor = System.Drawing.Color.Wheat
        Me.lbldesc.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lbldesc.Location = New System.Drawing.Point(374, 314)
        Me.lbldesc.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbldesc.Name = "lbldesc"
        Me.lbldesc.Size = New System.Drawing.Size(69, 24)
        Me.lbldesc.TabIndex = 62
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(372, 287)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 15)
        Me.Label3.TabIndex = 61
        Me.Label3.Text = "Descuento"
        '
        'fecha2
        '
        Me.fecha2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.fecha2.Location = New System.Drawing.Point(501, 184)
        Me.fecha2.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.fecha2.MaxDate = New Date(2100, 12, 31, 0, 0, 0, 0)
        Me.fecha2.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.fecha2.Name = "fecha2"
        Me.fecha2.Size = New System.Drawing.Size(135, 22)
        Me.fecha2.TabIndex = 64
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(454, 166)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(101, 15)
        Me.Label7.TabIndex = 63
        Me.Label7.Text = "Fecha de entrega"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.WindowsApplication1.My.Resources.Resources.Proveedor_fw
        Me.PictureBox1.Location = New System.Drawing.Point(16, 14)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(137, 103)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 33
        Me.PictureBox1.TabStop = False
        '
        'FrmOrdencompra
        '
        Me.AcceptButton = Me.btnaceptar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(687, 695)
        Me.Controls.Add(Me.fecha2)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lbldesc)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ListView1)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.btnsalir)
        Me.Controls.Add(Me.btngrabar)
        Me.Controls.Add(Me.btnnuevo)
        Me.Controls.Add(Me.lblson)
        Me.Controls.Add(Me.lblneto)
        Me.Controls.Add(Me.btnelimi)
        Me.Controls.Add(Me.btnaceptar)
        Me.Controls.Add(Me.lblim)
        Me.Controls.Add(Me.txtcan)
        Me.Controls.Add(Me.lblpre)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.cbopro)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.cboempleado)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.fecha1)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Font = New System.Drawing.Font("Californian FB", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "FrmOrdencompra"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmOrdencompra"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.picproveedor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents btnsalir As System.Windows.Forms.Button
    Friend WithEvents btngrabar As System.Windows.Forms.Button
    Friend WithEvents btnnuevo As System.Windows.Forms.Button
    Friend WithEvents lblson As System.Windows.Forms.Label
    Friend WithEvents lblneto As System.Windows.Forms.Label
    Friend WithEvents btnelimi As System.Windows.Forms.Button
    Friend WithEvents btnaceptar As System.Windows.Forms.Button
    Friend WithEvents lblim As System.Windows.Forms.Label
    Friend WithEvents txtcan As System.Windows.Forms.TextBox
    Friend WithEvents lblpre As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cbopro As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cboempleado As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents fecha1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents picproveedor As System.Windows.Forms.PictureBox
    Friend WithEvents lbltele As System.Windows.Forms.Label
    Friend WithEvents lblruc As System.Windows.Forms.Label
    Friend WithEvents cboprovedor1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblnrofac As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lbldesc As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents fecha2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
End Class
