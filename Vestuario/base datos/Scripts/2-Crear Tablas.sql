Use Vestuario;
-- Crear Tabla Categoria
CREATE TABLE IF NOT EXISTS categoria (
  CategoriaID smallint(5) unsigned NOT NULL,
  Nombre varchar(50) NOT NULL,
  Imagen varchar(255) NOT NULL,
  UltimaMod date NOT NULL,
  FechaCreacion date NOT NULL,
  Flags int(10) unsigned NOT NULL,
  PRIMARY KEY (CategoriaID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Ingresar Registros a la Tabla Categoria
INSERT INTO categoria
 (CategoriaID, Nombre, Imagen, UltimaMod, FechaCreacion,Flags) VALUES
(1, 'Casacas', '', '2012-06-20', '2012-06-20', 0),
(2, 'Camisas', '', '2012-06-20', '2012-06-20', 0),
(3, 'Zapatos', '', '2012-06-20', '2012-06-20', 0),
(4, 'Polos', '', '2012-06-20', '2012-06-20', 0);

-- Crear Tabla Categoria de Cuenta
CREATE TABLE IF NOT EXISTS categoriacuenta (
  CategoriaCtaID int(10) unsigned NOT NULL AUTO_INCREMENT,
  NombreCatCta varchar(20) NOT NULL,
  Flags int(10) unsigned NOT NULL,
  PRIMARY KEY (CategoriaCtaID)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- Ingresando Registros a la Tabla Categoría Cuenta
INSERT INTO categoriacuenta (CategoriaCtaID, NombreCatCta, Flags) VALUES
(1, 'Cliente', 0),
(2, 'Empleado', 0),
(3, 'Administrador', 0),
(4, 'Supervisor', 0);

-- Crear la Tabla Clientes
CREATE TABLE IF NOT EXISTS clientes (
  ClienteID smallint(5) unsigned NOT NULL,
  CuentaID varchar(20) NOT NULL,
  TarjetaID int(10) unsigned NOT NULL,
  Nombre varchar(20) NOT NULL,
  Apellidos varchar(25) NOT NULL,
  EMail varchar(50) NOT NULL,
  Direccion varchar(50) NOT NULL,
  Ciudad varchar(50) NOT NULL,
  Provincia varchar(50) NOT NULL,
  CodigoPostal varchar(10) NOT NULL,
  Telefono varchar(20) DEFAULT NULL,
  FAX varchar(20) DEFAULT NULL,
  NumeroTarjeta varchar(20) NOT NULL,
  NombreEntarjeta varchar(50) NOT NULL,
  FechaCaducidad date NOT NULL,
  LimiteCredito decimal(10,2) NOT NULL,
  UltimaMod timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FechaCreacion timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  Pais varchar(50) NOT NULL,
  Flags int(10) unsigned NOT NULL,
  PRIMARY KEY (ClienteID),
  KEY TarjetaID (TarjetaID),
  KEY CuentaID (CuentaID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Insertando Registros a la Tabla Clientes 
INSERT INTO clientes (ClienteID, CuentaID, TarjetaID, Nombre, Apellidos, EMail, Direccion, Ciudad,
 Provincia, CodigoPostal, Telefono, FAX, NumeroTarjeta, NombreEntarjeta, FechaCaducidad, 
LimiteCredito, UltimaMod, FechaCreacion, Pais, Flags) VALUES
(1, 'dgarcia', 1, 'Demetrio Carlos', 'Garcia Mendoza',
 'dcgarciam90@hotmail.com', 'Av. Dos de Mayo 456', 
'LIma', 'Lima', 'Lima01', NULL, NULL, '5670987009812609',
 'Demetrio Carlos García Mendoza', '2014-01-30', '5000.00', '2012-06-20 20:49:19',
 '2012-06-20 00:00:00', 'Perú', 0),
(2, 'ktorres', 2, 'Karina', 'Torres Delgado', 'ktorres60@hotmail.com', 
'Av. Los Portales 560', 'Cercado de Lima', 'Lima', 'Lima01', '', '', 
'2345900090560090', 'Karina Torres Delagado', '0000-00-00', '5000.00', 
'2012-06-23 22:25:40', '2012-06-23 00:00:00', 'Perú', 0);

-- Creando la tabla Control

CREATE TABLE IF NOT EXISTS control (
  parametro varchar(20) NOT NULL,
  valor varchar(20) NOT NULL,
  PRIMARY KEY (parametro)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Insertando Registros a la tabla Control

INSERT INTO control (parametro, valor) VALUES
('clientes', '2'),
('igv', '0.18'),
('pedidos', '17'),
('prendas', '6');

-- Creando la tabla Cuentas

CREATE TABLE IF NOT EXISTS cuentas (
  CuentaID varchar(20) NOT NULL,
  CategoriaCtaID int(10) unsigned NOT NULL,
  UltimaMod date NOT NULL,
  FechaAlta date NOT NULL,
  PalabraPaso varchar(20) NOT NULL,
  Flags int(10) unsigned NOT NULL,
  PRIMARY KEY (CuentaID),
  KEY CategoriaCtaID (CategoriaCtaID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Ingresando Registros a la Tabla Cuentas

INSERT INTO cuentas 
(CuentaID, CategoriaCtaID, UltimaMod, FechaAlta, PalabraPaso, Flags) VALUES
('admin', 4, '2012-06-20', '2012-06-20', 'admin', 0),
('dgarcia', 1, '2012-06-20', '2012-06-20', 'dgarcia100', 0),
('ktorres', 1, '2012-06-23', '2012-06-23', 'ktorres100', 0);

-- Creando Tabla Envios

CREATE TABLE IF NOT EXISTS envios (
  EnvioID int(10) unsigned NOT NULL AUTO_INCREMENT,
  NombreEnvio varchar(45) NOT NULL,
  Flags int(10) unsigned NOT NULL,
  PRIMARY KEY (EnvioID)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- Ingresando Registros Tabla Envios

INSERT INTO envios (EnvioID, NombreEnvio, Flags) VALUES
(1, 'Por Avión', 0),
(2, 'Entrega a Domicilio(Local)', 0),
(3, 'Por Asignar', 0);

-- Creando Tabla Estados

CREATE TABLE IF NOT EXISTS estados (
  EstadoID int(10) unsigned NOT NULL AUTO_INCREMENT,
  NombreEstado varchar(30) NOT NULL,
  Flags int(10) unsigned NOT NULL,
  PRIMARY KEY (EstadoID)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- Ingresando Registros Tabla Estados

INSERT INTO `estados` (`EstadoID`, `NombreEstado`, `Flags`) VALUES
(1, 'Ingresado', 0),
(2, 'En Proceso', 0),
(3, 'Anulado', 0),
(4, 'Atendido', 0),
(5, 'Cerrado', 0);

-- Crear Tabla Formas de Pagos

CREATE TABLE IF NOT EXISTS formaspagos (
  FPagoID int(10) unsigned NOT NULL AUTO_INCREMENT,
  NombreFPago varchar(45) NOT NULL,
  Flags int(10) unsigned NOT NULL,
  PRIMARY KEY (FPagoID)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- Ingresar registros Tabla Formas de Pago

INSERT INTO formaspagos (FPagoID, NombreFPago, Flags) VALUES
(1, 'Al Contado', 0),
(2, 'Con Tarjeta', 0);

-- Crear Tabla Item de Pedidos

CREATE TABLE IF NOT EXISTS itempedido (
  PrendaID char(13) NOT NULL,
  idtienda int(10) unsigned NOT NULL,
  PedidoID mediumint(8) unsigned NOT NULL,
  Unidades smallint(5) unsigned NOT NULL DEFAULT '1',
  Flags int(10) unsigned NOT NULL,
  KEY PedidoID (PedidoID),
  KEY idtienda (idtienda),
  KEY PrendaID (PrendaID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Ingresar Registros a la tabla Item de Pedidos

INSERT INTO itempedido (PrendaID, idtienda, PedidoID, Unidades, Flags) VALUES
('2', 1, 1, 1, 0),
('4', 1, 1, 1, 0),
('5', 1, 2, 1, 0),
('6', 1, 2, 1, 0),
('1', 1, 3, 1, 0),
('6', 1, 3, 1, 0),
('3', 1, 4, 1, 0),
('4', 1, 4, 2, 0),
('3', 1, 5, 1, 0),
('4', 1, 5, 2, 0),
('6', 1, 6, 1, 0),
('6', 1, 7, 1, 0),
('5', 1, 8, 1, 0),
('4', 1, 9, 1, 0),
('3', 1, 10, 1, 0),
('3', 1, 11, 1, 0),
('6', 1, 12, 1, 0),
('1', 1, 13, 1, 0),
('1', 1, 14, 1, 0),
('4', 1, 14, 1, 0),
('1', 1, 16, 1, 0);

-- --------------------------------------------------------

-- Creando la tabla de Errores

CREATE TABLE IF NOT EXISTS logerrores(
  CuentaID varchar(20) NOT NULL,
  Fecha datetime NOT NULL,
  NombreCuenta varchar(50) NOT NULL,
  DescripcionError text NOT NULL,
  Flags int(10) unsigned NOT NULL,
  KEY CuentaID (CuentaID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Crear la tabla Pedidos

CREATE TABLE IF NOT EXISTS pedidos (
  PedidoID mediumint(8) unsigned NOT NULL,
  EstadoID int(10) unsigned NOT NULL,
  FPagoID int(10) unsigned NOT NULL,
  EnvioID int(10) unsigned NOT NULL,
  ClienteID smallint(5) unsigned NOT NULL,
  FechaPedido date NOT NULL,
  PrecioSIGV decimal(12,2) NOT NULL DEFAULT '0.00',
  IGV decimal(12,2) NOT NULL DEFAULT '0.00',
  TotalCargo decimal(12,2) NOT NULL DEFAULT '0.00',
  FechaEstado date NOT NULL,
  FechaEnvio date NOT NULL,
  RefEnvio varchar(255) NOT NULL,
  UltimaMod datetime NOT NULL,
  FechaCreacion datetime NOT NULL,
  Flags int(10) unsigned NOT NULL,
  PRIMARY KEY (PedidoID),
  KEY ClienteID (ClienteID),
  KEY EnvioID (EnvioID),
  KEY FPagoID (FPagoID),
  KEY EstadoID (EstadoID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Ingresando registros Tabla Pedidos

INSERT INTO pedidos (PedidoID, EstadoID, FPagoID, EnvioID, ClienteID, FechaPedido, PrecioSIGV, IGV,
 TotalCargo, FechaEstado, FechaEnvio, RefEnvio, UltimaMod, FechaCreacion, Flags) VALUES
(1, 1, 2, 3, 1, '2012-06-22', '0.00', '0.00', '0.00', '2012-06-22', '2012-06-24', 'Aceptado-Confirmar Envio', '2012-06-22 00:00:00', '2012-06-22 00:00:00', 0),
(2, 1, 2, 3, 1, '2012-06-22', '377.00', '57.51', '434.51', '2012-06-22', '2012-06-24', 'Aceptado-Confirmar Envio', '2012-06-22 00:00:00', '2012-06-22 00:00:00', 0),
(3, 1, 2, 3, 1, '2012-06-22', '227.00', '40.86', '267.86', '2012-06-22', '2012-06-24', 'Aceptado-Confirmar Envio', '2012-06-22 00:00:00', '2012-06-22 00:00:00', 0),
(4, 1, 2, 3, 2, '2012-06-23', '285.00', '51.30', '336.30', '2012-06-23', '2012-06-25', 'Aceptado-Confirmar Envio', '2012-06-23 00:00:00', '2012-06-23 00:00:00', 0),
(5, 1, 2, 3, 2, '2012-06-23', '285.00', '51.30', '336.30', '2012-06-23', '2012-06-25', 'Aceptado-Confirmar Envio', '2012-06-23 00:00:00', '2012-06-23 00:00:00', 0),
(6, 1, 2, 3, 1, '2012-06-23', '52.00', '9.36', '61.36', '2012-06-23', '2012-06-25', 'Aceptado-Confirmar Envio', '2012-06-23 00:00:00', '2012-06-23 00:00:00', 0),
(7, 1, 2, 3, 1, '2012-06-23', '52.00', '9.36', '61.36', '2012-06-23', '2012-06-25', 'Aceptado-Confirmar Envio', '2012-06-23 00:00:00', '2012-06-23 00:00:00', 0),
(8, 1, 2, 3, 1, '2012-06-23', '325.00', '58.50', '383.50', '2012-06-23', '2012-06-25', 'Aceptado-Confirmar Envio', '2012-06-23 00:00:00', '2012-06-23 00:00:00', 0),
(9, 1, 2, 3, 1, '2012-06-23', '45.00', '8.10', '53.10', '2012-06-23', '2012-06-25', 'Aceptado-Confirmar Envio', '2012-06-23 00:00:00', '2012-06-23 00:00:00', 0),
(10, 1, 2, 3, 1, '2012-06-23', '195.00', '35.10', '230.10', '2012-06-23', '2012-06-25', 'Aceptado-Confirmar Envio', '2012-06-23 00:00:00', '2012-06-23 00:00:00', 0),
(11, 1, 2, 3, 1, '2012-06-23', '195.00', '35.10', '230.10', '2012-06-23', '2012-06-25', 'Aceptado-Confirmar Envio', '2012-06-23 00:00:00', '2012-06-23 00:00:00', 0),
(12, 1, 2, 3, 2, '2012-06-23', '52.00', '9.36', '61.36', '2012-06-23', '2012-06-25', 'Aceptado-Confirmar Envio', '2012-06-23 00:00:00', '2012-06-23 00:00:00', 0),
(13, 1, 2, 3, 2, '2012-06-23', '175.00', '31.50', '206.50', '2012-06-23', '2012-06-25', 'Aceptado-Confirmar Envio', '2012-06-23 00:00:00', '2012-06-23 00:00:00', 0),
(14, 1, 2, 3, 2, '2012-06-23', '220.00', '39.60', '259.60', '2012-06-23', '2012-06-25', 'Aceptado-Confirmar Envio', '2012-06-23 00:00:00', '2012-06-23 00:00:00', 0),
(16, 1, 2, 3, 2, '2012-06-23', '175.00', '31.50', '206.50', '2012-06-23', '2012-06-25', 'Aceptado-Confirmar Envio', '2012-06-23 00:00:00', '2012-06-23 00:00:00', 0);

-- --------------------------------------------------------

-- Crear Tabla Prendas

CREATE TABLE IF NOT EXISTS prendas (
  PrendaID char(13) NOT NULL,
  CategoriaID smallint(5) unsigned NOT NULL,
  Nombre varchar(200) NOT NULL,
  DetalleMenor varchar(200) NOT NULL,
  DetalleMayor text NOT NULL,
  Imagen varchar(255) NOT NULL,
  Precio decimal(12,2) NOT NULL DEFAULT '0.00',
  UltimaMod date NOT NULL,
  Flags int(10) unsigned NOT NULL,
  PRIMARY KEY (PrendaID),
  KEY CategoriaID (CategoriaID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Ingresar Registros Tabla Prendas

INSERT INTO prendas (PrendaID, CategoriaID, Nombre, DetalleMenor, DetalleMayor, Imagen,
 Precio, UltimaMod, Flags) VALUES
('1', 1, 'Casaca de Cuero para Varón', 'Genuino', 'Modelo Corto, Talla Large, Color Negro, a la Cadera,Marca GIORGIO''S.', '', '175.00', '2012-06-20', 0),
('2', 1, 'Casaca de Cuero para Varón', 'Genuino', 'A.G. Silver, Talla Large, Color Negro', '', '185.00', '2012-06-20', 0),
('3', 1, 'Casaca de Cuero para Mujer', 'Genuina', 'Color Roja, Talla Mediano,a la Cadera,Marca Fasions Meza', '', '195.00', '2012-06-20', 0),
('4', 2, 'Camisa para Varón', 'Modelo Estándar', 'US BASIC MANGA CORTA, TEJIDO OXFORD 70% ALGODON Y 30% POLIESTER, CON DOS BOTONES NACARADOS EN CUELLO Y UN BOLSILLO. PESO 130 GRS. TALLAS M, L, XL, XXL. COLORES: CELESTE, ROJO, BLANCO, GRIS OSCURO, AZUL ROYAL Y NEGRO.', '', '45.00', '2012-06-20', 0),
('5', 3, 'Zapatos de Vestir para Varón', 'Modelo Corfán', 'De Cuero Fino, Color Marrón, Marca Daniel´s, Talla 38 a 43.', '', '325.00', '2012-06-20', 0),
('6', 2, 'Camisa Manga Larga', 'Phoenix', 'CAMISA DE CABALLERO, CON DOS BOLSILLOS EN PECHO, UNO EN LA MANGA IZQUIERDA Y BOTONES EN EL CUELLO. BOTONES GRABADOS. TEJIDO SARGA PIEL DE MELOCOTON 100% ALGODON. PESO 150 GRS. TALLAS: S, M, L, XL, XXL. COLORES: NEGRO Y MARINO.', '', '52.00', '2012-06-20', 0);

-- Crear tabla Proveedores

CREATE TABLE IF NOT EXISTS proveedores (
  ProveedorID smallint(5) unsigned NOT NULL,
  Nombre varchar(20) NOT NULL,
  Apellidos varchar(25) NOT NULL,
  Empresa varchar(50) NOT NULL,
  EMail varchar(50) NOT NULL,
  UltimaMod date NOT NULL,
  FechaCreacion date NOT NULL,
  Flags int(10) unsigned NOT NULL,
  PRIMARY KEY (ProveedorID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Ingresar Registros Tabla Proveedores

INSERT INTO proveedores (ProveedorID, Nombre, Apellidos, Empresa, 
EMail, UltimaMod, FechaCreacion, Flags) VALUES
(1, 'Daniel Alberto', 'Carrizales Llanos', 'Fabricante Carrizales Moda S.A.', 'dcarrizales678@carrizales.com', '2012-06-20', '2012-06-20', 0);

-- --------------------------------------------------------

-- Crear Tabla Tarjeta

CREATE TABLE IF NOT EXISTS tarjeta (
  TarjetaID int(10) unsigned NOT NULL AUTO_INCREMENT,
  NombreTarjeta varchar(50) NOT NULL,
  Flags int(10) unsigned NOT NULL,
  PRIMARY KEY (TarjetaID)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- Ingresar Registros a la Tabla Tarjeta

INSERT INTO tarjeta (TarjetaID, NombreTarjeta, Flags) VALUES
(1, 'Tarjeta Visa', 0),
(2, 'Tarjeta Mastercard', 0),
(3, 'Tarjeta Ripley', 0),
(4, 'Tarjeta Saga Falabella', 0);

-- Crear Tabla Tienda

CREATE TABLE IF NOT EXISTS tienda (
  idtienda int(10) unsigned NOT NULL,
  ProveedorID smallint(5) unsigned NOT NULL,
  PrendaID char(13) NOT NULL,
  existencias int(10) unsigned NOT NULL,
  Flags int(10) unsigned NOT NULL,
  PRIMARY KEY (idtienda,PrendaID),
  KEY PrendaID (PrendaID),
  KEY ProveedorID (ProveedorID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Ingresando Registros a la Tabla Tienda

INSERT INTO tienda (idtienda, ProveedorID, PrendaID, existencias, Flags) VALUES
(1, 1, '1', 95, 0),
(1, 1, '2', 49, 0),
(1, 1, '3', 41, 0),
(1, 1, '4', 18, 0),
(1, 1, '5', 28, 0),
(1, 1, '6', 5, 0);

-- Creando la Tabla UsuarioSys

CREATE TABLE IF NOT EXISTS usuariosys (
  CuentaID varchar(20) NOT NULL,
  Nombres varchar(20) NOT NULL,
  Apellidos varchar(25) NOT NULL,
  Flags int(10) unsigned NOT NULL,
  KEY CuentaID (CuentaID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- Filtros para la tabla Clientes

ALTER TABLE clientes
  ADD CONSTRAINT clientes_ibfk_1 FOREIGN KEY (TarjetaID)
  REFERENCES tarjeta (TarjetaID) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT clientes_ibfk_2 FOREIGN KEY (CuentaID) 
 REFERENCES cuentas (CuentaID) ON DELETE NO ACTION ON UPDATE CASCADE;

-- Filtros para la tabla Cuentas

ALTER TABLE cuentas
  ADD CONSTRAINT cuentas_ibfk_1 
 FOREIGN KEY (CategoriaCtaID) 
REFERENCES categoriacuenta (CategoriaCtaID) ON DELETE NO ACTION ON UPDATE CASCADE;

-- Filtros para la tabla Itempedido`

ALTER TABLE itempedido
  ADD CONSTRAINT itempedido_ibfk_1 FOREIGN KEY (PedidoID) 
  REFERENCES pedidos (PedidoID) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT itempedido_ibfk_2 FOREIGN KEY (idtienda)
  REFERENCES tienda (idtienda) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT itempedido_ibfk_3 FOREIGN KEY (PrendaID) 
 REFERENCES prendas (PrendaID) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Filtros para la tabla Logerrores

ALTER TABLE logerrores
  ADD CONSTRAINT logerrores_ibfk_1 FOREIGN KEY (CuentaID)
  REFERENCES cuentas (CuentaID) ON DELETE NO ACTION ON UPDATE CASCADE;

-- Filtros para la tabla `pedidos`

ALTER TABLE pedidos
  ADD CONSTRAINT pedidos_ibfk_1 FOREIGN KEY (ClienteID) 
 REFERENCES clientes (ClienteID) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT pedidos_ibfk_2 FOREIGN KEY (EnvioID)
 REFERENCES envios (EnvioID) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT pedidos_ibfk_3 FOREIGN KEY (FPagoID) 
 REFERENCES formaspagos (FPagoID) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT pedidos_ibfk_4 FOREIGN KEY (EstadoID) 
REFERENCES estados (EstadoID) ON DELETE NO ACTION ON UPDATE CASCADE;

-- Filtros para la tabla Prendas

ALTER TABLE prendas
  ADD CONSTRAINT prendas_ibfk_1 FOREIGN KEY (CategoriaID) 
REFERENCES categoria (CategoriaID) ON DELETE NO ACTION ON UPDATE CASCADE;

-- Filtros para la tabla Tienda
--
ALTER TABLE tienda
  ADD CONSTRAINT tienda_ibfk_1 FOREIGN KEY (PrendaID) 
  REFERENCES prendas (PrendaID) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT tienda_ibfk_2 FOREIGN KEY (ProveedorID) 
  REFERENCES proveedores (ProveedorID) ON DELETE NO ACTION ON UPDATE CASCADE;

-- Filtros para la tabla Usuariosys

ALTER TABLE usuariosys
  ADD CONSTRAINT usuariosys_ibfk_1 
FOREIGN KEY (CuentaID) 
REFERENCES cuentas (CuentaID) ON DELETE NO ACTION ON UPDATE CASCADE;
