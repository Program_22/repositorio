<?php
require_once( "egcc.php" );
fnSessionStart();
if( !$_SESSION["codigo"] ) {
	fnRedirect( "default.php" );
	return;
}
$canasta = $_SESSION["canasta"];
if( is_null( $canasta ) ) {
	fnShowMsg( "Mensaje", "Su canasta esta vac�a." );
	return;
}
ksort( $canasta );
$cn = fnConnect( $msg );
if(!$cn){
	fnShowMsg( "ERROR", $msg );
	return;
}
say("<center><h2>Orden de Pedido por Confirmar</h2></center>");
say("<table width='400' align='center'>");
say("<tr height=25>");
say("<th width=30 align=center valign=middle>C�digo</th>");
say("<th width=150 align=center valign=middle>Nombre</th>");
say("<th width=70 align=center valign=middle>Cantidad</th>");
say("<th width=70 align=center valign=middle>Precio</th>");
say("<th width=70 align=center valign=middle>Subtotal</th>");
say("</tr>");
$total = 0;
foreach ( $canasta as $item => $valor ) {
	$sql = "select PrendaID as CodigoPrenda, Nombre as Nombre,";
	$sql .= "Precio as Precio ";
	$sql .= "from Prendas as P ";
	$sql .= "where PrendaID = '$item' ";
	$rs = mysql_query( $sql, $cn );
	$row = mysql_fetch_assoc( $rs );
	$subtotal = $row["Precio"] * $valor;
	$total += $subtotal;
	say("<tr>");
	say("<td align=center>".$row["CodigoPrenda"]."</td>");
	say("<td align=left>".$row["Nombre"]."</td>");
	say("<td align=center>".$valor."</td>");
	say("<td align=right>".$row["Precio"]."</td>");
	say("<td align=right>".$subtotal."</td>");
	say("</tr>");
}
say("<tr height=25>");
say("<th align=left valign=middle colspan=4>Total</th>");
say("<th align=right valign=middle>$total</th>");
say("<th align=right valign=middle> </th>");
say("</tr>");
say("</table>");
?>
<TABLE align="center">
<TR>
    <TD width="200" align="center" valign="middle">
    <?php say(fnLink("default.php?op=6","","Confirmar Compra","Confirmar Compra")); ?>
    </TD>
</TR>
</TABLE>