<?php require "egcc.php"; ?>
<link rel="stylesheet" href="egcc.css" type="text/css">
<body>
<?php
// Conexi�n con la base de datos
$cn = fnConnect( $msg );
// Verificar la conexi�n con la base de datos
if( !$cn ) {
	fnShowMsg( "Error", $msg );
	say( "</body>" );
	return;
}
say( "<h1>Registro de Nuevo Cliente</h1>" );
if( !$_POST["seguro"] ) {
// ******************************
// Formulario de Ingreso de Datos
// ******************************
?>
	<form method="POST" action="registroClientes.php">
	<table width="300" border='1'>
	<tr>
	<td> Cuenta de Acceso </td>
	<td> <input type="text" name="CuentaAcceso" size=20> </td>
	</tr>
    <tr>
	<td> Contrase�a </td>
	<td> <input type="password" name="ClaveAcceso" size=20> </td>
	</tr>
    <td> Tarjeta Cr�dito </td>
	<td> 
		<select name="tarj" size="1">
		<?php
		$sql = "select TarjetaID, NombreTarjeta from tarjeta";
		$rs = mysql_query( $sql, $cn );
		while( $row = mysql_fetch_row( $rs ) ) {
			$op = "<option value='" . $row[0] . "'>" . $row[1] . "</option>";
			say( $op );
		}
		?>
		</select> 
	</td>
	</tr>
    <tr>
	<td>N�mero de Tarjeta </td>
	<td> <input type="text" name="NroTarjeta" size=20> </td>
	</tr>
    <tr>
	<td> Nombre en Tarjeta </td>
	<td> <input type="text" name="NombreTarjeta" size=50> </td>
	</tr>
	<tr>
   	<td> Fecha Caducidad </td>
	<td> <input type="text" name="FechaCaducidad" size=20> </td>
	</tr>
    <tr>
   	<td> L�mite Cr�dito</td>
	<td> <input type="text" name="LimiteCredito" size=10> </td>
	</tr>
	<td> Nombres </td>
	<td> <input type="text" name="Nombres" size=20> </td>
	</tr>
	<tr>
	<td> Apellidos </td>
	<td> <input type="text" name="Apellidos" size=25> </td>
	</tr>
	<tr>
	<td> Email </td>
	<td> <input type="text" name="Email" size=50> </td>
	</tr>
	<tr>
    <tr>
	<td> Direcci�n </td>
	<td> <input type="text" name="Direccion" size=50> </td>
	</tr>
    <tr>
	<td> Ciudad </td>
	<td> <input type="text" name="Ciudad" size=50> </td>
	</tr>
    <tr>
	<td>Provincia </td>
	<td> <input type="text" name="Provincia" size=50> </td>
	</tr>
	<tr>
	<td> C�digo Postal </td>
	<td> <input type="text" name="CodPostal" size=10> </td>
	</tr>
    <tr>
	<td> Tel�fono </td>
	<td> <input type="text" name="Telefono" size=20> </td>
	</tr>
    <tr>
	<td> Fax </td>
	<td> <input type="text" name="Fax" size=20> </td>
	</tr>
    <tr>
	<td> Pais </td>
	<td> <input type="text" name="Pais" size=50> </td>
	</tr>

	<tr>
	<td colspan="2" align="center">
	<input type="hidden" name="seguro" value="12345">
	<input type="reset" value="Limpiar">
	<input type="submit" value="Enviar">
	</td>
	</table>
	</form>
<?php
} else {
// *******************
// Procesar Formulario
// *******************
	// Captura de Datos
    $Cuenta=$_POST["CuentaAcceso"];
	$Clave=$_POST["ClaveAcceso"];
	$_TarjetaID=$_POST["tarj"];
    $_NroTarjeta=$_POST["NroTarjeta"];
    $_NombTarjeta=$_POST["NombreTarjeta"];
	$_FCaducidad=$_POST["FechaCaducidad"];
	$_LCredito=$_POST["LimiteCredito"];
	$_Nomb=$_POST["Nombres"];
    $_Apel=$_POST["Apellidos"];
    $_Correo=$_POST["Email"];
	$_Direc=$_POST["Direccion"];
	$_Ciud=$_POST["Ciudad"];
	$_Prov=$_POST["Provincia"];
	$_CPostal=$_POST["CodPostal"];
	$_Telef=$_POST["Telefono"];
	$_FaxClte=$_POST["Fax"];
    $_PaisClte=$_POST["Pais"];
	// Inicio de Transaccion
	mysql_query( "begin", $cn );
	// Generar Codigo
	$sql = "select valor from control ";
	$sql .= "where parametro = 'clientes'";
	$rs = mysql_query( $sql, $cn );
	$cont = mysql_result( $rs, 0, 0 );
	$sql = "update control set valor = valor + 1 ";
	$sql .= "where parametro = 'clientes'";
	$rpta = mysql_query( $sql, $cn );
	if( !rpta ) {
		mysql_query( "rollback", $cn );
		fnShowMsg( "Error", "No se puede generar el su c�digo." );
		say( "</body>" );
		return;
	}
	//Registrar la Cuenta de Acceso
    $sql = "insert into cuentas(CuentaID,CategoriaCtaID,UltimaMod,FechaAlta,PalabraPaso,Flags) ";
    $sql.= "values('$Cuenta',1,curdate(),curdate(),'$Clave',0)";
	$rpta = mysql_query( $sql, $cn );
    if( !rpta ) {
		mysql_query( "rollback", $cn );
		fnShowMsg( "Error", "No se puede generar la cuenta de Acceso, Verificar Datos" );
		say( "</body>" );
		return;
	}
    $codigo = $cont + 1;
	// Registrar el Cliente
	$sql = "insert into clientes(ClienteID, CuentaID, TarjetaID,Nombre,Apellidos,";
	$sql .= "Email,Direccion,Ciudad,Provincia,CodigoPostal,Telefono,FAX,NumeroTarjeta,";
	$sql .= "NombreEntarjeta,FechaCaducidad,LimiteCredito,UltimaMod,FechaCreacion,Pais,Flags) ";
	$sql .="values ( $codigo, '$Cuenta',$_TarjetaID,'$_Nomb','$_Apel','$_Correo',";
	$sql .= "'$_Direc','$_Ciud','$_Prov','$_CPostal','$_Telef','$_FaxClte','$_NroTarjeta','$_NombTarjeta',";
	$sql .="$_FCaducidad,$_LCredito,curdate(),curdate(),'$_PaisClte',0)";
	$rpta = mysql_query( $sql, $cn );
	if(!$rpta){
		mysql_query( "rollback", $cn );
		$msg = "Datos ingresados no son correctos.<br>";
		$msg .= "SQL: $sql";
		fnShowMsg( "Error", $msg );
		say( "</body>" );
		return;
	}
	mysql_query( "commit", $cn );
	$msg = "Cliente registrado correctamente.<br>";
	$msg .= "C�digo Generado: $codigo<br>";
	fnShowMsg( "Mensaje", $msg );
	say( fnLink( "registroClientes.php","","Nuevo Empleado", "Nuevo Empleado" ) );
   
}	
?>
</body>