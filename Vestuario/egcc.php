<?php

/*
 * Función para conectarse a la base datos
*/
function fnConnect( &$msg ){
	$cn=mysql_connect("localhost","root","1234");
	if(!$cn){
		$msg = "Error en la conexión.";
		return 0;
	}
	$n = mysql_select_db("vestuario",$cn);
	if(!$n){
		$msg = "Base de datos no existe.";
		mysql_close($cn);
		return 0;
	}
	return $cn;
}

/*
 * Función que imprime un mensaje en el navegador.
 * 
*/

function say($cad){
	echo $cad . "\n";
}

/*
 * Función que retorna la fecha actual.
*/
function fnNow(){
        $fecha="";        
        $timezone  = -5;
	$hoy = getdate(time());
	$fecha = $hoy["mday"]."-".$hoy["mon"]."-".$hoy["year"]."\n";
        $fecha=$fecha."Hora:".gmdate("H:i:s A", time()+ 3600*($timezone+date("I")));
	return $fecha;
       
}

/*
 * Función que inicia una sesión.
*/
function fnSessionStart(){
	//session_start();
	if(!isset($_SESSION["codigo"])){
		$_SESSION["codigo"] = "";
	    $_SESSION["nombre"] = "Usuario An"."&oacute"."nimo";
	    $_SESSION["canasta"] = null;
	    $_SESSION["seguro"] =  fnRnd( 1000, 9999 );
	}
}

/*
 * Función que finaliza una sesión.
*/
function fnSessionEnd(){
	session_unset();
	session_destroy();
}

/*
 * Funci�n que muestra un mensaje.
*/
function fnShowMsg($title,$msg){
    say("<table align='center' width='300' border='1'>");
    say("<tr>");
    say("<th>$title</th>");
    say("</tr>");
    say("<tr>");
	say("<td>$msg</td>");    
    say("</tr>");
    say("</table>");
}

/*
 * Función que muestra una línea de cabecera.
*/
function fnHeader(){
	$usuario = $_SESSION["nombre"];
	say("<table width='760' cellspacing='0' height='30'>");
    say("<tr>");
    say("<th align=left valign=middle>Usuario: $usuario</th>");
    say("<th align=right valign=middle>Fecha: ".fnNow()."</th>");
    say("</tr>");
    say("</table>");
}

/*
 * Función que muestra un botón para regresar a la página anterior.
*/
function fnBack(){
    return "<input type='button' Value='Back' onClick='history.back();'>";
}

function fnRedirect($pagina){
    $cad = "Location: http://" . $_SERVER['HTTP_HOST']
        . dirname($_SERVER['PHP_SELF']) . "/$pagina";
    header( $cad, True );
}

/*
 * Función que retorna un link.
*/
function fnLink($link,$target,$mouseover,$msg){
	$cad = "<A href='$link' target='$target' ";
	$cad .= "onmouseout=\"self.status='';return true\" ";
	$cad .= "onmouseover=\"self.status='$mouseover' ;return true\">";
	$cad .= "$msg</A>";
	return $cad;
}

/*
 * Función que retorna el menú de la aplicación.
*/
function fnMenu(){
	$usuario = $_SESSION["nombre"];
	$cad  = "<table border='1' width='100' style='font-size:10px'>";
	$cad .= "<tr><td align='center'>" ;
	if( $_SESSION["codigo"] ) {
		$cad .= fnLink("cerrar.php","","Terminar de Sesi�n","Terminar");
	} else {
		$cad .= fnLink("default.php?op=1","","Inicio de Sesi�n","Inicio");
	}
	$cad .= "</td></tr>";

	$cad .= "<tr><td align='center'>" ;
	$cad .= fnLink("default.php?op=2","","Mostrar Catalogo","Catalogo");
	$cad .= "</td></tr>";
	
	if( $_SESSION["codigo"] ) {
		$cad .= "<tr><td align='center'>" ;
		$cad .= fnLink("default.php?op=3","","Mostrar Canasta","Canasta");
		$cad .= "</td></tr>";
		
	}
    if($usuario=='admin') {
        $cad .= "<tr><td align='center'>" ;
		$cad .= fnLink("default.php?op=7","","Mostrar Clientes","Clientes");
		$cad .= "</td></tr>";

        $cad .= "<tr><td align='center'>" ;
		$cad .= fnLink("default.php?op=8","","Mostrar Categorias","Categorias Productos");
		$cad .= "</td></tr>";

		$cad .= "<tr><td align='center'>" ;
		$cad .= fnLink("default.php?op=9","","Mostrar Pedidos","Pedidos");
		$cad .= "</td></tr>";

		$cad .= "<tr><td align='center'>" ;
		$cad .= fnLink("default.php?op=12","","Mostrar Tienda","Tienda Stock");
		$cad .= "</td></tr>";
	}

	$cad .= "</table>" ;

	return $cad;
}


/*
 * Retorna un numero aleatorio entre $minimo y $maximo.
*/
function fnRnd($minimo, $maximo){
    srand((double)microtime()*1000000);
    $randval = rand($minimo, $maximo);
    return $randval;
}

/*
 * Funci�n que imprime las etiquetas de fin de pagina.
 * 
*/
function fnPageEnd(){
    say("</body>");
    say("</html>");
}
?>