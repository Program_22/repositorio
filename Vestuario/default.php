<?php
require_once( "egcc.php" );
fnSessionStart();
?>
<HTML>
<HEAD>
	<TITLE>Moda Fashions Confecciones Meza</TITLE>
	<meta name="Author" content="Dante García Paico">
	<LINK rel="stylesheet" type="text/css" href="egcc.css">
</HEAD>
<BODY>
<?php
require( "titulo.html" );
fnHeader();
?>
<table width="760" cellspacing="0">
<tr>
<td width='100' valign="top">
<?php echo fnMenu(); ?>
</td>
<td valign="middle">
<?php
$op = 2;
if(isset($_GET["op"]) ) {
	$op = $_GET["op"];
}
switch ($op) {
	case 1:
		require( "inicio.php" );
		break;
	case 2:
		require( "catalogo.php" );
		break;
	case 3:
		require( "canasta_list.php" );
		break;
	case 4:
		require( "pagar.php" );
		break;
	case 5:
		require( "canasta_edit.php" );
		break;
	case 6:
		require( "pagar_conf.php" );
	   	break;
	case 7:
		require( "listado_cltes.php" );
	   	break;
	case 8:
		require( "listado_categorias.php" );
	   	break;
	case 9:
		require( "listado_pedidos.php" );
	   	break;
	case 10:
		require( "error.php" );
		break;
    case 11:
		require( "listado_detalles_pedidos.php" );
	   	break;
    case 12:
		require( "listado_tienda.php" );
	   	break;
}
?>
</td>
</tr>
</table>
<?php require( "pie.html" ); ?>
</BODY>
</HTML>
