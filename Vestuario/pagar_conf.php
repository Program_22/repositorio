<?php
require_once( "egcc.php" );
fnSessionStart();
if( !$_SESSION["codigo"] ) {
	fnRedirect( "default.php" );
	return;
}
$canasta = $_SESSION["canasta"];
if( is_null( $canasta ) ) {
	fnShowMsg( "Mensaje", "Su canasta esta vac�a." );
	return;
}
ksort( $canasta );
$cn = fnConnect( $msg );
if(!$cn){
	fnShowMsg( "ERROR", $msg );
	return;
}
// Obtener Datos
$codigo = $_SESSION["codigo"];
//Obtener el C�digo del Cliente
$sql = "select ClienteID from clientes where CuentaID='$codigo'";
$rs = mysql_query($sql, $cn );
$codclte = mysql_result( $rs, 0, 0 );
// Obtener IGV
$sql = "select valor from control where parametro='igv'";
$rs = mysql_query($sql, $cn );
$igv = mysql_result( $rs, 0, 0 );
// Iniciar transacci�n
mysql_query( "BEGIN", $cn );
// Generar Numero de Pedido
$sql = "select valor from control where parametro='pedidos'";
$rs = mysql_query($sql, $cn );
$idped = mysql_result( $rs, 0, 0 );
$sql = "update control set valor = valor + 1 ";
$sql .= "where parametro='pedidos'";
mysql_query( $sql, $cn );
// Pagar Pedido
$sql = "insert into pedidos(PedidoID,EstadoID,FPagoID,EnvioID,ClienteID,FechaPedido,PrecioSIGV,IGV,";
$sql.="TotalCargo,FechaEstado,FechaEnvio,RefEnvio,UltimaMod,FechaCreacion,Flags) ";
$sql.= "values( $idped, 1,2,3,$codclte,curdate(),0,0,0,curdate(),curdate()+2,'Aceptado-Confirmar Envio',";
$sql.= "curdate(),curdate(), 0)";
mysql_query( $sql, $cn );
// Obtener total e insertar detalles
$total = 0;
foreach ( $canasta as $item => $cant ) {
	$sql = "select t.idtienda,t.PrendaID,t.existencias,P.Nombre,P.Precio From tienda t ";
	$sql.="inner join prendas p on t.PrendaID=p.PrendaID ";
	$sql.= "where t.PrendaID = '$item' ";
	$rs = mysql_query( $sql, $cn );
	$row = mysql_fetch_row( $rs );
	if( $cant > $row[2] ) {
		mysql_query( "ROLLBACK", $cn );
		$msg = "El Producto <b>$row[3]</b> no tiene stock suficiente.";
		fnShowMsg( "ERROR", $msg );
		return;
	}
	$subtotal = $row[4] * $cant;
	$total += $subtotal;
	//Capturar C�digo de Tienda
	$codtienda=$row[0];
    // Grabar detalle Pedido
	$sql = "insert into itempedido( PrendaID,idtienda,PedidoID,Unidades,Flags) ";
	$sql .= "values('$item',$codtienda, $idped,$cant,0)";
   	mysql_query( $sql, $cn );
	// Actualizar stock
	$sql = "update tienda set existencias = existencias - $cant ";
	$sql .= "where PrendaID = '$item' ";
	mysql_query( $sql, $cn );
}
// Actualizar pedido
$subtotal = $total * ( 1 + $igv );
$impuesto = $total * $igv;
$sql = "update pedidos set PrecioSIGV = $total, ";
$sql .= "IGV = $impuesto, TotalCargo = $subtotal ";
$sql .= "where PedidoID = $idped ";
mysql_query( $sql, $cn );
mysql_query( "COMMIT", $cn );
$_SESSION["canasta"] = null;
say( "<center><h2>Pedido Nro. $idped</h2><center>" );
$msg = "<h4>Se�or: " . $_SESSION["nombre"] . "</h4>";
$msg .= "Su transacci�n se proces� con exito.<br>";
$msg .= "En 48 horas llegar� su pedido.<br><br>";
$msg .= "Gracias por su confianza en nosotros.<br>";
fnShowMsg( "MENSAJE", $msg );
?>