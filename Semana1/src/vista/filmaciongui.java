package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

import Modelo.filmacion;

import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.ScrollPane;
import javax.swing.JSpinner;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Window.Type;

public class filmaciongui extends JFrame implements ActionListener {
	private JTextField txtCodigo;
	private JButton btnProcesar;
	private JLabel lblCodigo;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JTextField txtTitulo;
	private JTextField txtprecio;
	private JButton btnNuevo;
	private JButton btnSalir;
	private JComboBox cboMoneda;
	private JLabel lblNewLabel;
	private JScrollPane sp;
	private JTextArea txtDato;
	private JTextField txtMinuto;
	private JLabel lblNewLabel_5;
	private JLabel lblNewLabel_6;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					filmaciongui frame = new filmaciongui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public filmaciongui() {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		getContentPane().setForeground(Color.GRAY);
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Maylon\\Desktop\\camara.jfif"));
		setTitle("Filmacion");
		setBounds(100, 100, 485, 585);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		{
			txtCodigo = new JTextField();
			txtCodigo.addActionListener(this);
			txtCodigo.setBounds(115, 54, 144, 20);
			getContentPane().add(txtCodigo);
			txtCodigo.setColumns(10);
		}
		{
			btnProcesar = new JButton("Procesar");
			btnProcesar.addActionListener(this);
			btnProcesar.setBounds(30, 265, 89, 23);
			getContentPane().add(btnProcesar);
		}
		{
			lblCodigo = new JLabel("Codigo ");
			lblCodigo.setBounds(26, 57, 46, 14);
			getContentPane().add(lblCodigo);
		}
		{
			lblNewLabel_1 = new JLabel("Titulo");
			lblNewLabel_1.setBounds(26, 99, 46, 14);
			getContentPane().add(lblNewLabel_1);
		}
		{
			lblNewLabel_2 = new JLabel("Moneda");
			lblNewLabel_2.setBounds(30, 176, 46, 14);
			getContentPane().add(lblNewLabel_2);
		}
		{
			lblNewLabel_3 = new JLabel("Precio");
			lblNewLabel_3.setBounds(30, 221, 46, 14);
			getContentPane().add(lblNewLabel_3);
		}
		{
			lblNewLabel_4 = new JLabel("Datos");
			lblNewLabel_4.setBounds(30, 320, 46, 14);
			getContentPane().add(lblNewLabel_4);
		}
		{
			txtTitulo = new JTextField();
			txtTitulo.setColumns(10);
			txtTitulo.setBounds(115, 91, 144, 31);
			getContentPane().add(txtTitulo);
		}
		{
			txtprecio = new JTextField();
			txtprecio.setColumns(10);
			txtprecio.setBounds(115, 215, 144, 20);
			getContentPane().add(txtprecio);
		}
		{
			btnNuevo = new JButton("Nuevo");
			btnNuevo.addActionListener(this);
			btnNuevo.setBounds(155, 265, 89, 23);
			getContentPane().add(btnNuevo);
		}
		{
			btnSalir = new JButton("Salir");
			btnSalir.addActionListener(this);
			btnSalir.setBounds(279, 265, 89, 23);
			getContentPane().add(btnSalir);
		}
		{
			cboMoneda = new JComboBox();
			cboMoneda.addActionListener(this);
			cboMoneda.setModel(new DefaultComboBoxModel(new String[] {"Soles ", "Dolares"}));
			cboMoneda.setBounds(115, 173, 86, 20);
			getContentPane().add(cboMoneda);
		}
		{
			lblNewLabel = new JLabel("Minutos");
			lblNewLabel.setBounds(26, 139, 46, 14);
			getContentPane().add(lblNewLabel);
		}
		{
			sp = new JScrollPane();
			sp.setBounds(30, 345, 338, 174);
			getContentPane().add(sp);
			{
				txtDato = new JTextArea();
				sp.setViewportView(txtDato);
			}
		}
		{
			txtMinuto = new JTextField();
			txtMinuto.setBounds(115, 136, 144, 20);
			getContentPane().add(txtMinuto);
			txtMinuto.setColumns(10);
		}
		{
			lblNewLabel_5 = new JLabel("New label");
			lblNewLabel_5.setIcon(new ImageIcon("C:\\Users\\Maylon\\Desktop\\camara1"));
			lblNewLabel_5.setBounds(299, 83, 124, 95);
			getContentPane().add(lblNewLabel_5);
		}
		{
			lblNewLabel_6 = new JLabel("Filmacion ");
			lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNewLabel_6.setBounds(202, 11, 71, 32);
			getContentPane().add(lblNewLabel_6);
		}

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == txtCodigo) {
			do_txtCodigo_actionPerformed(e);
		}
		if (e.getSource() == cboMoneda) {
			do_comboBox_actionPerformed(e);
		}
		if (e.getSource() == btnProcesar) {
			do_btnProcesar_actionPerformed(e);
		}
		if (e.getSource() == btnNuevo) {
			do_btnNuevo_actionPerformed(e);
		}
		if (e.getSource() == btnSalir) {
			do_btnSalida_actionPerformed(e);
		}
	}
	protected void do_btnSalida_actionPerformed(ActionEvent e) {
dispose();
	}
	protected void do_btnNuevo_actionPerformed(ActionEvent e) {
		txtDato.setText("");
		txtCodigo.setText("");
		txtTitulo.setText("");
		txtMinuto.setText("");
		txtprecio.setText("");
	}
	
	protected void do_btnProcesar_actionPerformed(ActionEvent e) {
		filmacion a=new filmacion();
		a.codigo=Integer.parseInt(txtCodigo.getText());
		a.Titulo=txtTitulo.getText();
		a.minuto=Integer.parseInt(txtMinuto.getText());
		a.precio=Double.parseDouble(txtprecio.getText());
	
		imprimir("codigo:" + a.codigo);
		imprimir("Titulo:" + a.Titulo);
		imprimir("Minutos filmados:" + a.minuto);
		imprimir("Monto a pagar:" + a.precio);
		imprimir("---------------------");

	}
	void imprimir(String s) {
		txtDato.append(s + "\n");
	}
	protected void do_comboBox_actionPerformed(ActionEvent e) {
		double n1=Double.parseDouble(txtMinuto.getText());
		double cambio=0;
		
		int i=cboMoneda.getSelectedIndex();
		if(i==0) {
		cambio=n1;
	
		}
		else if(i==1) {
			cambio=n1/3.5;
		}
		txtprecio.setText(String.valueOf(cambio));
	}
		
	protected void do_txtCodigo_actionPerformed(ActionEvent e) {
	
	}
}

