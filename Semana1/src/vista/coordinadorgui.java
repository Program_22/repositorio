package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Modelo.coordinador;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class coordinadorgui extends JFrame implements ActionListener {
	private JLabel lblNewLabel;
	private JLabel lblNombre;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JScrollPane scrollPane;
	private JTextArea txtDato;
	private JTextField txtCodigo;
	private JTextField txtNombre;
	private JTextField txtNum;
	private JLabel lblNewLabel_1;
	private JButton btnProcesar;
	private JButton btnNuevo;
	private JButton btnSalir;
	private JComboBox cboCategoria;
	private JLabel lblNewLabel_4;
	private JTextField txtCategoria;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					coordinadorgui frame = new coordinadorgui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public coordinadorgui() {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setForeground(Color.LIGHT_GRAY);
		getContentPane().setForeground(Color.GRAY);
		setTitle("Coordinador");
		setBounds(100, 100, 516, 507);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		{
			lblNewLabel = new JLabel("CODIGO");
			lblNewLabel.setBounds(22, 54, 60, 14);
			getContentPane().add(lblNewLabel);
		}
		{
			lblNombre = new JLabel("NOMBRE");
			lblNombre.setBounds(22, 94, 60, 14);
			getContentPane().add(lblNombre);
		}
		{
			lblNewLabel_2 = new JLabel("CATEGORIA");
			lblNewLabel_2.setBounds(22, 129, 86, 14);
			getContentPane().add(lblNewLabel_2);
		}
		{
			lblNewLabel_3 = new JLabel("NUM.CELULAR");
			lblNewLabel_3.setBounds(22, 167, 86, 14);
			getContentPane().add(lblNewLabel_3);
		}
		{
			scrollPane = new JScrollPane();
			scrollPane.setBounds(22, 288, 413, 156);
			getContentPane().add(scrollPane);
			{
				txtDato = new JTextArea();
				scrollPane.setViewportView(txtDato);
			}
		}
		{
			txtCodigo = new JTextField();
			txtCodigo.setBounds(119, 51, 127, 20);
			getContentPane().add(txtCodigo);
			txtCodigo.setColumns(10);
		}
		{
			txtNombre = new JTextField();
			txtNombre.setBounds(119, 91, 184, 20);
			getContentPane().add(txtNombre);
			txtNombre.setColumns(10);
		}
		{
			txtNum = new JTextField();
			txtNum.setColumns(10);
			txtNum.setBounds(119, 157, 184, 20);
			getContentPane().add(txtNum);
		}
		{
			lblNewLabel_1 = new JLabel("Datos");
			lblNewLabel_1.setBounds(22, 263, 46, 14);
			getContentPane().add(lblNewLabel_1);
		}
		{
			btnProcesar = new JButton("PROCESAR");
			btnProcesar.addActionListener(this);
			btnProcesar.setBounds(22, 220, 117, 23);
			getContentPane().add(btnProcesar);
		}
		{
			btnNuevo = new JButton("NUEVO");
			btnNuevo.addActionListener(this);
			btnNuevo.setBounds(186, 220, 89, 23);
			getContentPane().add(btnNuevo);
		}
		{
			btnSalir = new JButton("SALIR");
			btnSalir.addActionListener(this);
			btnSalir.setBounds(346, 220, 89, 23);
			getContentPane().add(btnSalir);
		}
		{
			cboCategoria = new JComboBox();
			cboCategoria.addActionListener(this);
			cboCategoria.setModel(new DefaultComboBoxModel(new String[] {"CATEGORIA ", "CATEGORIA 1", "CATEGORIA 2"}));
			cboCategoria.setBounds(119, 126, 117, 20);
			getContentPane().add(cboCategoria);
		}
		{
			lblNewLabel_4 = new JLabel("Ganancia");
			lblNewLabel_4.setBounds(22, 195, 86, 14);
			getContentPane().add(lblNewLabel_4);
		}
		{
			txtCategoria = new JTextField();
			txtCategoria.setColumns(10);
			txtCategoria.setBounds(119, 189, 184, 20);
			getContentPane().add(txtCategoria);
		}

	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btnNuevo) {
			do_btnNuevo_actionPerformed(arg0);
		}
		if (arg0.getSource() == btnSalir) {
			do_btnSalir_actionPerformed(arg0);
		}
		if (arg0.getSource() == cboCategoria) {
			do_cboCoordinador_actionPerformed(arg0);
		}
		if (arg0.getSource() == btnProcesar) {
			do_btnProcesar_actionPerformed(arg0);
		}
	}
	protected void do_btnProcesar_actionPerformed(ActionEvent arg0) {
		coordinador d=new coordinador();
		d.codigo=Integer.parseInt(txtCodigo.getText());
		d.nombre=txtNombre.getText();
		d.numerodecelular=Integer.parseInt(txtNum.getText());
		d.categoria=Integer.parseInt(txtCategoria.getText());
		
		imprimir("Codigo :"+ d.codigo);
		imprimir("Nombre :" + d.nombre);
		imprimir("Num.Celular :" + d.numerodecelular);
		imprimir("Categoria :" + d.categoria);
		imprimir("---------------------");
		
		
		
		
		}
	void imprimir(String s) {
		txtDato.append(s +"\n");
	}
	
	protected void do_cboCoordinador_actionPerformed(ActionEvent arg0) {
	
		int cambio=0;
		int i=cboCategoria.getSelectedIndex();
		if(i==0) {
		cambio=7000;
		}
		else if(i==1) {
		cambio=6000;
		}
		else if(i==2) {
			cambio=5000;
			}
		txtCategoria.setText(String.valueOf(cambio));
	}
	protected void do_btnSalir_actionPerformed(ActionEvent arg0) {
		dispose();
	}
	protected void do_btnNuevo_actionPerformed(ActionEvent arg0) {
		txtCodigo.setText("");
		txtNombre.setText("");
		txtNum.setText("");
		txtCategoria.setText("");
		
	}
	}
	

