package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JTextField;

import Modelo.expositor;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class expositorgui extends JFrame implements ActionListener {
	private JLabel lblNewLabel;
	private JTextField txtCodigo;
	private JLabel lblNombre;
	private JTextField txtNombre;
	private JLabel lblHaberBasico;
	private JTextField txtTarifa;
	private JTextField txtHoras;
	private JLabel lblAfp;
	private JButton btnProcesar;
	private JButton btnNuevo;
	private JButton btnSalir;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane_1;
	private JTextArea txtDato;
	private JLabel lblNewLabel_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					expositorgui frame = new expositorgui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public expositorgui() {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setTitle("Expositor");
		setBounds(100, 100, 434, 586);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		{
			lblNewLabel = new JLabel("CODIGO");
			lblNewLabel.setBounds(10, 50, 111, 14);
			getContentPane().add(lblNewLabel);
		}
		{
			txtCodigo = new JTextField();
			txtCodigo.setBounds(58, 47, 150, 20);
			getContentPane().add(txtCodigo);
			txtCodigo.setColumns(10);
		}
		{
			lblNombre = new JLabel("NOMBRE y APELLIDO");
			lblNombre.setBounds(10, 91, 158, 14);
			getContentPane().add(lblNombre);
		}
		{
			txtNombre = new JTextField();
			txtNombre.setBounds(10, 116, 292, 20);
			getContentPane().add(txtNombre);
			txtNombre.setColumns(10);
		}
		{
			lblHaberBasico = new JLabel("TARIFA*HORA");
			lblHaberBasico.setBounds(10, 151, 111, 14);
			getContentPane().add(lblHaberBasico);
		}
		{
			txtTarifa = new JTextField();
			txtTarifa.setColumns(10);
			txtTarifa.setBounds(10, 176, 126, 20);
			getContentPane().add(txtTarifa);
		}
		{
			txtHoras = new JTextField();
			txtHoras.setColumns(10);
			txtHoras.setBounds(10, 232, 67, 20);
			getContentPane().add(txtHoras);
		}
		{
			lblAfp = new JLabel("HORAS");
			lblAfp.setBounds(10, 207, 67, 14);
			getContentPane().add(lblAfp);
		}
		{
			btnProcesar = new JButton("PROCESAR");
			btnProcesar.addActionListener(this);
			btnProcesar.setBounds(10, 290, 102, 23);
			getContentPane().add(btnProcesar);
		}
		{
			btnNuevo = new JButton("NUEVO");
			btnNuevo.addActionListener(this);
			btnNuevo.setBounds(130, 290, 102, 23);
			getContentPane().add(btnNuevo);
		}
		{
			btnSalir = new JButton("SALIR");
			btnSalir.addActionListener(this);
			btnSalir.setBounds(253, 290, 116, 23);
			getContentPane().add(btnSalir);
		}
		{
			scrollPane = new JScrollPane();
			scrollPane.setBounds(97, 499, 176, -36);
			getContentPane().add(scrollPane);
		}
		{
			scrollPane_1 = new JScrollPane();
			scrollPane_1.setBounds(10, 371, 359, 159);
			getContentPane().add(scrollPane_1);
			{
				txtDato = new JTextArea();
				scrollPane_1.setViewportView(txtDato);
			}
		}
		{
			lblNewLabel_2 = new JLabel("Datos");
			lblNewLabel_2.setBounds(10, 346, 46, 14);
			getContentPane().add(lblNewLabel_2);
		}

	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btnNuevo) {
			do_btnNuevo_actionPerformed(arg0);
		}
		if (arg0.getSource() == btnProcesar) {
			do_btnProcesar_actionPerformed(arg0);
		}
		if (arg0.getSource() == btnSalir) {
			do_btnSalir_actionPerformed(arg0);
		}
	}
	protected void do_btnSalir_actionPerformed(ActionEvent arg0) {
		dispose();
	}
	protected void do_btnProcesar_actionPerformed(ActionEvent arg0) {

		
expositor b=new expositor();
b.codigo=Integer.parseInt(txtCodigo.getText());
b.tarifaporhora=Integer.parseInt(txtTarifa.getText());
b.horastrabajada=Integer.parseInt(txtHoras.getText());
b.nombre=txtNombre.getText();

imprimir("Codigo: " + b.codigo);
imprimir("Nombre: " + b.nombre);
imprimir("Tarifa*hora:  " + b.tarifaporhora);
imprimir("Horas: " + b.horastrabajada);


imprimir("sueldo:  " + b.Sueldo());
imprimir("---------------------------");



	}
 void imprimir(String a) {
		txtDato.append(a +"\n");
	}
	protected void do_btnNuevo_actionPerformed(ActionEvent arg0) {
		txtCodigo.setText("");
		txtNombre.setText("");
		txtTarifa.setText("");
		txtHoras.setText("");
	}
}
