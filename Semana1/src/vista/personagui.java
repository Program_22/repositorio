package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import Modelo.persona;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class personagui extends JFrame implements ActionListener {
	private JLabel lblNewLabel;
	private JTextField txtEstatura;
	private JLabel lblNewLabel_1;
	private JTextField txtPeso;
	private JScrollPane scrollPane;
	private JTextArea txtDato;
	private JButton btnProcesar;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JButton btnSalir;
	private JButton btnNuevo;
	private JTextField txtApellido;
	private JTextField txtNombre;
	private JTextField txtEdad;
	private JLabel lblNewLabel_4;
	private JLabel lblNewLabel_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					personagui frame = new personagui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public personagui() {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setTitle("Persona");
		setBounds(100, 100, 533, 569);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		{
			lblNewLabel = new JLabel("ESTATURA");
			lblNewLabel.setBounds(10, 204, 65, 14);
			getContentPane().add(lblNewLabel);
		}
		{
			txtEstatura = new JTextField();
			txtEstatura.setBounds(127, 201, 86, 20);
			getContentPane().add(txtEstatura);
			txtEstatura.setColumns(10);
		}
		{
			lblNewLabel_1 = new JLabel("PESO");
			lblNewLabel_1.setBounds(10, 235, 46, 14);
			getContentPane().add(lblNewLabel_1);
		}
		{
			txtPeso = new JTextField();
			txtPeso.setBounds(127, 232, 86, 20);
			getContentPane().add(txtPeso);
			txtPeso.setColumns(10);
		}
		{
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 333, 423, 172);
			getContentPane().add(scrollPane);
			{
				txtDato = new JTextArea();
				scrollPane.setViewportView(txtDato);
			}
		}
		{
			btnProcesar = new JButton("PROCESAR");
			btnProcesar.addActionListener(this);
			btnProcesar.setBounds(50, 281, 89, 23);
			getContentPane().add(btnProcesar);
		}
		{
			lblNewLabel_2 = new JLabel("APELLIDO");
			lblNewLabel_2.setBounds(10, 119, 65, 14);
			getContentPane().add(lblNewLabel_2);
		}
		{
			lblNewLabel_3 = new JLabel("NOMBRE");
			lblNewLabel_3.setBounds(10, 82, 65, 14);
			getContentPane().add(lblNewLabel_3);
		}
		{
			btnSalir = new JButton("SALIR");
			btnSalir.addActionListener(this);
			btnSalir.setBounds(344, 281, 89, 23);
			getContentPane().add(btnSalir);
		}
		{
			btnNuevo = new JButton("NUEVO");
			btnNuevo.setBounds(198, 281, 89, 23);
			getContentPane().add(btnNuevo);
		}
		{
			txtApellido = new JTextField();
			txtApellido.setColumns(10);
			txtApellido.setBounds(127, 116, 148, 20);
			getContentPane().add(txtApellido);
		}
		{
			txtNombre = new JTextField();
			txtNombre.setColumns(10);
			txtNombre.setBounds(127, 79, 148, 20);
			getContentPane().add(txtNombre);
		}
		{
			txtEdad = new JTextField();
			txtEdad.setColumns(10);
			txtEdad.setBounds(127, 159, 86, 20);
			getContentPane().add(txtEdad);
		}
		{
			lblNewLabel_4 = new JLabel("EDAD");
			lblNewLabel_4.setBounds(10, 162, 65, 14);
			getContentPane().add(lblNewLabel_4);
		}
		{
			lblNewLabel_5 = new JLabel("Datos");
			lblNewLabel_5.setBounds(50, 308, 46, 14);
			getContentPane().add(lblNewLabel_5);
		}

	}

	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btnSalir) {
			do_btnSalir_actionPerformed(arg0);
		}
		if (arg0.getSource() == btnProcesar) {
			do_btnProcesar_actionPerformed(arg0);
		}
	}

	protected void do_btnProcesar_actionPerformed(ActionEvent arg0) {
		persona c = new persona();
		c.nombre = txtNombre.getText();
		c.apellido = txtApellido.getText();
		c.edad = Integer.parseInt(txtEdad.getText());
		
		c.estatura = Double.parseDouble(txtEstatura.getText());
		c.peso = Double.parseDouble(txtPeso.getText());
	
		imprimir("Nombre: " + c.nombre);
		imprimir("Apellido: " + c.apellido);
		imprimir("Edad: " + c.edad);
		
		imprimir("Estatura:" + c.estatura);
		imprimir("Peso: " + c.peso);
		 imprimir("Condicion: " + c.condicion());
		imprimir("--------------------------------------");
	}

	void imprimir(String s) {
		txtDato.append(s + "\n");

	}

	protected void do_btnSalir_actionPerformed(ActionEvent arg0) {
		dispose();
	}
}
