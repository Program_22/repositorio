#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_N 100

//Declaracion del TDA cola
typedef struct _nodo {
	int valor;
	struct _nodo *siguiente;
} tipoNodo;

typedef tipoNodo *pNodo; // tipo para declarar nodos a un entero

/* Funciones con colas: */
//inserta un elemento en la cola
void Encola(pNodo *primero, pNodo *ultimo, int v);
//quita un elemento de la cola
int Desencola(pNodo *primero, pNodo *ultimo);
//verifica si la cola esta vacia
int es_vacia (pNodo *pila);
//matriz de adyaciencia
int matriz [MAX_N][MAX_N];
//numero de nodos
int nodos;
//componentes conexas
int comp[MAX_N];

void generamatriz(void); // genera la matriz de adyacencia
//regresa todos los nodos que pudo visitae incluidos el mismo
void recorre (int n, int *conjunto, int v);
//recorre el grafo en  anchura
void anchura (int nodo_inicial, int N);
//encuentra las componentes conexas en un grafo
void comp_conexas (int *comp, int N);

int main (void)
{
	int nodoinicial, numvertices;
	int conjunto [MAX_N];
	int i;
	//a partir del archivo
	generamatriz();
	//se recorre en anchura el grafo comenzando con el nodo 1
	printf ("Recorrido en anchura\n");
	anchura(1,6);
	//se recorre el  grafo en profundidad comenzando con el nodo 1
	printf ("los nodos que se pueden visitar incluidos a el mismo son:\n");
	recorre(0,conjunto,nodos);
	for (i=0;i<6;i++)
		printf ("%d  \n",conjunto [i]);
	printf ("Componentes conexas\n");
	comp_conexas (comp, nodos);
	for (i=0;i<6;i++)
		if (comp[i]!=0)
			printf ("%d  \n",comp [i]);
	
	return 0;
	
}

/************************************************************************
Funciones implementadas para grafos
************************************************************************/

void generamatriz(void)
{
	FILE *pf;  //puntero a archivo
	char caracter; // variable de tipo caracter que va a servir para almacenar
	// los caracteres leidos del archivo
	int valor,x,y;
	
	pf = fopen ("grafos.txt","r"); // se abre el archivo en forma de lectura
	
	//en el caso que no se pueda abrir el archivo se manda un mensaje de error
	if (!pf)
	{
		printf ("ERROR: el fichero  no existe o no se puede abrir\n");
		system ("PAUSE"); //mensaje "presiona una tecla para continuar"
	}
	else
	{
		nodos=int ((caracter=fgetc(pf))-'0'); //se almacena el numero de nodos
		
		printf ("el numero de nodos del grafo es %d\n", nodos);
		while (!feof (pf))
		{
			caracter=fgetc(pf);
			x= int ((caracter=fgetc(pf))-'0'); //nodo arigen
			caracter=fgetc(pf);
			y= int ((caracter=fgetc(pf))-'0');//nodo destino
			caracter=fgetc(pf);
			valor= int ((caracter=fgetc(pf))-'0');//peso del nodo
			if (x<0) break;
			printf ("%d %d %d\n",x,y,valor);
			matriz[x][y]=valor; //se almacena en la matriz de adyacencia los
			//valores obtenidos
		}
		fclose (pf);//se cierra el fichero
		
	}
	
}

void recorre (int n, int *conjunto, int v)
{
	pNodo c=NULL, ultimo=NULL;
	int w,i;
	memset (conjunto, 0, sizeof (int)*v); //Coloca ceros a todas las casillas del arreglo
	Encola (&c,&ultimo,n);//coloca en la cola el nodo inicial
	while (es_vacia(&c))//mientras la cola no este vacia
	{
		w = Desencola (&c,&ultimo);//desencola el vertice de la cola
		
		for (i=0;i<v;i++)//mientras i sea menor que el numero de vertices
		{
			//visita todos los nodoa a los cuales puede acceder desde el
			//mismo
			if (matriz[w][i] != 0 && conjunto [i]==0)
			{
				Desencola (&c,&ultimo);
				conjunto[i]=0;
			}
		}
		
	}
}

void anchura (int inicial, int N)
{
	int visitas [MAX_N];
	pNodo c=NULL,ultimo=NULL;
	int w,i;
	
	memset (visitas,0, sizeof (visitas));//coloca 0's a todas las casillas del arreglo
	visitas [inicial]=1;//paso 1
	Encola (&c,&ultimo,inicial);//paso 2
	
	while (es_vacia(&c)) //paso 3
	{
		w=Desencola (&c,&ultimo);
		printf ("%d\n",w); //paso 5
		
		for (i=0;i<N;i++)
		{
			if (matriz[w][i] != 0 && visitas[i]==0)
			{
				Encola (&c,&ultimo,i);
				visitas[i]=1;
			}
		}
	}
}

void comp_conexas (int *comp, int N)
{
	int i, j, k;
	int Aux[MAX_N];
	memset (comp, 0, sizeof(int)*N);
	i=1;j=0;
	
	for (i=0;j<N;j++)
		if (comp[j]==0)
			recorre(j,Aux,N);
	
	for (k=0;kvalor = v;
	/* Este ser� el �ltimo nodo, no debe tener siguiente */
	nuevo->siguiente = NULL;
	/* Si la cola no estaba vac�a, a�adimos el nuevo a continuaci�n de ultimo */
	if(*ultimo) (*ultimo)->siguiente = nuevo;
	/* Ahora, el �ltimo elemento de la cola es el nuevo nodo */
	*ultimo = nuevo;
	/* Si primero es NULL, la cola estaba vac�a, ahora primero apuntar� tambi�n al nuevo nodo */
	if(!*primero) *primero = nuevo;
}

int Desencola(pNodo *primero, pNodo *ultimo) {
	pNodo nodo; /* variable auxiliar para manipular nodo */
	int v;      /* variable auxiliar para retorno */
	
	/* Nodo apunta al primer elemento de la pila */
	nodo = *primero;
	if(!nodo) return 0; /* Si no hay nodos en la pila retornamos 0 */
	/* Asignamos a primero la direcci�n del segundo nodo */
	*primero = nodo->siguiente;
	/* Guardamos el valor de retorno */
	v = nodo->valor;
	/* Borrar el nodo */
	free(nodo);
	/* Si la cola qued� vac�a, ultimo debe ser NULL tambi�n*/
	if(!*primero) *ultimo = NULL;
	return v;
}

int es_vacia (pNodo *cola)
{
	if (*cola == NULL) return 0;
	return 1;
}
