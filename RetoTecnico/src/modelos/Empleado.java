/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author Alexander
 */
public class Empleado {

    private String Nombre;
    private int Edad;
    private Double Salario;
    private Double Bonificacion;

    public Empleado(String Nombre, int Edad,Double Salario) {
        this.Nombre = Nombre;
        this.Edad = Edad;
        this.Salario= Salario;
    }

    /**
     * @return the Nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @param Nombre the Nombre to set
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    /**
     * @return the Edad
     */
    public int getEdad() {
        return Edad;
    }

    /**
     * @param Edad the Edad to set
     */
    public void setEdad(int Edad) {
        this.Edad = Edad;
    }

    /**
     * @return the Salario
     */
    public Double getSalario() {
        return Salario;
    }

    /**
     * @param Salario the Salario to set
     */
    public void setSalario(Double Salario) {
        this.Salario = Salario;
    }

    /**
     * @return the Bonificacion
     */
    public Double getBonificacion() {
        return Bonificacion;
    }

    /**
     * @param Bonificacion the Bonificacion to set
     */
    public void setBonificacion(Double Bonificacion) {
        this.Bonificacion = Bonificacion;
    }

}
