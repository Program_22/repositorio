/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author Alexander
 */
public class Repartidor extends Empleado{
    private int Zona;

    public Repartidor(String Nombre, int Edad, Double Salario,int Zona) {
        super(Nombre, Edad, Salario);
        this.Zona = Zona;
    }
    public void AplicaRegla(){
      Regla();   
    }
    private void Regla(){
        if(getEdad()<25 && this.Zona==3){
            
            setBonificacion(15000.0);
           
        }else{
            setBonificacion(0.0);
            
            
        }
        
        
        
    }

    public int getZona() {
        return Zona;
    }

    public void setZona(int Zona) {
        this.Zona = Zona;
    }

}
