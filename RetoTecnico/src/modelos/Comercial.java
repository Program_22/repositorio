/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author Alexander
 */
public class Comercial extends Empleado {
    private Double ComisionComercial;

    public Comercial(String Nombre,int Edad,Double Salario,Double ComisionComercial) {
        super(Nombre, Edad, Salario);
        this.ComisionComercial = ComisionComercial;
    }
    public void AplicaRegla(){
        Regla();
    }
    
    public void Regla(){
       if(getEdad()<30 && this.ComisionComercial>400000){
            
            setBonificacion(15000.0);
            
            
        }else{
            setBonificacion(0.0);
}
       
    }

    public Double getComisionComercial() {
        return ComisionComercial;
    }

    public void setComisionComercial(Double ComisionComercial) {
        this.ComisionComercial = ComisionComercial;
    }
    
}