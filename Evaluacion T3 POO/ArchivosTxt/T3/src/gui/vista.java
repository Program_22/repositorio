
package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import array.ArregloDetallefactura;
import clases.boleta;
import clases.detallefactura;
import clases.documento;
import clases.factura;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class vista extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JLabel lblCodigo;
	private JLabel lblproducto;
	private JLabel lblprecio;
	private JTextField txtnrocliente;
	private JTextField txtproducto;
	private JScrollPane scpSalida;
	private JTextArea txtSalida;
	private JButton btnAceptar;
	private JButton btnLimpiar;
	

	ArregloDetallefactura a;
	private JButton btnBuscar;
	private JButton btnEliminar;
	private JTextField txtprecio;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vista frame = new vista();
					frame.setVisible(true);
				} catch (Exception e) { 
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public vista() {
		
		setBounds(100, 100, 514, 421);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		lblCodigo = new JLabel("Nro.Cliente");
		lblCodigo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCodigo.setBounds(10, 10, 90, 25);
		getContentPane().add(lblCodigo);
		
		lblproducto = new JLabel("Producto");
		lblproducto.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblproducto.setBounds(10, 40, 90, 25);
		getContentPane().add(lblproducto);
		
		lblprecio = new JLabel("Precio");
		lblprecio.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblprecio.setBounds(10, 70, 90, 25);
		getContentPane().add(lblprecio);
		
		txtnrocliente = new JTextField();
		txtnrocliente.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txtnrocliente.setBounds(110, 10, 80, 25);
		getContentPane().add(txtnrocliente);
		txtnrocliente.setColumns(10);
		
		txtproducto = new JTextField();
		txtproducto.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txtproducto.setColumns(10);
		txtproducto.setBounds(110, 40, 150, 25);
		getContentPane().add(txtproducto);
		
		scpSalida = new JScrollPane();
		scpSalida.setBounds(10, 163, 460, 160);
		getContentPane().add(scpSalida);
		
		txtSalida = new JTextArea();
		txtSalida.setEditable(false);		//HAce que el txtSalida no sea editable
		scpSalida.setViewportView(txtSalida);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(this);
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAceptar.setBounds(370, 10, 100, 25);
		getContentPane().add(btnAceptar);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(this);
		btnLimpiar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnLimpiar.setBounds(370, 130, 100, 25);
		getContentPane().add(btnLimpiar);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(this);
		btnBuscar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnBuscar.setBounds(370, 40, 100, 25);
		getContentPane().add(btnBuscar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(this);
		btnEliminar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnEliminar.setBounds(370, 70, 100, 25);
		getContentPane().add(btnEliminar);
		{
			txtprecio = new JTextField();
			txtprecio.setFont(new Font("Tahoma", Font.PLAIN, 12));
			txtprecio.setColumns(10);
			txtprecio.setBounds(110, 70, 150, 25);
			getContentPane().add(txtprecio);
		}
		
		
		a = new ArregloDetallefactura();

	}
	public void actionPerformed(ActionEvent arg0) {
		
		if (arg0.getSource() == btnEliminar) {
			btnEliminaractionPerformed(arg0);
		}
		if (arg0.getSource() == btnBuscar) {
			btnBuscaractionPerformed(arg0);
		}
		if (arg0.getSource() == btnAceptar) {
			btnAceptaractionPerformed(arg0);
		}
		if (arg0.getSource() == btnLimpiar) {
			btnLimpiaractionPerformed(arg0);
		}
	}
	
	protected void btnLimpiaractionPerformed(ActionEvent arg0) {
		txtnrocliente.setText("");
		txtproducto.setText("");
		txtprecio.setText("");
		
	}
	protected void btnAceptaractionPerformed(ActionEvent arg0) {
	documento objD=new documento("1475","10-22-2020");
	listado(objD);
		
	boleta objB=new boleta("1475","22-10-2020","70446435","boleta de venta");
	listado(objB);
	objB.imprimirElectronica();
	listado(objB);
	objB.imprimirFisica();
	listado(objB);
	
	factura objF=new factura("1475","22-10-2020","100704464351");
	listado(objF);
	objF.imprimirFisica();
	listado(objF);
	
	objF.imprimirElectronica1();
	listado(objF);
	objF.imprimirElectronica();
	listado(objF);
	objF.guardarEnArchivo();
	listado(objF);
		int nrocliente;
	double precio;
	String producto;
	nrocliente=Integer.parseInt(txtnrocliente.getText());
	producto=txtproducto.getText();
	precio=Double.parseDouble(txtprecio.getText());
	detallefactura obj=new detallefactura(nrocliente,producto,precio);
	a.agregar(obj);
	listado(obj);
	imprimir("En arreglo" +a.elementos());
	imprimir("--------");
	}
		
		public void listado(detallefactura objE) {
			imprimir("Detalle de factura ");
			imprimir("Cliente :" + objE.getNrocliente());
			imprimir("Producto :" + objE.getProducto());
			imprimir("Precio :" + objE.getPrecio());
			imprimir("cantidad" + objE.getCantidad());
			imprimir("-------------");
			
		}
		public void listado(documento objD) {
			imprimir(">>>Documento ");
			imprimir(objD.datosDocumento());
			imprimir("Impresion D"+objD.archivable());
			imprimir("Impresion D"+ objD.impresion());
			imprimir("-------");
		}
		public void listado(boleta objB) {
			imprimir(">>boleta");
			imprimir("imprimir :"+objB.datosDocumento());
			imprimir("Impresion D"+objB.archivable());
			imprimir("Impresion D"+ objB.impresion());
			imprimir("-------");
		}
		public void listado(factura objB) {
			imprimir(">>boleta");
			imprimir("imprimir :"+objB.datosDocumento());
			imprimir("Impresion D"+objB.archivable());
			imprimir("Impresion D"+ objB.impresion());
			imprimir("-------");
		}
		
	public void imprimir(String s) {
		txtSalida.append(s+"\n");
	}
	protected void btnBuscaractionPerformed(ActionEvent arg0) {
		int nrocliente;
		nrocliente=Integer.parseInt(txtnrocliente.getText());
		detallefactura obj=a.buscar(nrocliente);
		if(obj==null) {
			imprimir("No se encontro la factura :" +nrocliente);
			
		}else {
			imprimir("Resultado de la busqueda :" +nrocliente);
			listado(obj);
				
			}
	}
	protected void btnEliminaractionPerformed(ActionEvent arg0) {
		int nrocliente;
		nrocliente=Integer.parseInt(txtnrocliente.getText());
		if(a.eliminar(nrocliente)) {
			imprimir("Se elimino la factura :"+nrocliente);
		}
	
	}

}
