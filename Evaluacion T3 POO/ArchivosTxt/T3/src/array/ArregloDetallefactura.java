package array;

import java.util.ArrayList;

import clases.detallefactura;

public class ArregloDetallefactura {
ArrayList<detallefactura> a=new ArrayList<detallefactura>();

public int elementos() {
	System.out.println("Ingresando al metodo elementos()\n");
	return a.size();
}
public void agregar(detallefactura e) {
	System.out.println(" Ingresando al metodo agregar");
	a.add(e);
}
public detallefactura obtener(int posicion) {
	return a.get(posicion);
	
}
public detallefactura buscar(int nrocliente) {
	for(int i=0;i<elementos();i++) {
		if(obtener(i).getNrocliente()==nrocliente) {
return obtener(i);			
		}
	}
	return null;
}
public detallefactura encontrar(int nrocliente) {
	for(detallefactura objE:a) {
		if(objE.getNrocliente()==nrocliente){
			return objE;
		}
	}
	return null;
}
public boolean eliminar(int nrocliente) {
	detallefactura objfac=buscar(nrocliente);
	if(objfac !=null) {
		a.remove(objfac);
		return true;
	}
	return false;
}
public String toString()
{
String mensaje="";
for(detallefactura objE:a) {
	mensaje +="Reporte del arreglo";
	mensaje += "\tCodigo: " + objE.getNrocliente() + "\n";
	mensaje += "\tNombre: " + objE.getProducto() + "\n";
	mensaje += "\tFacultad: " + objE.getCantidad() + "\n";
	mensaje += "\tNota1: " + objE.getPrecio() + "\n";

}
return mensaje;
}
}

