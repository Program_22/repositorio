package clases;

public class detallefactura {
	private int nrocliente;
	private String producto;
	private static int cantidad;
	private double precio;
	
	static {
		cantidad=0;
	}
	
	public detallefactura(){
		cantidad++;
	}
	public detallefactura(int nrocliente, String producto, double precio) {
		this.nrocliente = nrocliente;
		this.producto = producto;
		this.precio = precio;
	
	}
	
	public int getNrocliente() {
		return nrocliente;
	}
	public void setNrocliente(int nrocliente) {
		this.nrocliente = nrocliente;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public static int getCantidad() {
		  return detallefactura.cantidad;
	}
	public static void setCantidad(int cantidad) {
		detallefactura.cantidad = cantidad;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
}



