package clases;



public class documento {
	protected String idDocumento;
	protected String fechaDocumento;
	
	
	
	public documento(String idDocumento, String fechaDocumento) {
		this.idDocumento = idDocumento;
		this.fechaDocumento = fechaDocumento;
	
	}
	public String datosDocumento() {
		return "Documento: "+idDocumento+"\n"+
	"Fecha "+fechaDocumento;
		
	}
	public String impresion() {
		
		return idDocumento.toLowerCase()+"."+fechaDocumento.toLowerCase()+"@Upn.edu.pe";
	}
	public String archivable() {
		return idDocumento.toLowerCase()+"."+fechaDocumento.toLowerCase()+"@Upn.edu.pe";
	}
	
}