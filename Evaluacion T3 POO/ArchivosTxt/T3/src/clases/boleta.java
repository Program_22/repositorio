package clases;


import interfaz.imprimible;

public class boleta extends documento implements imprimible{
private String dnicliente;
private String descripcion;
private String impresion;



public boleta(String idDocumento, String fechaDocumento, String dnicliente, String descripcion) {
	super(idDocumento, fechaDocumento);//Herencia
	this.dnicliente = dnicliente;
	this.descripcion = descripcion;
	impresion="";
}

@Override
public String datosDocumento() {
	
	return super.datosDocumento()+"\n"+
	"DNI :"+this.dnicliente+"\n"+
	"Descripicion :"+ this.descripcion+"\n"+
	"Impresion: "+this.impresion;
}


public String getDnicliente() {
	return dnicliente;
}
public void setDnicliente(String dnicliente) {
	this.dnicliente = dnicliente;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}

@Override
public void imprimirFisica() {
this.impresion="Se ha enviado ...a la impresora";
	
}

@Override
public void imprimirElectronica() {
this.impresion="Se ha generado la ...electronica.";
	
}

}
