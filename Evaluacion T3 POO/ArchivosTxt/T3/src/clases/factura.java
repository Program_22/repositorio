package clases;

import interfaz.archivable;
import interfaz.imprimible;

public class factura extends documento implements imprimible,archivable{
private String ruc;
private String impresion;
private String archivo;
private detallefactura objFac;


public factura(String idDocumento, String fechaDocumento,String ruc) {
	super(idDocumento,fechaDocumento);
	this.ruc = ruc;
	this.impresion="";
	this.archivo="";
	objFac=new detallefactura(0, ruc, 0);
}

public detallefactura getObjFac() {
	return objFac;
}

public void setObjFac(detallefactura objFac) {
	this.objFac = objFac;
}

public String getRuc() {
	return ruc;
}

public void setRuc(String ruc) {
	this.ruc = ruc;
}

@Override
public String datosDocumento() {
	
	return super.datosDocumento()+"\n"+
	"RUC :"+this.ruc+"\n"+
	"Impresion"+this.impresion+"\n"+
	"Archivo"+this.archivo+"\n";


}

@Override
public void imprimirFisica() {
this.impresion="Se ha enviado la ...a la impresora";
	
}

@Override
public void imprimirElectronica() {
this.impresion="Se ha generado la --- electronica";
	
}

@Override
public void guardarEnArchivo() {
this.archivo="Se ha enviado la ... al archivo.text";
	
}

@Override
public void imprimirElectronica1() {
this.archivo="Se ha enviado a la al archivo";
	
}

}



