//Desarrollado por Gus.....
package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import arreglo.ArregloEstudiantes;
import clase.Estudiante;

import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejemplo1 extends JFrame implements ActionListener {
	//Variables globales
	private JLabel lblCodigo;
	private JLabel lblNombre;
	private JLabel lblFacultad;
	private JLabel lblNota1;
	private JLabel lblNota2;
	private JTextField txtCodigo;
	private JTextField txtNombre;
	private JComboBox cboFacultad;
	private JTextField txtNota1;
	private JTextField txtNota2;
	private JScrollPane scpSalida;
	private JTextArea txtSalida;
	private JButton btnAceptar;
	private JButton btnLimpiar;
	
	//Arreglo de Estudiantes (variable global)
	ArregloEstudiantes aEstudiantes;
	private JButton btnBuscar;
	private JButton btnEliminar;
	private JButton btnEliminarDesaprobados;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejemplo1 frame = new Ejemplo1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejemplo1() {
		
		setBounds(100, 100, 514, 421);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		lblCodigo = new JLabel("Codigo");
		lblCodigo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCodigo.setBounds(10, 10, 90, 25);
		getContentPane().add(lblCodigo);
		
		lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNombre.setBounds(10, 40, 90, 25);
		getContentPane().add(lblNombre);
		
		lblFacultad = new JLabel("Facultad");
		lblFacultad.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblFacultad.setBounds(10, 70, 90, 25);
		getContentPane().add(lblFacultad);
		
		lblNota1 = new JLabel("Nota1");
		lblNota1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNota1.setBounds(10, 100, 90, 25);
		getContentPane().add(lblNota1);
		
		lblNota2 = new JLabel("Nota2");
		lblNota2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNota2.setBounds(10, 130, 90, 25);
		getContentPane().add(lblNota2);
		
		txtCodigo = new JTextField();
		txtCodigo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txtCodigo.setBounds(110, 10, 80, 25);
		getContentPane().add(txtCodigo);
		txtCodigo.setColumns(10);
		
		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txtNombre.setColumns(10);
		txtNombre.setBounds(110, 40, 150, 25);
		getContentPane().add(txtNombre);
		
		cboFacultad = new JComboBox();
		//Cargando el JComboBox a trav�s de Model
		//comboBox.setModel(new DefaultComboBoxModel(new String[] {"-- Seleccione --", "Ingenieria", "Medicina", "Humanidades", "Psicologia"}));

		//Cargando el JComboBox manualmente
		cboFacultad.addItem("-- Seleccione --"); 	//Indice: 0
		cboFacultad.addItem("Ingenieria"); 			//Indice: 1
		cboFacultad.addItem("Medicina"); 			//Indice: 2
		cboFacultad.addItem("Humanidades"); 		//Indice: 3
		cboFacultad.addItem("Psicologia"); 			//Indice: 4
		cboFacultad.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cboFacultad.setBounds(110, 70, 150, 25);
		getContentPane().add(cboFacultad);
		
		txtNota1 = new JTextField();
		txtNota1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txtNota1.setColumns(10);
		txtNota1.setBounds(110, 100, 80, 25);
		getContentPane().add(txtNota1);
		
		txtNota2 = new JTextField();
		txtNota2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txtNota2.setColumns(10);
		txtNota2.setBounds(110, 130, 80, 25);
		getContentPane().add(txtNota2);
		
		scpSalida = new JScrollPane();
		scpSalida.setBounds(10, 163, 460, 160);
		getContentPane().add(scpSalida);
		
		txtSalida = new JTextArea();
		txtSalida.setEditable(false);		//HAce que el txtSalida no sea editable
		scpSalida.setViewportView(txtSalida);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(this);
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAceptar.setBounds(370, 10, 100, 25);
		getContentPane().add(btnAceptar);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(this);
		btnLimpiar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnLimpiar.setBounds(370, 130, 100, 25);
		getContentPane().add(btnLimpiar);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(this);
		btnBuscar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnBuscar.setBounds(370, 40, 100, 25);
		getContentPane().add(btnBuscar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(this);
		btnEliminar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnEliminar.setBounds(370, 70, 100, 25);
		getContentPane().add(btnEliminar);
		
		btnEliminarDesaprobados = new JButton("Borra Varios");
		btnEliminarDesaprobados.addActionListener(this);
		btnEliminarDesaprobados.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnEliminarDesaprobados.setBounds(370, 100, 100, 25);
		getContentPane().add(btnEliminarDesaprobados);
		
		//Construir el arreglo de estudiantes
		aEstudiantes = new ArregloEstudiantes();

	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btnEliminarDesaprobados) {
			btnEliminarDesaprobadosactionPerformed(arg0);
		}
		if (arg0.getSource() == btnEliminar) {
			btnEliminaractionPerformed(arg0);
		}
		if (arg0.getSource() == btnBuscar) {
			btnBuscaractionPerformed(arg0);
		}
		if (arg0.getSource() == btnAceptar) {
			btnAceptaractionPerformed(arg0);
		}
		if (arg0.getSource() == btnLimpiar) {
			btnLimpiaractionPerformed(arg0);
		}
	}
	
	protected void btnLimpiaractionPerformed(ActionEvent arg0) {
		txtCodigo.setText("");
		txtNombre.setText("");
		txtNota1.setText("");
		txtNota2.setText("");
		txtSalida.setText("");
		cboFacultad.setSelectedIndex(0); 	//Selecciona el elemento en la posici�n (indice) 0
	}
	
	protected void btnAceptaractionPerformed(ActionEvent arg0) {
		//Variables locales
		int codigo, facultad;
		String nombre;
		double nota1, nota2;

		codigo = Integer.parseInt( txtCodigo.getText() );
		nombre = txtNombre.getText();
		facultad = cboFacultad.getSelectedIndex();
		nota1 = Double.parseDouble( txtNota1.getText() );
		nota2 = Double.parseDouble( txtNota2.getText() );
		
		//Creaci�n del objeto Estudiante
		Estudiante objEstudiante = new Estudiante(codigo, nombre, facultad);
		objEstudiante.setNota1(nota1);
		objEstudiante.setNota2(nota2);
		
		aEstudiantes.agregar(objEstudiante);
		listado(objEstudiante);
		imprimir("En arreglo: " + aEstudiantes.elementos());
		imprimir("--------------");
	}
	
	public void listado(Estudiante objE) {
		imprimir("Codigo: " + objE.getCodigo());
		imprimir("Nombre: " + objE.getNombre());
		imprimir("Facultad: " + objE.getFacultad() + ".- " + cboFacultad.getItemAt(objE.getFacultad()) );
		imprimir("Nota1: " + objE.getNota1() + " " + ((objE.getNota1() == Estudiante.NOTA_NO_INGRESADA)?"(Sin Registro)":""));
		imprimir("Nota2: " + objE.getNota2() + " " + ((objE.getNota2() == Estudiante.NOTA_NO_INGRESADA)?"(Sin Registro)":""));
		imprimir("Promedio: " + objE.promedio());
		imprimir("--------------");
		imprimir("Estudiantes: " + objE.getCantidad() );
		imprimir("--------------");
	}
	
	public void imprimir(String s) {
		txtSalida.append(s + "\n");
	}
	
	protected void btnBuscaractionPerformed(ActionEvent arg0) {
		int codigo;
		codigo = Integer.parseInt( txtCodigo.getText() );
		
		Estudiante objEstudiante = aEstudiantes.buscar(codigo);
		if(objEstudiante == null) {
			//Mensaje de informaci�n sobre el resultado no logrado
			imprimir("No se encontr� al Estudiante con c�digo: " + codigo);
		} else {
			imprimir("Resultado de la b�squeda: " + codigo);
			listado(objEstudiante);
		}
	}
	
	protected void btnEliminaractionPerformed(ActionEvent arg0) {
		int codigo;
		codigo = Integer.parseInt( txtCodigo.getText() );
		
		if(aEstudiantes.eliminar(codigo)) {
			imprimir("Se elimin� el Estudiante con c�digo: " + codigo);
		}
	}
	
	
	protected void btnEliminarDesaprobadosactionPerformed(ActionEvent arg0) {
		int cantEliminados = aEstudiantes.eliminarDesaprobados(12);
		imprimir("Estudiantes eliminados : " + cantEliminados);
		imprimir(aEstudiantes.toString());
	}
}





