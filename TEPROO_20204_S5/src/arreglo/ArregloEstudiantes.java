package arreglo;

import java.util.ArrayList;
import java.util.Iterator;

import clase.Estudiante;

public class ArregloEstudiantes {
	
	//Crear el arreglo din�mico de estudiantes
	private ArrayList<Estudiante> arrEstudiantes = new ArrayList<Estudiante>();
	
	//Operaciones sobre el arreglo
	//----------------------------
	
	//Devuelve la cantidad de elementos en el arreglo
	public int elementos() {
		System.out.println("-->Ingresando al metodo elementos()\n");
		return arrEstudiantes.size();	//Retorna tama�o del array
	}

	//Agregar un objeto al arreglo
	public void agregar(Estudiante e) {
		System.out.println("-->Ingresando al metodo agregar(Estudiante):" + e + "\n");
		arrEstudiantes.add(e);
	}

	//Devuelve el objeto Estudiante en la posicion recibida
	public Estudiante obtener(int posicion) {
		return arrEstudiantes.get(posicion);
	}
	
	//Ubica un objeto Estudiante segun su valor de Codigo
	public Estudiante buscar(int codigo) {
		for(int i = 0; i < elementos(); i++) {
			if(obtener(i).getCodigo() == codigo) {
				return obtener(i);
			}
		}
		
		return null;
	}
	
	//Igual que buscar(int) pero usando foreach
	public Estudiante encontrar(int codigo) {
		//Recorrer la colecci�n de objetos
		for(Estudiante objE: arrEstudiantes) {
			if(objE.getCodigo() == codigo) {
				return objE;
			}
		}
		
		return null;
	}
	
	//Ubica un objeto Estudiante segun su valor de Codigo y lo elimina
	public boolean eliminar(int codigo) {
		Estudiante objEstudiante = buscar(codigo);
		if(objEstudiante != null) {
			arrEstudiantes.remove(objEstudiante);
			return true;
		}
		
		return false;
	}

	//Calcula el promedio general usando foreach para recorrer la colecci�n
	public double promedioGeneral() {
		double sumaNotas = 0;
		
		for(Estudiante objE: arrEstudiantes) {
			sumaNotas += objE.getNota1() + objE.getNota2();
		}
		
		return sumaNotas/elementos();
	}
	
	//Realiza una eliminaci�n masiva bajo ciertas condiciones.
	//Usa un iterador para recorrer la colecci�n
	public int eliminarDesaprobados(double minimoAprobatorio) {
		//El Iterador es una interfaz que permite el recorrido de una coleccion
		Iterator<Estudiante> it = arrEstudiantes.iterator();
		int eliminados = 0;
		
		while(it.hasNext() ) {
			if(it.next().promedio() < minimoAprobatorio) {
				it.remove();
				eliminados++;
			}
		}
		
		return eliminados;
	}
	
	//Construye una cadena con cada elemento del arreglo
	public String toString() {
		String mensaje = "";
		for(Estudiante objE: arrEstudiantes) {
			mensaje += "Reporte r�pido del arreglo\n";
			mensaje += "\tCodigo: " + objE.getCodigo() + "\n";
			mensaje += "\tNombre: " + objE.getNombre() + "\n";
			mensaje += "\tFacultad: " + objE.getFacultad() + "\n";
			mensaje += "\tNota1: " + objE.getNota1() + "\n";
			mensaje += "\tNota2: " + objE.getNota2() + "\n";
			mensaje += "\tPromedio: " + objE.promedio() + "\n";
			mensaje += "--------------\n";
		}
		
		return mensaje;
	}
}





