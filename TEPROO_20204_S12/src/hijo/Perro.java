package hijo;

import padre.Mamifero;

public class Perro extends Mamifero {
	
	public String mensaje() {
		return "Soy un perro";
	}
	
	public String hacerRuido() {
		return "Guau guau!";
	}
}
