package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import hijo.Perro;
import padre.Animal;
import padre.Mamifero;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejemplo1 extends JFrame implements ActionListener {
	private JButton btnUpcasting;
	private JScrollPane scpSalida;
	private JTextArea txtSalida;
	private JButton btnDowncasting;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejemplo1 frame = new Ejemplo1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejemplo1() {
		setBounds(100, 100, 418, 374);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		btnUpcasting = new JButton("Upcasting");
		btnUpcasting.addActionListener(this);
		btnUpcasting.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnUpcasting.setBounds(40, 14, 126, 25);
		getContentPane().add(btnUpcasting);
		
		scpSalida = new JScrollPane();
		scpSalida.setBounds(13, 53, 360, 221);
		getContentPane().add(scpSalida);
		
		txtSalida = new JTextArea();
		scpSalida.setViewportView(txtSalida);
		
		btnDowncasting = new JButton("Downcasting");
		btnDowncasting.addActionListener(this);
		btnDowncasting.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnDowncasting.setBounds(209, 14, 126, 25);
		getContentPane().add(btnDowncasting);

	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btnDowncasting) {
			btnDowncastingactionPerformed(arg0);
		}
		if (arg0.getSource() == btnUpcasting) {
			btnUpcastingctionPerformed(arg0);
		}
	}
	
	protected void btnUpcastingctionPerformed(ActionEvent arg0) {

		Object  oa = new Animal();
		Object  om = new Mamifero();
		Object  op = new Perro();
		Animal am = new Mamifero();
		Animal ap = new Perro();
		Mamifero mp = new Perro();

		txtSalida.setText("");
		imprimir("UPCASTING");
		imprimir(">> Objeto am");
		imprimir("Ruido: " + am.hacerRuido());
		imprimir("--------------");
		imprimir(">> Objeto ap");
		imprimir("Ruido: " + ap.hacerRuido());
		imprimir("--------------");
		imprimir(">> Objeto mp");
		imprimir("Ruido: " + mp.mensaje());
		imprimir("Ruido: " + mp.hacerRuido());
	}
	
	public void imprimir(String s) {
		txtSalida.append(s + "\n");
	}

	protected void btnDowncastingactionPerformed(ActionEvent arg0) {
		Object  oa = new Animal();
		Object  om = new Mamifero();
		Object  op = new Perro();
		Animal am = new Mamifero();
		Animal ap = new Perro();
		Mamifero mp = new Perro();
		
		Animal ao = (Animal) oa;
		Mamifero mo = (Mamifero) om;
		Perro po = (Perro) op;
		Mamifero ma = (Mamifero) am;
		Perro pa = (Perro) ap;
		Perro pm = (Perro) mp;

		txtSalida.setText("");
		imprimir("DOWNCASTING");
		imprimir(">> Objeto ao");
		imprimir("Ruido: " + ao.hacerRuido());
		imprimir("--------------");
		imprimir(">> Objeto mo");
		imprimir("Mensaje: " + mo.mensaje());
		imprimir("Ruido: " + mo.hacerRuido());
		imprimir("--------------");
		imprimir(">> Objeto ma");
		imprimir("Mensaje: " + ma.mensaje());
		imprimir("Ruido: " + ma.hacerRuido());
		imprimir("--------------");
		imprimir(">> Objeto po");
		imprimir("Mensaje: " + po.mensaje());
		imprimir("Ruido: " + po.hacerRuido());
		imprimir("--------------");
		imprimir(">> Objeto pa");
		imprimir("Mensaje: " + pa.mensaje());
		imprimir("Ruido: " + pa.hacerRuido());
		imprimir("--------------");
		imprimir(">> Objeto pm");
		imprimir("Mensaje: " + pm.mensaje());
		imprimir("Ruido: " + pm.hacerRuido());
	}
}




