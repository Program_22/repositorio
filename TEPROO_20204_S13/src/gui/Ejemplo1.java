package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import hijo.Docente;
import hijo.Estudiante;
import hijo.Investigador;
import padre.Persona;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejemplo1 extends JFrame implements ActionListener {
	private JButton btnCasting;
	private JScrollPane scpSalida;
	private JTextArea txtSalida;
	private JButton btnPolimorfismo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejemplo1 frame = new Ejemplo1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejemplo1() {
		setBounds(100, 100, 450, 484);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		btnCasting = new JButton("Casting");
		btnCasting.addActionListener(this);
		btnCasting.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCasting.setBounds(17, 19, 131, 31);
		getContentPane().add(btnCasting);
		
		scpSalida = new JScrollPane();
		scpSalida.setBounds(17, 57, 392, 344);
		getContentPane().add(scpSalida);
		
		txtSalida = new JTextArea();
		scpSalida.setViewportView(txtSalida);
		
		btnPolimorfismo = new JButton("Polimorfismo");
		btnPolimorfismo.addActionListener(this);
		btnPolimorfismo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnPolimorfismo.setBounds(278, 16, 131, 31);
		getContentPane().add(btnPolimorfismo);

	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btnPolimorfismo) {
			btnPolimorfismoActionPerformed(arg0);
		}
		if (arg0.getSource() == btnCasting) {
			btnCastingActionPerformed(arg0);
		}
	}
	
	protected void btnCastingActionPerformed(ActionEvent arg0) {
		
		txtSalida.setText("");
		
		
		Object perToObject = new Persona();
		Object docToObject = new Docente();
		Object invToObjet = new Investigador();
		
		Persona docToPersona = new Docente();
		Persona invToPersona = new Investigador();
		
		Docente invToDocente = new Investigador();

	
		
		imprimir(">>> UPCASTING");
		imprimir("Objecto perToObject: " + perToObject.getClass());
		imprimir("Objecto docToObject: " + docToObject.getClass());
		imprimir("Objecto invToObjet: " + invToObjet.getClass());
		imprimir("");
		imprimir("Objecto docToPersona: " + docToPersona.getClass());
		imprimir("Qui�n eres?: " + docToPersona.identidad());
		imprimir("");
		imprimir("Objecto invToPersona: " + invToPersona.getClass());
		imprimir("Qui�n eres?: " + invToPersona.identidad());
		imprimir("");
		imprimir("Objecto invToDocente: " + invToDocente.getClass());
		imprimir("Qui�n eres?: " + invToDocente.identidad());
		imprimir("Qu� haces?: " + invToDocente.dedicacion() );
		imprimir("");
		
		Persona objToPersona = (Persona) perToObject;
		Docente objToDocente = (Docente) docToObject;
		Investigador objToInvestigador = (Investigador) invToObjet;
		
		imprimir(">>> DOWNCASTING");
		imprimir("Objecto objToPersona: " + objToPersona.getClass());
		imprimir("Qui�n eres?: " + objToPersona.identidad());
		imprimir("");
		imprimir("Objecto objToDocente: " + objToDocente.getClass());
		imprimir("Qui�n eres?: " + objToDocente.identidad());
		imprimir("Qu� haces?: " + objToDocente.dedicacion());
		imprimir("");
		imprimir("Objecto objToInvestigador: " + objToInvestigador.getClass());
		imprimir("Qui�n eres?: " + objToInvestigador.identidad());
		imprimir("Qu� haces?: " + objToInvestigador.dedicacion());
		imprimir("");

		Docente perToDocente = (Docente) docToPersona; 
		Investigador perToInvestigador = (Investigador) invToPersona; 
		Investigador docToInvestigador = (Investigador) invToDocente; 
	
		imprimir("Objecto perToDocente: " + perToDocente.getClass());
		imprimir("Qui�n eres?: " + perToDocente.identidad());
		imprimir("Qu� haces?: " + perToDocente.dedicacion());
		imprimir("");
		imprimir("Objecto perToInvestigador: " + perToInvestigador.getClass());
		imprimir("Qui�n eres?: " + perToInvestigador.identidad());
		imprimir("Qu� haces?: " + perToInvestigador.dedicacion());
		imprimir("");
		imprimir("Objecto docToInvestigador: " + docToInvestigador.getClass());
		imprimir("Qui�n eres?: " + docToInvestigador.identidad());
		imprimir("Qu� haces?: " + docToInvestigador.dedicacion());
		imprimir("");
}
	
	private void imprimir(String s) {
		txtSalida.append(s + "\n");
	}
	
	protected void btnPolimorfismoActionPerformed(ActionEvent arg0) {
		Persona objPersona = new Persona();
		Estudiante objEstudiante = new Estudiante();
		Docente objDocente = new Docente();
		Investigador objInvestigador = new Investigador();
		txtSalida.setText("");
		listado(objPersona);
		listado(objEstudiante);
		listado(objDocente);
		listado(objInvestigador);
	}
	
	//M�todo polim�rfico
	private void listado(Persona objP) {
		imprimir("Qui�n eres?: " + objP.identidad() + " => " + objP.getClass());
		
		if(objP instanceof Investigador) {
			imprimir("Buena suerte en tu b�squeda!!!");
		}else if(objP instanceof Docente) {
			imprimir("Por favor, no se olvide de marcar asistencia");
		}else if(objP instanceof Estudiante) {
			imprimir("Por favor, no se olvide de entregar su proyecto");
		}else {
			imprimir("Buen d�a!");
		}
		imprimir("");
	}
}














