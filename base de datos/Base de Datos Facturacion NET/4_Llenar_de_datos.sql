
Insert Distritos Values ('Callao-01','Callao','CLL','Callao') 
Insert Distritos Values ('Callao-02','Bellavista','BLL','Callao') 
Insert Distritos Values ('Callao-03','Carmen de la Legua','CDL','Callao') 
Insert Distritos Values ('Callao-04','La Perla','PER','Callao') 
Insert Distritos Values ('Callao-05','La Punta','PTA','Callao') 
Insert Distritos Values ('Callao-06','Ventanilla','VEN','Callao') 


Insert Distritos Values ('Lima-01','Lima(Cercado)','LIM','Lima') 
Insert Distritos Values ('Lima-02','Ancon','ANC','Lima') 
Insert Distritos Values ('Lima-03','Ate Vitarte','ATE','Lima') 
Insert Distritos Values ('Lima-04','Barranco','BAR','Lima') 
Insert Distritos Values ('Lima-05','Bre�a','BRE','Lima')
Insert Distritos Values ('Lima-06','Carabayllo','CRB','Lima') 
Insert Distritos Values ('Lima-07','Comas','COM','Lima') 
Insert Distritos Values ('Lima-08','Chaclacayo','CHC','Lima') 
Insert Distritos Values ('Lima-09','Chorrillos','CHO','Lima') 
Insert Distritos Values ('Lima-10','El Agustino','AGU','Lima')
 
Insert Distritos Values ('Lima-11','Jesus Maria','JMA','Lima') 
Insert Distritos Values ('Lima-12','La Molina','MOL','Lima') 
Insert Distritos Values ('Lima-13','La Victoria','VCT','Lima') 
Insert Distritos Values ('Lima-14','Lince','LIN','Lima') 
Insert Distritos Values ('Lima-15','Lurigancho(Chosica)','LRG','Lima') 
Insert Distritos Values ('Lima-16','Lurin','LUR','Lima') 
Insert Distritos Values ('Lima-17','Magdalena Del Mar','MAG','Lima') 
Insert Distritos Values ('Lima-18','Miraflores','MIR','Lima') 
Insert Distritos Values ('Lima-19','Pachacamac','PCH','Lima') 
Insert Distritos Values ('Lima-20','Pucusana','PUC','Lima') 

Insert Distritos Values ('Lima-21','Pueblo Libre','PLI','Lima') 
Insert Distritos Values ('Lima-22','Puente Piedra','PTP','Lima') 
Insert Distritos Values ('Lima-23','Punta Negra','NEG','Lima') 
Insert Distritos Values ('Lima-24','Punta Hermosa','HER','Lima') 
Insert Distritos Values ('Lima-25','Rimac','RIM','Lima') 
Insert Distritos Values ('Lima-26','San Bartolo','SBR','Lima') 
Insert Distritos Values ('Lima-27','San Isidro','SIS','Lima') 
Insert Distritos Values ('Lima-28','Independencia','IND','Lima') 
Insert Distritos Values ('Lima-29','San Juan De Miraflores','SJM','Lima') 
Insert Distritos Values ('Lima-30','San Luis','SLU','Lima') 

Insert Distritos Values ('Lima-31','San Martin de Porres','SMA','Lima') 
Insert Distritos Values ('Lima-32','San Miguel','SMI','Lima') 
Insert Distritos Values ('Lima-33','Satiago De Surco','SUR','Lima') 
Insert Distritos Values ('Lima-34','Surquillo','SRQ','Lima')
Insert Distritos Values ('Lima-35','Villa Maria Del Triunfo','VMA','Lima') 
Insert Distritos Values ('Lima-36','Nuevo San Juan','NSJ','Lima')
Insert Distritos Values ('Lima-37','Santa Maria del Mar','SMR','Lima') 
Insert Distritos Values ('Lima-38','Sta Rosa','SRO','Lima') 
Insert Distritos Values ('Lima-39','Los Olivos','LOL','Lima')
Insert Distritos Values ('Lima-40','Cieneguilla','CNG','Lima')  

Insert Distritos Values ('Lima-41','San Borja','CBO','Lima') 
Insert Distritos Values ('Lima-42','Villa El Salvador','VES','Lima') 
Insert Distritos Values ('Lima-43','Santa Anita','SAN','Lima')

Insert Clientes Values ('Cli001','Manuel','Carri�n Delgado','Calle 29 Coop. Primavera Mz. R1. Lote 16','557-2147','12345678910','12345678','MCDelg@Hotmail.com','Lima-14')
Insert Clientes Values ('Cli002','Ana','Puelles Salgado','Jr. Viru 406 N�8','481-6507','12345678901','40344427','APuelles@Hotmail.com','Lima-10')
Insert Clientes Values ('Cli003','Daniel','Zapata Ancimas','Jr. Horacio Urteaga 314 ','920-1090','12346778910','40301579','DZapata@Hotmail.com','Lima-28')
Insert Clientes Values ('Cli004','Fernando','Talledo Cervera','Residencial Rio Sta Mz G Lte 12','539-0886','21414956715','41495671','Ftc3298@Hotmail.com','Lima-39')

Insert Usuarios Values ('Usu001','Dario','Barreto','Av La Marina 1254','483-5881','11111','Lima-39') 
Insert Usuarios Values ('Usu002','Santiago','Guerra Torres','Enrique Rocha # 124',4534-5781','22222','Lima-13')

update permisos set
tra_factura='1',
tra_boleta='1',
man_cliente='1',
man_distrito='1',
man_usuario='1',
man_articulo='1',
vis_cliente='1',
vis_distrito='1',
vis_usuario='1',
rep_stockact='1',
rep_factura='1',
rep_boleta='1',
rep_auditoria='1',
her_backup='1',
her_accesousu='1'
where idusuario='Usu001'

Update permisos set
tra_factura='1',
tra_boleta='1',
man_cliente='1',
man_distrito='1',
man_usuario='1',
man_articulo='1',
vis_cliente='1',
vis_distrito='1',
vis_usuario='1',
rep_stockact='1',
rep_factura='1',
rep_boleta='1',
rep_auditoria='1',
her_backup='1',
her_accesousu='1'
where idusuario='Usu002'

Insert Articulos Values ('Art001', 'CL18','PC, Mac.', 'Resoluci�n de im�gen de 640 x 480 captura de imagenes 640 x 480 compresi�n JPEG, profundidad de color de 24 Bits con visor optico integrado, interfaz USB a 100 Kbps con flash integrado', 366.66, 10)
Insert Articulos Values ('Art002', 'CL50','PC, Mac.', 'Manual 4 modos: 1600Z 1200, 1290 x 960, 640 x 480 VGA, 1280 x 960 (BN), Resolucion de imagen hasta 1600 x 1200 con tecnologia PhotoGenie, interfase RS-232, velocidad de transmision hasta 230, 400 cps; cuatro modos de flash', 1345.04, 5)
Insert Articulos Values ('Art003', 'MAVICA FD85', 'PC, Mac.','600 x 480, graba en disquete', 798.00, 10)
Insert Articulos Values ('Art004', 'MAVICA FD95', 'PC, Mac.','1600 x 1200, graba en disquete y memoria', 1621.00, 10)
Insert Articulos Values ('Art005', 'PHOTO PC 650', 'PC.','1.09 mega pixeles. 8MB est�ndar, 64MB m�ximo memoria, pantalla LCD color 1.8" TFT, capacidad para 88 im�genes m�ximo, conectividad serial y USB', 420.00, 10)
Insert Articulos Values ('Art006', 'PHOTO PC 750Z', 'PC.','1.25 mega pixeles. 12MB est�ndar, 64MB m�ximo memoria, pantalla LCD color 2.0" solar Assist, capacidad para 178 im�genes m�ximo, zoom digital 2x, conectividad serial y USB', 680.00, 5)

Insert Articulos Values ('Art007', 'FLYGTH SIMULATOR', 'PC.','Cuatro botones, compatible win 95/98', 45.00, 20)
Insert Articulos Values ('Art008', 'GP-11', 'PC.','DB-15, 8 teclas avanzada de disparo dos teclas de turbo boton de rueda de disparo', 16.50, 30)
Insert Articulos Values ('Art009', 'SF-5', 'PC.','DB-15, rueda de autodisparo, 3 ejes y cuatro botones acelerador para manejar velocidades, punto de mira y rueda de auto disparo', 17.50, 30)
Insert Articulos Values ('Art010', 'ZOLTRIX ZX FIGHTER', 'PC.','Cuatro botones, ergon�mico, color transparente f�cil de utilizar, acelerador rotadorio de gran prscici�n', 15.00, 50)

Insert Articulos Values ('Art011', '12J3614', 'Portatil.','Mouse de color negro', 36.00, 20)
Insert Articulos Values ('Art012', '922-3969', 'iMac Mac.','Mouse USB', 202.00, 10)
Insert Articulos Values ('Art013', 'BUTTON SCROLL-POINT PRO ANTIQUE SAGE MOUSE', 'PC.','Tres botones, ergon�mico, boton de desplazamiento, cable adaptador, PS/2 para USB desplazamiento vertical y horizontal en 360�, zoom', 55.46, 10)
Insert Articulos Values ('Art014', 'EASY MOUSE', 'PC.','Serial, tres botones', 4.00, 50)
Insert Articulos Values ('Art015', 'IMIC', 'PC, Mac.','Tres botones, traslucidos', 3.50, 50)
Insert Articulos Values ('Art016', 'FOK-520', 'PC.','Interfaces serial resoluci�n 50 dpi, modelo traslucido en variados colores, 2 botones', 6.95, 20)
Insert Articulos Values ('Art017', 'M34', 'PC, Mac.','2 botones', 7.50, 30)
Insert Articulos Values ('Art018', 'MINI MOUSE PORT NOTEWORTHY', 'Portatiles Toshiba','USB mobile, 2 botones', 34.00, 10)
Insert Articulos Values ('Art019', 'MOUSE PHONE', 'PC.','Blanco y negro, 2 botones, interface PS/2, telefono con funciones standar', 45.00, 15)
Insert Articulos Values ('Art020', 'PILOT WHEE', 'PC, Mac', 'Tres botones, salida PS/2, boton para Internet', 20.00, 20)

Insert Articulos Values ('Art021', 'ASTRA 2200', 'Mac OS, Win 95/98/NT', 'Scaners escritorio forMaro A4, 68 millones de colores, 36bit de profundidad, resolucion �ptica de 600 x 1200 ppp, inteface USB/SCSI, opci�n para transparencia, software incluido', 385.00, 10)
Insert Articulos Values ('Art022', 'ASTRA 2400S', 'Mac OS, Win 95/98/NT', 'Scaners escritorio forMaro A4, 68 millones de colores, 36bit de profundidad, resolucion �ptica de 600 x 2400 ppp, rango din�mico de 3.6, inteface USB/SCSI, opci�n para transparencia, software incluido', 510.00, 6)
Insert Articulos Values ('Art023', 'COLOR PAGE S VIVID III', 'PC.', 'Scaners escritorio, pagina completa 600 x 1200 dpi paralelo', 95.00, 20)
Insert Articulos Values ('Art024', 'DR-3020', 'PC. Win 95/98/NT', 'De Escritorio alta velocidad 40 ppm monocrom�tico resoluci�n de hasta 300 x 150 dpi interfase SCSI-II con Isis y Taiwan driver, modo de exploraci�n simple y duplex alimentaci�n autom�tica y manual, bandeja para 100 hojas entre otras', 5689.00, 5)
Insert Articulos Values ('Art025', 'DR-4080U', 'PC. Win 95/98/NT', 'Universal alta velocidad 47 ppm en modo simple de exploracion y 104 ppm en modo duplex monocrom�tico resoluci�n de hasta 400 x 400 dpi interfase SCSI-II con Isis y Taiwan driver, modo de exploraci�n simple y duplex, reconoce c�digo de barras por hardware, bandeja para 100 hojas entre otras', 11700.00, 5)
Insert Articulos Values ('Art026', 'EXPRESSION 836XL', 'PC.', 'Tipo escritorio, color 36 bits tonos grises 12 bits, resoluci�n �ptica 800 x 1600 dpi, resoluci�n m�xima de 6400 x 6400 dpi, interface SCSI Adaptec software de edici�n de imagenes Photo Shop', 3160.00, 10)
Insert Articulos Values ('Art027', 'IMAGE MOUSE', 'PC, Mac.', 'Scanners para microfichas, tipo escritorio monocrom�tico captura menor a un segundo por frame, resoluci�n �ptima a modo video digital, interfase USB.', 2200.00, 10)
Insert Articulos Values ('Art028', 'IMAGE MOUSE PLUS', 'PC, Mac.', 'Scanners para rollos y microfichas. Tipo escritorio, monocrom�tico, captura menor a un segundo por frame, resoluci�n optima a modo video digital, intrefase USB', 8200.00, 10)
Insert Articulos Values ('Art029', 'SACNJET 3400', 'Win 95/98', 'Cama plana, resoluci�n �ptica 600dpi, mejorada 9,600 dpi; 36 bit color, interfase paralela y USB, 3 botones, velocidad menor a 60 seg. foto 4 x 6" llevada a Word', 200.00, 20)
Insert Articulos Values ('Art030', 'SACNJET 4300', 'Win 95/98/2000', 'Cama plana, resoluci�n �ptica 600dpi, mejorada 9,600 dpi; 36 bit color, interfase paralela y USB, 3 botones, velocidad menor a 60 seg. foto 4 x 6" llevada a Word', 190.00, 20)

Insert Articulos Values ('Art031', '76H0109', 'Portatil', 'Teclado color negro', 119.00, 10)
Insert Articulos Values ('Art032', 'BTC-AT', 'PC.', 'AT, 121 teclas en espa�ol, conector DIN,', 23.00, 10)
Insert Articulos Values ('Art033', 'BTC-WIN 98-AT', 'PC.', 'AT, OEM Win98, 105 teclas en espa�ol, conector DIN,', 9.00, 50)
Insert Articulos Values ('Art034', 'BTC-WIN 98-PS/2', 'PC.', 'Win98, 105 teclas en espa�ol, conector PS/2,', 10.50, 30)
Insert Articulos Values ('Art035', 'CLICKER', 'PC, Mac.', 'AT, PS/2, doble golpe', 7.00, 40)
Insert Articulos Values ('Art036', 'ERGONOMICO', 'PC, Mac.', 'AT, PS/2, natural', 19.00, 10)
Insert Articulos Values ('Art037', 'INTEL KEYBOARD', 'PC.', 'Teclas especiales para Internet, Interfase PS/2 o USB', 25.00, 20)
Insert Articulos Values ('Art038', 'LEC WIN98-AT', 'PC.', 'Win98, 110 teclas en espa�ol, conector DIN.', 9.00, 20)
Insert Articulos Values ('Art039', 'SATELLITE Y SATELLITE PRO', 'Portatil toshiba', 'Teclado en espa�ol.', 217.00, 5)

Insert Articulos Values ('Art040', 'INTERNET HARDWARE VALUE PACK', 'PC.', 'Mouse y teclado con caracteristicas especiales para Internet.', 37.00, 10)
Insert Articulos Values ('Art041', 'HARDWARE VALUE PACK', 'PC.', 'Mouse y teclado .', 45.00, 10)

Insert Articulos Values ('Art042', '4300', 'Win NT/95/98/2000, Unix, Mac.', 'Para CAD, B&N, 300 dpi Interfase Dual Ultrafast SCSI, ancho de 36".', 16280.00, 2)
Insert Articulos Values ('Art043', '5010', 'Win NT/95/98/2000, Unix, Mac.', 'Para CAD, GIS y artes gr�ficas, 24 bit color, B&N, 8 bit escala de grises, 500 dpi, interfase dual Ultrafast SCSI, ancho 36".', 28680.00, 2)
Insert Articulos Values ('Art044', 'DESIGNJET 450C', 'PC. Mac.', '600 dpi, A.0 color en 4 minutos, interfase paralela y red, aliment manual, rollo opcional.', 3949, 5)
Insert Articulos Values ('Art045', 'DESIGNJET 750C', 'PC. Mac, Unix.', '600 dpi, A.0 color en 4 minutos, interfase paralela y red, aliment manual, rollo opcional.', 8022, 2)
Insert Articulos Values ('Art046', 'DESIGNJET 1050C', 'PC. Mac, Unix, OS/2.', '1200 dpi, A.1 color en 45 segundos, interfase paralela y red, aliment manual, rollo opcional.', 10427, 3)
Insert Articulos Values ('Art047', 'SR-850E', 'Windows, Mac.', 'Formto amplio photer resoluci�n 720 x 720 dpi B&N, 360 x 360 dpi Color, puerto RS-232C y paralelo sentronic.', 9427, 2)

Insert Articulos Values ('Art048', 'BJC-1000', 'Windows', 'De escritorio, resoluci�n de 720 dpi, 4.0 ppm B&N, 0.6 ppm a color, incluye BC-05 para impresiones en color herramientas para banner impresi{on espejo; opci�n cartucho foto BC-06 y cartucho negro BC-02, cartucho tintas de neon BC-09.', 89.00, 10)
Insert Articulos Values ('Art049', 'BJC-2100', 'Windows, Mac', 'De escritorio, resoluci�n de 720 dpi, 5.0 ppm B&N, 2.5 ppm a color, interfase USB y paralelo sentronic. Cartucho BC-21 para impresiones en color herramientas para banner impresi�n espejo; opci{on cartucho scanner IS-22, cartucho foto BC-22E y cartucho negro BC-20, cartucho tintas de neon BC-29.', 109.00, 10)
Insert Articulos Values ('Art050', 'DESJECKT JET 610C', 'PC.', '600 x 600 dpi B&N, 600 x 300 dpi color, 3 ppm B&N, 1 ppm color, 32 KB bufer', 210.00, 10)
Insert Articulos Values ('Art051', 'DOCUPRINTC6', 'Windows', '600 x 600 dpi 4 ppm B&N, 1.5 ppm color cartuchos independientes, bandeja 100 hojas cabezal de impresi{on de foto integrado calidad fotogr�fica, etc', 124.00, 10)
Insert Articulos Values ('Art052', 'DOCUPRINTC8', 'Windows', '1200 x 600 dpi 5 ppm B&N, 2.5 ppm color cartuchos independientes, imprime 7 ppm, cartucho alta velocidad opcional, diversos tipos de papel, etc', 174.00, 5)
Insert Articulos Values ('Art053', 'STYLUS COLOR 480', 'PC.', 'Interfase paralela 720 dpi, 5 ppm B&N, 1.5 ppm a color aprox, forMaro A4, carta, Oficio; 2 cartuchos (color y negor)', 99.00, 15)
Insert Articulos Values ('Art054', 'XEROX INKJECT C6', 'Portatiles Toshiba', '4 ppm en negro 1.5 a full color, 600 x 600 dpi en negro, 600 x 300 dpi color, 4 cartucho individuales interfases paralelas bidirecional de alta velocidad', 115.00, 20)
Insert Articulos Values ('Art055', 'Z11 COLOR JETPRINTER', 'PC.', '1200 x 1200 dpi, 4 ppm en B&N, 2.5 ppm en color', 122.34, 20)

insert indices values('USUARIOS',2)
insert indices values('DISTRITO_LIMA',43)
insert indices values('DISTRITO_CALLAO',6)
insert indices values('CLIENTES',4)
insert indices values('FACTURAS',0)
insert indices values('ARTICULOS',55)
insert indices values('BOLETA',0)
