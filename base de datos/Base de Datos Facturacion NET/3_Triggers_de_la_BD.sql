create trigger Tr_Dismin_STock_F
on Detalle_Factura
for Insert
as
Declare @Idarticulo char(10),
@Cant_det int

Select @Idarticulo = Idarticulo,
	@Cant_det = Cant_det
from Inserted

Begin Transaction
    Update Articulos Set Stock_arti = Stock_arti - @Cant_det
    where Idarticulo=@Idarticulo

if (@@error=0)
 Begin
	commit transaction
	print 'Se Actualizo el Stock'
 End
else
 Begin
	rollback transaction
	print 'Se Deshizo el Ingreso'
 End

go
--boleta

create trigger Tr_Dismin_STock_B
on Detalle_Boleta
for Insert
as
Declare @Idarticulo char(10),
@Cant_Bol int

Select @Idarticulo = Idarticulo,
	@Cant_Bol = Cant_Bol
from Inserted

Begin Transaction
    Update Articulos Set Stock_arti = Stock_arti - @Cant_Bol
    where Idarticulo=@Idarticulo

if (@@error=0)
 Begin
	commit transaction
	print 'Se Actualizo el Stock'
 End
else
 Begin
	rollback transaction
	print 'Se Deshizo el Ingreso'
 End
GO

--PERMISOS DE USUARIO
create trigger Tr_Permisos_usuario
on Usuarios
for Insert
as
Declare @IdUsuario char(10)
Select @IdUsuario = IdUsuario from inserted

begin transaction
  insert permisos values(@IdUsuario,'0','0','0','0','0','0','0','0','0','0','0','0','0','0','0')

if (@@error=0)
 Begin
	commit transaction
	print 'Permisos Concedidos al usuario'
 End
else
 Begin
	rollback transaction
	print 'El usuario NO ingreso'
 End
GO

--historial Factura
Create Trigger tr_Historial_Factura
on Factura
for Insert
as
declare @IdFactura char(10),
@FechaFac datetime,@Subtotal_Fac decimal(12,2),
@Igv decimal(12,2)

Select @IdFactura=Idfactura,
@fechafac = fechafac,@subtotal_fac=subtotal_fac,
@igv=igv from inserted

Begin Transaction
  insert HistorialFactura values 
(@IdFactura,@Fechafac,@Subtotal_Fac,@Igv,System_User,Host_Name())

if (@@error=0)
 Begin
	commit transaction
	print 'Factura Archivada en Historial de Facturas'
 End
else
 Begin
	rollback transaction
	print 'Error de Factura en el Historial'
 End
Go

--historial de boleta
Create Trigger tr_Historial_Boleta
on Boleta
for Insert
as
declare @IdBoleta char(10),
@FechaBol datetime,@Total_bol decimal(12,2)

Select @idboleta = idboleta,
@fechabol=fechabol,@total_bol =total_bol
from inserted

begin transaction
insert HistorialBoleta values 
(@idboleta,@fechabol,@total_bol,system_user,host_name())

if (@@error=0)
 Begin
	commit transaction
	print 'Boleta Archivada en Historial de Boletas'
 End
else
 Begin
	rollback transaction
	print 'Error de Boleta en el Historial'
 End
go
