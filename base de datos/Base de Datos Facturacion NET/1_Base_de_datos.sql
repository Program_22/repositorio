--use master
--go
--drop database FACTURACION_NET
--GO
CREATE DATABASE FACTURACION_NET
GO
USE FACTURACION_NET
GO
--create de la table de distritos
CREATE TABLE Distritos
(
IdDistrito	Char(10)  Primary Key,
nom_dis    	VarChar(30) Not Null,
iniciales  	Char(3)     ,
Zona		varchar(30) not null 
)
go
CREATE TABLE Clientes
(
IdCliente  	Char(10)Primary Key,
nom_cli    	VarChar(50) Not Null,
ape_cli    	VarChar(50) Not Null,
dir_cli    	VarChar(50) ,
tel_cli    	VarChar(8),  
Ruc_cli	   	Varchar(15),
DNI_cli		Varchar(8)  Not Null,
Email_cli  	Varchar(50),
IdDistrito 	Char(10) Foreign Key References Distritos(IdDistrito),
)
go
CREATE TABLE Usuarios
(
IdUsuario	Char(10)  Primary Key,
nom_usu  	VarChar(20) Not Null,
ape_usu    	VarChar(20) Not Null,
dir_usu     	VarChar(50) Not Null,
tel_usu     	VarChar(8),  
clave         	VarChar(15)  Not Null,
IdDistrito 	Char(10) Foreign Key References Distritos(IdDistrito),
)
go

CREATE TABLE Articulos
(
IdArticulo      Char(10)Primary Key,
Modelo          varChar(50) Not Null,
Plataforma      VarChar(50),
Caracteristicas	VarChar(350) Not Null,
pre_arti        Decimal(12,2)Not Null, 
stock_arti      Integer Not Null
)
go
CREATE TABLE Factura(
IdFactura    	Char(10)Primary Key,
FechaFac     	DateTime,
IGV	        		Decimal(12,2) Not Null,
SubTotal_Fac    		Decimal(12,2) Not Null,
Anulado         		Char(1),
Transferido     		Char(1),
Desc_Fac        		decimal (12,2),
IdCliente       		Char(10)Foreign Key References Clientes(IdCliente)
)
go

CREATE TABLE Detalle_Factura(
IdFactura      	Char(10) Foreign Key References Factura(IdFactura),
IdArticulo   	Char(10)Foreign Key References Articulos(IdArticulo),
Cant_det     	Integer Not Null,
Prec_det     	Decimal(12,2) Not Null,
Importe      	Decimal(12,2) Not Null
)
go
CREATE TABLE HistorialFactura(
NumeroFactura      	Char(10),
Fecha		   	Datetime,
SubTotal		Decimal(12,2),
IGV			Decimal(12,2),
UsuarioSistema	VarChar(20),
EstacionUsuario	VarChar(20)
)
go
CREATE TABLE Boleta(
IdBoleta    	Char(10)Primary Key,
FechaBol     	DateTime,
Total_Bol    	Decimal(12,2) Not Null,
Anulado         Char(1),
Transferido     Char(1),
Desc_Bol        decimal (12,2),
IdCliente       Char(10)Foreign Key References Clientes(IdCliente)
)
go
CREATE TABLE Detalle_Boleta(
IdBoleta      	Char(10) Foreign Key References Boleta(IdBoleta),
IdArticulo   	Char(10)Foreign Key References Articulos(IdArticulo),
Cant_Bol     	Integer Not Null,
Prec_Bol     	Decimal(12,2) Not Null,
Importe      	Decimal(12,2) Not Null
)
go
CREATE TABLE HistorialBoleta(
NumeroBoleta      	Char(10),
Fecha		   	Datetime,
SubTotal		Decimal(12,2),
UsuarioSistema	VarChar(20),
EstacionUsuario	VarChar(20)
)
go

CREATE TABLE indices(
tabla varchar(20),
indice int
)
go

CREATE TABLE Permisos(
Idusuario char(10),
Tra_Factura char(1),
Tra_Boleta char(1),
Man_Cliente char(1),
Man_Distrito char(1),
Man_Usuario char(1),
Man_Articulo char(1),
Vis_Cliente char(1),
Vis_Distrito char(1),
Vis_usuario char(1),
Rep_StockAct char(1),
Rep_Factura char(1),
Rep_Boleta char(1),
Rep_Auditoria char(1),
Her_Backup char(1),
Her_AccesoUsu char(1)
)
