drop database compras
go

--USE MASTER
--GO

create database [Compras] on (NAME = N'compras',
FILENAME = 'D:\Base de Datos\compras.mdf',
SIZE = 5MB, MAXSIZE = 3000 MB, FILEGROWTH = 10%)
LOG ON (NAME = 'compras_log',
FILENAME = 'D:\Base de Datos\compras.LDF',
SIZE = 1MB, FILEGROWTH = 10%)
COLLATE Modern_Spanish_CI_AS
go

--Abrir la base de datos
use compras
go

create table Proveedores (
--crear el pk pra el campo Codigpro llamado id_pro
codigpro char(4) not null constraint id_pro primary key,
--crear una restriccion UNIQUE para el campo cifprof
--el campo cifpro debe admitir valores unicos
cifpro char(12) not null constraint u_cif unique,
nombrpro char(30) not null,
direcpro char(30) not null,
--crear una restriccion CHECK para el campo cpostpro
cpostpro char(5) not null check (cpostpro like '[0-9][0-9][0-9][0-9][0-9]'),
localpro char(20) not null,
telefpro char(17) not null,
faxpro char(17),
emailpro char(25),
procepro char(5) not null check (procepro in ('UE','NO UE')))
go

--crear la tabla articulos
create table Articulos (
codigart char(6) not null constraint id_art primary key,
descrart char(40) not null,
preunart decimal(9,2) not null,
stockart integer not null check (stockart >=0),
stockmin integer not null check (stockmin >=0),
fecbaja date)
go

--crear la tabla pedidos
create table Pedidos(
numped integer not null constraint id_ped primary key,
--crea la restriccion para que la fechaped asuma la fecha del sistema
fechaped date not null default getdate(),
codigpro char(4)not null,
igvped char(4) not null check (igvped=0.18),
fentrped date not null,
--crear una restriccion fk(para relacionarlo con otra tabla Proveedores)
	constraint f_pro foreign key (codigpro)
	references Proveedores (codigpro),
	--crear una restriccion check para que la
	--fecha pedido <= fecha enterega del pedido
	constraint c_fecha check (fechaped <= fentrped))
go

create index FK_prop on Pedidos (codigpro)
go

create table Lineas(
	numped integer not null,
	numlin smallint not null,
	codigart char(6) not null,
	unilin decimal(6,0) not null,
	preunlin decimal(9,2) not null,
	desculin decimal(4,1) not null check (desculin>=0 and desculin <= 100),
	--campo calculo
	totallin as ([preunlin] * [unilin] * (1-[desculin]/100.0)),
		constraint id_lin primary key (numped,numlin),
		constraint f_ped foreign key (numped) references Pedidos(numped),
		constraint f_art foreign key (codigart) references Articulos (codigart),
)
go

--crear un Indice FK_LineasArt
--para la tabla lineas campo Codigart
create index FK_LineasArt on Lineas (codigart)
go
--crear un Indice FK_Lineasped
--para la tabla lineas campo numped
create index FK_LineasPed on Lineas (numped)
go

insert into Articulos (codigart, descrart,preunart,stockart,stockmin) values
('0001','MESA OFICINA 90 X 1,80',225,100,1),
('0002','SILLA ERGONOMICA MOD. MX',120,25,1),
('0003','ARMARIO DIPLOMATICO',300,2,1),
('0004','ARCHIVADOR MOD. TR',180,3,1);

insert into Proveedores(codigpro,cifpro,nombrpro,direcpro,cpostpro,localpro,faxpro,telefpro,emailpro,procepro) values
('P001','A39184215','Bau Pi,Pablo','Alta 3,2�','39390','Santander','(34) 942 223 344','(34) 942 223 345','mailto:baupi@eresmas.es','UE'),
('P002','A48162311','Zar Luna,Ana','Ercilla 22,1�','48002','Bilbao','(34) 947 865 434','(34) 947 865 413','mailto:zarana@eresmas.es','UE'),
('P003','B28373212','Gras Leon,Luz','Pez 14,5� dcha.','28119','Madrid','(34) 916 677 889','(34) 916 677 829',Null,'UE'),
('P004','B84392314','Gil Laso,Luis','Uria 18, 2�','85223','Oviedo','(34) 952 345 678','(34) 952 345 663',Null,'UE');


insert into Pedidos (numped,codigpro,fechaped,igvped,fentrped) values
(1,'P001','22-05-2010',0.18,'16-06-2010'),
(2,'P002','10-06-2010',0.18,'15-08-2010'),
(3,'P003','25-10-2010',0.18,'15-12-2010'),
(4,'P004','13-08-2010',0.18,'10-09-2010');

insert into Lineas (numped,numlin,codigart,unilin,preunlin,desculin) values
(1,1,'0001',1,220,0),
(1,3,'0003',2,295,0),
(1,2,'0002',3,120,2),
(1,5,'0003',2,300,3),
(1,4,'0002',5,120,0),
(1,6,'0002',1,110,0),
(1,7,'0002',4,120,0),
(1,8,'0004',10,180,0);
 
--insert un nuevo registro a la tabla proveedores
insert into Proveedores(codigpro,cifpro,nombrpro,direcpro,cpostpro,localpro,telefpro,faxpro,procepro) values
('P005','A39144325','Angulo Lastra, Antonio','Hernan Cortes','18392','Santander','(34) 942 282 022','(34) 942 282 022','UE');
go

--actualizar la tabla proveedores
--el valor del campo emailpro cuan el codigo proveedor sea P004
update Proveedores set emailpro='mailto:gil@unican.es'
where codigpro='P004'
go

select * from Proveedores
Go

Delete from Proveedores 
Where localpro= 'Santander'
Go

Create table Prov_Tmp (
codigpro char(4) NOt null CONSTRAINT id_pro_tmp Primary key,
cifpro Char(12) Not null  Constraint u_cif_tmp Unique,
nombrpro char(30) not null,
direcpro char(30) not null,
cpostpro char(5) not null check (cpostpro like '[0-9][0-9][0-9][0-9][0-9]'),
localpro char(20) not null,
telefpro char(17) not null,
faxpro char(17),
emailpro char(25),
procepro char(5) not null check (procepro in ('UE','NO UE')))
go

--Mostrando Registros 
Select * from Prov_Tmp
--Insertar Registros a la tabla Prov_Tmp
Insert Into Prov_Tmp (codigpro, cifpro, nombrpro,
direcpro, cpostpro,  localpro, telefpro, faxpro, procepro)
values('P006','A05678900', 'Lee Romero, Juan',
'Hern�n Cort�s, 19', '39002', 'Santander', '(34) 942 202 021','(34) 942 202 022', 'UE')
go

--Mostrar todos los registros de la tabla Articulo
Select * from Pedidos

Select nombrpro, telefpro From Proveedores
go

--Uso del alias As
Select nombrpro As Proveedor,
telefpro AS Telefono From Proveedores
Go


--Mostrar todos los articulos con precio unitario > a 100
--y el stock <= 100

	Select * From Articulos
	Where ( preunart > 100 ) and ( stockart >= 100)
	Go 

--Between se usa para definir en un campo condiciones con valores dentro de un rango
--Mostrar todos los articulos con Precio unitario entre 100 y 300
Select * From Articulos
Where preunart Between 180 And 300
Go

--Sin Between
Select * From Articulos 
Where (preunart >=180 ) And (preunart <= 300)
Go

--Mostrar todos los proveedores que pertenezcan al local
--Santander o madrid o Barcelona
--uso de la clausula IN ( comprar el valor de campo con una lista de valores)
Select codigpro, nombrpro, direcpro, cpostpro, localpro From Proveedores
Where localpro In ('Santander','Madrid', ' barcelona')
Go

Select codigpro, nombrpro, direcpro, cpostpro, localpro From Proveedores
Where (localpro = 'Santander') Or ( localpro= 'Madrid') or (localpro= 'Barcelona') 
Go

--Mostrar los proveedores cuyo nombre inicia la primera letra
--desde la A hasta la J
Select codigpro, nombrpro, direcpro, cpostpro, localpro From Proveedores
Where nombrpro Like '[A-J]'
Go

Select * From 


--Uso del Distinct 
--Aplicado a un campo muestra solo valores �nicos 
--Sin el Distinct 
Select localpro From Proveedores order by localpro 
go
--con el Distinct
Select Distinct Localpro from Proveedores order by localpro
go


--Clase 22-10-2015--

--Creando una consulta combinada 
--Mostrar los proveedores con Pedidos entre 20/01/2010 hasta 15/09/2010
SELECT DISTINCT Proveedores.codigpro, nombrpro, direcpro, localpro
From Proveedores INner Join Pedidos
ON Proveedores.codigpro = Pedidos.codigpro 
Where fechaped Between '2010/01/20' AND '2010/09/15'


--Encontrar todos los Art�culos, codigart, descrart
Select Distinct Articulos.codigart, descrart
From Pedidos INNER Join
    (Lineas Inner Join Articulos ON Lineas.codigart = Articulos.codigart)
	                             ON Pedidos.numped = Lineas.numped 
		Where fechaped Between '15/5/2010' And '30/05/2010'
		go

	--Localizar todos los Pedidos que tienen varias l�neas 
	--
	--
	--
	--
	Select * From Lineas 
	--Mostrar todos los Proveedores que tienen solicitud de pedidos 
	Select Proveedores.codigpro , nombrpro, Pedidos , numped
	  From Proveedores  Left join Pedidos 
	    On Proveedores.codigpro = Pedidos. codigpro
	Go


	--Preferencia por la derecha
	Select Pedidos.numped, Proveedores, codigpro, nombrpro
	From Pedidos Right Join Proveedores 
	ON Proveedores. codigpro = Pedidos .codigpro
	go

	--lista de proveedores Santander indicando en su caso s han recibido
	Select Proveedores .codigpro , nombrpro, localpro, Pedidos.numped 
	   From Proveedores Left Join Pedidos 
	     On Proveedores .codigpro = Pedidos . codigpro 
	Where localpro = 'SANTANDER'

	--Listar los articulos que no han sido pedidos entre el 24 de setiembre y 21 de Noviembre
	Select Distinct Articulos .codigart , descrart From Articulos 
	Where Articulos .codigart NOt In( Select Lineas .codigart From Lineas ,Pedidos 
	Where Pedidos . numped = Lineas .numped And Pedidos .fechaped 
	  Between '24/09/2010' And '21/11/2010')
	  Go

	  --Subconsulta 
	  --Mostrar el articulo mas barato y mas caro 
	  select * from Articulos 
	  Select descrart,preunart From Articulos  
	  where preunart = (select min(preunart) from Articulos) 
	  or preunart = ( select max(preunart) from Articulos)
	  go

	 Select descrart, preunart from Articulos 


	 --Encontrar los proveedores de madrid
	 --a los que se le han realizado algun pedido
	 --entre el 24/09/2010 y el 21/11/2010
	 select distinct proveedores. codigpro, nombrpro, localpro From Proveedores
	 Where Exists (Select * From Pedidos 
	 Where Proveedores.codigpro = Pedidos,codigpro 
	 And fechaped Between '24/09/2010' And '21/11/2010')
	 And localpro = 'madrid'

	 --Funcione de grupo
	 --mostrar la cantidad de articulos registrados
	 --articulos con maxim,o precio, minimo precio, precio promedio
     --y la valoracion ==> precio unitario Stock

	 Select Count(codigart) As Cantidad, Max(preunart) As Max,
	             Min(Preunart) As Min,
				 convert (decimal (10,2), AVG(preunart) AS Precio_medio,
				 convert (decimal (10,2), SUM(preunart*stockart) As Valoracion
				 From Articulos 

	--Funciones de fecha 
	--Mostrar por cada pedido el dia,mes, a�o, asi como el dia de la semana
	Select numped.
	Day(fechaped) as dia_mes, Month(fechaped) as mes,
	Year( fechaped) as a�o, Datepart(dw, fechaped) as dia_sem
	from Pedidos

	--Mostrar por cada pedido el importe de la venta aplicando los dsctos
	Select numped, SUM((preunlin*unilin)*(1-desculin/100)) as importe 
	From lineas
	Group by numped 

	--Usando Fechas 
	--Select numped,
	--
	--
	--Datepart(dd,fechaped) as dia_mes, datepad(mm,fechaped) as mes,
	--Datepart(yy,fechaped) as a�o, Datepart(dw, fechaped) as dia_sem
    --from pedido

	--lista el importe de pedidos qie tienen m�s de una linea 
	select numped, convert(decimal(10,2), SUm((preunlin*unilin)*(1-desculin/100)))
	as Imported From Lineas Group By numped Having COUNT (*) > 1
	Go
	--Clausula Order By 
	--mostrar los articulos con nombre y precio, ordenados por precio en forma des
	select descrart, preunart from Articulos 
	order by 2 desc
	--otra forma

	select descrart,preunart from Articulos 
	order by preunart desc


