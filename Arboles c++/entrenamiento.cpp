
#include <iostream>
#include <cstdlib>
#include<string.h>
using namespace std;

struct nodo{
	int nro;
	string nombre;
	struct nodo *izq, *der;
};

typedef struct nodo *ABB;


ABB crearNodo(string a,int x)
{
	ABB nuevoNodo = new(struct nodo);
	nuevoNodo->nombre=a;
	nuevoNodo->nro = x;
	nuevoNodo->izq = NULL;
	nuevoNodo->der = NULL;
	
	return nuevoNodo;
}
void insertar(ABB &arbol, string a,int x)
{
	if(arbol==NULL)
	{
		arbol = crearNodo(a,x);
	}
	else if(x < arbol->nro)
	   insertar(arbol->izq,a,x);
	else if(x > arbol->nro)
		insertar(arbol->der,a,x);
}

void preOrden(ABB arbol,string a)
{
	if(arbol!=NULL)
	{
		cout << arbol->nro<<"->"<<arbol->nombre <<" ";
		preOrden(arbol->izq,arbol->nombre);
		preOrden(arbol->der,arbol->nombre);
	}
}

void enOrden(ABB arbol,string a)
{
	if(arbol!=NULL)
	{
		enOrden(arbol->izq,arbol->nombre);
		cout << arbol->nro<<"->"<<arbol->nombre<< " ";
		enOrden(arbol->der,arbol->nombre);
	}
}

void postOrden(ABB arbol,string a)
{
	if(arbol!=NULL)
	{
		postOrden(arbol->izq,arbol->nombre);
		postOrden(arbol->der,arbol->nombre);
		cout << arbol->nro <<"->"<<arbol->nombre<< " ";
	}
}

void verArbol(ABB arbol,string a ,int n)
{
	if(arbol==NULL)
		return;
	verArbol(arbol->der,arbol->nombre, n+1);
	
	for(int i=0; i<n; i++)
		cout<<"   ";
	
	cout<< arbol->nombre<<"->"<<arbol->nro <<endl;
	
	verArbol(arbol->izq, arbol->nombre,n+1);
}

int main(void)
{
	ABB arbol = NULL;   // creado Arbol
	
	int n;  // numero de nodos del arbol
	int x; // elemento a insertar en cada nodo
	string a;
	
	cout << "\n\t\t  ..[ ARBOL BINARIO DE BUSQUEDA ]..  \n\n";
	
	cout << " Numero de nodos del arbol:  ";
	cin >> n;
	cout << endl;
	
	for(int i=0; i<n; i++)
	{
	cout<<"Ingrese el nombre :";
	cin>>a;
	cout << " Numero del nodo " << i+1 << ": ";
	cin >> x;
		if(x>)
		insertar( arbol, a,x);
	}
	
	cout << "\n Mostrando ABB \n\n";
	verArbol( arbol, a,0);
	
	cout << "\n Recorridos del ABB";
	
	cout << "\n\n En orden   :  ";   enOrden(arbol,a);
	cout << "\n\n Pre Orden  :  ";   preOrden(arbol,a);
	cout << "\n\n Post Orden :  ";   postOrden(arbol,a);
	
	cout << endl << endl;
	
	system("pause");
	return 0;
}


