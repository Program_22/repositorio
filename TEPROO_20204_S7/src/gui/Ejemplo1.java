package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import clase.Estudiante;
import clase.Facultad;
import clase.Universidad;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejemplo1 extends JFrame implements ActionListener {
	private JButton btnAceptar;
	private JScrollPane scpSalida;
	private JTextArea txtSalida;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejemplo1 frame = new Ejemplo1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejemplo1() {
		setBounds(100, 100, 246, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(this);
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAceptar.setBounds(84, 14, 126, 25);
		getContentPane().add(btnAceptar);
		
		scpSalida = new JScrollPane();
		scpSalida.setBounds(13, 53, 264, 147);
		getContentPane().add(scpSalida);
		
		txtSalida = new JTextArea();
		scpSalida.setViewportView(txtSalida);

	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btnAceptar) {
			btnAceptaractionPerformed(arg0);
		}
	}
	
	protected void btnAceptaractionPerformed(ActionEvent arg0) {

		//Implementar la agregaci�n Universidad (todo) y Estudiante (parte)
		//Crear los estudiantes
		Estudiante objEstudiante1 = new Estudiante("E00001", "Juan P�rez");
		Estudiante objEstudiante2 = new Estudiante("E00002", "Olga Talledo");
		
		//Crear la universidad
		Universidad objUniversidad = new Universidad("U001", "UPN");
		
		//Agregar los estudiantes a la universidad
		objUniversidad.setObjEstudiante1(objEstudiante1);
		objUniversidad.setObjEstudiante2(objEstudiante2);
		
		imprimir("Universidad: " + objUniversidad.getNombre());
		imprimir("Estudiante1: " + objUniversidad.getObjEstudiante1().getNombre());
		imprimir("Estudiante2: " + objUniversidad.getObjEstudiante2().getNombre());
		
		//Implementar la composici�n Universidad (todo) y Facultad (parte)
		objUniversidad.getObjFacultad().setIdFacultad("F00001");
		objUniversidad.getObjFacultad().setNombre("Ingenier�a");

		imprimir("Facultad: " + objUniversidad.getObjFacultad().getNombre() );
	}
	
	public void imprimir(String s) {
		txtSalida.append(s + "\n");
	}
}


