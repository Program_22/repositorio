package clase;

public class Universidad {
	
	private String idUniversidad, nombre;
	
	//Premisa: Una universidad esta formada por 0 ó 2 estudiantes
	//Atributos para la agreación
	private Estudiante objEstudiante1, objEstudiante2;

	//Premisa: Una universidad esta formada (compuesta) por 1 facultad
	//Atributos para la composición
	private Facultad objFacultad;

	public Universidad(String idUniversidad, String nombre) {
		this.idUniversidad = idUniversidad;
		this.nombre = nombre;
		
		//Implementación de la composición
		objFacultad = new Facultad();
	}

	public String getIdUniversidad() {
		return idUniversidad;
	}

	public void setIdUniversidad(String idUniversidad) {
		this.idUniversidad = idUniversidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Estudiante getObjEstudiante1() {
		return objEstudiante1;
	}

	//Implementar la agreación.
	//El objEstudiante1 fue creado fuera de la clase Universidad
	//Su existencia no depende de la existencia de la Universidad
	public void setObjEstudiante1(Estudiante objEstudiante1) {
		this.objEstudiante1 = objEstudiante1;
	}

	public Estudiante getObjEstudiante2() {
		return objEstudiante2;
	}

	//Implementar la agreación.
	//El objEstudiante2 fue creado fuera de la clase Universidad
	//Su existencia no depende de la existencia de la Universidad
	public void setObjEstudiante2(Estudiante objEstudiante2) {
		this.objEstudiante2 = objEstudiante2;
	}

	//Implementar la agreación.
	//El objEstudiante2 fue creado fuera de la clase Universidad
	//Su existencia no depende de la existencia de la Universidad
	public Facultad getObjFacultad() {
		return objFacultad;
	}

	public void setObjFacultad(Facultad objFacultad) {
		this.objFacultad = objFacultad;
	}
	
}
