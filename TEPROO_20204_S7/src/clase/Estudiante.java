package clase;

public class Estudiante {
	
	private String idEstudiante, nombre;

	public Estudiante(String idEstudiante, String nombre) {
		this.idEstudiante = idEstudiante;
		this.nombre = nombre;
	}

	public String getIdEstudiante() {
		return idEstudiante;
	}

	public void setIdEstudiante(String idEstudiante) {
		this.idEstudiante = idEstudiante;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}



