package com.example.demo.herencia;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="profesores")
@PrimaryKeyJoinColumn(referencedColumnName="pro_persona")
public class Profesor {
	@Column(name="pro_especiliadad")
	private String especialidad;

	public Profesor(String especialidad) {
	
		this.especialidad = especialidad;
	}

	public Profesor() {

	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	

}
