package com.example.demo.herencia;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="personas")
@Inheritance(strategy=InheritanceType.JOINED)
public class Persona {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	@Column(name="per_codigo")
	private int codigo; 
	
	@Column(name="per_nombre")
	private String nombre;
	
	@Column(name="per_documento")
	private String documento; 
	
	@Column(name="per_direccion")
	private String direccion;

	public Persona() {

	}

	public Persona(int codigo, String nombre, String documento, String direccion) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.documento = documento;
		this.direccion = direccion;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	} 
	
}
