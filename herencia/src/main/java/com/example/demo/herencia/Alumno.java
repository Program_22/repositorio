package com.example.demo.herencia;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="Alumnos")
@PrimaryKeyJoinColumn(referencedColumnName="alu_persona")
public class Alumno extends Persona{
	
	@Column(name="alu_semestre")
	private String semestre;

	public String getSemestre() {
		return semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	public Alumno() {

	}

	public Alumno(String semestre) {
		super();
		this.semestre = semestre;
	}

}