package proyectocine;

public class ClienteCine {
//Declarar atributos para la clase
private String NomPelicula;
private int NEntradas;
private int Dia;
private int precio;
private double Consumo;
private double Desc;
private double Total; 
//Crear Constructores para la clase Cliente Cine
public ClienteCine(){
    }
public ClienteCine(String Nom,int NE){
    this.NomPelicula=Nom;
    this.NEntradas=NE;
    }
public ClienteCine(int Dia){
    this.Dia=Dia;
}
    //Método para leer el Nombre de Película 
    public String getNomPelicula() {
        return NomPelicula;
    }

    //Método para escribir el Nombre de Película
    public void setNomPelicula(String NomPelicula) {
        this.NomPelicula = NomPelicula;
    }

   
    public int getNEntradas() {
        return NEntradas;
    }

   
    public void setNEntradas(int NEntradas) {
        this.NEntradas = NEntradas;
    }

   
    public int getDia() {
        return Dia;
    }

    
    public void setDia(int Dia) {
        this.Dia = Dia;
    }

    
    public int getPrecio() {
        return precio;
    }

    
    public void setPrecio(int precio) {
        this.precio = precio;
    }

    
    public double getConsumo() {
        return Consumo;
    }

    
    public void setConsumo(double Consumo) {
        this.Consumo = Consumo;
    }

   public double getDesc() {
       return Desc;
    }

   
    public void setDesc(double Desc) {
        this.Desc = Desc;
    }

   
    public double getTotal() {
        return Total;
    }

    public void setTotal(double Total) {
        this.Total = Total;
    }
  //Método para calcular el descuento
    public void CalcularDescuento(){
        //Si el Número de Entradas es >4
        if(getNEntradas()>4)
            setDesc(0.05*getNEntradas()*getPrecio());
        else setDesc(0);    
    }
    //Método para calcular el Total
    public void CalcularTotal(int Dia){
        switch(Dia){
            case 1: setPrecio(8);break;
            case 2: setPrecio(6);break;
            case 3: setPrecio(8);break;    
            case 4: setPrecio(8);break;    
            case 5: setPrecio(10);break;    
            case 6: setPrecio(10);break;    
            case 7: setPrecio(10);break;
            default: System.out.println("Dia Equivocado");break;    
        }
        setTotal(getNEntradas()*getPrecio()-getDesc());
    }
    //Método para Imprimir Boleto
    public void ImprimirBoleto(){
        System.out.println("Película   :       " + getNomPelicula());
        System.out.println("Entradas :       " + getNEntradas());
        System.out.println("Total a Pagar : " + getTotal());
    }   
}
