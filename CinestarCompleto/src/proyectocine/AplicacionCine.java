package proyectocine;

//Importar la Clase para la lectura del Buffer del Teclado
import java.io.BufferedReader;
//Importar la Clase para el Control de Errores
import java.io.IOException;
//Importar la clase para la lectura de datos por teclado
import java.io.InputStreamReader;
public class AplicacionCine {
    //Crear el Método main() para ejecutar la clase
    // y Asociarlo al control de errores
    public static void main(String args[]) throws IOException{
        //Crear el Arreglo Venta Asociado a la clase ClienteCine
        //Arreglo Venta para 50 elementos
        ClienteCine[] Venta = new ClienteCine[50];
        //Inicializar el Arreglo Venta a traves del objeto unaVenta
        ClienteCine unaVenta = new ClienteCine();
        //Declarar otras variables
        int N=0, i=0, j=0;
        char resp;
        //Crear el objeto p para la lectura de datos por teclado
        InputStreamReader p = new InputStreamReader(System.in);
        //Asociar el objeto p al Buffer del teclado
        BufferedReader q= new BufferedReader (p);
        //Pedir datos para la película
        do
        {
            System.out.println("Ingrese Pelicula y Nro. de entradas : " );
            Venta[i] = new ClienteCine(q.readLine(),Integer.parseInt(q.readLine()));
            System.out.println("Ingrese Dia Lunes=1....Domingo=7 : ");
            Venta[i].CalcularTotal(Integer.parseInt(q.readLine()));
            //Calcular Descuento
            Venta[i].CalcularDescuento();
            //Imprimir Boleto
            Venta[i].ImprimirBoleto();
            System.out.println("Desea Continuar S/N : ");
            resp = q.readLine().charAt(0);
            i++; //Incrementar i en 1
        }
        while(((resp=='s') ||(resp=='S')) && (i<50));
        //Mostrar Resultados
        for (j=0;j<i;j++){
            N=N+Venta[j].getNEntradas();    
        }
        System.out.println("Se Vendieron " + N+ " Entradas");    
    }
}
