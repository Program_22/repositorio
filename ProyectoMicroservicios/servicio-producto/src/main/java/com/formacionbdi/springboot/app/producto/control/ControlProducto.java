package com.formacionbdi.springboot.app.producto.control;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
<<<<<<< HEAD
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
=======
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
>>>>>>> dev
import org.springframework.web.bind.annotation.RestController;

import com.formacionbdi.springboot.app.producto.models.entity.Producto;
import com.formacionbdi.springboot.app.producto.models.servicio.Sproducto;

@RestController
public class ControlProducto {
	@Autowired
<<<<<<< HEAD
	private Sproducto service; 
	
	@Value("${server.port}")
	private Integer port;



@GetMapping("/listar")
public List<Producto> listar(){
	return service.findAll().stream().map(producto->{
//		producto.setPort(Integer.parseInt(env.getProperty("local.server.port")));
		producto.setPort(port);
		return producto;
	}).collect(Collectors.toList());
}
@GetMapping("/ver/{id}")
public Producto detalle(@PathVariable Long id) {
	Producto producto=service.findById(id);
//	producto.setPort(Integer.parseInt(env.getProperty("local.server.port")));
	producto.setPort(port);
	return producto;
}
=======
	private Sproducto service;

	@Value("${server.port}")
	private Integer port;

	@GetMapping("/listar")
	public List<Producto> listar() {
		return service.findAll().stream().map(producto -> {
//		producto.setPort(Integer.parseInt(env.getProperty("local.server.port")));
			producto.setPort(port);
			return producto;
		}).collect(Collectors.toList());
	}

	@GetMapping("/ver/{id}")
	public Producto detalle(@PathVariable Long id) {
		Producto producto = service.findById(id);
//	producto.setPort(Integer.parseInt(env.getProperty("local.server.port")));
		producto.setPort(port);
		return producto;
	}

	@PostMapping("/crear")
	@ResponseStatus(HttpStatus.CREATED)
//Señala todo el cuerpo producto 
	public Producto crear(@RequestBody Producto producto) {
		return service.save(producto);

	}

	@PutMapping("/editar/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Producto editar(@RequestBody Producto producto, @PathVariable Long id) {

// Selecciona el valor que voy a modificar id
		Producto productoDb = service.findById(id);
//Insertar el valor nombre
		productoDb.setNombre(producto.getNombre());
//Inserta el valor precio
		productoDb.setPrecio(producto.getPrecio());
//Devuelve el ingreso al service y guardalo
		return service.save(productoDb);
	}

	@DeleteMapping("/eliminar/{id}")
	@ResponseStatus(HttpStatus.CREATED)
//Solo se puede poner el valor del id
	public void eliminar(@PathVariable Long id) {
		// Eliminar este valor escogido
		service.deleteById(id);
	}
>>>>>>> dev
}