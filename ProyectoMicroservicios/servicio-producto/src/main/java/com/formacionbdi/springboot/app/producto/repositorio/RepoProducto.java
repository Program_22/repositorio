package com.formacionbdi.springboot.app.producto.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacionbdi.springboot.app.producto.models.entity.Producto;
@Repository
public interface RepoProducto extends JpaRepository<Producto,Long>{

}
