//Desarrollado por Gus.....
package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import arreglo.ArregloEstudiantes;
import clase.Estudiante;

import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.awt.event.ActionEvent;
import javax.swing.JTable;

public class Ejemplo1 extends JFrame implements ActionListener {
	//Variables globales
	private JLabel lblCodigo;
	private JLabel lblNombre;
	private JLabel lblFacultad;
	private JLabel lblNota1;
	private JLabel lblNota2;
	private JTextField txtCodigo;
	private JTextField txtNombre;
	private JComboBox<String> cboFacultad;
	private JTextField txtNota1;
	private JTextField txtNota2;
	private JScrollPane scpSalida;
	private JButton btnAgregar;
	private JButton btnLimpiar;
	private JButton btnGuardar;
	private JButton btnRecuperar;
	private JTable tblEstudiantes;
	private DefaultTableModel dtmEstudiantes;
	
	//Arreglo de Estudiantes (variable global)
	ArregloEstudiantes aEstudiantes;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejemplo1 frame = new Ejemplo1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejemplo1() {
		
		setBounds(100, 100, 514, 421);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		lblCodigo = new JLabel("Codigo");
		lblCodigo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCodigo.setBounds(10, 10, 90, 25);
		getContentPane().add(lblCodigo);
		
		lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNombre.setBounds(10, 40, 90, 25);
		getContentPane().add(lblNombre);
		
		lblFacultad = new JLabel("Facultad");
		lblFacultad.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblFacultad.setBounds(10, 70, 90, 25);
		getContentPane().add(lblFacultad);
		
		lblNota1 = new JLabel("Nota1");
		lblNota1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNota1.setBounds(10, 100, 90, 25);
		getContentPane().add(lblNota1);
		
		lblNota2 = new JLabel("Nota2");
		lblNota2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNota2.setBounds(10, 130, 90, 25);
		getContentPane().add(lblNota2);
		
		txtCodigo = new JTextField();
		txtCodigo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txtCodigo.setBounds(110, 10, 80, 25);
		getContentPane().add(txtCodigo);
		txtCodigo.setColumns(10);
		
		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txtNombre.setColumns(10);
		txtNombre.setBounds(110, 40, 150, 25);
		getContentPane().add(txtNombre);
		
		cboFacultad = new JComboBox<String>();

		//Cargando el JComboBox manualmente
		cboFacultad.addItem("-- Seleccione --"); 	//Indice: 0
		cboFacultad.addItem("Ingenieria"); 			//Indice: 1
		cboFacultad.addItem("Medicina"); 			//Indice: 2
		cboFacultad.addItem("Humanidades"); 		//Indice: 3
		cboFacultad.addItem("Psicologia"); 			//Indice: 4
		cboFacultad.setFont(new Font("Tahoma", Font.PLAIN, 12));
		cboFacultad.setBounds(110, 70, 150, 25);
		getContentPane().add(cboFacultad);
		
		txtNota1 = new JTextField();
		txtNota1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txtNota1.setColumns(10);
		txtNota1.setBounds(110, 100, 80, 25);
		getContentPane().add(txtNota1);
		
		txtNota2 = new JTextField();
		txtNota2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txtNota2.setColumns(10);
		txtNota2.setBounds(110, 130, 80, 25);
		getContentPane().add(txtNota2);
		
		scpSalida = new JScrollPane();
		scpSalida.setBounds(10, 163, 460, 160);
		getContentPane().add(scpSalida);
		
		//***************************//
		//  Construcci�n del JTable  //
		//***************************//
		tblEstudiantes = new JTable();
		tblEstudiantes.setFillsViewportHeight(true);
		scpSalida.setViewportView(tblEstudiantes);
		
		//**************************************************//
		//  Creaci�n y configuraci�n del DefaultTableModel  //
		//  Aqu� se cargar�n los datos del ArregloAlumnos   //
		//**************************************************//
		dtmEstudiantes = new DefaultTableModel();
		dtmEstudiantes.addColumn("C�digo");		//Primera columna con indice 0
		dtmEstudiantes.addColumn("Nombre");		//Segunda columna con indice 1
		dtmEstudiantes.addColumn("Facultad");	//Tercera columna con indice 2
		dtmEstudiantes.addColumn("Nota 2");		//Cuarta columna con indice 3
		dtmEstudiantes.addColumn("Nota 2");		//Quinta columna con indice 4
		dtmEstudiantes.addColumn("Promedio");	//Sexta columna con indice 5
		tblEstudiantes.setModel(dtmEstudiantes);
		
		//*************************************************//
		//  Creaci�n y configuraci�n del TableColumnModel  //
		//  Aqu� se definen las columnas del JTable        //
		//*************************************************//
		TableColumnModel tcmEdtudiantes = tblEstudiantes.getColumnModel(); 
		tcmEdtudiantes.getColumn(0).setPreferredWidth(10);	//Ancho de 10 puntos
		tcmEdtudiantes.getColumn(1).setPreferredWidth(25);	//Ancho de 25 puntos
		tcmEdtudiantes.getColumn(2).setPreferredWidth(25);	//Ancho de 25 puntos
		tcmEdtudiantes.getColumn(3).setPreferredWidth(15);	//Ancho de 15 puntos
		tcmEdtudiantes.getColumn(4).setPreferredWidth(15);	//Ancho de 15 puntos
		tcmEdtudiantes.getColumn(5).setPreferredWidth(15);	//Ancho de 15 puntos
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(this);
		btnAgregar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAgregar.setBounds(370, 10, 100, 25);
		getContentPane().add(btnAgregar);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(this);
		btnLimpiar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnLimpiar.setBounds(370, 40, 100, 25);
		getContentPane().add(btnLimpiar);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(this);
		btnGuardar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnGuardar.setBounds(370, 70, 100, 25);
		getContentPane().add(btnGuardar);
		
		btnRecuperar = new JButton("Recuperar");
		btnRecuperar.addActionListener(this);
		btnRecuperar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnRecuperar.setBounds(370, 100, 100, 25);
		getContentPane().add(btnRecuperar);
		
		//Construir el arreglo de estudiantes
		aEstudiantes = new ArregloEstudiantes();

	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btnRecuperar) {
			btnRecuperaractionPerformed(arg0);
		}
		if (arg0.getSource() == btnGuardar) {
			btnGuardaractionPerformed(arg0);
		}
		if (arg0.getSource() == btnAgregar) {
			btnAceptaractionPerformed(arg0);
		}
		if (arg0.getSource() == btnLimpiar) {
			btnLimpiaractionPerformed(arg0);
		}
	}
	
	protected void btnLimpiaractionPerformed(ActionEvent arg0) {
		txtCodigo.setText("");
		txtNombre.setText("");
		txtNota1.setText("");
		txtNota2.setText("");
		cboFacultad.setSelectedIndex(0); 	//Selecciona el elemento en la posici�n (indice) 0
	}
	
	protected void btnAceptaractionPerformed(ActionEvent arg0) {
		//Variables locales
		int codigo, facultad;
		String nombre;
		double nota1, nota2;

		codigo = Integer.parseInt( txtCodigo.getText() );
		nombre = txtNombre.getText();
		facultad = cboFacultad.getSelectedIndex();
		nota1 = Double.parseDouble( txtNota1.getText() );
		nota2 = Double.parseDouble( txtNota2.getText() );
		
		//Creaci�n del objeto Estudiante
		Estudiante objEstudiante = new Estudiante(codigo, nombre, facultad, nota1, nota2);
		aEstudiantes.agregar(objEstudiante);
		listado();
	}
	
	public void listado() {
		Estudiante objE;
		dtmEstudiantes.setRowCount(0);
		for(int i = 0; i < aEstudiantes.elementos(); i++) {
			objE = aEstudiantes.obtener(i);

			Object[] tupla = { objE.getCodigo(),
					   objE.getNombre(),
					   objE.getFacultad() + ".- " + cboFacultad.getItemAt(objE.getFacultad()),
					   objE.getNota1() + " " + ((objE.getNota1() == Estudiante.NOTA_NO_INGRESADA)?"(Sin Registro)":""),
					   objE.getNota2() + " " + ((objE.getNota2() == Estudiante.NOTA_NO_INGRESADA)?"(Sin Registro)":""),
					   objE.promedio() };
	
			dtmEstudiantes.addRow(tupla);
		}
	}
	
	public void btnGuardaractionPerformed(ActionEvent arg0) {
		boolean exito = ArregloEstudiantes.archivar(aEstudiantes);
		if(!exito) {
			JOptionPane.showMessageDialog(this, "No se pudo almacenar la colecci�n.","Guardar colecci�n",JOptionPane.ERROR_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(this, "La colecci�n se registr� satisfactoriamente.","Guardar colecci�n",JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	public void btnRecuperaractionPerformed(ActionEvent arg0) {
		ArregloEstudiantes objArregloEstudiantes = ArregloEstudiantes.recuperar();
		if(objArregloEstudiantes == null) {
			JOptionPane.showMessageDialog(this, "No se pudo recuperar la colecci�n.","Guardar colecci�n",JOptionPane.ERROR_MESSAGE);
		} else {
			aEstudiantes = objArregloEstudiantes;
			listado();
		}
	}
}





