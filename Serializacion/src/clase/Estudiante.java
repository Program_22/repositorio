package clase;

public class Estudiante{
	private int codigo, facultad;
	private double nota1, nota2;
	private String nombre;
	private static int cantidad;	//Variable estatica que se queda a nivel de la clase
	public static final double NOTA_NO_INGRESADA;
	
	//Bloque de inicialización de cantida
	static {
		cantidad = 0;
		NOTA_NO_INGRESADA = 77;
	}

	//Constructor sin parámetros
	public Estudiante() {
		cantidad++;
	}

	//Constructor inicializa los atributos con los parámetros recibidos
	public Estudiante(int codigo, String nombre, int facultad, double nota1, double nota2) {
		this();
		this.codigo = codigo;
		this.nombre = nombre;
		this.facultad = facultad;
		this.nota1 = nota1;
		this.nota2 = nota2;
	}
	
	//Constructor inicializa solo los atributos codigo, nombre y facultad
	public Estudiante(int codigo, String nombre, int facultad) {
		//Invocando al Constructor de 5 parámetros
		this(codigo, nombre, facultad, Estudiante.NOTA_NO_INGRESADA, Estudiante.NOTA_NO_INGRESADA);
	}
	
	//"Setters" - Asignación en cada atributo
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setFacultad(int facultad) {
		this.facultad = facultad;
	}

	public void setNota1(double nota1) {
		this.nota1 = nota1;
	}

	public void setNota2(double nota2) {
		this.nota2 = nota2;
	}

	//Setter de la variable static
	public static void setCantidad(int cantidad) {
		Estudiante.cantidad = cantidad;
	}

	//"Getters" - Devuelve el valor de cada atributo
	public int getCodigo() {
		return this.codigo;
	}

	public String getNombre() {
		return this.nombre;
	}

	public int getFacultad() {
		return this.facultad;
	}

	public double getNota1() {
		return this.nota1;
	}

	public double getNota2() {
		return this.nota2;
	}
	
	//Gette de la variable static
	public static int getCantidad() {
		return Estudiante.cantidad;
	}
	
	//Operaciones
	public double promedio() {
		return (this.nota1 + this.nota2) / 2.0;
	}

    public String toString(){
        String mensaje = "";
        
		mensaje += "\tCodigo: " + getCodigo() + "\n";
		mensaje += "\tNombre: " + getNombre() + "\n";
		mensaje += "\tFacultad: " + getFacultad() + "\n";
		mensaje += "\tNota1: " + getNota1() + "\n";
		mensaje += "\tNota2: " + getNota2() + "\n";
		mensaje += "\tPromedio: " + promedio() + "\n";
		mensaje += "\t-----------------\n";
		
        return mensaje;
    }
}






