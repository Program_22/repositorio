package arreglo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import clase.Estudiante;

public class ArregloEstudiantes implements Serializable{
	
	//Crear el arreglo din�mico de estudiantes
	private ArrayList<Estudiante> arrEstudiantes = new ArrayList<Estudiante>();

	//Constante para definir el nombre del archivo
	private static final String ARCHIVO_ARREGLO_ESTUDIANTES;
	
	static {
		ARCHIVO_ARREGLO_ESTUDIANTES = "arregloEstudiantes.obj";
	}
	
	//Operaciones sobre el arreglo
	//----------------------------
	//Devuelve la cantidad de elementos en el arreglo
	public int elementos() {
		return arrEstudiantes.size();	//Retorna tama�o del array
	}

	//Agregar un objeto al arreglo
	public void agregar(Estudiante e) {
		arrEstudiantes.add(e);
	}

	//Devuelve el objeto Estudiante en la posicion recibida
	public Estudiante obtener(int posicion) {
		return arrEstudiantes.get(posicion);
	}
	
	//Ubica un objeto Estudiante segun su valor de Codigo
	public Estudiante buscar(int codigo) {
		for(int i = 0; i < elementos(); i++) {
			if(obtener(i).getCodigo() == codigo) {
				return obtener(i);
			}
		}
		
		return null;
	}
	
	//Igual que buscar(int) pero usando foreach
	public Estudiante encontrar(int codigo) {
		//Recorrer la colecci�n de objetos
		for(Estudiante objE: arrEstudiantes) {
			if(objE.getCodigo() == codigo) {
				return objE;
			}
		}
		
		return null;
	}
	
	//Ubica un objeto Estudiante segun su valor de Codigo y lo elimina
	public boolean eliminar(int codigo) {
		Estudiante objEstudiante = buscar(codigo);
		if(objEstudiante != null) {
			arrEstudiantes.remove(objEstudiante);
			return true;
		}
		
		return false;
	}

	
	//Construye una cadena con cada elemento del arreglo
	public String toString() {
		String mensaje = "Reporte r�pido del arreglo\n";
		
		//Concatenar cada objeto como cadena
		for(Estudiante objE: arrEstudiantes) {
			mensaje += objE.toString();
		}
		
		return mensaje;
	}
	
	//Enviar la colecci�n a un archivo
	public static boolean archivar(ArregloEstudiantes objArregloEstudiantes) {
		try {
			ObjectOutputStream objeto = new ObjectOutputStream(new FileOutputStream(ARCHIVO_ARREGLO_ESTUDIANTES));
			objeto.writeObject("Arreglo de estudiantes\n");	//Escribir linea descriptiva (opcional)
			objeto.writeObject(objArregloEstudiantes);		//Escribir el arreglo de objetos
			objeto.close();		
			System.out.println("--> Serializar objeto: " + "(" + objArregloEstudiantes.getClass() + ")\n" + objArregloEstudiantes);
			return true;
		}catch(Exception e) {
            System.out.println("--> Fallo al serializar el objeto: " + e.getMessage());
			return false;
		}
	}
	
	public static ArregloEstudiantes recuperar() {
		try {
            ObjectInputStream objeto = new ObjectInputStream(new FileInputStream(ARCHIVO_ARREGLO_ESTUDIANTES));
            String cabecera = (String) objeto.readObject();
            ArregloEstudiantes objArregloEstudiantes = (ArregloEstudiantes) objeto.readObject();
            objeto.close();
            System.out.println("--> Objeto recuperado: " + "(" + objArregloEstudiantes.getClass() + ")\n" + objArregloEstudiantes);
			return objArregloEstudiantes;
		}catch(Exception e) {
            System.out.println("--> Fallo en la recuperaci�n del objeto serializado: " + e.getMessage());
			return null;
		}
	}
}


















