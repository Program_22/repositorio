//Clase para definir las propiedades del libro
package PCK_Biblioteca;

public class Libro {
    // datos privados de la clase
    private String  Titulo, Autor, Codigo,Editorial, FechaDev;
    private String dia, mes, anio, Estado;
    private	int Edicion;
    //Definir el constructor de la clase libro
    // constructor vacío
    public Libro() { }// fin del constructor vacío
    //Inicializar las propiedades del libro
    //Crear un constructor que recibe como objeto el libro llamado uno
    public Libro(Libro uno){
        setTitulo(uno.getTitulo());
        setAutor(uno.getAutor());
        setCodigo(uno.getCodigo());
        setEditorial(uno.getEditorial());
        setFechaDev(uno.getFechaDev());	
        setDia(uno.getDia());
        setMes(uno.getMes());
        setAnio(uno.getAnio());
        setEstado(uno.getEstado());
        setEdicion(uno.getEdicion());
}// fin del constructor
    
    
    //Definir los metodos de lectura y escritura
    //Metodo para leer el titulo
    public String getTitulo() {
        return Titulo;
    }

    //Metodo para escribir el titulo
    public void setTitulo(String Titulo) {
        this.Titulo = Titulo;
    }

    /**
     * @return the Autor
     */
    public String getAutor() {
        return Autor;
    }

    /**
     * @param Autor the Autor to set
     */
    public void setAutor(String Autor) {
        this.Autor = Autor;
    }

    /**
     * @return the Codigo
     */
    public String getCodigo() {
        return Codigo;
    }

    /**
     * @param Codigo the Codigo to set
     */
    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    /**
     * @return the Editorial
     */
    public String getEditorial() {
        return Editorial;
    }

    /**
     * @param Editorial the Editorial to set
     */
    public void setEditorial(String Editorial) {
        this.Editorial = Editorial;
    }

    /**
     * @return the FechaDev
     */
    public String getFechaDev() {
        return FechaDev;
    }

    /**
     * @param FechaDev the FechaDev to set
     */
    public void setFechaDev(String FechaDev) {
        this.FechaDev = FechaDev;
    }

    /**
     * @return the dia
     */
    public String getDia() {
        return dia;
    }

    /**
     * @param dia the dia to set
     */
    public void setDia(String dia) {
        this.dia = dia;
    }

    /**
     * @return the mes
     */
    public String getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(String mes) {
        this.mes = mes;
    }

    /**
     * @return the anio
     */
    public String getAnio() {
        return anio;
    }

    /**
     * @param anio the anio to set
     */
    public void setAnio(String anio) {
        this.anio = anio;
    }

    /**
     * @return the Estado
     */
    public String getEstado() {
        return Estado;
    }

    /**
     * @param Estado the Estado to set
     */
    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    /**
     * @return the Edicion
     */
    public int getEdicion() {
        return Edicion;
    }

    /**
     * @param Edicion the Edicion to set
     */
    public void setEdicion(int Edicion) {
        this.Edicion = Edicion;
    }
    
}
