
package PCK_Biblioteca;

//Importar la clase JOptionPane para mensajes
import javax.swing.JOptionPane;

public class PanelIngresos extends javax.swing.JPanel {
    //objeto de admiinstración de todos los libros
    private static Libros libros = new Libros();
    //Metodo para leer libros
    public static Libros getLibros() {
        return libros;
    }
    // un objeto para administrar un libro
    private Libro unLibro = new Libro();

    /** Creates new form PanelIngresos */
    public PanelIngresos() {
        initComponents();
        //Agregar los años a la lista Edicion
	for (int i=2000; i <=2050; i++)
          cboEdicion.addItem(""+i);

    }

  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnAceptar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        txtRegistro = new javax.swing.JTextField();
        txtCodigo = new javax.swing.JTextField();
        txtTitulo = new javax.swing.JTextField();
        txtAutor = new javax.swing.JTextField();
        txtEditorial = new javax.swing.JTextField();
        cboEdicion = new javax.swing.JComboBox();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtSalida = new javax.swing.JTextArea();

        setBackground(new java.awt.Color(48, 179, 190));
        setLayout(new java.awt.BorderLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("INGRESOS");
        add(jLabel1, java.awt.BorderLayout.PAGE_START);

        jPanel1.setLayout(new java.awt.GridLayout(1, 3));

        btnAceptar.setText("Aceptar");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });
        jPanel1.add(btnAceptar);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel1.add(btnCancelar);

        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        jPanel1.add(btnSalir);

        add(jPanel1, java.awt.BorderLayout.PAGE_END);

        jPanel2.setLayout(new java.awt.GridLayout(1, 3));

        jPanel3.setBackground(new java.awt.Color(48, 179, 190));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Etiquetas"));
        jPanel3.setLayout(new java.awt.GridLayout(6, 1));

        jLabel2.setText("# Registro");
        jPanel3.add(jLabel2);

        jLabel3.setText("Código");
        jPanel3.add(jLabel3);

        jLabel4.setText("Título");
        jPanel3.add(jLabel4);

        jLabel5.setText("Autor");
        jPanel3.add(jLabel5);

        jLabel6.setText("Editorial");
        jPanel3.add(jLabel6);

        jLabel7.setText("Año de Edición");
        jPanel3.add(jLabel7);

        jPanel2.add(jPanel3);

        jPanel4.setBackground(new java.awt.Color(48, 179, 190));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));
        jPanel4.setLayout(new java.awt.GridLayout(6, 1));

        txtRegistro.setEditable(false);
        jPanel4.add(txtRegistro);
        jPanel4.add(txtCodigo);
        jPanel4.add(txtTitulo);
        jPanel4.add(txtAutor);
        jPanel4.add(txtEditorial);

        jPanel4.add(cboEdicion);

        jPanel2.add(jPanel4);

        jPanel5.setBackground(new java.awt.Color(48, 179, 190));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Salida"));
        jPanel5.setLayout(new java.awt.BorderLayout());

        txtSalida.setColumns(20);
        txtSalida.setEditable(false);
        txtSalida.setRows(5);
        jScrollPane1.setViewportView(txtSalida);

        jPanel5.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel2.add(jPanel5);

        add(jPanel2, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
    //llamar a la funcio Aceptar ingresos
    aceptarIngresos();
}//GEN-LAST:event_btnAceptarActionPerformed

private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
    //llamar a la funcion Preparar nuevo ingreso
    prepararNuevoIngreso();
}//GEN-LAST:event_btnCancelarActionPerformed

private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
    String msg="Salir de la Aplicacion";
    JOptionPane.showMessageDialog(this, msg);
}//GEN-LAST:event_btnSalirActionPerformed
//-----------------------------
//Crear la funcion Aceptar Ingresos
public void aceptarIngresos() {
    //Llamar a la funcion Ingresar Datos
    ingresarDatos();
    //Llamar a la funcion Datos correctos
    if ( datosCorrectos() )	{
	//Buscar si existe el libro pasando su codigo
        int i=getLibros().busca(getUnLibro().getCodigo());
	if(i == -1) {// codigo NO REPETIDO (libro no existe)
            almacenaDatos(); //Grabar los datos
            mostrarIngresos(); //Mostrar el ingreso
            prepararNuevoIngreso(); //Otro ingreso
		}	
	else{
          JOptionPane.showMessageDialog(this,"Código Repetido");
          txtCodigo.requestFocus();
	}	
	}
}

//Funcion para ingresar datos
public void ingresarDatos() {
    // creamos espacio en memoria para un libro
    unLibro=new Libro();//Crear la instancia
	
    //ingresar datos de un libro
    getUnLibro().setCodigo( leerCodigo() );
    getUnLibro().setTitulo( leerTitulo() );
    getUnLibro().setAutor( leerAutor() );
    getUnLibro().setEditorial( leerEditorial() );
    getUnLibro().setEdicion( Integer.parseInt(leerAnioDeEdicion()));
    getUnLibro().setEstado("Libre");
    getUnLibro().setFechaDev("00/00/00");
}
//Metodo para leer el codigo
public String leerCodigo() {
    return txtCodigo.getText();
}
//Metodo para leer titulo
public String leerTitulo() {
    return txtTitulo.getText();
}
//Metodo para leer autor
public String leerAutor() {
    return txtAutor.getText();
}
//Metodo para leer editorial
public String leerEditorial() {
    return  txtEditorial.getText();
}
//Metodo para leer año de edicion
public String leerAnioDeEdicion() {
    String AnioEdicion;
    AnioEdicion = cboEdicion.getSelectedItem().toString();
    return AnioEdicion; 
}
//Funcion para validar datos ingresados
public boolean datosCorrectos() {
	String ValidarCodigo;	
	int longitudCodi;
	// leer codigo 
	ValidarCodigo = txtCodigo.getText();
	// obtener longitud del codigo leido
	longitudCodi = ValidarCodigo.length();  
	// verificar que tenga 5 caracteres	
	if ( longitudCodi < 5 || longitudCodi > 5)	{
            mostarMensaje(longitudCodi);
            return false; // datos incorrectos
	}
	else
            return true; // datos correctos
}
//Funcion para mostrar mensaje
public void mostarMensaje(int ContCodi) {
 JOptionPane.showMessageDialog(this,"El Código tiene " +
  ContCodi + " cifras y debe tener 5 cifras");
 txtCodigo.requestFocus();
}
//Funcion para almacenar datos en el vector 
public void almacenaDatos() {
// guardar un libro en el vector
getLibros().addLibro(getUnLibro());
// imprimir numero de libros
mostrarNumeroDeRegistro();
}
//Funcion para mostrar el numero de registro
public void mostrarNumeroDeRegistro() {
 txtRegistro.setText(""+getLibros().numeroLibros());
}
//Funcion para mostrar ingresos
public void mostrarIngresos() {
 borrarArea();
// mostrar datos ingresados de un libro
imprime("Número de Registro :" + getLibros().numeroLibros());
imprime("Código\t:" + getUnLibro().getCodigo());
imprime("Titulo\t:" + getUnLibro().getTitulo());
imprime("Autor\t:" + getUnLibro().getAutor());
imprime("Editorial\t:" + getUnLibro().getEditorial());
imprime("Año de Edición\t:" + getUnLibro().getEdicion());
//adicionar los códigos ingresados a los combos respectivos
//para su posterior consulta

PanelMovimientos.getComboCodigos().addItem(getUnLibro().getCodigo());
PanelDisponibilidad.getComboCodigos().addItem(getUnLibro().getCodigo());
}
//Limpiar la salida de datos
public void borrarArea() {
    txtSalida.setText("");
}
//Imprimir texto en la salida de datos
public void imprime(String cadena) {
    txtSalida.append(cadena + "\n");
}
//Funcion para preparar nuevo ingreso
public void prepararNuevoIngreso(){
    txtCodigo.setText("");
    txtTitulo.setText("");
    txtAutor.setText("");
    txtEditorial.setText("");
    cboEdicion.setSelectedIndex(0);
    txtCodigo.requestFocus();
}


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox cboEdicion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txtAutor;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtEditorial;
    private javax.swing.JTextField txtRegistro;
    private javax.swing.JTextArea txtSalida;
    private javax.swing.JTextField txtTitulo;
    // End of variables declaration//GEN-END:variables

    //Metodo para leer un libro
    public Libro getUnLibro() {
        return unLibro;
    }

}
