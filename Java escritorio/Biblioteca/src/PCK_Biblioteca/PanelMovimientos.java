
package PCK_Biblioteca;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

public class PanelMovimientos extends javax.swing.JPanel {
    // objetos para acceder a la información de un libro
    private Libro original, cambiado;

    /** Creates new form PanelMovimientos */
    public PanelMovimientos() {
        initComponents();
        // llena ítems del comboBox para el día
        for (int i=1; i <=31; i++)
            cboDia.addItem(""+i);
        // llena ítems del comboBox para el año
	for (int i=2003; i <=2050; i++)
            cboAño.addItem(""+i);
    }

      @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnAceptar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        cboCodigos = new javax.swing.JComboBox();
        txtTitulo = new javax.swing.JTextField();
        txtAutor = new javax.swing.JTextField();
        txtEstado = new javax.swing.JTextField();
        cboNuevoEstado = new javax.swing.JComboBox();
        jPanel5 = new javax.swing.JPanel();
        cboDia = new javax.swing.JComboBox();
        cboMes = new javax.swing.JComboBox();
        cboAño = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(51, 131, 255));
        setLayout(new java.awt.BorderLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("MOVIMIENTOS");
        add(jLabel1, java.awt.BorderLayout.PAGE_START);

        jPanel1.setBackground(new java.awt.Color(51, 131, 255));
        jPanel1.setLayout(new java.awt.GridLayout(1, 3));

        btnAceptar.setText("Aceptar");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });
        jPanel1.add(btnAceptar);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel1.add(btnCancelar);

        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        jPanel1.add(btnSalir);

        add(jPanel1, java.awt.BorderLayout.PAGE_END);

        jPanel2.setBackground(new java.awt.Color(51, 131, 255));
        jPanel2.setLayout(new java.awt.GridLayout(1, 2));

        jPanel3.setBackground(new java.awt.Color(51, 131, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Etiquetas"));
        jPanel3.setLayout(new java.awt.GridLayout(6, 1));

        jLabel2.setText("Seleccione un Código");
        jPanel3.add(jLabel2);

        jLabel3.setText("Título");
        jPanel3.add(jLabel3);

        jLabel4.setText("Autor");
        jPanel3.add(jLabel4);

        jLabel5.setText("Estado");
        jPanel3.add(jLabel5);

        jLabel6.setText("Elija nuevo estado");
        jPanel3.add(jLabel6);

        jLabel7.setText("Fecha de Devolución");
        jPanel3.add(jLabel7);

        jPanel2.add(jPanel3);

        jPanel4.setBackground(new java.awt.Color(51, 131, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));
        jPanel4.setLayout(new java.awt.GridLayout(6, 1));

        cboCodigos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Codigos Disponibles" }));
        cboCodigos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCodigosActionPerformed(evt);
            }
        });
        jPanel4.add(cboCodigos);

        txtTitulo.setEditable(false);
        jPanel4.add(txtTitulo);

        txtAutor.setEditable(false);
        jPanel4.add(txtAutor);

        txtEstado.setEditable(false);
        jPanel4.add(txtEstado);

        cboNuevoEstado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nuevo Estado", "Libre", "Prestamo en Sala", "Prestamo en Domicilio" }));
        cboNuevoEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboNuevoEstadoActionPerformed(evt);
            }
        });
        jPanel4.add(cboNuevoEstado);

        jPanel5.setBackground(new java.awt.Color(51, 131, 255));
        jPanel5.setLayout(new java.awt.GridLayout(1, 3));

        jPanel5.add(cboDia);

        cboMes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));
        jPanel5.add(cboMes);

        jPanel5.add(cboAño);

        jPanel4.add(jPanel5);

        jPanel2.add(jPanel4);

        add(jPanel2, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

private void cboCodigosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCodigosActionPerformed
    seleccionarLibro();
}//GEN-LAST:event_cboCodigosActionPerformed

private void cboNuevoEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboNuevoEstadoActionPerformed
    seleccionarEstado();
}//GEN-LAST:event_cboNuevoEstadoActionPerformed

private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
    aceptarMovimientos();
}//GEN-LAST:event_btnAceptarActionPerformed

private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
    cancelarMovimientos();
}//GEN-LAST:event_btnCancelarActionPerformed

private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
    String msg="<html><p color=red>Este es un Reto para Ud:<br>"+"<p color=blue size=+2>"+
            "Modifique los constructores de tal manera que:<br>" +
            "El constructor del PanelPrincipal debe recibir al frame cuando es invocado,<br>"+
            "El frame recibido debe enviarse al constructor de éste panel<br>"+
            "En éste botón invoque al método dispose() desde el objeto que recibe al frame</html>";
    JOptionPane.showMessageDialog(this, msg);
}//GEN-LAST:event_btnSalirActionPerformed
public void cancelarMovimientos(){
	// leer posicion del codigo elegido
	int indice=cboCodigos.getSelectedIndex()-1;

	if (indice>=0){// se ha seleccionado un codigo

	// recupera estado original
	PanelIngresos.getLibros().setLibro(indice,original);

	// borra datos mostrados
	txtAutor.setText("");
	txtTitulo.setText("");
	txtEstado.setText("");
	cboDia.setSelectedIndex(0);
	cboMes.setSelectedIndex(0);
	cboAño.setSelectedIndex(0);
	cboNuevoEstado.setSelectedIndex(0);
	
	btnAceptar.setEnabled(false);
	
	// ocultar devolucion
	ocultaDevolucion(false);
	
	// refresca la seleccion de los codigos
	cboCodigos.setSelectedIndex(0);
	}
}
//------------------------------------
public void aceptarMovimientos() {
	int indice;
	String Fecha="",dia,mes,anio;

	// leer posicion del codigo elegido
	indice=cboCodigos.getSelectedIndex()-1;

	if (indice==-1)// no se ha seleccionado codigo
		borrar();
	else{

	// leer dia, mes año elegidos
	dia = cboDia.getSelectedItem().toString();
	mes = cboMes.getSelectedItem().toString();
	anio = cboAño.getSelectedItem().toString();

	// juntar nueva fecha
	Fecha = dia +"/" + mes + "/" + anio;

	// actualiza fecha de devolucion
	cambiado.setFechaDev(Fecha);
	
	// actualiza libro cambiado en el vector
	PanelIngresos.getLibros().setLibro(indice,cambiado);

	ocultaDevolucion(false);
	btnAceptar.setEnabled(false);
	seleccionarLibro();
	}
}	
//------------------------------
public void seleccionarEstado() {
	int posEstado,posCodigo;
	String Estado="";

	// leer posicion de estado y codigo
	posEstado=cboNuevoEstado.getSelectedIndex();
	posCodigo=cboCodigos.getSelectedIndex()-1;

	// si no se ha seleccionado en los combos
	if (posCodigo==-1||posEstado==0)
		btnAceptar.setEnabled(false);
	else{
		btnAceptar.setEnabled(true);
		// determinar nuevo estado del libro
		switch (posEstado) {
			case 1: // libre
				Estado="Libre";
				ocultaDevolucion(false);
				break;
			case 2:// prestamo en sala
				Estado="En Sala";
				ocultaDevolucion(false);
				break;
			case 3:// prestamo a domicilio
				Estado="A Domicilio";
				ocultaDevolucion(true);
				break;
		}
				
	// actualizar nuevo estado en el libro cambiado
	cambiado.setEstado(Estado);
	}
}
//..................................
public void ocultaDevolucion(boolean estado){
	
	jPanel5.setVisible(estado);
}

//------------------------------
public void seleccionarLibro() {
	borrar();
	// lee posicion del codigo seleccionado
	int indice=cboCodigos.getSelectedIndex()-1;
	
	if (indice>=0){
		// obtiene datos del libro seleccionado
		original = PanelIngresos.getLibros().getLibro(indice);
		cambiado = new Libro(original);

		// ver libro seleccionado
		txtTitulo.setText(cambiado.getTitulo());
		txtAutor.setText(cambiado.getAutor());
		txtEstado.setText(cambiado.getEstado());
	}
}
//..................................
public void borrar(){
	txtAutor.setText("");
	txtEstado.setText("");
	txtTitulo.setText("");
}
//----------------------------
public static JComboBox getComboCodigos(){ return cboCodigos;}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox cboAño;
    private static javax.swing.JComboBox cboCodigos;
    private javax.swing.JComboBox cboDia;
    private javax.swing.JComboBox cboMes;
    private javax.swing.JComboBox cboNuevoEstado;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JTextField txtAutor;
    private javax.swing.JTextField txtEstado;
    private javax.swing.JTextField txtTitulo;
    // End of variables declaration//GEN-END:variables

}
