
package PCK_Biblioteca;

import java.awt.BorderLayout;
import javax.swing.JOptionPane;

public class PanelListado extends javax.swing.JPanel {
    private PanelTabla	panTabla;

    /** Creates new form PanelListado */
    public PanelListado() {
        initComponents();
	panTabla = new PanelTabla();
	panTabla.setOpaque(false);
	panTabla.setVisible(false);
	add(panTabla,BorderLayout.CENTER);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnRefrescar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();

        setBackground(new java.awt.Color(204, 204, 255));
        setLayout(new java.awt.BorderLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("LISTADO");
        add(jLabel1, java.awt.BorderLayout.PAGE_START);

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setLayout(new java.awt.GridLayout(1, 2));

        btnRefrescar.setText("Refrescar Tabla");
        btnRefrescar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefrescarActionPerformed(evt);
            }
        });
        jPanel1.add(btnRefrescar);

        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        jPanel1.add(btnSalir);

        add(jPanel1, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

private void btnRefrescarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefrescarActionPerformed
    mostrarListado();
}//GEN-LAST:event_btnRefrescarActionPerformed

private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
    String msg="<html><p color=red>Este es un Reto para Ud:<br>"+"<p color=blue size=+2>"+
            "Modifique los constructores de tal manera que:<br>" +
            "El constructor del PanelPrincipal debe recibir al frame cuando es invocado,<br>"+
            "El frame recibido debe enviarse al constructor de éste panel<br>"+
            "En éste botón invoque al método dispose() desde el objeto que recibe al frame</html>";
    JOptionPane.showMessageDialog(this, msg);
}//GEN-LAST:event_btnSalirActionPerformed
public void mostrarListado() {
	inicializaTabla();
	panTabla.setVisible(true);
	// proceso repetitivo
	for(int i = 0; i < PanelIngresos.getLibros().numeroLibros(); i++) {
		//referencia cada objeto del arreglo
		Libro unLibro = PanelIngresos.getLibros().getLibro(i);
		
		Object[] datos = {unLibro.getCodigo(),
				   unLibro.getTitulo(),
				   unLibro.getAutor(),
				   String.valueOf(unLibro.getEdicion()),
				   unLibro.getEstado()};
					
		panTabla.getMiModelo().addRow(datos);
	}
}
//-------------------------
public	void inicializaTabla(){
	// obtiene numero de filas de la tabla
	int filas = panTabla.getMiTabla().getRowCount();
		
	// remueve todas las filas de la tabla
	for (int fila=0; fila<filas; fila++)
			panTabla.getMiModelo().removeRow(0);
			
	panTabla.setVisible(false);
}
//-------------------------


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRefrescar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

}
