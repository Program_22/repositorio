
package PCK_Biblioteca;

import java.util.*;// para la clase ArrayList
public class Libros {
    //Declarar el Arreglo v
    private ArrayList v;
   // constructor
   public Libros(){
       //Crear la instancia del Arreglo v y es un objeto
       v = new ArrayList();
   }
   //Crear las operaciones para el arreglo v
   // Crear un metodo addLibro para agregar un nuevo Libro
   public void addLibro(Libro e){
        v.add(e); //Metodo add agrega el libro al arreglo v
   }
  //Crear el metodo getLibro 
   //para obtener un Libro de la posicion dada
   public Libro getLibro(int i){
        return (Libro)v.get(i);
   }
  //Crear el metodo setLibro para reemplazar un Libro ya existente
  public void setLibro(int i, Libro e){
        v.set(i,e); //Reemplaza el libro(e) segun posicion i
  }
//Metodo para retornar el numero de Libros guardados en el ArrayList
public int numeroLibros(){
        return v.size(); //Metodo size() obtiene el tamaño del arreglo v
}
//Metodo para buscar un Libro por su codigo
public int busca(String codigo){
  for (int i=0; i<numeroLibros(); i++)
        if (codigo.equals(getLibro(i).getCodigo()))
            return i;// retorna la posición encontrada
  return -1; //significa que no lo encuentra
}
// Metodo para eliminar un Libro
public void elimina(int i){
    v.remove(i); //remove elimina el libro segun posicion
}
//Metodo para eliminar todos los objetos del ArrayList
public void eliminaTodo(){
    v.removeAll(v); //removeAll elimina todos los elementos del arreglo
}
}//Fin de la clase