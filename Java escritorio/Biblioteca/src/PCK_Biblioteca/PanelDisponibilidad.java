
package PCK_Biblioteca;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;

public class PanelDisponibilidad extends javax.swing.JPanel {

    /** Creates new form PanelDisponibilidad */
    public PanelDisponibilidad() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnSalir = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        cboCodigos = new javax.swing.JComboBox();
        txtTitulo = new javax.swing.JTextField();
        txtAutor = new javax.swing.JTextField();
        txtEstado = new javax.swing.JTextField();
        txtFechaDev = new javax.swing.JTextField();

        setBackground(new java.awt.Color(231, 214, 18));
        setLayout(new java.awt.BorderLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("DISPONIBILIDAD");
        add(jLabel1, java.awt.BorderLayout.PAGE_START);

        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        add(btnSalir, java.awt.BorderLayout.PAGE_END);

        jPanel1.setBackground(new java.awt.Color(231, 214, 18));
        jPanel1.setLayout(new java.awt.GridLayout(1, 2));

        jPanel2.setBackground(new java.awt.Color(231, 214, 18));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Etiquetas"));
        jPanel2.setLayout(new java.awt.GridLayout(5, 1));

        jLabel2.setText("Seleccione un Código");
        jPanel2.add(jLabel2);

        jLabel3.setText("Título");
        jPanel2.add(jLabel3);

        jLabel4.setText("Autor");
        jPanel2.add(jLabel4);

        jLabel5.setText("Estado");
        jPanel2.add(jLabel5);

        jLabel6.setText("Fecha de Devolución");
        jPanel2.add(jLabel6);

        jPanel1.add(jPanel2);

        jPanel3.setBackground(new java.awt.Color(231, 214, 18));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));
        jPanel3.setLayout(new java.awt.GridLayout(5, 1));

        cboCodigos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Códigos Disponibles" }));
        cboCodigos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCodigosActionPerformed(evt);
            }
        });
        jPanel3.add(cboCodigos);

        txtTitulo.setEditable(false);
        jPanel3.add(txtTitulo);

        txtAutor.setEditable(false);
        jPanel3.add(txtAutor);

        txtEstado.setEditable(false);
        jPanel3.add(txtEstado);

        txtFechaDev.setEditable(false);
        jPanel3.add(txtFechaDev);

        jPanel1.add(jPanel3);

        add(jPanel1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

private void cboCodigosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCodigosActionPerformed
    borrar();
    seleccionarLibrosDisponibles();
}//GEN-LAST:event_cboCodigosActionPerformed

private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
    String msg="<html><p color=red>Este es un Reto para Ud:<br>"+"<p color=blue size=+2>"+
            "Modifique los constructores de tal manera que:<br>" +
            "El constructor del PanelPrincipal debe recibir al frame cuando es invocado,<br>"+
            "El frame recibido debe enviarse al constructor de éste panel<br>"+
            "En éste botón invoque al método dispose() desde el objeto que recibe al frame</html>";
    JOptionPane.showMessageDialog(this, msg);
}//GEN-LAST:event_btnSalirActionPerformed
	public void seleccionarLibrosDisponibles(){
	// leer posicion del codigo seleccionado
	int i=cboCodigos.getSelectedIndex()-1;

	if (i>=0){
		// obtiene libro seleccionado
		Libro unLibro = PanelIngresos.getLibros().getLibro(i);
		deshabilitaDevolucion(false);
	
		// ver datos de libro seleccionado
		txtTitulo.setText(unLibro.getTitulo());
		txtAutor.setText(unLibro.getAutor());
		txtEstado.setText(unLibro.getEstado());
		if (unLibro.getEstado().equals("A Domicilio")) {
			deshabilitaDevolucion(true);
			txtFechaDev.setText(unLibro.getFechaDev());
		}
	}
	}
	//........................................
	public void deshabilitaDevolucion(boolean estado) {
		
		txtFechaDev.setVisible(estado);	
	}
	//........................................
	public void borrar(){
		txtAutor.setText("");
		txtFechaDev.setText("");
		txtEstado.setText("");
		txtTitulo.setText("");
	}
        public static JComboBox getComboCodigos(){ return cboCodigos;}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSalir;
    private static javax.swing.JComboBox cboCodigos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTextField txtAutor;
    private javax.swing.JTextField txtEstado;
    private javax.swing.JTextField txtFechaDev;
    private javax.swing.JTextField txtTitulo;
    // End of variables declaration//GEN-END:variables

}
