
package PCK_Archivos;
//Importar la clase para el manejo de Arreglos
import java.util.ArrayList;
public class Empleados {
//Crear el Arreglo a de Tipo Clase Empleado
// arreglo de objetos para empleados
private ArrayList <Empleado>  a;  
// Crear el constructor
public Empleados(){
//Crear el Arreglo a Asociado a la Clase Empleado    
 a = new ArrayList();// crea el objeto
}
// Métodos de administracion del arreglo
// Adiciona un nuevo empleado
public void agrega(Empleado nuevo){
//Usar Método add del arreglo
//para agregar al Empleado
    a.add(nuevo);
}
//Método para obtiener un empleado
public Empleado getEmpleado(int i){
  //Método get para leer el empleado  
  return a.get(i) ;
}
// Reemplaza un empleado
//Método para Actualizar el Empleado
public void reemplaza(int i, 
        Empleado actualizado){
 //Método set para actualizar el Empleado   
 a.set(i, actualizado);
}
// Método elimina para eliminar un empleado
public void elimina(int i){
 a.remove(i); //Método remove para eliminar el empleado
}
// Método para eliminar a todos los empleados
public void elimina(){
  for (int i=0; i<numeroEmpleados(); i++)
   a.remove(0); //Eliminar empleado por empleado
}
// Método para obtiener número de empleados
public int numeroEmpleados()
 { 
     //Método size obtiene el tamaño del arreglo a
     return a.size(); 
 }
// Método para buscar un empleado por codigo
public int busca(int codigo){
  for (int i=0; i<numeroEmpleados(); i++){
    if (codigo==getEmpleado(i).getCodigo())
         return i; // retorna indice
            }
  // significa que no lo encontró          
  return -1; 
           
	}    
}
