/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package PCK_Archivos;
//Clase BufferedReader para acceso a la lectura del Buffer
import java.io.BufferedReader;
//Clase FileReader para la lectura de archivos
import java.io.FileReader;
//Clase FileWriter para la escritura del archivo
import java.io.FileWriter;
//Clase PrintWriter para mostrar datos por pantalla
import java.io.PrintWriter;
//Clase URL para la lista uniforme de recursos
import java.net.URL;
//Clase StringTokenizer para el manejo de cadenas
import java.util.StringTokenizer;
//Clase JOptionPane para mensajes
import javax.swing.JOptionPane;

public class PanelPrincipal extends javax.swing.JPanel {
//Variable para el objeto Empleado
private Empleado  objEmp;// un empleado
//Variable para todos los Empleados
private Empleados objEmpleados;	// todos los empleados
//Variable para representar al frame
private Principal p; // frame de origen
        
    
    /**
     * Creates new form PanelPrincipal
     */
    public PanelPrincipal() {
        initComponents();
        // recibe el frame
        this.p=p;
        // objeto que administra la información de empleados
	objEmpleados = new Empleados();
        // proceso de carga de data del archivo al arreglo de objetos
	cargaData(); 
    }
    //Crear la función para cargar los datos
    public void cargaData(){
	//Con Try se controlan los errores
        try {
        //Clase FileReader crear el archivo de texto
        //para lectura    
        FileReader fr = new FileReader("archivos/empleados.txt");
        //Clase BufferedReader para la lectura del buffer
	BufferedReader br = new BufferedReader(fr);
	String linea;
        //Leer línea por línea
	while((linea=br.readLine())!=null){
	 //st representa el campo leido de la línea de texto
         StringTokenizer st = new StringTokenizer(linea,",");
	 //Crear el objeto para un Empleado	
         objEmp = new Empleado();
         //Asignar el Código del Empleado
	 objEmp.setCodigo(Integer.parseInt(st.nextToken()));
	 //Asignar el nombre del Empleado
         objEmp.setNombre(st.nextToken());
         //Asignar el Sueldo del Empleado
         objEmp.setSueldo(Double.parseDouble(st.nextToken()));
	 //Agregar el empleado
         objEmpleados.agrega(objEmp);
	}
        //Cerrar el Buffer
	br.close();
        //En caso de Error
	}catch(Exception ex){
	  System.out.println(ex.toString());
	}// fin del catch
}// fin de cargaData
//-------------------------
//Función para Salir
public void salir(){
  grabar();
  this.setVisible(false);
  p.dispose();
}// fin de salir
//-------------------------
//Función para Grabar los datos en el archivo
public void grabar(){
//guarda la data en el archivo
try {
 // FileWriter clase para grabar el archivo
 FileWriter fw = new FileWriter("archivos/empleados.txt");
 PrintWriter pw = new PrintWriter(fw);
 for (int i=0; i<objEmpleados.numeroEmpleados(); i++){
  objEmp = objEmpleados.getEmpleado(i);
  pw.println(String.valueOf(objEmp.getCodigo())+","+ 
          objEmp.getNombre()+","+  
          String.valueOf(objEmp.getSueldo())+","+
  objEmp.descuentos()+","+
  objEmp.neto());
  }// fin del for
  pw.close();// cierra archivo
}catch(Exception ex){
 mensaje("Error en grabacion de archivo");
	}// fin del catch
}// fin de grabar
//-------------------------

public void eliminar(){
	int codigo = leerCodigo();
	if (codigo==-666)
		mensaje("Ingrese codigo entero");
	else{
		int p = objEmpleados.busca(codigo);
		if (p==-1)
			mensaje("Codigo no existe");
		else{
			objEmp = objEmpleados.getEmpleado(p);
			txtNombre.setText(objEmp.getNombre());
	txtSueldo.setText(String.valueOf(objEmp.getSueldo()));
	txtSalida.setText(String.format("%-20s%-10d\n","Codigo: ",objEmp.getCodigo()));
	txtSalida.append( String.format("%-20s%-30s\n","Nombre: ",objEmp.getNombre()));
	txtSalida.append( String.format("%-20s%10.1f\n","Sueldo: ",objEmp.getSueldo()));
	txtSalida.append( String.format("%-20s%10.1f\n","Descuentos:",objEmp.descuentos()));
	txtSalida.append( String.format("%-20s%10.1f\n","Neto: ",objEmp.neto()));

	// pide confirmacion
	int r = JOptionPane.showConfirmDialog(this,
                    "Esta seguro de eliminar a éste empleado ?","Reponder",0);
	if (r==0)// si
		objEmpleados.elimina(p);
		}// fin del else
	}// fin del else
				
	borrar();
}// fin de elimina
//-------------------------

public void listar(){
    if (objEmpleados.numeroEmpleados()==0)
		mensaje("Archivo vacío");
    else{
	// titulo
	txtSalida.setText(String.format("%-8s%-30s%10s%10s%10s\n",
                                    "Codigo","Nombre","Sueldo","Dsctos","Neto"));
        //detalle
	for (int i=0; i < objEmpleados.numeroEmpleados(); i++){
            objEmp = objEmpleados.getEmpleado(i);
            txtSalida.append(String.format("%-8d",objEmp.getCodigo()));
            txtSalida.append(String.format("%-30s",objEmp.getNombre()));
            txtSalida.append(String.format("%10.1f",objEmp.getSueldo()));
            txtSalida.append(String.format("%10.1f",objEmp.descuentos()));
            txtSalida.append(String.format("%10.1f\n",objEmp.neto()));
	}// fin del for
    }// fin del else
}// fin de listar
//-------------------------

public void modifica(){
	if (leerCodigo()==-666)
		mensaje("Ingrese codigo entero");
	else if (leerNombre()==null)
		mensaje("Ingrese nombre de empleado");
	else if (leerSueldo()==-666)
		mensaje("Ingrese sueldo numérico");
	else{
		int p=objEmpleados.busca(leerCodigo());
		objEmp = new Empleado(leerCodigo(),
				       leerNombre(),
				       leerSueldo());
		if (p==-1)// codigo nuevo
			objEmpleados.agrega(objEmp);
		else// codigo ya existente			
			objEmpleados.reemplaza(p,objEmp);
		borrar();
	}// fin del else
				
}// fin de modifica
//-------------------------

public void consulta(){
    int codigo = leerCodigo();
    if (codigo==-666)
    	mensaje("Ingrese codigo entero");
    else{
	int p = objEmpleados.busca(codigo);
	if (p==-1)
		mensaje("Codigo no existe");
	else{
		objEmp = objEmpleados.getEmpleado(p);
		txtNombre.setText(objEmp.getNombre());
		txtSueldo.setText(String.valueOf(objEmp.getSueldo()));
                txtSalida.setText(String.format("%-20s%-10d\n",
                                                "Codigo: ",
                                                objEmp.getCodigo()));
                txtSalida.append( String.format("%-20s%-30s\n",
                                                "Nombre: ",objEmp.getNombre()));
                txtSalida.append( String.format("%-20s%10.1f\n",
                                                "Sueldo: ",objEmp.getSueldo()));
                txtSalida.append( String.format("%-20s%10.1f\n",
                                                "Descuentos:",objEmp.descuentos()));
                txtSalida.append( String.format("%-20s%10.1f\n",
                                                "Neto: ",objEmp.neto()));
	}
    }
}// fin de consulta
//-------------------------
public void ingresar(){
	if (leerCodigo()==-666)
		mensaje("Ingrese codigo entero");
	else if (leerNombre()==null)
		mensaje("Ingrese nombre de empleado");
	else if (leerSueldo()==-666)
		mensaje("Ingrese sueldo numérico");
	else{
		objEmp = new Empleado( leerCodigo(),
				        leerNombre(),
					 leerSueldo()
				       );
		
		if (objEmpleados.busca(objEmp.getCodigo())!=-1)
			mensaje("Codigo Repetido");
		else			
			objEmpleados.agrega(objEmp);
		
		borrar();
		
	}
}// fin de ingresar
//-------------------------
public int leerCodigo(){
	try{
		int codigo=Integer.parseInt(txtCodigo.getText());
		return codigo;
	}catch(Exception ex){
	return -666;
	}// fin del catch
}// fin de leerCodigo
//-------------------------
public String leerNombre(){
	try{
		String nombre=txtNombre.getText();
		if (nombre.charAt(0)==' ')
			return nombre.trim();
		return nombre;
	}catch(Exception ex){
		return null;
	}// fin del catch
}// fin de leerNombre
//-------------------------
public double leerSueldo(){
	try{
	double sueldo=Double.parseDouble(txtSueldo.getText());
		return sueldo;
	}catch(Exception ex){
		return -666;
	}// fin del catch
}// fin de leerSueldo
//-------------------------
public void mensaje(String texto){
	JOptionPane.showMessageDialog(this,texto);		
}// fin de mensaje
//-------------------------
public void borrar(){
		txtCodigo.setText("");
		txtNombre.setText("");
		txtSueldo.setText("");
		txtSalida.setText("");
		txtCodigo.requestFocus();
}// fin de borrar
//-------------------------

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        btnIngresar = new javax.swing.JButton();
        btnConsultar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        btnBorrar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtSueldo = new javax.swing.JTextField();
        btnListar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtSalida = new javax.swing.JTextArea();
        btnSalir = new javax.swing.JButton();

        setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.GridLayout(3, 4));

        jLabel1.setText("Código");
        jPanel1.add(jLabel1);
        jPanel1.add(txtCodigo);

        btnIngresar.setText("Ingresar");
        btnIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarActionPerformed(evt);
            }
        });
        jPanel1.add(btnIngresar);

        btnConsultar.setText("Consultar");
        btnConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarActionPerformed(evt);
            }
        });
        jPanel1.add(btnConsultar);

        jLabel2.setText("Nombres");
        jPanel1.add(jLabel2);
        jPanel1.add(txtNombre);

        btnBorrar.setText("Borrar");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });
        jPanel1.add(btnBorrar);

        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        jPanel1.add(btnModificar);

        jLabel3.setText("Sueldo");
        jPanel1.add(jLabel3);
        jPanel1.add(txtSueldo);

        btnListar.setText("Listar");
        btnListar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListarActionPerformed(evt);
            }
        });
        jPanel1.add(btnListar);

        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        jPanel1.add(btnEliminar);

        add(jPanel1, java.awt.BorderLayout.PAGE_START);

        txtSalida.setColumns(20);
        txtSalida.setRows(5);
        jScrollPane1.setViewportView(txtSalida);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);

        btnSalir.setText("Salir Grabando");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        add(btnSalir, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void btnIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarActionPerformed
        // TODO add your handling code here:
        ingresar();
    }//GEN-LAST:event_btnIngresarActionPerformed

    private void btnConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarActionPerformed
        // TODO add your handling code here:
        consulta();
    }//GEN-LAST:event_btnConsultarActionPerformed

    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        // TODO add your handling code here:
        borrar();
    }//GEN-LAST:event_btnBorrarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        // TODO add your handling code here:
        modifica();
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnListarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListarActionPerformed
        // TODO add your handling code here:
        listar();
    }//GEN-LAST:event_btnListarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
        eliminar();
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
        salir();
    }//GEN-LAST:event_btnSalirActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnConsultar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnIngresar;
    private javax.swing.JButton btnListar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextArea txtSalida;
    private javax.swing.JTextField txtSueldo;
    // End of variables declaration//GEN-END:variables
}
