
package PCK_Archivos;

//Definir la clase Empleado
public class Empleado {
// Definir Atributos 
private int codigo; //Código del Empleado
private String nombre; //Nombre del Empleado
private double sueldo; //Sueldo del Empleado
//Crear el Constructor Vacío
public Empleado(){ }
//Crear el constructor con parametros
public Empleado(int codigo,
        String nombre, double sueldo){
 //Pasar los valores de los parámetros a los atributos
 this.codigo = codigo;
 this.nombre = nombre;
 this.sueldo = sueldo;
}
//Crear otros métodos
//Métodos de calculo
//Método para calcular el Descuento
public double descuentos(){
 return sueldo*0.15;
}
//Método para calcular el Neto
public double neto(){
return sueldo - descuentos();
}
//Definir los métodos de lectura y Escritura
 
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the sueldo
     */
    public double getSueldo() {
        return sueldo;
    }

    /**
     * @param sueldo the sueldo to set
     */
    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

}
