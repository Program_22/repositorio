package proyectopolimorfismopasajes;

//Importar la clase para controlar el buffer del teclado
import java.io.BufferedReader;
//Importar la clase para el control de errores
import java.io.IOException;
//Importar la clase para la lectura de datos por teclado
import java.io.InputStreamReader;
//Importar la clase para la salida de datos por pantalla
import java.io.PrintWriter;
public class AplicacionPasajes {
    //Crear el método main() para ejecutar la clase y asociarlo
 //al control de error IOException   
 public static void main(String[] args)throws IOException{
     //Crear el objeto x para la lectura de datos por teclado
     InputStreamReader x = new InputStreamReader(System.in);
     //Crear el objeto y para la lectura del buffer
     BufferedReader y = new BufferedReader(x);
     //Crear el objeto p para la salida de datos por pantalla
     PrintWriter p=new PrintWriter(System.out,true);
     //Declarar variables:
     char opdest;
     int cant;
     int opcImp;
     double tasa;
      //Trabajar con clase Ómnibus
     p.println("********** Ómnibus **********");
     //Crear el objeto o para representar a la clase Omnibus
     Omnibus o = new Omnibus();
     p.println("Ingrese Destino [A]rgentina [B]olivia [E]cuador : ");
     opdest=y.readLine().charAt(0);
     p.println("Ingrese Cantidad : ");
     cant= Integer.parseInt(y.readLine());
     o.calcularTotal(opdest, cant);
     //Llamar al método imprimir de Vehiculo
     o.imprimir();
     //Trabajar con clase Avión
     p.println("********** Avión **********");
     //Crear el objeto a para representar a la clase Avión
     Avion a = new Avion();
     p.println("Ingrese Destino [A]rgentina [B]olivia [E]cuador : ");
     opdest=y.readLine().charAt(0);
     p.println("Ingrese Cantidad : ");
     cant= Integer.parseInt(y.readLine());
     p.println("[1] Impuesto 5% [2]Pedir Impuesto : ");
     opcImp=Integer.parseInt(y.readLine());
     switch(opcImp){
         case 1: //Impuesto 5%
                 a.calcularTotal(opdest, cant);
                 break;
         case 2: //Impuesto solicitado
                 p.println("Ingrese un Porcentaje para Tasa : ");
                 tasa=Double.parseDouble(y.readLine());
                 a.calcularTotal(opdest, cant,tasa);
                 break;
     } 
    //Llamar al método imprimir de Vehiculo
     o.imprimir();   
 }
}
