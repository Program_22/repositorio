
package proyectopolimorfismopasajes;

//Importar la clase para salida de datos por pantalla
import java.io.PrintWriter;
public class Vehiculo {
  //Definir Atributos
  private String Destino;
  private int Cantidad;
  private double Precio;

  //Crear los métodos de lectura y escritura
   public String getDestino() {
        return Destino;
    }

   public void setDestino(String Destino) {
        this.Destino = Destino;
    }

   public int getCantidad() {
        return Cantidad;
    }

   public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    
    public double getPrecio() {
        return Precio;
    }

    
    public void setPrecio(double Precio) {
        this.Precio = Precio;
    }
  //Crear método calcularTotal
  public void calcularTotal() {
      
  }
  public void imprimir(){
      //Crear el objeto p a partir de la clase PrintWriter
      PrintWriter p=new PrintWriter(System.out,true);
      p.println("Destino: " + Destino);
      p.println("Precio: " + Precio);
  }
}
