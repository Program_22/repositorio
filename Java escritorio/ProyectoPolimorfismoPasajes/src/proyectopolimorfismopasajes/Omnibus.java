
package proyectopolimorfismopasajes;

//Importar la clase para salida de datos por pantalla
import java.io.PrintWriter;
//Aplicando Herencia: La clase Omnibus se hereda de la clase Vehiculo
public class Omnibus extends Vehiculo {
 private double total;
 private double descuento;
 //Metodo para asignar Destino y Precio
 private void asignarDestinoPrecio(char Destino){
    switch (Destino) {
        case 'A'://Argentina
                      super.setDestino("Argentina");
                      super.setPrecio(850.00);
                      break;
        case 'B'://Bolivia
                      super.setDestino("Bolivia");
                      super.setPrecio(740.00);
                      break;
        case 'E'://Ecuador
                      super.setDestino("Ecuador");
                      super.setPrecio(710.00);
                      break;
    }
} 
//crear método calcular Total
public void calcularTotal(char Destino, int Cantidad){
  //Asignar Destino y Precios
  asignarDestinoPrecio(Destino);  
  total=super.getPrecio() * Cantidad;
  total = total - calcularDescuento(Cantidad);
}
//Crear el método para Descuento
public double calcularDescuento(int Cantidad){
    if (Cantidad>4)
         setDescuento(total * 0.10);
    else
        setDescuento(0.00);
    return getDescuento();
}
//Crear Método para la impresión
public void imprimir(){
    //Crear el objeto p a partir de la clase PrintWriter
    PrintWriter p=new PrintWriter(System.out,true);
    super.imprimir();
    p.println("Descuento: " + descuento );
    p.println("Total a Pagar: " + total );
    
}
  //Crear métodos para lectura y escritura
    public double getTotal() {
        return total;
    }

    
    public void setTotal(double total) {
        this.total = total;
    }

    
    public double getDescuento() {
        return descuento;
    }

    
    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }
}
