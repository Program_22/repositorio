
package proyectopolimorfismopasajes;

//Importar la clase para salida de datos por pantalla
import java.io.PrintWriter;
//Aplicando Herencia: La clase Avion se hereda de la clase Vehiculo
public class Avion extends Vehiculo {
 private double total;
 private double impuesto;
 private double tasa;
//Crear metodo para Asignar Destino y Precio
private void asignarDestinoPrecio(char Destino){
    switch (Destino) {
        case 'A'://Argentina
                      super.setDestino("Argentina");
                      super.setPrecio(850.00);
                      break;
        case 'B'://Bolivia
                      super.setDestino("Bolivia");
                      super.setPrecio(740.00);
                      break;
        case 'E'://Ecuador
                      super.setDestino("Ecuador");
                      super.setPrecio(710.00);
                      break;
    }
} 
 //crear método calcular Total
 public void calcularTotal(char Destino, int Cantidad){
        asignarDestinoPrecio(Destino);
        total=super.getPrecio() * Cantidad ;
        total =total + calcularImpuesto();
}
//Sobreescribir el método calcularTotal 
public void calcularTotal(char Destino, int Cantidad, double tasa){
        asignarDestinoPrecio(Destino);
        total=super.getPrecio() * Cantidad ;
        total =total + calcularImpuesto(tasa);
} 
//Crear el método para impuesto con polimorfismo
public double calcularImpuesto(){
        impuesto=(total * 0.05);
        return impuesto;
}
public double calcularImpuesto(double tasaImpuesto){
        impuesto=(total * tasaImpuesto);
        return impuesto;
}
//Crear Método para la impresión
public void imprimir(){
    //Crear el objeto p a partir de la clase PrintWriter
    PrintWriter p=new PrintWriter(System.out,true);
    super.imprimir();
    p.println("Tasa de Impuesto: " + tasa );
    p.println("Total de Impuesto: " + impuesto );
    p.println("Total a Pagar: " + total );
    
}

    
    public double getTotal() {
        return total;
    }

    
    public void setTotal(double total) {
        this.total = total;
    }

    
    public double getImpuesto() {
        return impuesto;
    }

    
    public void setImpuesto(double impuesto) {
        this.impuesto = impuesto;
    }
 public double getTasa() {
        return tasa;
    }

    
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }
}
