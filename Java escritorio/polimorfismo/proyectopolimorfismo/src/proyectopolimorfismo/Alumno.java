
package proyectopolimorfismo;

import java.io.PrintWriter;
//aplicando herencia :la clase alumno se hereda de la clase persona 
public class Alumno extends persona  {
    //declarar atributos 
    private int pp;//promedio de practicas 
  private int ep;//examen parcial 
  private int ef;//examen final 
  private int pf;//promedio final 

    public int getPp() {
        return pp;
    }

    public void setPp(int pp) {
        this.pp = pp;
    }

    public int getEp() {
        return ep;
    }

    public void setEp(int ep) {
        this.ep = ep;
    }

    public int getEf() {
        return ef;
    }

    public void setEf(int ef) {
        this.ef = ef;
    }

    public int getPf() {
        return pf;
    }

    public void setPf(int pf) {
        this.pf = pf;
    }
//crear metodo para calcular promedio 
    public void calcularpromedio(){
        pf=(pp+ep+ef)/3;
        //metodo para imprimir datos
    }
    public void imprimir(){
//llamar al metodo imprimir de la clase persona 
        super.imprimir();
        //crear el objeto p a partir de la clase printwriter 
        PrintWriter p=new PrintWriter(System.out,true);
        p.println("promedio de practicas:"+pp);
        p.println("examen parcial:"+ep);
        p.println("examen final:"+ef);
        p.println("promedio final:"+pf);
    }
    
}