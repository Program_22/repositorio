package proyectopolimorfismo;
//importar la clase para controlar el buffer del teclado 
import java.io.BufferedReader;
//importar la clase para el control de errores
import java.io.IOException;
import java.io.InputStreamReader;
//importar la clase para lectrura de datos por teclado
import java.io.PrintWriter;
public class Aplicacion {
    public static void main(String[]args )throws IOException{
    InputStreamReader x=new InputStreamReader(System.in);
    BufferedReader y=new BufferedReader(x);
    PrintWriter p=new  PrintWriter(System.out,true);
    int op;
    double tarifa,descuento;
    p.println("********* alumno**********");
    Alumno a=new Alumno(); 
    p.println("ingrese DNI:");
    a.setDni(y.readLine());
     p.println("ingrese Nombre:");
     a.setNombre(y.readLine());
     p.println("ingrese promedio de practicas:");
     a.setPp(Integer.parseInt(y.readLine()));
     p.println("ingrese examen párcial:");
     a.setEp(Integer.parseInt(y.readLine()));
    p.println("ingrese examen final:");
    a.setEf(Integer.parseInt(y.readLine()));
    a.calcularpromedio();
    a.imprimir();
   p.println("*********profesor**********");
   Profesor prof=new Profesor();
    p.println("ingrese DNI:");
    prof.setDni(y.readLine());
     p.println("ingrese Nombre:");
     prof.setNombre(y.readLine());
     p.println("ingrese horas trabajadas:");
     //menu opciones
     prof.setHoras(Integer.parseInt(y.readLine()));
     p.println("********opciones***********");
     p.println("[1] con una tarifa constante");
     p.println("[2] con una tarifa ingresada");
     p.println("[3] con una tarifa y un descuento");
     p.println("ingrese opcion[1-3]:");
         op=Integer.parseInt(y.readLine());
         switch(op){
             case 1:prof.calcularsalario(); break;//tarifa constante
             case 2:p.println("ingresar tarifa:");
                 tarifa= Double.parseDouble(y.readLine());
                 prof.calcularsalario(tarifa);
                 break;
             case 3: p.println("ingresar tarifa:");
                  tarifa= Double.parseDouble(y.readLine());
                 p.println("ingresar descuento:");
                 descuento=Double.parseDouble(y.readLine());
                 prof.calcularsalario(tarifa,descuento);
                 break;                
         }
        //imprimir datos 
         prof.imprimir();
         
    }
    
}
