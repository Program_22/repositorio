package proyectopolimorfismo;
//importar la clase para la salida de datos por pantalla
import java.io.PrintWriter;

public class persona {
//declarar atributos
    private String nombre;
    private String dni;
    //crear un metodo para impresion de datos 
    public void imprimir(){
        //crear el objeto p a partir de la calse printwriter 
        PrintWriter p=new  PrintWriter(System.out,true);
        p.println("DNI:"+dni);
        p.println("Nombre:"+nombre);
    }
  //crear los metodos para la lectura y escritura de datos   
    //metodo para leer el nombre    
    public String getNombre() {
        return nombre;
    }

   //metodo par aescribir el nombre
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    //metodo para leer el dni 
    public String getDni() {
        return dni;
    }
//metodo para escribir el dni 
    public void setDni(String dni) {
        this.dni = dni;
    }
}
