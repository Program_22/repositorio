
package proyectopolimorfismo;
//importamos la clase para salida de datos por pantalla
import java.io.PrintWriter;
//aplicando herencia :la clase alumno se hereda de la clase persona 
public class Profesor extends persona{
    //declarar atributos 
    private int horas;
    private double salario;
    //crear los metodos para la lectura y escritura de datos 

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
    //metodo para calcular el salario 
    /*sobrecarga de metodos:
     * es un metodo con el mismo nombre pero con parametros diferentes
     */
    public void calcularsalario(){
        salario=horas*22.70;
        
    } 
    //hacer la sobrecarga del metodo calcularsalario 
    //en funcion a una tarifa
      public void calcularsalario(double tarifa){
salario=horas*tarifa;

      }
    //hacer la sobrecarga del metodo calcularsalario
      //en funcion a una tarifa y descuento
      public void calcularsalario(double tarifa,double descuento){
          salario=horas*tarifa-descuento;
          
      }
      //crear metodo para impresion de datos
      public void imprimir(){
          //llamar al metodo imprimir de la clase persona 
          super.imprimir();
            PrintWriter p=new  PrintWriter(System.out,true);
            p.println("salario:"+salario);
            
      }
}
