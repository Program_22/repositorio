package instituto;

public class Alumno {
 //Declarar atributos de la clase
 private String nom; //Nombre
 private char cat; //Categoría
 private int ccred; //Créditos
 private double prec; //Precio
 private double tot; //Total
 //Creando métodos para lectura y escritura de datos
    //Método para leer el nombre
    public String getNom() {
        return nom;
    }
    //Método para escribir el nombre 
    public void setNom(String nom) {
        this.nom = nom;
    }

    public char getCat() {
        return cat;
    }

    public void setCat(char cat) {
        this.cat = cat;
    }

    public int getCcred() {
        return ccred;
    }

    public void setCcred(int ccred) {
        this.ccred = ccred;
    }

    public double getPrec() {
        return prec;
    }

    
    public void setPrec(double prec) {
        this.prec = prec;
    }

    public double getTot() {
        return tot;
    }

    public void setTot(double tot) {
        this.tot = tot;
    }
    //Método para calcular el Precio
    public void calcularPrecio(){
        //Calcular el Precio en Función a la Categoría
        switch(cat){
            case 'A': prec=87.00; break; //Si la Categoria es A
            case 'B': prec=71.50; break; //Si la Categoria es B
            case 'C': prec=62.40; break; //Si la Categoria es C
        }
    }
    //Método para calcular el Total
    public void calcularTotal(){
        tot=prec * ccred;
    }
}
