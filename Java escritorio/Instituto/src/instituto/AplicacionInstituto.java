package instituto;

//Importar la clase para la lectura del Buffer
import java.io.BufferedReader;
//Importar la clase para control de Errores
import java.io.IOException;
//Importar clase para lectura de datos por teclado
import java.io.InputStreamReader;
public class AplicacionInstituto {
    //Creando el Método Main() para ejecutar la clase
    //Asociar el Método main() al control de errores
    public static void main(String args[]) throws IOException{
        //Declarar el Arreglo asociado a la clase Alumno para 80 elementos
        Alumno va[] = new Alumno[80];
        //Declarar otras variables
        //Contadores por categoria, suma y promedio
        int con=0,suma=0,prom,ca=0,cb=0,cc=0;
        //Pedir la cantidad de Alumnos
        int n= Integer.parseInt(leer("Ingresar Cantidad de Alumnos : "));
        //Crear un control de flujo repetitivo
        for(int i=0;i<n;i++){
            //Inicializar el Arreglo asociado a la clase Alumno
            va[i]= new Alumno();
            //Pedir el Nombre del Alumno
            va[i].setNom(leer("Nombre de Alumno : "));
            va[i].setCat(leer("Categoría de Alumno : ").charAt(0));
            va[i].setCcred(Integer.parseInt(leer("Cantidad de Créditos : ")));
            //Calcular el Precio
            va[i].calcularPrecio();
            va[i].calcularTotal();
        }
        //Mostrar resultados
        // \t representa a la tecla TAB (Tabulación)
         for(int i=0;i<n;i++){
             System.out.println(va[i].getNom() +"\t" + va[i].getCat() + "\t" +
                     va[i].getCcred() +"\t" + va[i].getPrec() + "\t" + va[i].getTot());
         }
         //Obtener Cantidad de Alumnos con menos de 10 créditos
         for(int i=0;i<n;i++)
             if (va[i].getCcred()<10)
                 con++;
         //Imprimir la Cantidad de Alumnos con menos de 10 créditos
         System.out.println("Alumnos Matriculados en menos de 10 Créditos : " + con);
         //Suma de Créditos
         for(int i=0;i<n;i++)
             suma+=va[i].getCcred();
         //Promedio
         prom= suma /n;
         //Imprimir promedio de créditos
         System.out.println("Promedio de Créditos : " + prom);
         //Promedio por Categoría
         for(int i=0;i<n;i++){
             switch(va[i].getCat()){
                 case 'A': ca++;break;
                 case 'B': cb++;break;
                 case 'C': cc++;break;
             }
         //Mostrar Alumnos por Categorías
         System.out.println("Alumnos Categoría A : " + ca);
         System.out.println("Alumnos Categoría B : " + cb);
         System.out.println("Alumnos Categoría C : " + cc);
         }
    }
    //Declarar el método leer() y asociarlo con el control de Error
    public static String leer(String texto) throws IOException{
        //Crear el objeto x para la lectura de datos por teclado
        InputStreamReader x= new InputStreamReader(System.in);
        //Asociar el objeto x al Buffer del teclado
        BufferedReader y = new BufferedReader(x);
        //Imprimir texto
        System.out.println(texto);
        //Pedir el dato
        String dato =y.readLine();
        return dato;
    }
}
