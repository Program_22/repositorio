
package PCK_Personal;

import javax.swing.JOptionPane;


public class PanelPrincipal extends javax.swing.JPanel {
//Declarar el objeto para administrar el Empleado
private AEmpleados ae;    
    public PanelPrincipal() {
        initComponents();
        //Crear el Objeto de Administración de Empleados
        ae = new AEmpleados();
    }
//Crear otros Métodos
//Crear el Método para obtener el Código del Empleado
    public void muestraCodigo(String codigo){
        TxtCodigo.setText(codigo);
    }
//Crear el Método para listar    
    public void lista(){
        borra();//LLamar al Método borrar controles
        //Imprimir encabezado
        imprime("Codigo\tNombre\tArea\tSueldo"
                + "\tHoras Extras\tAfiliado");
        //Leer los datos del Empleado
        for(int i=0; i<ae.getN(); i++){
            //Leer cada atributo del Empleado
            Empleado e = ae.getEmpleado(i);
            imprime(e);
        }
    }
    //Crear el Método para limpiar controles
    public void borra(){
        TxtNombre.setText("");
        txtHExtras.setText("");
        txtSalida.setText("");
        txtSueldo.setText("");
        //Selecciona el 1er. Elemento de lista
        cboArea.setSelectedIndex(0);
        rbtAFP.setSelected(true);
        rbtSNP.setSelected(false);
        //Poner el cursor
        TxtNombre.requestFocus();
    }
    //Método para imprimir los atributos del Empleado
    public void imprime(Empleado e){
        imprime(e.getCodigo()+"\t"+e.getNombre()+"\t"
          +e.nombreArea()+"\t"+
          e.getSueldoBase()+"\t"+
          e.getHorasExtras()+"\t"+e.nombreAfp());
    }
    //Método para imprimir en la caja de texto txtSalida
    public void imprime(String s){
        txtSalida.append(s+"\n");
    }
   //Método para mostrar mensaje
    public void mensaje(String s){
        JOptionPane.showMessageDialog(this,s);
    }
    //Método para formato de datos
    public String formato(double m, int nd){
        String s = String.format(" %1."+nd+"f",m);
        return s;
    }
    //Capturar el Código
    public String leeCodigo(){
        return TxtCodigo.getText();
    }
    //Capturar el Nombre
    public String leeNombre(){
        return TxtNombre.getText();
    }
    //Capturar el Area
    public int leeArea(){
        return cboArea.getSelectedIndex();
    }
    //Capturar el Sueldo
    public double leeSueldo(){
        return Double.parseDouble(txtSueldo.getText());
    }
    //Capturar la horas Extras
    public double leeHExtras(){
        return Double.parseDouble(txtHExtras.getText());
    }
    //Capturar la Afiliación 
    public int leeAfiliacion(){
        if(rbtAFP.isSelected())
            return 1;
        else
            return 2;//SNP
    }

    public void lista(Empleado e){
        borra();
        imprime("-----------------------------------------------------");
        imprime("Codigo\t: "+e.getCodigo());
        imprime("Nombre\t:"+e.getNombre());
        imprime("Area Laboral\t:"+e.nombreArea());
        imprime("Sueldo Base\t\t:"+formato(e.getSueldoBase(),2));
        imprime("Horas Extras\t:"+e.getHorasExtras());
        imprime("Monto x Horas Extras\t:"+formato(e.montoHExtras(),2));
        imprime("Sueldo Total\t\t:"+formato(e.sueldoTotal(),2));
        imprime("Afiliación\t:"+e.nombreAfp());
        imprime("Monto AFP\t:"+formato(e.montoAfp(),2));
        imprime("Monto SNP\t:"+formato(e.montoSnp(),2));
        imprime("Monto ESSalud\t:"+formato(e.montoEssalud(),2));
        imprime("Total Descuentos\t:"+formato(e.montoDescuentos(),2));
        imprime("Sueldo Neto\t\t:"+formato(e.sueldoNeto(),2));
        imprime("-----------------------------------------------------");
    }
 //Mostrar Titulo   
public String titulo(int area){
        switch(area){
            case 0: return "Reporte General";
            case 1: return "Reporte del área de Sistemas";
            case 2: return "Reporte del área de Administración";
            case 3: return "Reporte del área de Marketing";
            default: return "Reporte del área de Ventas";
        }
    }
//Obtener el Mayor Sueldo
    public double mayorSueldoTotal(int area){
        if(area==0)
            return mayorDeTodos();
        else{
        double m=-1;
        int c=0;
        for(int i=0; i<ae.getN(); i++){
            Empleado e = ae.getEmpleado(i);
            if(e.getArea()==area){
                c++;
                if(c==1)
                    m = e.sueldoTotal();
                else{
                    if(e.sueldoTotal()>m)
                        m=e.sueldoTotal();
                }
            }
        }
        return m;
        }
    }
//Obtener el Mayor de Todos
    public double mayorDeTodos(){
        double m=ae.getEmpleado(0).sueldoTotal();
        for(int i=0; i<ae.getN(); i++){
            Empleado e = ae.getEmpleado(i);
            if(e.sueldoTotal()>m)
                m =e.sueldoTotal();
        }
        return m;
    }
//Obtener el Sueldo Promedio
    public double sueldoPromedio(int area){
        if(area==0)
            return promedioDeTodos();
        else{
        double suma=-1;
        int c=0;
        for(int i=0; i<ae.getN(); i++){
            Empleado e = ae.getEmpleado(i);
            if(e.getArea()==area){
                c++;
                suma += e.sueldoTotal();
            }
        }
        if(c>0)
            return suma/c;
        else
            return suma;
        }
    }
//Obtener el Promedio de suledo de todos
    public double promedioDeTodos(){
        double suma=0;
        for(int i=0; i<ae.getN(); i++){
            Empleado e = ae.getEmpleado(i);
                suma += e.sueldoTotal();
        }
        return suma/ae.getN();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        TxtCodigo = new javax.swing.JTextField();
        TxtNombre = new javax.swing.JTextField();
        cboArea = new javax.swing.JComboBox();
        jPanel4 = new javax.swing.JPanel();
        txtSueldo = new javax.swing.JTextField();
        txtHExtras = new javax.swing.JTextField();
        rbtAFP = new javax.swing.JRadioButton();
        rbtSNP = new javax.swing.JRadioButton();
        jPanel2 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnBusca = new javax.swing.JButton();
        btnElimina = new javax.swing.JButton();
        btnLista = new javax.swing.JButton();
        btnReporte = new javax.swing.JButton();
        btnBorrar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtSalida = new javax.swing.JTextArea();

        setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.GridLayout(2, 1));

        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.LINE_AXIS));

        TxtCodigo.setEditable(false);
        TxtCodigo.setBorder(javax.swing.BorderFactory.createTitledBorder("Código"));
        jPanel3.add(TxtCodigo);

        TxtNombre.setBorder(javax.swing.BorderFactory.createTitledBorder("Nombre"));
        jPanel3.add(TxtNombre);

        cboArea.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Area Laboral", "Sistemas", "Administración", "Marketing", "Ventas" }));
        jPanel3.add(cboArea);

        jPanel1.add(jPanel3);

        jPanel4.setLayout(new javax.swing.BoxLayout(jPanel4, javax.swing.BoxLayout.LINE_AXIS));

        txtSueldo.setBorder(javax.swing.BorderFactory.createTitledBorder("Sueldo"));
        jPanel4.add(txtSueldo);

        txtHExtras.setBorder(javax.swing.BorderFactory.createTitledBorder("Horas Extras"));
        jPanel4.add(txtHExtras);

        buttonGroup1.add(rbtAFP);
        rbtAFP.setText("AFP");
        jPanel4.add(rbtAFP);

        buttonGroup1.add(rbtSNP);
        rbtSNP.setText("SNP");
        jPanel4.add(rbtSNP);

        jPanel1.add(jPanel4);

        add(jPanel1, java.awt.BorderLayout.PAGE_START);

        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        jPanel2.add(btnNuevo);

        btnBusca.setText("Busca");
        btnBusca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscaActionPerformed(evt);
            }
        });
        jPanel2.add(btnBusca);

        btnElimina.setText("Elimina");
        btnElimina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminaActionPerformed(evt);
            }
        });
        jPanel2.add(btnElimina);

        btnLista.setText("Lista");
        btnLista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListaActionPerformed(evt);
            }
        });
        jPanel2.add(btnLista);

        btnReporte.setText("Reporte");
        btnReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReporteActionPerformed(evt);
            }
        });
        jPanel2.add(btnReporte);

        btnBorrar.setText("Borrar");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });
        jPanel2.add(btnBorrar);

        add(jPanel2, java.awt.BorderLayout.PAGE_END);

        txtSalida.setColumns(20);
        txtSalida.setRows(5);
        jScrollPane1.setViewportView(txtSalida);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        //Crear el Nuevo Empleado
        Empleado nuevo = new Empleado(leeNombre(), leeArea(), leeSueldo(), leeHExtras(), leeAfiliacion());
        ae.agregaEmpleado(nuevo);
        muestraCodigo(nuevo.getCodigo());
        lista();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnBuscaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscaActionPerformed
        // TODO add your handling code here:
        //Buscar el Empleado
         int p=ae.busca(leeCodigo());
        if (p==-1)
            mensaje("Error: Código no existe");
        else{
            Empleado e = ae.getEmpleado(p);
            lista(e);
        }
    }//GEN-LAST:event_btnBuscaActionPerformed

    private void btnEliminaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminaActionPerformed
        // TODO add your handling code here:
        //Eliminar Empleado
         int p=ae.busca(leeCodigo());
        if (p==-1)
            mensaje("Error: Código no existe");
        else{
            ae.elimina(p);
            lista();
        }
    }//GEN-LAST:event_btnEliminaActionPerformed

    private void btnListaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListaActionPerformed
        // TODO add your handling code here:
        //Listar Empleados
          lista();
    }//GEN-LAST:event_btnListaActionPerformed

    private void btnReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReporteActionPerformed
        // TODO add your handling code here:
        //Reporte de Empleados
         if(ae.getN()==0)
            mensaje("Error: El arreglo esta vacío");
        else{
        int area=leeArea();
        imprime(titulo(area));
        double ms=mayorSueldoTotal(area);
        double sp=sueldoPromedio(area);
        if(ms>0 && sp>0){
            imprime("El mayor sueldo total es\t:"+formato(ms,2));
            imprime("El sueldo promedio es\t:"+formato(sp,2));
        }
        else
            imprime("No hay empleados en ésta área");
        }
    }//GEN-LAST:event_btnReporteActionPerformed

    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        // TODO add your handling code here:
        //Borrar Lista de Empleados
        borra();
        
    }//GEN-LAST:event_btnBorrarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField TxtCodigo;
    private javax.swing.JTextField TxtNombre;
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnBusca;
    private javax.swing.JButton btnElimina;
    private javax.swing.JButton btnLista;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnReporte;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cboArea;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton rbtAFP;
    private javax.swing.JRadioButton rbtSNP;
    private javax.swing.JTextField txtHExtras;
    private javax.swing.JTextArea txtSalida;
    private javax.swing.JTextField txtSueldo;
    // End of variables declaration//GEN-END:variables
}
