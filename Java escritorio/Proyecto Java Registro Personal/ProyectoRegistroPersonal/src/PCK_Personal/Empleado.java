
package PCK_Personal;


public class Empleado {

    /**
     * @return the porcentajeAfp
     */
    public static double getPorcentajeAfp() {
        return porcentajeAfp;
    }

    /**
     * @param aPorcentajeAfp the porcentajeAfp to set
     */
    public static void setPorcentajeAfp(double aPorcentajeAfp) {
        porcentajeAfp = aPorcentajeAfp;
    }

    /**
     * @return the porcentajeSnp
     */
    public static double getPorcentajeSnp() {
        return porcentajeSnp;
    }

    /**
     * @param aPorcentajeSnp the porcentajeSnp to set
     */
    public static void setPorcentajeSnp(double aPorcentajeSnp) {
        porcentajeSnp = aPorcentajeSnp;
    }

    /**
     * @return the porcentajeEssalud
     */
    public static double getPorcentajeEssalud() {
        return porcentajeEssalud;
    }

    /**
     * @param aPorcentajeEssalud the porcentajeEssalud to set
     */
    public static void setPorcentajeEssalud(double aPorcentajeEssalud) {
        porcentajeEssalud = aPorcentajeEssalud;
    }

    /**
     * @return the contador
     */
    public static int getContador() {
        return contador;
    }

    /**
     * @param aContador the contador to set
     */
    public static void setContador(int aContador) {
        contador = aContador;
    }
//Declarar Atributos 
  private String codigo, nombre;
  private int area;
  private double sueldoBase;
  private double horasExtras;
  private int afp;
  // valores comunes a todos los objetos
  private static double porcentajeAfp=0.128;
  private static double porcentajeSnp=0.13;
  private static double porcentajeEssalud=0.09;
  private static int contador=0;

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the area
     */
    public int getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(int area) {
        this.area = area;
    }

    /**
     * @return the sueldoBase
     */
    public double getSueldoBase() {
        return sueldoBase;
    }

    /**
     * @param sueldoBase the sueldoBase to set
     */
    public void setSueldoBase(double sueldoBase) {
        this.sueldoBase = sueldoBase;
    }

    /**
     * @return the horasExtras
     */
    public double getHorasExtras() {
        return horasExtras;
    }

    /**
     * @param horasExtras the horasExtras to set
     */
    public void setHorasExtras(double horasExtras) {
        this.horasExtras = horasExtras;
    }

    /**
     * @return the afp
     */
    public int getAfp() {
        return afp;
    }

    /**
     * @param afp the afp to set
     */
    public void setAfp(int afp) {
        this.afp = afp;
    }
// Crear el constructor
    public Empleado(String nombre, int area, 
      double sueldoBase, double horasExtras, int afp){
        contador++;//Incrementar el contador en 1
        //Autogenerar el Código del Empleado
        if(contador<10)
            codigo="E-00"+contador;
        else if(contador<100)
            codigo="E-0"+contador;
        else
            codigo="E-"+contador;
        //Asignar los valores a los atributos
        //this referencia al mismo objeto
        this.nombre = nombre;
        this.area = area;
        this.sueldoBase = sueldoBase;
        this.horasExtras= horasExtras;
        this.afp = afp;
    }
    // métodos adicionales
    //Calcular el Monto por Horas Extras
    public double montoHExtras(){
        return sueldoBase * horasExtras / 240;
    }
    //Calcular el Monto de AFP
    public double montoAfp(){
        if(afp==1)
            return sueldoBase * porcentajeAfp;
        else
            return 0;
    }
    //Calcular el Monto para el SNP
    public double montoSnp(){
        if(afp==2)
            return sueldoBase * porcentajeSnp;
        else
            return 0;
    }
    //Calcular el Monto para Essalud
    public double montoEssalud(){
        return sueldoBase * porcentajeEssalud;
    }
    //Calcular el Monto de Descuentos
    public double montoDescuentos(){
        if(afp==1)
            return montoAfp() + montoEssalud();
        else
            return montoSnp() + montoEssalud();
    }
    //Calcular el Sueldo total
    public double sueldoTotal(){
        return sueldoBase + montoHExtras();
    }
    //Calcular el monto del Sueldo Neto
    public double sueldoNeto(){
        return sueldoTotal() - montoDescuentos();
    }
    //Obtener el Nombre del Area del Empleado
    public String nombreArea(){
        switch(area){
            case 1: return "Sistemas";
            case 2: return "Administración";
            case 3: return "Marketing";
            case 4: return "Ventas";
            default:return "Otra";
        }
    }
    //Obtener el nombre de la Afiliación
    public String nombreAfp(){
        if(afp==1)
            return "AFP";
        else
            return "SNP";
    }

}
