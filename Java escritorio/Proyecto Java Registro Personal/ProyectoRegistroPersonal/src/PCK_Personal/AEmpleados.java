
package PCK_Personal;

//Importar la clase para manejar un Arreglo de Objetos Genéricos 
import java.util.*;
public class AEmpleados {
 //Crear un arreglo de objetos genéricos
 //Use la clase ArrayList
  private ArrayList<Empleado> a;
//Definir el Constructor
 public AEmpleados(){
  //Crear una instancia del Arreglo
   a= new ArrayList();   
 } 
 //Crear las operaciones de Administración
  //Agregar un nuevo Empleado
  //Pasar como parámetro la clase Empleado
 public void agregaEmpleado(Empleado e){
   a.add(e); //Agregando al nuevo Empleado  
 }
 //Obtener el empleado de una posición dada
 public Empleado getEmpleado(int i){
     return a.get(i);//Operación get lee el empleado según el índice
 }
 //Reemplazar un Empleado existente
 public void setEmpleado(int i, Empleado e){
     a.set(i, e); //Operación set realiza cambios en los datos del empleado
 }
 //Obtener la cantidad de empleados en el Arreglo
 // retorna el numero de empleados guardados en el arreglo
 //Operación getN()
public int getN(){
 return a.size();//Ontener el Tamaño del Arreglo
}
// busca un empleado por su codigo
public int busca(String codigo){
 for (int i=0; i<getN(); i++)
  if (codigo.equals(getEmpleado(i).getCodigo()))
    return i;// retorna la posición encontrada
 return -1; //significa que no lo encuentra
 }
// Operación para eliminar un empleado
public void elimina(int i){
 a.remove(i); //Método Remove() elimina un empleado
}
// elimina todos los objetos del arreglo
//Crear la operación elimina Todo
 public void eliminaTodo(){
  a.clear(); //Eliminando todo el Arreglo con método clear
}
} //Fin de la clase Empleado
