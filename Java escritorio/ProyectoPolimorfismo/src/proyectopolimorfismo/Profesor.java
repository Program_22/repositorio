
package proyectopolimorfismo;

//Importar la clase para salida de datos por pantalla
import java.io.PrintWriter;
//Aplicando Herencia: La clase Profesor se hereda de la clase Persona
public class Profesor extends Persona {
    //Declarar atributos
    private int horas;
    private double salario;
    //Crear los métodos para lectura y escritura de datos

    public int getHoras() {
        return horas;
    }


    public void setHoras(int horas) {
        this.horas = horas;
    }


    public double getSalario() {
        return salario;
    }


    public void setSalario(double salario) {
        this.salario = salario;
    }
//Método para calcular el salario aplicando sobrecarga de métodos
/*Sobrecarga de métodos:
 * Es un método con el mismo nombre pero con parámetros diferentes
*/    
    public void calcularSalario(){
        salario = horas * 22.70;
   }
   //Hacer la sobrecarga del método calcularSalario
  //en función a una tarifa
   public void calcularSalario(double tarifa){
       salario = horas * tarifa;
   }
   //Hacer la sobrecarga del método calcularSalario
  //en función a una tarifa y descuento
   public void calcularSalario(double tarifa, double descuento){
       salario = horas * tarifa - descuento;
   }
   //Crear el método para impresión de datos
   public void imprimir(){
       //Llamar al método imprimir de la clase Persona
       super.imprimir();
       //Crear el objeto p a partir de la clase PrintWriter
      PrintWriter p=new PrintWriter(System.out,true);
      p.println("Salario : " + salario);
   }
}
