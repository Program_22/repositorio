package proyectopolimorfismo;

//Importar la clase para salida de datos por pantalla
import java.io.PrintWriter;
//Aplicando Herencia: La clase Alumno se hereda de la clase Persona
public class Alumno extends Persona {
      //Declarar Atributos
    private int pp; //Promedio de prácticas
    private int ep; //Examen Parcial
    private int ef; //Examen Final
    private int pf; //Promedio Final

    //Crear los métodos para lectura y escritura de datos
    public int getPp() {
        return pp;
    }

    public void setPp(int pp) {
        this.pp = pp;
    }

    
    public int getEp() {
        return ep;
    }

    
    public void setEp(int ep) {
        this.ep = ep;
    }

    
    public int getEf() {
        return ef;
    }

    
    public void setEf(int ef) {
        this.ef = ef;
    }

    
    public int getPf() {
        return pf;
    }

    
    public void setPf(int pf) {
        this.pf = pf;
    }
 //Crear método para calcular promedio
 public void calcularPromedio(){
     pf = (pp +ep+ef)/3;
 }   
 //Método para imprimir datos
 public void imprimir(){
     //LLamar al método imprimir de la clase Persona
     super.imprimir();
     //Crear el objeto p a partir de la clase PrintWriter
      PrintWriter p=new PrintWriter(System.out,true);
      p.println("Promedio de Prácticas :" + pp);
      p.println("Examen Parcial :" + ep);
      p.println("Examen Final :" + ef);
      p.println("Promedio Final :" + pf);
 }
}
