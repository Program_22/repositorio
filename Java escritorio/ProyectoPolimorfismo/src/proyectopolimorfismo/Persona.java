package proyectopolimorfismo;
//Importar la clase para salida de datos por pantalla
import java.io.PrintWriter;
public class Persona {
   //Declarar atributos
    private String nombre;
    private String dni;
  //Crear un étodo para impresión de datos
  public void imprimir(){
      //Crear el objeto p a partir de la clase PrintWriter
      PrintWriter p=new PrintWriter(System.out,true);
      p.println("DNI : " + dni);
      p.println("Nombre : " + nombre);
  }  
  //Crear los métodos para lectura y escritura de datos
  //Método para leer el nombre
       public String getNombre() {
        return nombre;
    }
    //Método para escribir el nombre
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
  //Método para leer el DNI   
    public String getDni() {
        return dni;
    }
  //Método para escribir el DNI
    public void setDni(String dni) {
        this.dni = dni;
    }
  
}
