
package proyectopolimorfismo;

//Importar la clase para controlar el buffer del teclado
import java.io.BufferedReader;
//Importar la clase para el control de errores
import java.io.IOException;
//Importar la clase para la lectura de datos por teclado
import java.io.InputStreamReader;
//Importar la clase para la salida de datos por pantalla
import java.io.PrintWriter;

public class AplicacionPolimorfismo {
 //Crear el método main() para ejecutar la clase y asociarlo
 //al control de error IOException   
 public static void main(String[] args)throws IOException{
     //Crear el objeto x para la lectura de datos por teclado
     InputStreamReader x = new InputStreamReader(System.in);
     //Crear el objeto y para la lectura del buffer
     BufferedReader y = new BufferedReader(x);
     //Crear el objeto p para la salida de datos por pantalla
     PrintWriter p=new PrintWriter(System.out,true);
     //Declarar variables:
     int op;
     double tarifa, descuento;
     //Trabajar con clase alumno
     p.println("********** Alumno **********");
     //Crear el objeto a para representar a la clase Alumno
     Alumno a = new Alumno();
     p.println("Ingrese DNI : ");
     a.setDni(y.readLine());
     p.println("Ingrese Nombre : ");
     a.setNombre(y.readLine());
     p.println("Ingrese Promedio de Prácticas : ");
     a.setPp(Integer.parseInt(y.readLine()));
     p.println("Ingrese Examen Parcial : ");
     a.setEp(Integer.parseInt(y.readLine()));
     p.println("Ingrese Examen Final : ");
     a.setEf(Integer.parseInt(y.readLine()));
     //Calcular promedio
     a.calcularPromedio();
     //Mostrar Datos
     a.imprimir();
      p.println("********** Profesor **********");
     //Crear el objeto Prof para representar a la clase Profesor
      Profesor Prof = new Profesor();
      p.println("Ingrese DNI : ");
     Prof.setDni(y.readLine());
     p.println("Ingrese Nombre : ");
     Prof.setNombre(y.readLine());
     p.println("Ingrese Horas Trabajadas : ");
     Prof.setHoras(Integer.parseInt(y.readLine()));
     //Menú de opciones
     p.println("***** Opciones *****");
     p.println("[1] Con una Tarifa Constante");
     p.println("[2] Con una Tarifa Ingresada");
     p.println("[3] Con una Tarifa y un Descuento Ingresado");
     p.println("Ingrese Opcion [1-3] : ");
     op = Integer.parseInt(y.readLine());
     switch(op) {
         case 1: Prof.calcularSalario(); break; //Tarifa Constante 
         case 2: p.println("Ingresar Tarifa : "); //Tarifa Ingresada
                     tarifa = Double.parseDouble(y.readLine());
                     Prof.calcularSalario(tarifa);
                     break;
         case 3: p.println("Ingresar Tarifa : "); //Tarifa y Descuento Ingresado
                     tarifa = Double.parseDouble(y.readLine());
                     p.println("Ingresar Descuento : ");
                     descuento= Double.parseDouble(y.readLine());
                     Prof.calcularSalario(tarifa, descuento);
                     break;
     }
     //Imprimir datos
     Prof.imprimir();
 }   
}
