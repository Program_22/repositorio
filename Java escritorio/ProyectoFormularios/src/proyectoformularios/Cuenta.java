
package proyectoformularios;

public class Cuenta {
   //Declarar atributos miembros de la clase
    private int cta;
    private String tit;
    private double saldo;
    //Crear los m´todos para lectura y escritura
    
    public int getCta() {
        return cta;
    }

    /**
     * @param cta the cta to set
     */
    public void setCta(int cta) {
        this.cta = cta;
    }

    /**
     * @return the tit
     */
    public String getTit() {
        return tit;
    }

    /**
     * @param tit the tit to set
     */
    public void setTit(String tit) {
        this.tit = tit;
    }

    /**
     * @return the saldo
     */
    public double getSaldo() {
        return saldo;
    }

    /**
     * @param saldo the saldo to set
     */
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    
}
