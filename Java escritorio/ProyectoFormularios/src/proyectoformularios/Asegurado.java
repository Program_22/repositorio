
package proyectoformularios;

public class Asegurado {
    //Declarar Atributos
    private String codigo;
    private String nombres;
    private String apellidos;
    private char categoria;
    private double adicional;
    private double pago;
    private double total;
    private int edad;
    //Crear el método para Calcular Adicional
    public void calcularAdicional(){
        if (edad>55)
            adicional=0.10*pago;
    }
    //Crear el método para calcular Pago
    public void calcularPago(){
        //Pago en función a la categoria
        switch (categoria){
            case 'A': pago =98.00; break;
            case 'B': pago =76.00; break;
            case 'C': pago =55.00; break;    
        }
    }
    //Crear el método para calcular Total
    public void calcularTotal(){
        total = pago + adicional;
    }
    //Crear los métodos get y set para lectura y escritura de datos

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the nombres
     */
    public String getNombres() {
        return nombres;
    }

    /**
     * @param nombres the nombres to set
     */
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * @return the apellidos
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * @param apellidos the apellidos to set
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * @return the categoria
     */
    public char getCategoria() {
        return categoria;
    }

    /**
     * @param categoria the categoria to set
     */
    public void setCategoria(char categoria) {
        this.categoria = categoria;
    }

    /**
     * @return the adicional
     */
    public double getAdicional() {
        return adicional;
    }

    /**
     * @param adicional the adicional to set
     */
    public void setAdicional(double adicional) {
        this.adicional = adicional;
    }

    /**
     * @return the pago
     */
    public double getPago() {
        return pago;
    }

    /**
     * @param pago the pago to set
     */
    public void setPago(double pago) {
        this.pago = pago;
    }

    /**
     * @return the total
     */
    public double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(double total) {
        this.total = total;
    }

    /**
     * @return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }
    
}
