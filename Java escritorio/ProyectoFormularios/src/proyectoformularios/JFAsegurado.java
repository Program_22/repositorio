/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoformularios;

//Importar la clase JOptionPane para mostrar mensajes
import javax.swing.JOptionPane;
public class JFAsegurado extends javax.swing.JFrame {
//Crear un arreglo asociado a la clase Asegurado para 80 elementos
Asegurado a[] = new Asegurado[80];
int n;
    /**
     * Creates new form JFAsegurado
     */
//Constructor    
public JFAsegurado() {
        initComponents();
        //Invocar a la operación Mostrar Edades
        mostrarEdades();
    }
//Crear la operación mostrarEdades
public void mostrarEdades(){
    for (int i=0; i<=100;i++)
        cbxEdad.addItem(i);
}
//Crear la operación para Ingresar Datos
public void ingresarDatos(){
    String cod, nom, ape;
    char cat;
    int edad;
    double adic, pag, tot;
    //Capturar el Código
    cod = txtCodigo.getText();
    //Capturar el nombre
    nom = txtNombre.getText();
    //Capturar los Apellidos
    ape = txtApellido.getText();
    //Capturar la Categoría
    cat = cbxCategoria.getSelectedItem().toString().charAt(0);
    //Capturar la edad
    edad = cbxEdad.getSelectedIndex();
    //Crear la instancia del Asegurado e inicializarlo
    a[n]= new Asegurado();
    a[n].setCodigo(cod);
    a[n].setNombres(nom);
    a[n].setApellidos(ape);
    a[n].setCategoria(cat);
    a[n].setEdad(edad);
    a[n].calcularPago();
    a[n].calcularAdicional();
    a[n].calcularTotal();
    n++;
}
//Crear la operación Borrar Datos
public void borrarDatos(){
//Limpiar controles
txtCodigo.setText("");
txtNombre.setText("");
txtApellido.setText("");
//Seleccionar la primera Categoria
cbxCategoria.setSelectedIndex(0);
//Seleccionar la primera Edad
cbxEdad.setSelectedIndex(0);
//Poner el cursor en caja de texto Código
txtCodigo.requestFocus();
}
//Crear la operación Mostrar Datos
public void mostrarDatos(){
    for (int i=0;i<n;i++){
        tblReporte.setValueAt(a[i].getCodigo(), i, 0);
        tblReporte.setValueAt(a[i].getNombres(), i, 1);
        tblReporte.setValueAt(a[i].getApellidos(), i, 2);
        tblReporte.setValueAt(a[i].getCategoria(), i, 3);
        tblReporte.setValueAt(a[i].getEdad(), i, 4);
        tblReporte.setValueAt(a[i].getPago(), i, 5);
        tblReporte.setValueAt(a[i].getAdicional(), i, 6);
        tblReporte.setValueAt(a[i].getTotal(), i, 7);
    }
}
//Crear la operación para borrar datos de la Tabla
public void borrarTabla(){
    for (int i=0;i<n;i++)
        for (int j=0;j<8;j++)
            tblReporte.setValueAt("", i, j);
}
//Crear la operación mostrar según Categoria
public void mostrarSegunCategoria(){
    int indice;
    //Capturar el indice del elemento en lista
    indice= cbxOpcion.getSelectedIndex();
    switch(indice){
        case 0: mostrarDatos(); break;
        case 1: mostrar('A');break;//Asegurados de la Categoría A
        case 2: mostrar('B');break;//Asegurados de la Categoría B
        case 3: mostrar('C');break; //Asegurados de la Categoría C    
    }
}
//Crear la operación Mostrar
public void mostrar(char cat){
    int fila=0;
    for(int i=0;i<n;i++)
        if (a[i].getCategoria()==cat){
            tblReporte.setValueAt(a[i].getCodigo(), fila, 0);
            tblReporte.setValueAt(a[i].getNombres(), fila, 1);
            tblReporte.setValueAt(a[i].getApellidos(), fila, 2);
            tblReporte.setValueAt(a[i].getCategoria(), fila, 3);
            tblReporte.setValueAt(a[i].getEdad(), fila, 4);
            tblReporte.setValueAt(a[i].getPago(), fila, 5);
            tblReporte.setValueAt(a[i].getAdicional(), fila, 6);
            tblReporte.setValueAt(a[i].getTotal(), fila, 7);
            fila++;
        }
}
//Crear la operación para buscar al Asegurado
public void buscarAsegurado(){
    txtDatos.setText("");//Limpiar la caja de Texto TxtDatos
    String codigo;
    int indice=-1;
    //Capturar el código
    codigo=txtCodigoBusqueda.getText();
    for (int i=0;i<n;i++)
        //Comparar el código capturado
        if(codigo.equals(a[i].getCodigo()))
            indice=i;
    if (indice==-1) //No existe
        //Mostrar mensaje
        JOptionPane.showMessageDialog(this,"No se Encontró al Asegurado",
                "Aviso",JOptionPane.INFORMATION_MESSAGE);
    else{
     //Mostrar los datos el la caja de texto TextArea
     txtDatos.append("Código : "+a[indice].getCodigo()+"\n" );
     txtDatos.append("Nombre : "+a[indice].getNombres()+"\n" );
     txtDatos.append("Apellidos : "+a[indice].getApellidos()+"\n" );
     txtDatos.append("Categoría : "+a[indice].getCategoria()+"\n" );
     txtDatos.append("Edad : "+a[indice].getEdad()+"\n" );
     txtDatos.append("Pago : "+a[indice].getPago()+"\n" );
     txtDatos.append("Adicional : "+a[indice].getAdicional()+"\n" );
     txtDatos.append("Total : "+a[indice].getTotal()+"\n" );
    }
}
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tbpAsegurado = new javax.swing.JTabbedPane();
        pnlIngreso = new javax.swing.JPanel();
        lblCodigo = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblApellido = new javax.swing.JLabel();
        lblCategoria = new javax.swing.JLabel();
        lblEdad = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtApellido = new javax.swing.JTextField();
        cbxCategoria = new javax.swing.JComboBox();
        cbxEdad = new javax.swing.JComboBox();
        btnIngresar = new javax.swing.JButton();
        pnlBusqueda = new javax.swing.JPanel();
        lblCodigoBusqueda = new javax.swing.JLabel();
        txtCodigoBusqueda = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDatos = new javax.swing.JTextArea();
        pnlReporte = new javax.swing.JPanel();
        lblOpcion = new javax.swing.JLabel();
        cbxOpcion = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblReporte = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Registro de Asegurado");

        tbpAsegurado.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                tbpAseguradoAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        lblCodigo.setText("Código");

        lblNombre.setText("Nombres");

        lblApellido.setText("Apellidos");

        lblCategoria.setText("Categoría");

        lblEdad.setText("Edad");

        cbxCategoria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "A", "B", "C" }));

        btnIngresar.setText("Ingresar");
        btnIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlIngresoLayout = new javax.swing.GroupLayout(pnlIngreso);
        pnlIngreso.setLayout(pnlIngresoLayout);
        pnlIngresoLayout.setHorizontalGroup(
            pnlIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlIngresoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCodigo)
                    .addComponent(lblNombre)
                    .addComponent(lblApellido)
                    .addComponent(lblCategoria)
                    .addComponent(lblEdad))
                .addGap(48, 48, 48)
                .addGroup(pnlIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(txtNombre)
                        .addGroup(pnlIngresoLayout.createSequentialGroup()
                            .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                            .addComponent(btnIngresar))
                        .addComponent(txtApellido)
                        .addComponent(cbxCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cbxEdad, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(80, Short.MAX_VALUE))
        );
        pnlIngresoLayout.setVerticalGroup(
            pnlIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlIngresoLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(pnlIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCodigo)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnIngresar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNombre)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblApellido)
                    .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlIngresoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlIngresoLayout.createSequentialGroup()
                        .addComponent(lblCategoria)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblEdad))
                    .addGroup(pnlIngresoLayout.createSequentialGroup()
                        .addComponent(cbxCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbxEdad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(102, Short.MAX_VALUE))
        );

        tbpAsegurado.addTab("Ingreso", pnlIngreso);

        lblCodigoBusqueda.setText("Ingrese Código");

        txtCodigoBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCodigoBusquedaKeyPressed(evt);
            }
        });

        txtDatos.setColumns(20);
        txtDatos.setRows(5);
        jScrollPane1.setViewportView(txtDatos);

        javax.swing.GroupLayout pnlBusquedaLayout = new javax.swing.GroupLayout(pnlBusqueda);
        pnlBusqueda.setLayout(pnlBusquedaLayout);
        pnlBusquedaLayout.setHorizontalGroup(
            pnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBusquedaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlBusquedaLayout.createSequentialGroup()
                        .addComponent(lblCodigoBusqueda)
                        .addGap(26, 26, 26)
                        .addComponent(txtCodigoBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBusquedaLayout.createSequentialGroup()
                        .addGap(0, 22, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20))))
        );
        pnlBusquedaLayout.setVerticalGroup(
            pnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBusquedaLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(pnlBusquedaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCodigoBusqueda)
                    .addComponent(txtCodigoBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(63, Short.MAX_VALUE))
        );

        tbpAsegurado.addTab("Búsqueda", pnlBusqueda);

        pnlReporte.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                pnlReporteAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        lblOpcion.setText("Elija Opción");

        cbxOpcion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Todos", "A", "B", "C" }));
        cbxOpcion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxOpcionItemStateChanged(evt);
            }
        });

        tblReporte.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Código", "Nombres", "Apellidos", "Categoría", "Edad", "Pago", "Adicional", "Total"
            }
        ));
        jScrollPane2.setViewportView(tblReporte);

        javax.swing.GroupLayout pnlReporteLayout = new javax.swing.GroupLayout(pnlReporte);
        pnlReporte.setLayout(pnlReporteLayout);
        pnlReporteLayout.setHorizontalGroup(
            pnlReporteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlReporteLayout.createSequentialGroup()
                .addGroup(pnlReporteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlReporteLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(lblOpcion)
                        .addGap(18, 18, 18)
                        .addComponent(cbxOpcion, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlReporteLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 362, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnlReporteLayout.setVerticalGroup(
            pnlReporteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlReporteLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(pnlReporteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOpcion)
                    .addComponent(cbxOpcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tbpAsegurado.addTab("Reporte", pnlReporte);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tbpAsegurado)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tbpAsegurado)
                .addContainerGap())
        );

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-416)/2, (screenSize.height-338)/2, 416, 338);
    }// </editor-fold>//GEN-END:initComponents

    private void btnIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarActionPerformed
        // TODO add your handling code here:
        //Invocar a la operación Ingresar Datos
        ingresarDatos();
        //Invocar a la operacion borrarDatos
        borrarDatos();
    }//GEN-LAST:event_btnIngresarActionPerformed

    private void tbpAseguradoAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_tbpAseguradoAncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_tbpAseguradoAncestorAdded

    private void pnlReporteAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_pnlReporteAncestorAdded
        // TODO add your handling code here:
        //Llamar a la operación Mostrar Datos
        mostrarDatos();
    }//GEN-LAST:event_pnlReporteAncestorAdded

    private void cbxOpcionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxOpcionItemStateChanged
        // TODO add your handling code here:
        //Llamar a la operación borrar Tabla
        borrarTabla();
        //Llamar a la operación Mostrar Segun Categoría
        mostrarSegunCategoria();
    }//GEN-LAST:event_cbxOpcionItemStateChanged

    private void txtCodigoBusquedaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoBusquedaKeyPressed
        // TODO add your handling code here:
        //Llamar a la operación buscar Asegurado
        //Capturar la tecla enter
        if(evt.getKeyCode()==evt.VK_ENTER)
            buscarAsegurado();
    }//GEN-LAST:event_txtCodigoBusquedaKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFAsegurado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFAsegurado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFAsegurado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFAsegurado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new JFAsegurado().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIngresar;
    private javax.swing.JComboBox cbxCategoria;
    private javax.swing.JComboBox cbxEdad;
    private javax.swing.JComboBox cbxOpcion;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblApellido;
    private javax.swing.JLabel lblCategoria;
    private javax.swing.JLabel lblCodigo;
    private javax.swing.JLabel lblCodigoBusqueda;
    private javax.swing.JLabel lblEdad;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblOpcion;
    private javax.swing.JPanel pnlBusqueda;
    private javax.swing.JPanel pnlIngreso;
    private javax.swing.JPanel pnlReporte;
    private javax.swing.JTable tblReporte;
    private javax.swing.JTabbedPane tbpAsegurado;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtCodigoBusqueda;
    private javax.swing.JTextArea txtDatos;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
