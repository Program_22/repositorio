package Restaurante;
import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
public class RegistroRestaurante extends javax.swing.JPanel {
        
    Registro r[]  = new Registro[20];
    int n;  
    public RegistroRestaurante() {
        initComponents();   
        LlenarMesas();
        CodigoAutogenerado();        
    }
    public void LlenarMesas(){
        for (int i=1;i<=20;i++){
            cbonMesa.addItem(i);            
        }
    }
    
    public void CalcularDatos(){
        if (txtCantidad.getText().length()==0){   
            JOptionPane.showMessageDialog(this, "Ingrese una Cantidad");
        }else{
        int itemplato,cantid;
        itemplato = cboPlatoTipico.getSelectedIndex();
        r[n] = new Registro();
        r[n].CalcularPrecio(itemplato);
        txtPrecioPlato.setText(String.valueOf(r[n].getPrecioplato()));        
        cantid= Integer.parseInt(txtCantidad.getText()); 
        r[n].TotalParcial(cantid);
        txtTotalParcial.setText(String.valueOf(r[n].getTotalparcial()));
        r[n].CalcularIgv();
        txtIgv.setText(String.valueOf(r[n].getIgv()));
        r[n].Total();
        txtTotal.setText(String.valueOf(r[n].getTotal()));
        }
    }      
    
    public void GrabarRegistro(){
        String nombreplato,nmesa;
        int npedido,cantidad;
        double precioplato,igv,totalparcial,total;        
        npedido = Integer.parseInt(txtnPedido.getText());
        nombreplato = (String) cboPlatoTipico.getSelectedItem();
        nmesa = cbonMesa.getSelectedItem().toString();
        cantidad = Integer.parseInt(txtCantidad.getText());
        precioplato = Double.parseDouble(txtPrecioPlato.getText());
        igv = Double.parseDouble(txtIgv.getText());
        totalparcial = Double.parseDouble(txtTotalParcial.getText());
        total = Double.parseDouble(txtTotal.getText());
        r[n] = new Registro();
        r[n].setCodigo(npedido);
        r[n].setMesas(nmesa);
        r[n].setNombreplato(nombreplato);
        r[n].setPrecioplato(precioplato);
        r[n].setCantidad(cantidad);
        r[n].setTotalparcial(totalparcial);
        r[n].setIgv(igv);
        r[n].setTotal(total); 
        txtTexto.append("N° Pedido : " + npedido + "\n");
        txtTexto.append("N° Mesa: " + nmesa + "\n" );
        txtTexto.append("Nombre Plato : " + nombreplato + "\n" );
        txtTexto.append("Precio: " + precioplato + "\n" );
        txtTexto.append("Cantidad : " + cantidad + "\n" );
        txtTexto.append("Total parcial : " + totalparcial + "\n" );
        txtTexto.append("Igv : " + igv + "\n" );
        txtTexto.append("Total : " + total + "\n" );
        txtTexto.append("Total : " + total + "\n" );
        txtTexto.append("===============================" + "\n");
        n++;
    }
    
    public void LimpiarDatos(){
        txtnPedido.setText("");
        cboPlatoTipico.setSelectedIndex(0);
        cbonMesa.setSelectedIndex(0);
        txtCantidad.setText("");
        txtPrecioPlato.setText("");
        txtIgv.setText("");
        txtTotalParcial.setText("");
        txtTotal.setText("");        
    }
    
    public void MostrarTabla(){
        int fila= 0;
        for (int i=0;i<n;i++){        
            tblPedidos.setValueAt(r[i].getCodigo(), fila, 0);
            tblPedidos.setValueAt(r[i].getMesas(), fila, 1);
            tblPedidos.setValueAt(r[i].getNombreplato(), fila, 2);
            tblPedidos.setValueAt(r[i].getPrecioplato(), fila, 3);
            tblPedidos.setValueAt(r[i].getCantidad(), fila, 4);
            tblPedidos.setValueAt(r[i].getTotalparcial(), fila, 5);
            tblPedidos.setValueAt(r[i].getIgv(), fila, 6);
            tblPedidos.setValueAt(r[i].getTotal(), fila, 7);            
            fila++;
        }    
    }
    
    public void MostrarTablaConsulta(){     
        int numero2 =-9;        
        String busqueda2 = cboConsultar.getSelectedItem().toString();
        int conver = Integer.parseInt(busqueda2);
//        JOptionPane.showMessageDialog(this, conver);
        for (int i=0;i<n;i++) 
            if(conver == r[i].getCodigo())                
                numero2=i;        
        if (numero2==-8) {            
        }else if(numero2==0){ 
           
        }
        else{  
                tblConsulta.setValueAt(r[numero2].getCodigo(), 0, 0);
                tblConsulta.setValueAt(r[numero2].getMesas(), 0, 1);
                tblConsulta.setValueAt(r[numero2].getNombreplato(), 0, 2);
                tblConsulta.setValueAt(r[numero2].getPrecioplato(), 0, 3);
                tblConsulta.setValueAt(r[numero2].getCantidad(), 0, 4);
                tblConsulta.setValueAt(r[numero2].getTotalparcial(), 0, 5);
                tblConsulta.setValueAt(r[numero2].getIgv(), 0, 6);
                tblConsulta.setValueAt(r[numero2].getTotal(), 0, 7); 
        }
    }
    
    
    public void GrabarTexto(){       
        File f;
        f = new File("archivos/archivo.txt");
        //Escritura
        try{
        FileWriter w = new FileWriter(f);
        BufferedWriter bw = new BufferedWriter(w);
        PrintWriter wr = new PrintWriter(bw);	
        r[n] = new Registro();
        for (int i=0; i<1; i++){
        wr.write(String.valueOf(txtnPedido.getText())+ "," +
                String.valueOf(cbonMesa.getSelectedItem()) + "," + 
                cboPlatoTipico.getSelectedItem() + "," + 
                txtCantidad.getText() + "," + 
                txtPrecioPlato.getText() + "," + 
                txtTotalParcial.getText() + "," + 
                txtIgv.getText() + "," + 
                txtTotal.getText() 
//                r[n].getNombreplato() + "," + 
//                String.valueOf(r[n].getPrecioplato())+ "," +
//                String.valueOf(r[n].getCantidad()) + "," +
//                String.valueOf(r[n].getTotalparcial()) + "," +
//                String.valueOf(r[n].getIgv()) + "," + 
//                String.valueOf(r[n].getTotal())
        
        );
        }
        wr.close();
        bw.close();        
        }catch(IOException e){
        };    
         JOptionPane.showMessageDialog(this, "esta gravando");
    }
       
    public void EliminarPedido(){
        DefaultTableModel modelo = (DefaultTableModel)tblPedidos.getModel(); 
        int fila=0;
        fila = tblPedidos.getSelectedRow();        
        modelo.removeRow(fila);        
            
    }
    
    public void EliminarTodo(){
        for (int i=0;i<n;i++)
            for (int j=0;j<8;j++)
                tblPedidos.setValueAt("", i, j);
    }
    
    public void CodigoAutogenerado(){
        int f=0;
        for(int i=0;i<250;i++){
            txtnPedido.setText(String.valueOf(f));  
            f++;
        }
    }
    
    public void vaciarcombo2(){
    int itemCount2 = cboConsultar.getItemCount();
    for(int i=0;i<itemCount2;i++){
        cboConsultar.removeItemAt(0);
        }
    }    
    public void llenarcomboconsulta(){
        vaciarcombo2();
        int variable=0;
        r[n] = new Registro();
        for (int i=0;i<10;i++){            
            variable = r[i].getCodigo();
            cboConsultar.addItem(variable); 
        }  
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jtb1 = new javax.swing.JTabbedPane();
        jpmostrar = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblPedidos = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        btnEliiminarPediido = new javax.swing.JButton();
        btnEliminarTodo = new javax.swing.JButton();
        btnSalirMostrarRegistro = new javax.swing.JButton();
        jpconsulta = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        cboConsultar = new javax.swing.JComboBox();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblConsulta = new javax.swing.JTable();
        jtbinsertar = new javax.swing.JPanel();
        cbonMesa = new javax.swing.JComboBox();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txtnPedido = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        cboPlatoTipico = new javax.swing.JComboBox();
        txtTotal = new javax.swing.JTextField();
        txtTotalParcial = new javax.swing.JTextField();
        txtIgv = new javax.swing.JTextField();
        txtPrecioPlato = new javax.swing.JTextField();
        txtCantidad = new javax.swing.JTextField();
        btnCalcular = new javax.swing.JButton();
        btnGrabar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnCalcular1 = new javax.swing.JButton();
        jtbTexto = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtTexto = new javax.swing.JTextArea();
        jLabel24 = new javax.swing.JLabel();

        setLayout(new java.awt.BorderLayout());

        jtb1.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jtb1AncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        jpmostrar.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jpmostrarAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        tblPedidos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "N° Pedido", "N° Mesa", "Plato Típico", "Precio", "Cantidad", "Total Parcial", "IGV", "Total"
            }
        ));
        jScrollPane2.setViewportView(tblPedidos);

        jLabel8.setBackground(new java.awt.Color(204, 204, 255));
        jLabel8.setFont(new java.awt.Font("Verdana", 1, 18)); // NOI18N
        jLabel8.setText("Pedidos");

        btnEliiminarPediido.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btnEliiminarPediido.setText("Eliminar Pedido");
        btnEliiminarPediido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliiminarPediidoActionPerformed(evt);
            }
        });

        btnEliminarTodo.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btnEliminarTodo.setText("Eliminar Todo");
        btnEliminarTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarTodoActionPerformed(evt);
            }
        });

        btnSalirMostrarRegistro.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btnSalirMostrarRegistro.setText("Salir");
        btnSalirMostrarRegistro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirMostrarRegistroActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jpmostrarLayout = new javax.swing.GroupLayout(jpmostrar);
        jpmostrar.setLayout(jpmostrarLayout);
        jpmostrarLayout.setHorizontalGroup(
            jpmostrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpmostrarLayout.createSequentialGroup()
                .addGroup(jpmostrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpmostrarLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 536, Short.MAX_VALUE))
                    .addGroup(jpmostrarLayout.createSequentialGroup()
                        .addGroup(jpmostrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jpmostrarLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(btnEliiminarPediido)
                                .addGap(50, 50, 50)
                                .addComponent(btnEliminarTodo)
                                .addGap(44, 44, 44)
                                .addComponent(btnSalirMostrarRegistro))
                            .addGroup(jpmostrarLayout.createSequentialGroup()
                                .addGap(207, 207, 207)
                                .addComponent(jLabel8)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jpmostrarLayout.setVerticalGroup(
            jpmostrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpmostrarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jpmostrarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEliminarTodo)
                    .addComponent(btnEliiminarPediido)
                    .addComponent(btnSalirMostrarRegistro))
                .addContainerGap(195, Short.MAX_VALUE))
        );

        jtb1.addTab("Mostrar Registros", jpmostrar);

        jpconsulta.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jpconsultaAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        jLabel17.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jLabel17.setText("N° Pedido");

        cboConsultar.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboConsultarItemStateChanged(evt);
            }
        });
        cboConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboConsultarActionPerformed(evt);
            }
        });

        tblConsulta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "N° Pedido", "N° Mesa", "Plato Típico", "Precio", "Cantidad", "Total Parcial", "IGV", "Total"
            }
        ));
        jScrollPane3.setViewportView(tblConsulta);

        javax.swing.GroupLayout jpconsultaLayout = new javax.swing.GroupLayout(jpconsulta);
        jpconsulta.setLayout(jpconsultaLayout);
        jpconsultaLayout.setHorizontalGroup(
            jpconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpconsultaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpconsultaLayout.createSequentialGroup()
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                        .addComponent(cboConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 421, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpconsultaLayout.createSequentialGroup()
                        .addComponent(jScrollPane3)
                        .addContainerGap())))
        );
        jpconsultaLayout.setVerticalGroup(
            jpconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpconsultaLayout.createSequentialGroup()
                .addGap(0, 91, Short.MAX_VALUE)
                .addGroup(jpconsultaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(cboConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(146, 146, 146))
        );

        jtb1.addTab("Consultar", jpconsulta);

        cbonMesa.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        cbonMesa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbonMesaActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jLabel15.setText("N° Pedido");

        jLabel16.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jLabel16.setText("N° Mesa");

        txtnPedido.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        txtnPedido.setAutoscrolls(false);

        jLabel18.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jLabel18.setText("Plato Típico");

        jLabel19.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jLabel19.setText("Cantidad");

        jLabel20.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jLabel20.setText("Precio");

        jLabel21.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jLabel21.setText("Total Parcial");

        jLabel22.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jLabel22.setText("IGV 18%");

        jLabel23.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jLabel23.setText("Total");

        cboPlatoTipico.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        cboPlatoTipico.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Arroz con pollo", "Seco con Frejoles", "Patita con Mani", "Cau Cau", "Arroz a la Cubana", "Bisteck a lo Pobre" }));
        cboPlatoTipico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboPlatoTipicoActionPerformed(evt);
            }
        });

        txtTotal.setEditable(false);
        txtTotal.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        txtTotal.setAutoscrolls(false);

        txtTotalParcial.setEditable(false);
        txtTotalParcial.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        txtTotalParcial.setAutoscrolls(false);

        txtIgv.setEditable(false);
        txtIgv.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        txtIgv.setAutoscrolls(false);

        txtPrecioPlato.setEditable(false);
        txtPrecioPlato.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        txtPrecioPlato.setAutoscrolls(false);

        txtCantidad.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N

        btnCalcular.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btnCalcular.setText("Calcular");
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });

        btnGrabar.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btnGrabar.setText("Grabar");
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGrabarActionPerformed(evt);
            }
        });

        btnSalir.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnNuevo.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnCalcular1.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        btnCalcular1.setText("GrabarArchivo");
        btnCalcular1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcular1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jtbinsertarLayout = new javax.swing.GroupLayout(jtbinsertar);
        jtbinsertar.setLayout(jtbinsertarLayout);
        jtbinsertarLayout.setHorizontalGroup(
            jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jtbinsertarLayout.createSequentialGroup()
                .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jtbinsertarLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jtbinsertarLayout.createSequentialGroup()
                        .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jtbinsertarLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel21)
                                    .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel23)
                                            .addComponent(jLabel22)
                                            .addComponent(jLabel18)
                                            .addComponent(jLabel16)))
                                    .addComponent(jLabel19)
                                    .addComponent(jLabel20))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cbonMesa, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cboPlatoTipico, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtPrecioPlato, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtIgv, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtTotalParcial, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtnPedido, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jtbinsertarLayout.createSequentialGroup()
                                .addGap(58, 58, 58)
                                .addComponent(btnGrabar)
                                .addGap(87, 87, 87)
                                .addComponent(btnSalir)
                                .addGap(11, 11, 11)))
                        .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jtbinsertarLayout.createSequentialGroup()
                                .addGap(61, 61, 61)
                                .addComponent(btnCalcular1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jtbinsertarLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCalcular, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(82, 82, 82))
        );
        jtbinsertarLayout.setVerticalGroup(
            jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jtbinsertarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txtnPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(cbonMesa, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(cboPlatoTipico, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPrecioPlato, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(btnCalcular))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTotalParcial, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(btnNuevo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtIgv, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22)
                    .addComponent(btnCalcular1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23))
                .addGap(18, 18, 18)
                .addGroup(jtbinsertarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGrabar)
                    .addComponent(btnSalir))
                .addGap(45, 45, 45))
        );

        jtb1.addTab("Ingresar Registro", jtbinsertar);

        jtbTexto.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                jtbTextoAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        txtTexto.setColumns(20);
        txtTexto.setRows(5);
        jScrollPane1.setViewportView(txtTexto);

        jLabel24.setFont(new java.awt.Font("Verdana", 1, 12)); // NOI18N
        jLabel24.setText("Pedidos");

        javax.swing.GroupLayout jtbTextoLayout = new javax.swing.GroupLayout(jtbTexto);
        jtbTexto.setLayout(jtbTextoLayout);
        jtbTextoLayout.setHorizontalGroup(
            jtbTextoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jtbTextoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
            .addGroup(jtbTextoLayout.createSequentialGroup()
                .addGap(231, 231, 231)
                .addComponent(jLabel24)
                .addContainerGap(273, Short.MAX_VALUE))
        );
        jtbTextoLayout.setVerticalGroup(
            jtbTextoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jtbTextoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel24)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(140, Short.MAX_VALUE))
        );

        jtb1.addTab("Archivo de Texto", jtbTexto);

        add(jtb1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void cbonMesaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbonMesaActionPerformed
        // TODO add your handling code here:       
    }//GEN-LAST:event_cbonMesaActionPerformed

    private void cboPlatoTipicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboPlatoTipicoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboPlatoTipicoActionPerformed

    private void btnEliiminarPediidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliiminarPediidoActionPerformed
        // TODO add your handling code here:  
        EliminarPedido();
    }//GEN-LAST:event_btnEliiminarPediidoActionPerformed

    private void btnEliminarTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarTodoActionPerformed
        // TODO add your handling code here:
        EliminarTodo();
    }//GEN-LAST:event_btnEliminarTodoActionPerformed

    private void btnSalirMostrarRegistroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirMostrarRegistroActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_btnSalirMostrarRegistroActionPerformed

    private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
        // TODO add your handling code here:
        CalcularDatos();
    }//GEN-LAST:event_btnCalcularActionPerformed

    private void btnGrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGrabarActionPerformed
        // TODO add your handling code here:        
        GrabarRegistro();
        LimpiarDatos();        
    }//GEN-LAST:event_btnGrabarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_btnSalirActionPerformed

    private void cboConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboConsultarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboConsultarActionPerformed

    private void jtb1AncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jtb1AncestorAdded
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jtb1AncestorAdded

    private void jpmostrarAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jpmostrarAncestorAdded
        // TODO add your handling code here:
        MostrarTabla();
    }//GEN-LAST:event_jpmostrarAncestorAdded

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:        
        LimpiarDatos();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void jtbTextoAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jtbTextoAncestorAdded
        // TODO add your handling code here:      
    }//GEN-LAST:event_jtbTextoAncestorAdded

    private void jpconsultaAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_jpconsultaAncestorAdded
        // TODO add your handling code here:        
        llenarcomboconsulta();    
        
    }//GEN-LAST:event_jpconsultaAncestorAdded

    private void cboConsultarItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboConsultarItemStateChanged
        // TODO add your handling code here:   
        MostrarTablaConsulta();
    }//GEN-LAST:event_cboConsultarItemStateChanged

    private void btnCalcular1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcular1ActionPerformed
        // TODO add your handling code here:
        GrabarTexto();
    }//GEN-LAST:event_btnCalcular1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnCalcular1;
    private javax.swing.JButton btnEliiminarPediido;
    private javax.swing.JButton btnEliminarTodo;
    private javax.swing.JButton btnGrabar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnSalirMostrarRegistro;
    private javax.swing.JComboBox cboConsultar;
    private javax.swing.JComboBox cboPlatoTipico;
    private javax.swing.JComboBox cbonMesa;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPanel jpconsulta;
    private javax.swing.JPanel jpmostrar;
    private javax.swing.JTabbedPane jtb1;
    private javax.swing.JPanel jtbTexto;
    private javax.swing.JPanel jtbinsertar;
    private javax.swing.JTable tblConsulta;
    private javax.swing.JTable tblPedidos;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtIgv;
    private javax.swing.JTextField txtPrecioPlato;
    private javax.swing.JTextArea txtTexto;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtTotalParcial;
    private javax.swing.JTextField txtnPedido;
    // End of variables declaration//GEN-END:variables
}
