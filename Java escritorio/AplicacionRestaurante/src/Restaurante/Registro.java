package Restaurante;

public class Registro {
    
    private int codigo;
    private String mesas;
    private String nombreplato;
    private double precioplato;
    private int cantidad;
    private double total;
    private double totalparcial;
    private double igv;
       
    public void CalcularPrecio(int itemplato){
        switch (itemplato){
            case 0 : precioplato=12.50;break;
            case 1 : precioplato=18.00;break;
            case 2 : precioplato=10.00;break;
            case 3 : precioplato=9.00;break;
            case 4 : precioplato=7.50;break;
            case 5 : precioplato=17.00;break;
            default : precioplato=00.00;break;
        }
    }
    public void TotalParcial(int cantid){
        totalparcial = precioplato * cantid;
    }
    
    public void CalcularIgv(){
        igv = totalparcial * 0.18;
    }
    
    public void Total(){
        total = totalparcial + igv;
    }
    
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getMesas() {
        return mesas;
    }

    public void setMesas(String mesas) {
        this.mesas = mesas;
    }

    public String getNombreplato() {
        return nombreplato;
    }

    public void setNombreplato(String nombreplato) {
        this.nombreplato = nombreplato;
    }

    public double getPrecioplato() {
        return precioplato;
    }

    public void setPrecioplato(double precioplato) {
        this.precioplato = precioplato;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getTotalparcial() {
        return totalparcial;
    }

    public void setTotalparcial(double totalparcial) {
        this.totalparcial = totalparcial;
    }

    public double getIgv() {
        return igv;
    }

    public void setIgv(double igv) {
        this.igv = igv;
    }
            

    
    
    
    
}
