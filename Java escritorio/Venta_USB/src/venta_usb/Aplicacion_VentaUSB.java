package venta_usb;
//Importar la clase para la lectura de datos por teclado
import java.io.InputStreamReader;
//Importar clase para la lectura del Buffer
import java.io.BufferedReader;
//Importar la clase para el control de errores
import java.io.IOException;
//Importar la clase para impresión de datos por pantalla
import java.io.PrintWriter;
public class Aplicacion_VentaUSB {
    //Crear el método main para ejecutar la clase
    //throws asocia el control de error al método main()
    public static void main(String[] args)throws IOException{
        //Crear el objeto (x) para lectura de datos por teclado
        InputStreamReader x= new InputStreamReader(System.in);
        //Asociar el objeto x al buffer del teclado
        BufferedReader y=new BufferedReader(x);
        //Crear el objeto para la escritura de datos
        PrintWriter p= new PrintWriter(System.out,true);
        //Crear el objeto m desde la clase memoria
        Memoria m = new Memoria();
        //Pedir la capacidad de memoria
        p.println("Capacidad de Memoria [8,16,32,64] : ");
        m.setCapacidad(Integer.parseInt(y.readLine()));
        //Pedir la marca de memoria
        p.println("Marca de Memoria [I]mation,[S]can Disk,[L]G,[Y]Sony : ");
        //charAt(0): lee un caracter
        m.setMarca(y.readLine().charAt(0));
        //Pedir si tiene seguro
        p.println("Tiene Seguro(S/N) ? : ");
        m.setSeguro(y.readLine().charAt(0));
        //Calcular precio
        m.calcularPrecio();
        //Calcular el Incremento
        m.calcularIncremento();
        //Calcular el Adicional
        m.calcularAdicional();
        //Calcular el Total
        m.calcularTotal();
        //Imprimir Datos
        m.imprimir();
    }
}
