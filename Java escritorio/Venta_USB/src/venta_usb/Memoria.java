package venta_usb;
//Importar la Clase PrintWriter
//io es una clase con responsabilidad de Entrada y Salida de datos
import java.io.PrintWriter;
public class Memoria {
//Declarar atributos
    private double prec, total;
    private double incremento;
    private double adicional;
    private int capacidad;
    private char marca;
    private char seguro;
//Definir operaciones para la clase
    //Método para leer el precio
    public double getPrec() {
        return prec;
    }

    //Método para escribir el precio
    public void setPrec(double prec) {
        this.prec = prec;
    }

    
    public double getTotal() {
        return total;
    }

   
    public void setTotal(double total) {
        this.total = total;
    }

   
    public double getIncremento() {
        return incremento;
    }

   
    public void setIncremento(double incremento) {
        this.incremento = incremento;
    }

   
    public double getAdicional() {
        return adicional;
    }

   
    public void setAdicional(double adicional) {
        this.adicional = adicional;
    }

   
    public int getCapacidad() {
        return capacidad;
    }

   
    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

   
    public char getMarca() {
        return marca;
    }

    
    public void setMarca(char marca) {
        this.marca = marca;
    }

    
    public char getSeguro() {
        return seguro;
    }

    
    public void setSeguro(char seguro) {
        this.seguro = seguro;
    }
    //Método para calcular el precio
    public void calcularPrecio(){
        //Calcular el precio en función a la capacidad
        if(capacidad==8)
            prec=23.00;
        else if (capacidad==16)
            prec=35;
        else if (capacidad==32)
            prec=45;
        else if (capacidad==64)
            prec=60;
    }
    //Método para calcular el Incremento
    public void calcularIncremento(){
     //Calcular el Incremento en función a la marca
        switch(marca){
            case 'I': incremento=0;break; //Marca Imation
            case 'S': incremento=0.02;break; //Marca Scan Disk
            case 'L': incremento=0.08;break; //Marca LG
            case 'Y': incremento=0.10;break; //Marca Sony    
        }
}
    //Método para calcular el adicional
    public void calcularAdicional(){
        if(seguro=='S')
            adicional=5;
    }
    //Método para calcular el total
    public void calcularTotal(){
        total=prec + (prec * incremento)+adicional;
    }
    //Método para Imprimir los resultados
    public void imprimir(){
        //Crear el objeto PrintWriter para la escritura de datos en pantalla
        PrintWriter p=new PrintWriter(System.out,true);
        p.println("Capacidad: " + capacidad);
        p.println("Marca      : " + marca);
        p.println("Precio      : " + prec);
        p.println("Incremento: " + incremento);
        p.println("Adicional : " + adicional);
        p.println("Seguro    : " + seguro);
        p.println("Total a Pagar: " + total);
    }
}
