package herencia;
//importar la clase para la escritura de datos 
import java.io.PrintWriter;

public class alumno {
    //declarar atributos
  private double pMensual;//pago mensual 
  private double adicional;//pago adicional 
  private double total;//pago total 
  private char ingles;//si considera ingles 
 //crear los metodos de Lectura y Escritura
//metodo para leer el pago mensual 
    
    public double getpMensual() {
        return pMensual;
    }
    //metodo para escribir el pago mensual 
    public void setpMensual(double pMensual) {
        this.pMensual = pMensual;
    }

   
    public double getAdicional() {
        return adicional;
    }

   
    public void setAdicional(double adicional) {
        this.adicional = adicional;
    }

  
    public double getTotal() {
        return total;
    }

    
    public void setTotal(double total) {
        this.total = total;
    }

   
    public char getIngles() {
        return ingles;
    }

    public void setIngles(char ingles) {
        this.ingles = ingles;
    }
    //metodo para calcular el adicional 
    public void calcularadicional(){
        if(ingles=='s')
            adicional=75.20;
                   
        }
    //metodo para calcular el total 
        public void calculartotal(){
            total=pMensual+adicional;
    }
        //metodo para imprimir los datos 
        public void imprimir(){
            PrintWriter p=new PrintWriter(System.out,true);
            p.println("pago mensual:" + pMensual);
            p.println("ingles:"+ingles);
            p.println("adicional:"+adicional);
            p.println("ingles:"+total);
                    
        }
}

