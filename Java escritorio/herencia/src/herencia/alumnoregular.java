
package herencia;
//importar la clase para salida de datos

//clase alumno regular se hereda de clase alumno
public class alumnoregular extends alumno {
    //declarar atributo 
    private char categoria;//definir categoria
    private  char seguro;//definir seguro
    private int incremento;//definir incremento
    //crear los datos de lectura y escritura de datos

    //metodo para leer la categoria
    public char getCategoria() {
        return categoria;
    }
    
//metodo para escribir la categoria
    public void setCategoria(char categoria) {
        this.categoria = categoria;
    }

   
    public char getSeguridad() {
        return seguridad;
    }

   
    public void setSeguridad(char seguridad) {
        this.seguridad = seguridad;
    }

    
    public int getIncremento() {
        return incremento;
    }

    
    public void setIncremento(int incremento) {
        this.incremento = incremento;
    }
 //metodo para calcular el pago mensual 
    public void calcularpagomensual(){
        //el pago mensual esta en funcion de la categoria
        switch(categoria){
            case 'a':setpMensual(790.40);break;
            case 'b':setpMensual(650.80);break;
            case 'c':setpMensual(580.20);break;
        }
        //metodo para calcular el incremento 
        public void calcularincremento(){
            if(seguro=='s')
                incremento=0.02*getpMensual();
            
        
    }
  //metodo para calcular el total 
        public void calcularTotal(){
            super.calcularTotal();
           settotal(gettotal+incremento); 
        
        }
       //metodo para imprimir datos 
        public void imprimir(){
            PrintWriter p=new PrintWriter(System.out,true);
           p.println("categoria:"+categoria);
           p.println("seguro:"+seguro);
           p.println("incremento:"+incremento);
           super.imprimir();//llamar al metodo imprimir de clase alumno
           
        }
}
}
