
package herencia;
//importar la clase para salida de datos

//aplicar herencia:
//clase alumnocredito se hereda de alumno
public class alumnocredito extends alumno {
    //definiendo atributos
    private int creditos;//representar a los creditos de los alumnos
    private double pcarnet;//pago por carnet 
    //metodo para calcular el pago mensual 
    public void calcularpagomensual(){
        setpMensual(getCreditos()*45.10);       
    }
   //metodo para calcular pago por carnet 
    public void calcularpagocarnet(){
        if(getCreditos()>10)
            setPcarnet(18);
        
    }
   //metodo para calcular total 
    public void calculartotal(){
        super.calculartotal();
        //escribir el total
        setTotal(getTotal()+getPcarnet());
        
    }
  //metodo para impresion de datos 
    public void imprimir(){
        PrintWriter p=new PrintWriter(System.out,true);
        p.println("creditos:"+getCreditos());
        p.println("pago por carnet:"+ getPcarnet());
        super.imprimir();//llamar al metodo imprimir de la clase alumno
        
        
    }

    
    public int getCreditos() {
        return creditos;
    }

    /**
     * @param creditos the creditos to set
     */
    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }

    /**
     * @return the pcarnet
     */
    public double getPcarnet() {
        return pcarnet;
    }

    /**
     * @param pcarnet the pcarnet to set
     */
    public void setPcarnet(double pcarnet) {
        this.pcarnet = pcarnet;
    }
}
