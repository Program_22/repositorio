package padre;

public abstract class Persona {
	// Atributos protegidos 
	protected String nombre, apellido; 
	protected int edad; 

	// Constructor 
	public Persona(String nombre, String apellido, int edad) { 
		this.nombre = nombre; 
		this.apellido = apellido; 
		this.edad = edad; 
	} 
	
	// Operaciones 
	public String generarCorreo() { 
		return nombre.toLowerCase() + "." + apellido.toLowerCase() + "@upn.edu.pe"; 
	} 
	
	public String datosDeLaPersona() { 
		return "Nombre : " + nombre + "\n" + 
			   "Apellido : " + apellido + "\n" +
				"Edad : " + edad; 
	}
	
	// M�todo abstracto
	public abstract String identificacion();
}

