package hijo;

import padre.Persona;

public class Profesor extends Persona {
	// Atributos privados 
	private int horas; 
	private double tarifa; 
	
	// Constructor 
	public Profesor(String nombre, String apellido, int edad, int horas, double tarifa) { 
		super(nombre,apellido,edad); 
		this.horas = horas; 
		this.tarifa = tarifa; 
	} 

	// Operaciones 
	public double calcularSueldo() { 
		return horas*tarifa; 
	} 
	
	public String datosCompletos() { 
		return datosDeLaPersona() + "\n" +
			   "Horas : " + horas + "\n" +
				"Tarifa : " + tarifa; 
	} 

	public String identificacion() {
		return ">>> P R O F E S O R";
	}
}


