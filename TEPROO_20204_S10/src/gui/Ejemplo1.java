package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import hijo.Alumno;
import hijo.Profesor;
import padre.Persona;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejemplo1 extends JFrame implements ActionListener {
	private JButton btnAceptar;
	private JScrollPane scpSalida;
	private JTextArea txtSalida;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejemplo1 frame = new Ejemplo1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejemplo1() {
		setBounds(100, 100, 324, 371);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(this);
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAceptar.setBounds(84, 14, 126, 25);
		getContentPane().add(btnAceptar);
		
		scpSalida = new JScrollPane();
		scpSalida.setBounds(13, 53, 264, 215);
		getContentPane().add(scpSalida);
		
		txtSalida = new JTextArea();
		scpSalida.setViewportView(txtSalida);

	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btnAceptar) {
			btnAceptaractionPerformed(arg0);
		}
	}
	
	protected void btnAceptaractionPerformed(ActionEvent arg0) {

		Alumno a = new Alumno("Saul","Castillo",10,14,16,15); 
		listado(a); 
		
		Profesor d = new Profesor("Victor","Huertas",44,100,35.9); 
		listado(d); 
	}
	
	public void imprimir(String s) {
		txtSalida.append(s + "\n");
	}

	public void listado(Alumno a) { 
		imprimir(a.identificacion()); 
		imprimir(a.datosCompletos()); 
		imprimir("Correo : " + a.generarCorreo()); 
		imprimir("Promedio : " + a.calcularPromedio()); 
		imprimir(""); 
	} 
	
	public void listado(Profesor p) { 
		imprimir(p.identificacion()); 
		imprimir(p.datosCompletos()); 
		imprimir("Correo : " + p.generarCorreo()); 
		imprimir("Sueldo : " + p.calcularSueldo()); 
	} 
	
	
}




