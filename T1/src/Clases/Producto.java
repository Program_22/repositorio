package Clases;

public class Producto {
private int codigo,categoria;
private double precio;
private String nombre;
private static int cantidad;
public static final double PRECIO_NO_INGRESADO;

static {
	cantidad=0;
PRECIO_NO_INGRESADO=77;
}
public Producto(){
	cantidad++;
}


public Producto(int codigo,String nombre,int categoria,double precio) {
	this();
	this.codigo = codigo;
	this.nombre=nombre;
	this.categoria =categoria;
	this.precio=precio;
}
public Producto(int codigo,String nombre,int categoria) {
	this(codigo,nombre,categoria,Producto.PRECIO_NO_INGRESADO);
}
public int getCodigo() {
	return codigo;
}

public void setCodigo(int codigo) {
	this.codigo = codigo;
}

public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}

public int getCategoria() {
	return categoria;
}

public void setCategoria(int categoria) {
	this.categoria = categoria;
}

public double getPrecio() {
	return precio;
}

public void setPrecio(double precio) {
	this.precio = precio;
}
public static void setCantidad(int cantidad) {
Producto.cantidad=cantidad;
}
public static int getCantidad() {
	return Producto.cantidad;
}

public double precioFinal() {
	/*,desc;
if(categoria==1) {
	desc=precio*00.35;
}
else if(categoria==2) {
	desc=precio*00.5;
	
}
else if(categoria==3) {
	desc=precio*00.25;
}
	*/
	double pt;
pt=precio+precio*0.18;


return pt;
}
}