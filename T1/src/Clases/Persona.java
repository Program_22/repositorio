package Clases;

public class Persona {
	private int tipoDocumento;
	private String numeroDocumento,nombre,apellido,natalicio;
	private static int cantidad;
	static {
		cantidad=0;
	}
public Persona() {
	cantidad++;
}
	
	public Persona(int tipoDocumento, String numeroDocumento, String nombre, String apellido, String natalicio) {
		super();
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
		this.nombre = nombre;
		this.apellido = apellido;
		this.natalicio = natalicio;
	}
	public int getTipoDocumento() {
		return tipoDocumento;
		
	}
	public void setTipoDocumento(int tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNatalicio() {
		return natalicio;
	}
	public void setNatalicio(String natalicio) {
		this.natalicio = natalicio;
	}

	public static int getCantidad() {
		return cantidad;
	}

	public static void setCantidad(int cantidad) {
		Persona.cantidad = cantidad;
	}
	

}
