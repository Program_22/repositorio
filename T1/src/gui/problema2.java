package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Clases.Persona;


import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSplitPane;
import javax.swing.JPanel;
import javax.swing.ButtonGroup;

public class problema2 extends JFrame implements ActionListener {
	private JButton btnAceptar;
	private JButton btnLimpiar;
	private JLabel lblNewLabel;
	private JLabel lblNombre;
	private JTextField txtDoc;
	private JComboBox cboDoc;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_1;
	private JTextField txtNombre;
	private JScrollPane scp;
	private JTextArea txtSalida;
	private JRadioButton rbtSoltero;
	private JRadioButton rbtCasado;
	private JRadioButton rbtViudo;
	private JLabel lblNewLabel_3;
	private JTextField txtNatalicio;
	private JPanel panel;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */


	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					problema2 frame = new problema2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public problema2() {
		setTitle("Problema 1");
		setBounds(100, 100, 434, 363);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		{
			btnAceptar = new JButton("ACEPTAR");
			btnAceptar.addActionListener(this);
			btnAceptar.setBounds(320, 16, 89, 23);
			getContentPane().add(btnAceptar);
		}
		{
			btnLimpiar = new JButton("LIMPIAR");
			btnLimpiar.addActionListener(this);
			btnLimpiar.setBounds(320, 44, 89, 23);
			getContentPane().add(btnLimpiar);
		}
		{
			lblNewLabel = new JLabel("Documento");
			lblNewLabel.setBounds(10, 20, 68, 14);
			getContentPane().add(lblNewLabel);
		}
		{
			lblNombre = new JLabel("Nro. Documento");
			lblNombre.setBounds(10, 48, 96, 14);
			getContentPane().add(lblNombre);
		}
		{
			txtDoc = new JTextField();
			txtDoc.setColumns(10);
			txtDoc.setBounds(116, 45, 154, 20);
			getContentPane().add(txtDoc);
		}
		{
			cboDoc = new JComboBox();
			cboDoc.setModel(new DefaultComboBoxModel(new String[] {"--Seleccione--", "DNI", "CARNET DE EXTRANJERIA"}));
			cboDoc.setBounds(116, 17, 179, 20);
			getContentPane().add(cboDoc);
		}
		{
			lblNewLabel_2 = new JLabel("Nombre");
			lblNewLabel_2.setBounds(10, 79, 68, 14);
			getContentPane().add(lblNewLabel_2);
		}
		{
			lblNewLabel_1 = new JLabel("Estado Civil");
			lblNewLabel_1.setBounds(10, 110, 68, 14);
			getContentPane().add(lblNewLabel_1);
		}
		{
			txtNombre = new JTextField();
			txtNombre.setColumns(10);
			txtNombre.setBounds(116, 73, 154, 20);
			getContentPane().add(txtNombre);
		}
		{
			scp = new JScrollPane();
			scp.setBounds(10, 164, 371, 135);
			getContentPane().add(scp);
			{
				txtSalida = new JTextArea();
				scp.setViewportView(txtSalida);
			}
		}
		{
			lblNewLabel_3 = new JLabel("Natalicio");
			lblNewLabel_3.setBounds(10, 139, 68, 14);
			getContentPane().add(lblNewLabel_3);
		}
		{
			txtNatalicio = new JTextField();
			txtNatalicio.setColumns(10);
			txtNatalicio.setBounds(116, 133, 75, 20);
			getContentPane().add(txtNatalicio);
		}
		{
			panel = new JPanel();
			panel.setBounds(116, 97, 229, 32);
			getContentPane().add(panel);
			panel.setLayout(null);
			{
				rbtSoltero = new JRadioButton("Soltero");
				buttonGroup.add(rbtSoltero);
				rbtSoltero.setBounds(0, 7, 77, 23);
				panel.add(rbtSoltero);
				
			}
			{
				rbtCasado = new JRadioButton("Casado");
				buttonGroup.add(rbtCasado);
				rbtCasado.setBounds(79, 9, 68, 18);
				panel.add(rbtCasado);
			
			}
			{
				rbtViudo = new JRadioButton("Viudo");
				buttonGroup.add(rbtViudo);
				rbtViudo.setBounds(165, 9, 58, 18);
				panel.add(rbtViudo);
			
				
			}
		}

	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btnAceptar) {
			do_btnAceptar_actionPerformed(arg0);
		}
		if (arg0.getSource() == btnLimpiar) {
			do_btnLimpiar_actionPerformed(arg0);
		}
	}
	protected void do_btnLimpiar_actionPerformed(ActionEvent arg0) {
		txtSalida.setText("");
	}
	protected void do_btnAceptar_actionPerformed(ActionEvent arg0) {
		int tipoDocumento;
		String nombre,numeroDocumento,natalicio;
		
		tipoDocumento=cboDoc.getSelectedIndex();
		if(tipoDocumento==0) {
			JOptionPane.showMessageDialog(this, "El num no es un valor correcto","Nombre incorrecto",JOptionPane.WARNING_MESSAGE);
	cboDoc.requestFocus();
	return;
		}	
		
		numeroDocumento=txtDoc.getText();
		if(numeroDocumento.trim().equals("")) {
			JOptionPane.showMessageDialog(this, "El Numero no es un valor correcto","Nombre incorrecto",JOptionPane.WARNING_MESSAGE);
			txtDoc.selectAll();
			txtDoc.requestFocus();
			return;
	}
		natalicio=txtNatalicio.getText();
		if(natalicio.trim().equals("")) {
			JOptionPane.showMessageDialog(this, "El Nombre no es un valor correcto","Nombre incorrecto",JOptionPane.WARNING_MESSAGE);
			txtNatalicio.selectAll();
			txtNatalicio.requestFocus();
			return;
	}
		nombre=txtNombre.getText();
		if(nombre.trim().equals("")) {
			JOptionPane.showMessageDialog(this, "El Nombre no es un valor correcto","Nombre incorrecto",JOptionPane.WARNING_MESSAGE);
			txtNombre.selectAll();
			txtNombre.requestFocus();
			return;
	}
	
	Persona objProducto=new Persona();
	objProducto.getNatalicio();
	listado(objProducto);
	
	Persona objProducto2=new Persona();
	listado(objProducto2);
	}
	public void listado(Persona objE) {
		imprimir("Tipo: " + objE.getTipoDocumento());
		imprimir("Nro.Documento: " + objE.getNumeroDocumento());
		imprimir("Nombre: " + objE.getNombre());
		imprimir("Natalicio: " + objE.getNatalicio());
		imprimir("--------------------------");
		imprimir("Estudiantes: "+objE.getCantidad());
		imprimir("------------");
	}
	public void imprimir(String s) {
		txtSalida.append(s+"\n");
	}
}

