package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Clases.Producto;


import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class problema1 extends JFrame implements ActionListener {
	private JButton btnAceptar;
	private JButton btnLimpiar;
	private JLabel lblNewLabel;
	private JTextField txtCodigo;
	private JLabel lblNombre;
	private JTextField txtNombre;
	private JComboBox cboCategoria;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_1;
	private JTextField txtPrecio;
	private JScrollPane scp;
	private JTextArea txtSalida;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					problema1 frame = new problema1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public problema1() {
		setTitle("Problema 1");
		setBounds(100, 100, 407, 349);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		{
			btnAceptar = new JButton("ACEPTAR");
			btnAceptar.addActionListener(this);
			btnAceptar.setBounds(294, 11, 89, 23);
			getContentPane().add(btnAceptar);
		}
		{
			btnLimpiar = new JButton("LIMPIAR");
			btnLimpiar.addActionListener(this);
			btnLimpiar.setBounds(294, 39, 89, 23);
			getContentPane().add(btnLimpiar);
		}
		{
			lblNewLabel = new JLabel("C\u00F3digo");
			lblNewLabel.setBounds(10, 20, 46, 14);
			getContentPane().add(lblNewLabel);
		}
		{
			txtCodigo = new JTextField();
			txtCodigo.setBounds(106, 12, 75, 20);
			getContentPane().add(txtCodigo);
			txtCodigo.setColumns(10);
		}
		{
			lblNombre = new JLabel("Nombre");
			lblNombre.setBounds(10, 48, 46, 14);
			getContentPane().add(lblNombre);
		}
		{
			txtNombre = new JTextField();
			txtNombre.setColumns(10);
			txtNombre.setBounds(106, 45, 115, 20);
			getContentPane().add(txtNombre);
		}
		{
			cboCategoria = new JComboBox();
			cboCategoria.addActionListener(this);
			cboCategoria.setModel(new DefaultComboBoxModel(new String[] {"--Seleccione--", "Abarrotes", "Bebidas", "Limpieza", "Panaderia"}));
			cboCategoria.setBounds(106, 76, 115, 20);
			getContentPane().add(cboCategoria);
		}
		{
			lblNewLabel_2 = new JLabel("Categoria");
			lblNewLabel_2.setBounds(10, 79, 68, 14);
			getContentPane().add(lblNewLabel_2);
		}
		{
			lblNewLabel_1 = new JLabel("Precio");
			lblNewLabel_1.setBounds(10, 110, 68, 14);
			getContentPane().add(lblNewLabel_1);
		}
		{
			txtPrecio = new JTextField();
			txtPrecio.setColumns(10);
			txtPrecio.setBounds(106, 107, 75, 20);
			getContentPane().add(txtPrecio);
		}
		{
			scp = new JScrollPane();
			scp.setBounds(10, 164, 371, 135);
			getContentPane().add(scp);
			{
				txtSalida = new JTextArea();
				scp.setViewportView(txtSalida);
			}
		}

	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == cboCategoria) {
			do_cboCategoria_actionPerformed(arg0);
		}
		if (arg0.getSource() == btnAceptar) {
			do_btnAceptar_actionPerformed(arg0);
		}
		if (arg0.getSource() == btnLimpiar) {
			do_btnLimpiar_actionPerformed(arg0);
		}
	}
	protected void do_btnLimpiar_actionPerformed(ActionEvent arg0) {
		txtSalida.setText("");
	}
	protected void do_btnAceptar_actionPerformed(ActionEvent arg0) {
	
		int codigo,categoria;
		String nombre;
		double precio;

try {
	codigo=Integer.parseInt(txtCodigo.getText());
	
}catch(Exception e) {
	
	JOptionPane.showMessageDialog(this, "El Codigo no es un valor correcto","codigo incorrecto",JOptionPane.WARNING_MESSAGE);
	System.out.println("Se produjo un Excepcion: " + e.getMessage());
	txtCodigo.selectAll();
	txtCodigo.requestFocus();
	return;
}	
	nombre=txtNombre.getText();
	if(nombre.trim().equals("")) {
		JOptionPane.showMessageDialog(this, "El Nombre no es un valor correcto","Nombre incorrecto",JOptionPane.WARNING_MESSAGE);
		txtNombre.selectAll();
		txtNombre.requestFocus();
		return;
		
	}
	double n1=Double.parseDouble(txtPrecio.getText());
	categoria=cboCategoria.getSelectedIndex();
	if(categoria==0) {
		JOptionPane.showMessageDialog(this, "La categoria no es un valor correcto","Categoria incorrecto",JOptionPane.WARNING_MESSAGE);
		
		cboCategoria.requestFocus();
}
	else if(categoria==1) {
		n1=n1*00.35;
	}
	else if(categoria==2) {
		n1=n1*00.5;
	}
	else if(categoria==3) {
		n1=n1*00.25;
	}
	try {
		precio=Double.parseDouble(txtPrecio.getText());
	}catch(Exception e) {
		JOptionPane.showMessageDialog(this, "El Precio no es un valor correcto","Nombre incorrecto",JOptionPane.WARNING_MESSAGE);
		System.out.println("Se produjo un Excepcion: " + e.getMessage());
		txtPrecio.selectAll();
		txtPrecio.requestFocus();
		return;
	}
	Producto objProducto=new Producto(codigo,nombre,categoria);
	objProducto.setPrecio(precio);
	listado(objProducto);
	
	Producto objProducto2=new Producto(codigo,nombre,categoria);
	listado(objProducto2);
	}
	public void listado(Producto objE) {
		imprimir("Codigo: " + objE.getCodigo());
		imprimir("Nombre: " + objE.getNombre());
		imprimir("Categoria: " + objE.getCategoria() + ".- " + cboCategoria.getItemAt(objE.getCategoria()));
		imprimir("Precio: " + objE.getPrecio()+""+((objE.getPrecio()==Producto.PRECIO_NO_INGRESADO)?"(sin Registro)":""));
		imprimir("PrecioFinal: " + objE.precioFinal());
		imprimir("--------------------------");
		imprimir("Estudiantes: "+objE.getCantidad());
		imprimir("------------");
	}
	public void imprimir(String s) {
		txtSalida.append(s+"\n");
	}
	protected void do_cboCategoria_actionPerformed(ActionEvent arg0) {

}
}

	


