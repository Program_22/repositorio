package com.pago.app.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pago.app.entity.Cuenta;
import com.pago.app.service.CuentaService;

@RestController
public class CuentaControlador {
	 
	@Autowired
	CuentaService cuentaService;
	
	@PostMapping("/cuenta/crear")
	public ResponseEntity<Cuenta> postCuenta(@RequestBody Cuenta cuenta) {
		return ResponseEntity.status(HttpStatus.CREATED).body(cuentaService.save(cuenta));
	}

	@GetMapping("/cuenta/listar")
	public ResponseEntity<List<Cuenta>> getCuenta() {
		return ResponseEntity.status(HttpStatus.OK).body(cuentaService.findAll());
	}

	@GetMapping("/cuenta/ver/{Id}")
	public ResponseEntity<Cuenta> getCuenta(@PathVariable Long Id) {
		return ResponseEntity.status(HttpStatus.OK).body(cuentaService.findById(Id));
	}
	

	@PutMapping("/cuenta/ver/{Id}")
	public ResponseEntity<Cuenta> patchCuenta(@PathVariable Long Id, @RequestBody Cuenta cuenta) {
		return ResponseEntity.status(HttpStatus.OK).body(cuentaService.update(Id, cuenta));
	}

	@DeleteMapping("/cuenta/eliminar/{Id}")
	public ResponseEntity<Void> deleteCuenta(@PathVariable Long Id) {
		cuentaService.deleteById(Id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
	}
}
