package com.pago.app.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pago.app.entity.Banco;

import com.pago.app.service.BancoService;


@RestController
public class BancoController {
	@Autowired
	BancoService bancoService;
	@PostMapping("/banco/crear")
	public ResponseEntity<Banco> postBanco(@RequestBody Banco banco) {
		return ResponseEntity.status(HttpStatus.CREATED).body(bancoService.save(banco));
	}

	@GetMapping("/banco/listar") 
	public ResponseEntity<List<Banco>> getBanco() {
		return ResponseEntity.status(HttpStatus.OK).body(bancoService.findAll());
	}

	@GetMapping("/banco/ver/{Id}")
	public ResponseEntity<Banco> getBanco(@PathVariable Long Id) {
		return ResponseEntity.status(HttpStatus.OK).body(bancoService.findById(Id));
	}
	

	@PutMapping("/banco/ver/{Id}")
	public ResponseEntity<Banco> patchBanco(@PathVariable Long Id, @RequestBody Banco banco) {
		return ResponseEntity.status(HttpStatus.OK).body(bancoService.update(Id, banco));
	}

	@DeleteMapping("/banco/eliminar/{Id}")
	public ResponseEntity<Void> deleteCliente(@PathVariable Long Id) {
		bancoService.deleteById(Id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
	}
}
