package com.pago.app.controlador;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pago.app.entity.Cliente;
import com.pago.app.service.ClienteService;


@RestController
public class ClienteControlador {
	@Autowired
	ClienteService clienteService;
	@PostMapping("/cliente/crear")
	public ResponseEntity<Cliente> postCliente(@RequestBody Cliente cliente) {
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteService.save(cliente));
	}
 
	@GetMapping("/cliente/listar")
	public ResponseEntity<List<Cliente>> getCliente() {
		return ResponseEntity.status(HttpStatus.OK).body(clienteService.findAll());
	}

	@GetMapping("/cliente/ver/{Id}")
	public ResponseEntity<Cliente> getCliente(@PathVariable Long Id) {
		return ResponseEntity.status(HttpStatus.OK).body(clienteService.findById(Id));
	}
	

	@PutMapping("/cliente/ver/{Id}")
	public ResponseEntity<Cliente> patchCliente(@PathVariable Long Id, @RequestBody Cliente cliente) {
		return ResponseEntity.status(HttpStatus.OK).body(clienteService.update(Id, cliente));
	}

	@DeleteMapping("/cliente/eliminar/{Id}")
	public ResponseEntity<Void> deleteCliente(@PathVariable Long Id) {
		clienteService.deleteById(Id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
	}
}
