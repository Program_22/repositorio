package com.pago.app.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pago.app.entity.Transaccion;
import com.pago.app.service.TransaccionService;



@RestController
public class TransaccionControlador {
	@Autowired
	TransaccionService transaccionService;
	@PostMapping("/transaccion/crear")
	public ResponseEntity<Transaccion> postTransaccion(@RequestBody Transaccion transaccion) {
		return ResponseEntity.status(HttpStatus.CREATED).body(transaccionService.save(transaccion));
	}
 
	@GetMapping("/transaccion/listar")
	public ResponseEntity<List<Transaccion>> getTransaccion() {
		return ResponseEntity.status(HttpStatus.OK).body(transaccionService.findAll());
	}

	@GetMapping("/transaccion/ver/{Id}")
	public ResponseEntity<Transaccion> gettransaccion(@PathVariable Long Id) {
		return ResponseEntity.status(HttpStatus.OK).body(transaccionService.findById(Id));
	}
	

	@PutMapping("/transaccion/ver/{Id}")
	public ResponseEntity<Transaccion> patchtransaccion(@PathVariable Long Id, @RequestBody Transaccion transaccion) {
		return ResponseEntity.status(HttpStatus.OK).body(transaccionService.update(Id, transaccion));
	}

	@DeleteMapping("/transaccion/eliminar/{Id}")
	public ResponseEntity<Void> deleteProducto(@PathVariable Long Id) {
		transaccionService.deleteById(Id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
	}

}
