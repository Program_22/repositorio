package com.pago.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pago.app.configException.NotFoundException;
import com.pago.app.entity.Banco;

import com.pago.app.entity.Transaccion;

import com.pago.app.repositorio.TransaccionRepo;
@Service
@Transactional
public class TransaccionService {
	@Autowired
	 TransaccionRepo transaccionRepo;
	
	
	@Autowired
	 BancoService bancoService;
	public List<Transaccion> findAll() {
		List<Transaccion> tra= transaccionRepo.findAll();
		return tra;
				
	}
	
	public Transaccion save(Transaccion transaccion) {
		Banco bt=bancoService.findById(transaccion.getBanco().getId_banco());
		Transaccion tra=transaccionRepo.save(transaccion);
		tra.setBanco(bt);
		return tra;
	}



	public Transaccion findById(Long id) {
		if (!transaccionRepo.existsById(id)) {
			throw new NotFoundException("No se encuentra la transaccion con id: " + id);
		} else {
			Transaccion tra = transaccionRepo.getOne(id);
			return tra;
		}
	}
	public Transaccion update(Long id, Transaccion transaccion) {
		if (!transaccionRepo.existsById(id)) {
			throw new NotFoundException("No se encuentra la transaccion con id: " + id);
		} else {
			transaccion.setId_transaccion(id);;
			Transaccion tra = transaccionRepo.save(transaccion);
			return tra;
		}
	}

	public void deleteById(Long id) {
		if (!transaccionRepo.existsById(id)) {
			throw new NotFoundException("No se encuentra la transaccion con id: " + id);
		} else {
			transaccionRepo.deleteById(id);
		}
	}

	
		
	}


