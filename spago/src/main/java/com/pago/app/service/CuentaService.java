package com.pago.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pago.app.configException.NotFoundException;
import com.pago.app.entity.Cliente;
import com.pago.app.entity.Cuenta;
import com.pago.app.repositorio.CuentaRepo;

@Transactional
@Service
public class CuentaService {
@Autowired
 CuentaRepo cuentaRepo;

public List<Cuenta> findAll() {
	List<Cuenta> ct= cuentaRepo.findAll();
	return ct;
			
}
public Cuenta save(Cuenta cuenta) {
	Cuenta ct=cuentaRepo.save(cuenta);
	return ct;
}



public Cuenta findById(Long id) {
	if (!cuentaRepo.existsById(id)) {
		throw new NotFoundException("No se encuentra la cuenta con id: " + id);
	} else {
		Cuenta ct = cuentaRepo.getOne(id);
		return ct;
	}
}
public Cuenta update(Long id, Cuenta cuenta) {
	if (!cuentaRepo.existsById(id)) {
		throw new NotFoundException("No se encuentra la cuenta con id: " + id);
	} else {
		cuenta.setId_cuenta(id);;
		Cuenta ct = cuentaRepo.save(cuenta);
		return ct;
	}
}

public void deleteById(Long id) {
	if (!cuentaRepo.existsById(id)) {
		throw new NotFoundException("No se encuentra la cuenta con id: " + id);
	} else {
		cuentaRepo.deleteById(id);
	}
}

}
