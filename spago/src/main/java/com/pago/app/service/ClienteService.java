package com.pago.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pago.app.configException.NotFoundException;
import com.pago.app.entity.Cliente;
import com.pago.app.repositorio.ClienteRepo;

@Transactional
@Service
public class ClienteService {
	@Autowired
 ClienteRepo clienteRepo;
	
	public List<Cliente> findAll() {
		List<Cliente> cliente= clienteRepo.findAll();
		return cliente;
				
	}
	public Cliente save(Cliente cliente) {
		Cliente cli=clienteRepo.save(cliente);
		return cli;
	}

	
	
	public Cliente findById(Long id) {
		if (!clienteRepo.existsById(id)) {
			throw new NotFoundException("No se encuentra el cliente con id: " + id);
		} else {
			Cliente cli = clienteRepo.getOne(id);
			return cli;
		}
	}
	public Cliente update(Long id, Cliente cliente) {
		if (!clienteRepo.existsById(id)) {
			throw new NotFoundException("No se encuentra el cliente con id: " + id);
		} else {
			cliente.setId(id);;
			Cliente cli = clienteRepo.save(cliente);
			return cli;
		}
	}

	public void deleteById(Long id) {
		if (!clienteRepo.existsById(id)) {
			throw new NotFoundException("No se encuentra el cliente con id: " + id);
		} else {
			clienteRepo.deleteById(id);
		}
	}

}
