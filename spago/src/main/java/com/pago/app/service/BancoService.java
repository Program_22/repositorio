package com.pago.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pago.app.configException.NotFoundException;
import com.pago.app.entity.Banco;
import com.pago.app.entity.Cuenta;
import com.pago.app.repositorio.BancoRepo;

@Transactional
@Service
public class BancoService {
	@Autowired
 BancoRepo bancoRepo;
	
	
	public List<Banco> findAll() {
		List<Banco> bt= bancoRepo.findAll();
		return bt;
				
	}
	public Banco save(Banco banco) {
		Banco  bt=bancoRepo.save(banco);
		return bt;
	}



	public Banco findById(Long id) {
		if (!bancoRepo.existsById(id)) {
			throw new NotFoundException("No se encuentra el banco con id: " + id);
		} else {
			Banco bt = bancoRepo.getOne(id);
			return bt;
		}
	}
	public Banco update(Long id, Banco banco) {
		if (!bancoRepo.existsById(id)) {
			throw new NotFoundException("No se encuentra el banco con id: " + id);
		} else {
			banco.setId_banco(id);
			Banco ct = bancoRepo.save(banco);
			return ct;
		}
	}

	public void deleteById(Long id) {
		if (!bancoRepo.existsById(id)) {
			throw new NotFoundException("No se encuentra el banco con el id: " + id);
		} else {
			bancoRepo.deleteById(id);
		}
	}


}
