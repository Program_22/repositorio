package com.pago.app.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;


import com.pago.app.entity.Transaccion;



public interface TransaccionRepo extends JpaRepository<Transaccion,Long> {

}
