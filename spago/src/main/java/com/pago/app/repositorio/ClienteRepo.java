package com.pago.app.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;


import com.pago.app.entity.Cliente;




public interface ClienteRepo extends JpaRepository<Cliente,Long>{

}
