package com.pago.app.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import com.pago.app.entity.Banco;




public interface BancoRepo extends JpaRepository<Banco,Long> {

}
