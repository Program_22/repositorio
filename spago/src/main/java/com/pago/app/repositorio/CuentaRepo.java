package com.pago.app.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pago.app.entity.Cuenta;



public interface CuentaRepo extends JpaRepository<Cuenta,Long> {
	

}
