package com.pago.app.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name = "Cuenta")

public class Cuenta implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cuenta")
	private Long id_cuenta;
	@Column(name = "nro_cuenta")
	private String nro_cuenta;
	@Column(name = "fecha")
	private String fecha;
	@ManyToOne
	@JoinColumn(name = "cliente")
	private Cliente cliente;
	@OneToMany(mappedBy = "cuenta", fetch = FetchType.LAZY)
	private List<Transaccion> transaccion;

	public Cuenta() {
		transaccion = new ArrayList<>();
	}

	public Cuenta(Long id_cuenta, String nro_cuenta, String fecha, Cliente cliente) {
		super();
		this.id_cuenta = id_cuenta;
		this.nro_cuenta = nro_cuenta;
		this.fecha = fecha;
		this.cliente = cliente;
	}

	public Long getId_cuenta() {
		return id_cuenta;
	}

	public void setId_cuenta(Long id_cuenta) {
		this.id_cuenta = id_cuenta;
	}

	public String getNro_cuenta() {
		return nro_cuenta;
	}

	public void setNro_cuenta(String nro_cuenta) {
		this.nro_cuenta = nro_cuenta;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	private static final long serialVersionUID = 1L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
