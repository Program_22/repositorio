package com.pago.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;



@Entity
@Table(name="Banco")
public class Banco implements Serializable{

	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_banco")
	private Long id_banco;
	private String nombre_entidad;

	
	
	

	public Banco(Long id_banco, String nombre_entidad) {
		super();
		this.id_banco = id_banco;
		this.nombre_entidad = nombre_entidad;
	}
	public Long getId_banco() {
		return id_banco;
	}
	public void setId_banco(Long id_banco) {
		this.id_banco = id_banco;
	}
	public String getNombre_entidad() {
		return nombre_entidad;
	}
	public void setNombre_entidad(String nombre_entidad) {
		this.nombre_entidad = nombre_entidad;
	}
	public Banco() {
	
	}
	



	private static final long serialVersionUID = 1L;
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
