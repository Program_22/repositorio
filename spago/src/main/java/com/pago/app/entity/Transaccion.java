package com.pago.app.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;



@Entity
@Table(name = "transaccion")
//@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Transaccion implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_transaccion")
	private Long id_transaccion;
	private double monto_transaccion;
	private String tipo;
	@ManyToOne
	@JoinColumn(name = "id_cuenta")
	private Cuenta cuenta;
	@JoinColumn(unique = true, name = "banco_id")
	@OneToOne(cascade = CascadeType.MERGE)
	private Banco banco;

	public Transaccion() {

	
	}

	

	public Banco getBanco() {
		return banco;
	}



	public void setBanco(Banco banco) {
		this.banco = banco;
	}



	public Long getId_transaccion() {
		return id_transaccion;
	}

	public void setId_transaccion(Long id_transaccion) {
		this.id_transaccion = id_transaccion;
	}

	public double getMonto_transaccion() {
		return monto_transaccion;
	}

	public void setMonto_transaccion(double monto_transaccion) {
		this.monto_transaccion = monto_transaccion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}






	public Transaccion(Long id_transaccion, double monto_transaccion, String tipo, Cuenta cuenta, Banco banco) {
		super();
		this.id_transaccion = id_transaccion;
		this.monto_transaccion = monto_transaccion;
		this.tipo = tipo;
		this.cuenta = cuenta;
		this.banco = banco;
	}






	private static final long serialVersionUID = 1L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
