package com.pago.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpagoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpagoApplication.class, args);
	}

}
